# Data Model for the Digital Situation Map

```mermaid
classDiagram

    Mission "1" .. "1..*" Unit

    class Location {
        -wgs84: WGS84Location
        -utm: UTMLocation
        -gk: GKLocation
    }

    class UTMLocation {
        -String zone
        -int x
        -int y
    }

    class WGS84Location {
        -double latitude
        -double longitude
    }

    class GKLocation {
        -int zone
        -double x
        -double y
    }

    class Address {
        -String street
        -String houseNumber
        -int zipCode
        -String locality
    }

    class Mission {
        -MapLocation mapLocation
        -GPSLocation gpsLocation
        -Address address
        -String type
    }

    class Unit {
       -String callSign
       -String tacticalSignPath
       -MapLocation location
    }


```
