# Event Messaging Technology

* Status: accepted
* Deciders: Phillipp von Rotenhan, Mathias Müller, Christopher Nagy
* Date: 21.02.2023

## Context and Problem Statement

We need a protocol to exchange events between client applications and backend that low bandwith consumption and has support for Java/Kotlin and JavaScript.

## Decision Drivers

* Limited Bandwidth
* Unstable Internet Connection (multiple reconnects may occure)

## Considered Options

* MQTT
* WebSockets + Protobuf
* GRPC
* AMQP

## Decision Outcome

Chosen option: 

* __MQTT__, because it is well documented, widely used and specifically tailored to be used for IoT applications, and therefore tailored for low bandwidth usage.
* __JSON__ as a data format, because it is easy to implement and the data format is easy to switch / refactor.

### Positive Consequences

* Low bandwidth consumption
* Reliable messaging (exactly once delivery)

### Negative Consequences

* Needs a separate MQTT broker as part of the backend infrastructure
