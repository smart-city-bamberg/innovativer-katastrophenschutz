# Data / Event flow for a Location Update

```mermaid
sequenceDiagram

    participant locSource as Location Source
    participant mqtt as MQTT broker
    participant frontend as Frontend
    participant backend as Backend
    participant mongo as MongoDB
    participant eventstore as EventStore DB

    par 
    frontend ->> mqtt: Subscribe
    activate mqtt
    mqtt -->> frontend: 
    deactivate mqtt

    and 
    backend ->> mqtt: Subscribe
    activate mqtt
    mqtt -->> backend: 
    deactivate mqtt

    and backend ->> eventstore: Subscribe
    activate eventstore
    eventstore -->> backend: 
    deactivate eventstore
    end
    
    locSource ->> mqtt: Publish Location
    mqtt ->> backend: Publish Location
    backend ->> eventstore: Store Event
    activate eventstore
    
    par
    eventstore -->> backend: 
    
    backend ->> mqtt: Publish Event
    mqtt ->> frontend: Receive Event
    
    and
    eventstore ->> backend: Notify new Event
    deactivate eventstore

    backend ->> mongo: Store current state
    activate mongo
    mongo -->> backend: 
    deactivate mongo
    end

```

# Publish Situation report

```mermaid
sequenceDiagram

    participant frontend as Frontend
    participant mqtt as MQTT broker
    participant backend as Backend
    participant mongo as MongoDB
    participant eventstore as EventStore DB

    par 
    frontend ->> mqtt: Subscribe
    activate mqtt
    mqtt -->> frontend: 
    deactivate mqtt

    and 
    backend ->> mqtt: Subscribe
    activate mqtt
    mqtt -->> backend: 
    deactivate mqtt

    and 
    backend ->> eventstore: Subscribe
    activate eventstore
    eventstore -->> backend: 
    deactivate eventstore
    end
    
    par
    frontend ->> mqtt: Publish Situation Report
    mqtt ->> backend: Publish Situation Report
    
    and 
    loop Every Image
      frontend ->> backend: Send Image
      activate backend
      backend ->> backend: Store Image
      backend -->> frontend: 
      deactivate backend
    end
    end
    
    backend ->> eventstore: Store Event
    eventstore ->> backend: Notify new Event

    backend ->> mongo: Store current state
    activate mongo
    mongo -->> backend: 
    deactivate mongo

    backend ->> mqtt: Publish Event
    mqtt ->> frontend: Receive Event
```
