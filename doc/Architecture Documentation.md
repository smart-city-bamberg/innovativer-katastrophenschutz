# Architecture Documentation

The C4 Model is written in [Structurizr DSL](https://github.com/structurizr/dsl).
To edit the models go to https://structurizr.com/dsl and paste the content of the [structurizr_c4_model.dsl](structurizr_c4_model.dsl) file into the browser workbench.
A german version can be found in [german/architecture.md](german/architecture.md).

## System Context Diagram

```mermaid
graph TB
  linkStyle default fill: #ffffff

subgraph diagram ["Digital Situation Map (DigiMap) - System Context"]
style diagram fill: #ffffff, stroke: #ffffff

subgraph group1 ["Lion5"]
style group1 fill: #ffffff, stroke: #cccccc, color: #cccccc, stroke-dasharray: 5

4("<div style='font-weight: bold'>Digital Situation Map (DigiMap)</div><div style='font-size: 70%; margin-top: 0px'>[Software System]</div><div style='font-size: 80%; margin-top:10px'>Provides a shared, unified<br />view of the disaster<br />situation to efficiently plan<br />and coordinate resources or<br />measures for the current<br />mission.</div>")
style 4 fill: #1168bd, stroke: #0b4884, color: #ffffff
end

1["<div style='font-weight: bold'>Upper and middle management</div><div style='font-size: 70%; margin-top: 0px'>[Person]</div><div style='font-size: 80%; margin-top:10px'>Coordinator of the mission<br />resources on site</div>"]
style 1 fill: #08427b, stroke: #052e56, color: #ffffff
2["<div style='font-weight: bold'>Lower management</div><div style='font-size: 70%; margin-top: 0px'>[Person]</div><div style='font-size: 80%; margin-top:10px'>Receives commands from the<br />upper/middle management and<br />executes them with along with<br />their group.</div>"]
style 2 fill: #08427b, stroke: #052e56, color: #ffffff
24("<div style='font-weight: bold'>Bayern Atlas</div><div style='font-size: 70%; margin-top: 0px'>[Software System]</div><div style='font-size: 80%; margin-top:10px'>Map software and material<br />provided by the free state of<br />Bavaria</div>")
style 24 fill: #999999, stroke: #6b6b6b, color: #ffffff
29("<div style='font-weight: bold'>OpenStreetMap</div><div style='font-size: 70%; margin-top: 0px'>[Software System]</div><div style='font-size: 80%; margin-top:10px'>Open source map resources</div>")
style 29 fill: #999999, stroke: #6b6b6b, color: #ffffff
3["<div style='font-weight: bold'>Administrator</div><div style='font-size: 70%; margin-top: 0px'>[Person]</div><div style='font-size: 80%; margin-top:10px'>Manages the master data and<br />access to the application</div>"]
style 3 fill: #08427b, stroke: #052e56, color: #ffffff
32("<div style='font-weight: bold'>Einsatzleitsystem</div><div style='font-size: 70%; margin-top: 0px'>[Software System]</div><div style='font-size: 80%; margin-top:10px'>System for detecting an<br />emergency and automated<br />alerting of emergency<br />resources</div>")
style 32 fill: #999999, stroke: #6b6b6b, color: #ffffff
35("<div style='font-weight: bold'>GPS Tracker</div><div style='font-size: 70%; margin-top: 0px'>[Hardware]</div><div style='font-size: 80%; margin-top:10px'>Means of operation with GPS<br />tracker (e.g. radio,<br />stationary tracker, etc.)</div>")
style 35 fill: #dddddd, stroke: #9a9a9a, color: #000000

3-. " <div>Provides and maintains master<br />data of application users and<br />mission resources.</div><div style='font-size: 70%'></div> " .-> 4
24-. " <div>Provides local GIS data such<br />as information about floods<br />in Bavaria.</div><div style='font-size: 70%'></div> " .-> 4
4-. " <div>Provides an export of current<br />map layers to visualize them<br />in</div><div style='font-size: 70%'>[Bayern]</div> " .-> 24
29-. " <div>Provides map resources such<br />as tiles.</div><div style='font-size: 70%'></div> " .-> 4
32-. " <div>Sends mission data in EDI<br />format or as an alarm message<br /></div><div style='font-size: 70%'>[Alarmdepeche]</div> " .-> 4
35-. "<div>Publishes current location<br />information.</div><div style='font-size: 70%'></div> " .-> 4
2-. " <div>Submits reports of the<br />current situation.</div><div style='font-size: 70%'></div> " .-> 4
1-. " <div>Tracks the current mission<br />situation on the dashboard,<br />and coordinates units and<br />management measures. Uses<br />mission data for follow-up<br />tasks.</div><div style='font-size: 70%'></div>" .-> 4
end
```

## Container Diagram

```mermaid
graph TB
  linkStyle default fill:#ffffff

  subgraph diagram ["Digital Situation Map (DigiMap) - Containers"]
    style diagram fill:#ffffff,stroke:#ffffff

    subgraph 4 ["Digital Situation Map (DigiMap)"]
      style 4 fill:#ffffff,stroke:#0b4884,color:#0b4884

      12("<div style='font-weight: bold'>DigiMap Backend</div><div style='font-size: 70%; margin-top: 0px'>[Container: Java and Spring Boot]</div><div style='font-size: 80%; margin-top:10px'>Processes all events during<br />the mission, and provides<br />APIs to manage the master<br />data.</div>")
      style 12 fill:#438dd5,stroke:#2e6295,color:#ffffff
      15[("<div style='font-weight: bold'>EventStore</div><div style='font-size: 70%; margin-top: 0px'>[Container: EventstoreDB]</div><div style='font-size: 80%; margin-top:10px'>Stores all retrieved events<br />(e.g. location updates) as a<br />temporally ordered stream.</div>")]
      style 15 fill:#438dd5,stroke:#2e6295,color:#ffffff
      18[("<div style='font-weight: bold'>Database</div><div style='font-size: 70%; margin-top: 0px'>[Container: MongoDB]</div><div style='font-size: 80%; margin-top:10px'>Stores all master data<br />regarding users or mission<br />resources (e.g., units), and<br />provides the current state of<br />missions for read or write<br />operations.</div>")]
      style 18 fill:#438dd5,stroke:#2e6295,color:#ffffff
      21["<div style='font-weight: bold'>MQTT Broker</div><div style='font-size: 70%; margin-top: 0px'>[Container: Eclipse Mosquitto]</div><div style='font-size: 80%; margin-top:10px'>Enables asynchronous,<br />real-time communication<br />between the DigiMap backend<br />and clients such as the<br />DigiMap PWA. Enables clients<br />to subscribe to topics to<br />retrieve all updates for a<br />specific mission.</div>"]
      style 21 fill:#438dd5,stroke:#2e6295,color:#ffffff
      5["<div style='font-weight: bold'>DigiMap PWA</div><div style='font-size: 70%; margin-top: 0px'>[Container: Vue and TypeScript]</div><div style='font-size: 80%; margin-top:10px'>Visualizes the current<br />mission information on a<br />digital map, and on a<br />configurable dashboard<br />including other context<br />information. Optimized for<br />mobile usage, but with<br />limited dashboard<br />functionality.</div>"]
      style 5 fill:#438dd5,stroke:#2e6295,color:#ffffff
    end

    5-. "<div>Manages master data, such as<br />user accounts or mission<br />resources.</div><div style='font-size: 70%'></div>" .->12
    12-. "<div>Provides a REST API to<br />retrieve and manage master<br />data.</div><div style='font-size: 70%'></div>" .->5
    12-. "<div>Writes retrieved events<br />during a running mission onto<br />the stream.</div><div style='font-size: 70%'></div>" .->15
    15-. "<div>Publishes new events after<br />recording them to the stream.</div><div style='font-size: 70%'></div>" .->12
    12-. "<div>Updates the respective<br />mission's state depending on<br />new events or updates.</div><div style='font-size: 70%'></div>" .->18
    18-. "<div>Provides the currently known<br />state of all running or<br />finished missions to be<br />retrieved by clients.</div><div style='font-size: 70%'></div>" .->12
    5-. "<div>Publishes and subscribes to<br />mission events such as<br />location updates, commands,<br />or reports.</div><div style='font-size: 70%'></div>" .->21
    12-. "<div>Publishes and subscribes to<br />mission events such as<br />location updates, commands,<br />or reports.</div><div style='font-size: 70%'></div>" .->21
  end
```

## Deployment Diagram

This diagram explicitly does not visualize traffic going through the ingress controller because it is assumed the reader knows 
the purpose and function of an ingress controller in kubernetes.
Therefore, relationships are directly drawn to the specific services and not going through the ingress-controller.

```mermaid
C4Deployment
  title "Deployment Diagram Digitale Lagekarte"

  Deployment_Node(device, "(mobile) device", "any desktop or mobile OS") {
    Deployment_Node(browser, "Web Browser", "Google Chrome, Mozilla Firefox,<br/> Apple Safari or Microsoft Edge") {
      Container(spa, "Single Page Application", "Vue and Typescript", "Visualizes the current mission information on a digital map, <br/> and on a configurable dashboard including other context information.")
    }
  }

  Deployment_Node(gcp, "Google Cloud Platform") {
    Container(ident, "Google Identity Platform", "Managed User Authentication Service", "manages user identities and passwords")
    Container(dns, "Google Cloud DNS", "Managed DNS Server", "")
    Container(docker, "Google Artifact Registry", "Managed Container Registry", "store images for deployment <br/> into the kubernetes cluster")
    Container(storage, "Google Cloud Storage", "Managed Cloud Storage", "Object storage for images etc.")

    Deployment_Node(gke, "Google Kubernetes Engine", "Kubernetes") {
      Container(pwa, "DigiMap PWA", "Vue and Typescript", "Delivers the SPA and the static web content to the clients")
      Container(backend, "Backend Application", "Java and Spring Boot", "Processes all events during the mission,<br/> and provides APIs to manage the master data.")
      Container(broker, "MQTT Broker", "eclipse mosquitto")
      Container(mongo, "Document Database", "mongo DB")
      Container(eventstore, "EventStore", "eventstore DB")
      Container(ingress, "Ingress Controller", "nginx-ingress")
      Container(cert, "Certificate Manager", "cert-manager")
    }
  }

  Rel_D(spa, ident, "Login and get JWT", "json/HTTPS")
  Rel_Up(spa, backend, "Makes API calls to", "json/HTTPS")
  BiRel(spa, broker, "Publishes/Receives messages", "mqtt")
  BiRel(spa, storage, "Up-/Downlaod images", "https")
  Rel(backend, storage, "Downlaod images", "https")
  BiRel(backend, broker, "Publishes/Receives messages", "mqtt")
  BiRel(backend, eventstore, "store/read events", "GRPC")
  BiRel(backend, mongo, "store/read data", "MongoDB Wire Protocol" )
  Rel(pwa, spa, "Delivers to the web browser")
  Rel_D(backend, ident, "Verify JWT")
  Rel(cert, ingress, "manage TLS certs")
  Rel(cert, broker, "manage TLS certs")
  
  UpdateRelStyle(spa, backend, $offsetY="20")
  UpdateRelStyle(spa, broker, $offsetY="40", $offsetX="-200")
  UpdateRelStyle(spa, ident ,$offsetY="-40", $offsetX="-20")
  UpdateRelStyle(spa, storage, $offsetY="-20", $offsetX="-20")
  UpdateRelStyle(backend, storage, $offsetY="-50", $offsetX="-20")
  UpdateRelStyle(backend, broker, $offsetX="-150")
  UpdateRelStyle(backend, mongo, $offsetX="-40")
  UpdateRelStyle(cert, ingress, $offsetY="-20", $offsetX="-40")

```

Deployment-wise, the digital situation map consists of three major parts:

- the client, which is delivered as progressive web application. This is runnable on any browser and also works offline. 
- the core business application which designed as a set of containers that can be operated on a Kubernetes cluster
- a set of Google Cloud Platform services providing infrastructure functionality

### Kubernetes Services

The following helm charts are deployed to the cluster:

- `ingress-nginx/ingress-nginx` is the ingress controller for managing ingress routes
- `mongodb/community-operator` is the MongoDB Community Operator for managing MongoDB instances
- `jetstack/cert-manager` is a cert-manager for managing TLS certificates

Additionally to those 3rd party helm charts the `smart-city-digimap` helm chart in this repository contains these workloads:

- Deploying and configuring `EventstoreDB` as a StatefulSet
- Deploying and configuring the MQTT Broker `Eclipse Mosquitto` as a StatefulSet
- Deploying the Lagekarte `Backend Application`
- Deploying the Lagekarte `DigiMap PWA`

Depending on the availability requirements the replica count can and should be adapted.  

### Google Cloud Platform Services

The following Google cloud platform services are used:
- Google Identity Platform for managing user accounts
- Google Cloud DNS as a managed naming services
- Google Artifact Registry for managing Linux Container Images
- Google Cloud Storage for storing images uploaded by users