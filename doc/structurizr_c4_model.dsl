workspace Digimap "C4 Model for the Smart City - Digimap Project" {

    model {
        commander = person "Upper and middle management" "Coordinator of the mission resources on site"

        sec_leader = person "Lower management" "Receives commands from the upper/middle management and executes them with along with their group."

        admin = person "Administrator" "Manages the master data and access to the application"

        group Lion5 {
            digimap = softwareSystem "Digital Situation Map (DigiMap)" "Provides a shared, unified view of the disaster situation to efficiently plan and coordinate resources or measures for the current mission." {

                pwa = container "DigiMap PWA" "Visualizes the current mission information on a digital map, and on a configurable dashboard including other context information. Optimized for mobile usage, but with limited dashboard functionality." "Vue and TypeScript" "Web Browser" {
                    sec_leader -> this "Submits reports of the current situation."
                    commander -> this "Tracks the current mission situation on the dashboard, and coordinates units and management measures. Uses mission data for follow-up tasks."
                    admin -> this "Provides and maintains master data of application users and mission resources."
                }

                backend = container "DigiMap Backend" "Processes all events during the mission, and provides APIs to manage the master data." "Java and Spring Boot"{
                    pwa -> this "Manages master data, such as user accounts or mission resources."
                    this -> pwa "Provides a REST API to retrieve and manage master data."
                }

                eventstore = container "EventStore" "Stores all retrieved events (e.g. location updates) as a temporally ordered stream." "EventstoreDB" "Database" {
                     backend -> this "Writes retrieved events during a running mission onto the stream."
                     this -> backend "Publishes new events after recording them to the stream."
                }

                mongodb = container "Database" "Stores all master data regarding users or mission resources (e.g., units), and provides the current state of missions for read or write operations." "MongoDB" "Database" {
                    backend -> this "Updates the respective mission's state depending on new events or updates."
                    this -> backend "Provides the currently known state of all running or finished missions to be retrieved by clients."
                }

                mqttBroker = container "MQTT Broker" "Enables asynchronous, real-time communication between the DigiMap backend and clients such as the DigiMap PWA. Enables clients to subscribe to topics to retrieve all updates for a specific mission." "Eclipse Mosquitto" "Queue" {
                    pwa -> this "Publishes and subscribes to mission events such as location updates, commands, or reports."
                    backend -> this "Publishes and subscribes to mission events such as location updates, commands, or reports."
                }
            }
        }

        atlas = softwareSystem "Bayern Atlas" "Map software and material provided by the free state of Bavaria" "External System" {
            this -> pwa "Provides local GIS data such as information about floods in Bavaria."
            pwa -> this "Provides an export of current map layers to visualize them in "Bayern Atlas"."
        }

        osm = softwareSystem "OpenStreetMap" "Open source map resources" "External System" {
            this -> pwa "Provides map resources such as tiles."
        }

        mcs = softwareSystem "Einsatzleitsystem" "System for detecting an emergency and automated alerting of emergency resources" "External System" {
            this -> backend "Sends mission data in EDI format or as an alarm message ("Alarmdepeche")."
        }

        vehicle = element "GPS Tracker" "Hardware" "Means of operation with GPS tracker (e.g. radio, stationary tracker, etc.)" {
            this -> mqttBroker "Publishes current location information."
        }

        production = deploymentEnvironment "Production" {

            deploymentNode "Google Cloud" {
                deploymentNode "K8s-Cluster" {
                    tags "Google Cloud Platform - Kubernetes Engine"
                    containerInstance pwa
                    containerInstance backend
                    containerInstance eventstore
                    containerInstance mongodb
                    containerInstance mqttBroker

                }

                infrastructureNode CloudDNS {
                    tag "Google Cloud Platform - Cloud DNS"
                }
            }

        }
    }

    views {
        systemContext digimap {
            include *
            autolayout
        }

        container digimap {
            include *
            exclude atlas osm mcs vehicle commander sec_leader admin
            autolayout
        }

        deployment digimap production {
            include *
            autolayout lr
        }

        theme https://static.structurizr.com/themes/google-cloud-platform-v1.5/theme.json

        styles {
            element "Person" {
                color #ffffff
                fontSize 22
                shape Person
            }
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "External System" {
                background #999999
                color #ffffff
            }
            element "Container" {
                background #438dd5
                color #ffffff
            }
            element "Web Browser" {
                shape WebBrowser
            }
            element "Mobile App" {
                shape MobileDeviceLandscape
            }
            element "Database" {
                shape Cylinder
            }
            element "Queue" {
                shape Pipe
            }
            element "Component" {
                background #85bbf0
                color #000000
            }
        }
    }
}
