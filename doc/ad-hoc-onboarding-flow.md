

```mermaid
sequenceDiagram
autonumber
actor user_a as Existing User
actor user_b as Ad-Hoc User
actor admin as Mission Admin
participant backend as Backend
participant Firebase

opt Setup
admin ->> backend: Create Units / Departments / Users
activate backend
backend -->> admin: 
deactivate backend
end

admin ->> backend: Add User to department
activate backend
backend -->> admin: 
deactivate backend

opt Start Mission
admin ->> backend: Create Mission
activate backend
backend ->> backend: Create Code
backend --> admin: 
deactivate backend

admin ->> backend: Add Departments / Units to mission
activate backend
backend ->> backend: Add Units to Mission
backend ->> backend: Set Permissions of Users
backend ->> admin: 
deactivate backend
end
user_a ->> backend: get Code
activate backend
backend --> user_a: Code
deactivate backend

user_a -->> user_a: Show Code on Screen

user_b ->> user_a: Get Join-Code

user_b ->> user_b : Enter Join-Code

user_b ->> Firebase: Create Anonymous User
activate Firebase
Firebase -->> user_b: Token
deactivate Firebase

user_b ->> backend: Join-Code
activate backend
backend -->> user_b: mission name
deactivate backend

user_b ->> user_b: Enter Pin-Code

user_b ->> backend: Verify Join-/Pin Code
activate backend
backend -->> user_b: accepted
deactivate backend

user_b ->> user_b : Enter unit/department infos

user_b ->> backend: Set unit/department
activate backend
alt Admin Online
backend -->> admin: New User in Waiting Room
deactivate backend
else Admin Opens App
admin ->> backend: Get Users in Waiting Room
activate backend
backend -->> admin: Waiting Room
deactivate backend
end

user_b -->> user_b: Waiting Room

admin ->> backend: Approve / Deny
activate backend
alt Approve
backend ->> backend: Create Unit and/or department
backend ->> backend: Assign Permissions for User
backend -->> user_b: Approval
user_b -->> user_b: Continue to mission
else
  backend ->> backend: Delete user
  backend -->> user_b: Denial
  user_b -->> user_b : Show denial
end
deactivate backend
```

Notes:
* For simplicity the Frontend is combined with the Users
* Adding a Unit to mission
  * All units in the Department get assigned to the Mission
  * All Users of the Department, the unit is part of
* The existing user may also be the admin or another adhoc onboarded one
* Request "set unit/department" is already authorized with JWT token
* The admin is notified via an MQTT message
* The waiting room can also be polled via HTTP GET
* Idea: The unit gets a "Waiting" or "Approved" status, alternatively in a separate "Waiting for Approval" document (To be determined)
  * with a separate document:
    * only create unit, if approved
* When the Ad-Hoc User enters the unit / department data, there is no Dropdown / Autocomplete, to hide the information, how is part of the mission (just a Free text input)

The logical relationship model, between the domain objects:
```mermaid
classDiagram
  class Department
  class User 
  class Unit {
    CreationMethod creationMethod
  }
  class Mission
  
  class CreationMethod {
    PERMANENT
    TEMPORARY
    TEMP_DISABLED
  }
  <<Enumeration>> CreationMethod
  
  Unit --> CreationMethod
  
  class AdHocJoinRequest {
    string userFirebaseId
    string unitName
    string userName
    Mission mission
    Status ApprovalStatus
    Date? approvalTime
    Unit? createdUnit
  }
  
  Mission "1" <-- "0..n" AdHocJoinRequest

  Department "1" --> "0...n" User
  Unit "0..n" --> "0" Department
  Unit "0...1" --> "0..1" User

  Mission "1" --> "1..n" Unit
```
* For now, a temporary unit, does not have a department (this might be added later)
* The State TEMP_DISABLED is when the user has been kicked from the mission or has logged out
* The CreationMethod enum is redundant info (if there's a JoinRequest for the unit, it is temporary), but it makes implementation much easier

# Skipped for now, but Future Work
* A new Login always creates a new Temporary unit => The approving admin, is able to change the name / assign to already defined unit (permanent or temporary)
* Kicking a user by the admin will not be implemented yet
* Logout HTTP call to disable the user & unit is implemented later
