# Domain Model for the Digital Situation Map

```mermaid
classDiagram
    direction TB
    Leitstelle "1" -- "1..*" Alarmierung : issues

    Alarmierung <|-- Alarmfax
    Alarmierung <|-- EDI
    Alarmierung "1..*" --> "1" Einsatz : creates

    Einsatz "1" -- "0..*" Einsatzabschnitt : contains
    Einsatz "1" -- "1..*" Einheit : employs
    Führungskraft "1" -- "0..*" POI : creates
    Einsatz "1" -- "1" Gesamteinsatzleiter : leads

    Führungskraft "1" -- "1..*" Einsatzkraft : leads
    Einsatzkraft "1..*" -- "1" Einheit : belongs-to

    Einsatzkraft <|-- Führungskraft

    Führungskraft "1" -- "1..*" Einheit : leads

    Führungskraft "1" -- "0..*" Führungskraft : leads

    Einheit "1" -- "1..*" Einsatzmittel : has

    Einheit "1" -- "0..*" Meldung : issues

    Einheit "1" -- "0..*" Befehl : issues

    Meldung "0..*" -- "1" Einheit : receives
    Meldung "0..*" -- "1" Leitstelle: receives

    Meldung <|-- Gefahrenmeldung

    Gefahrenmeldung "1" -- "1..*" Einheit : receives

    Befehl "1" -- "1..*" Einheit : receives

    Einsatzabschnitt "1" -- "1" Abschnittsleiter : leads

    Einsatzabschnitt "1" -- "1..*" Einheit : contains

    Einsatzabschnitt "1" -- "0..*" Einsatzabschnitt : contains


    Führungskraft <|-- Gesamteinsatzleiter

    Führungskraft <|-- Abschnittsleiter



    POI "0..*" -- "1" Einsatz : has

    POI <|-- Bereitstellungsraum
    POI <|-- Übergabepunkt
    POI <|-- Gefahrenstelle
    POI <|-- Meldekopf
    POI <|-- Lotsenstelle
    POI <|-- Abfahrtsweg
    POI <|-- Anfahrtsweg


    Gefahrenstelle "1" -- "1" Gefahrenmeldung : declares

    class POI {
        Position
    }

    class Einsatz {
        Einsatzort
        Art
    }

    class Einheit {
       Funkrufname
       Taktisches Zeichen
       Aktueller Standort
    }

    class Einsatzmittel {
        Funkrufname
        Organisation
        Art/Typ
        Aktueller Standort
    }

    class Führungskraft {
        Funkrufname
        Aktueller Standort
    }

    class Alarmierung {
        Einsatzstichwort
        Zeitpunkt
    }

    class Einsatzabschnitt {
        räumliches und/oder fachliches Gebiet
    }

    class Befehl {
        Zeitpunkt
    }

    class Meldung {
        Zeitpunkt
    }
```

POI Auflistung ist nicht abgeschlossen/vollständig.

