workspace Digimap "C4 Model for the Smart City - Digimap Project" {

    model {
        commander = person "Obere und Mittlere Führungsebene" "Koordinator der Einsatzmittel am Einsatzort"
        sec_leader = person "Untere Führungsebene" "Nimmt die Befehle des/der Einsatzleiter entgegen und führt diese mit zusammen mit seinem Trupp/Gruppe aus."
        admin = person "Administrator" "Verwaltet den Zugang zu Digimap"


        group Lion5 {
            digimap = softwareSystem "Digitale Lagekarte" "Ermöglicht der Einsatzleitung eine geteilte, einheitliche sicht einer Katastrophenlage oder eines Großeinsatzes um die Einsatzmittel und -maßnahmen reibungslos zu koordinieren." {

                pwa = container "Progressive Web App" "Stellt die Digitale Lagekarte zusammen mit weitern Kontextinformationen in einer Art Dashboard da. Die mobile Nutzung ist mit reduziertem Funktionsumfang möglich." "Vue and TypeScript" "Web Browser"{
                    sec_leader -> this "Übermittelt Lagemeldungen"
                    commander -> this "Nutzt DigiMap im akuten Einsatzgeschen zur koordination der Maßnahmen. Zusätzlich nutzt er die Einsatzdaten zur Nachbereitung."
                    admin -> this "Pflegt die Stammdaten der Nutzer und der Einsatzmittel."
                }
                backend = container "Digimap Backend" "Verarbeitet alle Events während des Einsatzes und bietet Schnittstellen zum managen der Stammdaten." "Java and Spring Boot"{
                    pwa -> this "Update Events (Standort, Befehle, Lageberichte)"
                    this -> pwa "Sendet Statusänderungen wärend des Einsatzes an alle Nutzer"
                    pwa -> this "Aktualisierungen der Stammdaten per REST"
                }

                eventstore = container "Eventstore" "Speicher auditlog/accesslog events für die Stammdaten und Sämtliche Events wärend eines Einsatzes." "EventstoreDB" "Database" {
                     backend -> this "Schreibt alle Events wärend eines Einsatzes"
                     this -> backend "Sendet alle neuen Events"
                }
                mongodb = container "Database" "Speichert die Stammdaten der einzelnen Einheiten, Stellt den aktuellen Zustand wärend eines Einsatzes zum lesen bereit." "MongoDB" "Database" {
                    backend -> this "Aktualisiert den Gesamtstatus des Einsatzes aufgrund der Events."
                }

                mqttBroker = container "MQTT Broker" "Stellt die bidirektionalie echtzeit Kommunikation zwischen Frontend und Backend sicher." "Eclipse Mosquitto" "Queue" {
                    pwa -> this "Empfängt und Sendet Echtzeitinformationen bezüglich laufender Einsätze."
                    backend -> this "Empfängt und Sendet Echtzeitinformationen bezüglich laufender Einsätze."
                }
            }
        }

        atlas = softwareSystem "Bayern Atlas" "Kartensoftware und -material bereitgestellt vom Freistaat Bayern" "External System" {
            this -> pwa "Stellt GIS Daten (z.B. Hochwasserinformationen) bereit"
            pwa -> this "Export von Kartenzwischenständen und Visualisierung im Bayernatlas"
        }

        osm = softwareSystem "OpenStreetMap" "Open Source Kartenmaterial" "External System" {
            this -> pwa "Stellt Kartenmaterial bereit"
        }

        mcs = softwareSystem "Einsatzleitsystem" "System zum Erfassen eines Notfalls und automatisierten Alarmierung der Einsatzmittel" "External System" {
            this -> backend "Sendet Einsatzdaten im EDI format oder als Alarmdepeche"
        }

        vehicle = element "GPS Tracker" "Hardware" "Einsatzmittel mit GPS Tracker (z.B. Funkgerät, stationärer Tracker, etc.)" {
            this -> mqttBroker "Sendet Standortinformationen"
        }
    }

    views {
        systemContext digimap {
            include *
            autolayout
        }

        container digimap {
            include *
            autolayout
        }

        theme default

        styles {
            element "Person" {
                color #ffffff
                fontSize 22
                shape Person
            }
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "External System" {
                background #999999
                color #ffffff
            }
            element "Container" {
                background #438dd5
                color #ffffff
            }
            element "Web Browser" {
                shape WebBrowser
            }
            element "Mobile App" {
                shape MobileDeviceLandscape
            }
            element "Database" {
                shape Cylinder
            }
            element "Queue" {
                shape Pipe
            }
            element "Component" {
                background #85bbf0
                color #000000
            }
        }
    }
}
