resource "google_artifact_registry_repository" "container-registry" {
  location      = var.region
  repository_id = "docker"
  description   = "Digimap Docker Repository"
  format        = "DOCKER"

  cleanup_policy_dry_run = true
  cleanup_policies {
    id     = "delete-untagged-images older than 30 days"
    action = "DELETE"
    condition {
      tag_state  = "UNTAGGED"
      older_than = "2592000s"
    }
  }
  cleanup_policies {
    id     = "delete-images-older than 9 months"
    action = "DELETE"
    condition {
      tag_state  = "TAGGED"
      older_than = "23328000s"
    }
  }
  cleanup_policies {
    id     = "delete-staging images older than 30 days"
    action = "DELETE"
    condition {
      tag_state    = "TAGGED"
      tag_prefixes = ["staging"]
      older_than   = "2592000s"
    }
  }
}
