variable "gke_username" {
  default     = ""
  description = "GKE username"
}

variable "gke_password" {
  default     = ""
  description = "GKE password"
}

variable "gke_num_nodes" {
  default     = 2
  description = "number of GKE nodes"
}

data "google_compute_default_service_account" "default" {
}

resource "google_container_cluster" "cluster" {
  name     = "${var.project_id}-gke"
  location = "${var.region}-a"

  remove_default_node_pool = true
  initial_node_count       = 1

  network    = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name
}

# Separately Managed Node Pool
resource "google_container_node_pool" "node_pool" {
  name               = google_container_cluster.cluster.name
  location           = "${var.region}-a"
  cluster            = google_container_cluster.cluster.name
  initial_node_count = var.gke_num_nodes
  autoscaling {
    max_node_count = 3
    min_node_count = var.gke_num_nodes
  }

  node_config {
    service_account = data.google_compute_default_service_account.default.email

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env = var.project_id
    }

    # see: https://cloud.google.com/kubernetes-engine/docs/how-to/preemptible-vms
    preemptible  = true
    machine_type = "e2-standard-2"
    tags         = ["gke-node", "${var.project_id}-gke"]

    metadata = {
      disable-legacy-endpoints = "true"
    }
  }

  upgrade_settings {
    max_surge       = 1
    max_unavailable = 0
  }
}

output "kubernetes_cluster_name" {
  value       = google_container_cluster.cluster.name
  description = "GKE Cluster Name"
}

output "kubernetes_cluster_host" {
  value       = google_container_cluster.cluster.endpoint
  description = "GKE Cluster Host"
}
