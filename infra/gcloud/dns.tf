resource "google_dns_managed_zone" "digimap-zone" {
  name        = "lagekarte-app"
  project     = var.project_id
  dns_name    = "${var.domain}."
  description = "DNS zone for domain: ${var.domain}"
  dnssec_config {
    state = "on"
  }
}

resource "google_dns_record_set" "a" {
  name         = google_dns_managed_zone.digimap-zone.dns_name
  managed_zone = google_dns_managed_zone.digimap-zone.name
  type         = "A"
  ttl          = 300

  rrdatas = [google_compute_address.default.address]
}

resource "google_dns_record_set" "api_a" {
  name         = "api.${google_dns_managed_zone.digimap-zone.dns_name}"
  managed_zone = google_dns_managed_zone.digimap-zone.name
  type         = "A"
  ttl          = 300

  rrdatas = [google_compute_address.default.address]
}

resource "google_dns_record_set" "broker_a" {
  name         = "broker.${google_dns_managed_zone.digimap-zone.dns_name}"
  managed_zone = google_dns_managed_zone.digimap-zone.name
  type         = "A"
  ttl          = 300

  rrdatas = [google_compute_address.default.address]
}

resource "google_dns_record_set" "grafana_a" {
  name         = "grafana.${google_dns_managed_zone.digimap-zone.dns_name}"
  managed_zone = google_dns_managed_zone.digimap-zone.name
  type         = "A"
  ttl          = 300

  rrdatas = [google_compute_address.default.address]
}

resource "google_dns_record_set" "wildcard_a" {
  name         = "*.${google_dns_managed_zone.digimap-zone.dns_name}"
  managed_zone = google_dns_managed_zone.digimap-zone.name
  type         = "A"
  ttl          = 300

  rrdatas = [google_compute_address.default.address]
}

resource "google_dns_record_set" "cname" {
  name         = "www.${google_dns_managed_zone.digimap-zone.dns_name}"
  managed_zone = google_dns_managed_zone.digimap-zone.name
  type         = "CNAME"
  ttl          = 300

  rrdatas = [google_dns_managed_zone.digimap-zone.dns_name]
}

resource "google_dns_record_set" "ns" {
  name         = google_dns_managed_zone.digimap-zone.dns_name
  managed_zone = google_dns_managed_zone.digimap-zone.name
  type         = "NS"
  ttl          = 21600

  rrdatas = [
    "ns-cloud-b1.googledomains.com.",
    "ns-cloud-b2.googledomains.com.",
    "ns-cloud-b3.googledomains.com.",
    "ns-cloud-b4.googledomains.com."
  ]
}

resource "google_dns_record_set" "soa" {
  name         = google_dns_managed_zone.digimap-zone.dns_name
  managed_zone = google_dns_managed_zone.digimap-zone.name
  type         = "SOA"
  ttl          = 21600

  rrdatas = ["ns-cloud-b1.googledomains.com. cloud-dns-hostmaster.google.com. 1 21600 3600 259200 300"]
}

resource "google_dns_record_set" "firebase_txt" {
  name         = google_dns_managed_zone.digimap-zone.dns_name
  managed_zone = google_dns_managed_zone.digimap-zone.name
  type         = "TXT"
  ttl          = 3600

  rrdatas = ["\"v=spf1 include:_spf.firebasemail.com ~all\"", "\"firebase=${var.firebase_project_id}\""]
}

resource "google_dns_record_set" "firebase_cname1" {
  name         = "firebase1._domainkey.${google_dns_managed_zone.digimap-zone.dns_name}"
  managed_zone = google_dns_managed_zone.digimap-zone.name
  type         = "CNAME"
  ttl          = 3600

  rrdatas = ["mail-${google_dns_managed_zone.digimap-zone.name}.dkim1._domainkey.firebasemail.com."]
}

resource "google_dns_record_set" "firebase_cname2" {
  name         = "firebase2._domainkey.${google_dns_managed_zone.digimap-zone.dns_name}"
  managed_zone = google_dns_managed_zone.digimap-zone.name
  type         = "CNAME"
  ttl          = 3600

  rrdatas = ["mail-${google_dns_managed_zone.digimap-zone.name}.dkim2._domainkey.firebasemail.com."]
}
