# Google Cloud infastructure

This creates/updates the infrastructure inside the configured GCP project.


Run
```shell
terraform init
```

and then
```shell
terraform apply
```

to create the necessary infrastructure
