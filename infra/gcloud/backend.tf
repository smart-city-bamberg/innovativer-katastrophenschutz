terraform {
  backend "gcs" {
    bucket = "<name of your generated bucket to store the TF state>"
    prefix = "terraform/state"
  }
}
