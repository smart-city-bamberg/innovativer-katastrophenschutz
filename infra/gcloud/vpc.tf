resource "google_compute_network" "vpc" {
  name                    = "${var.project_id}-vpc"
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "subnet" {
  name          = "${var.project_id}-subnet"
  region        = var.region
  project       = var.project_id
  ip_cidr_range = "10.10.0.0/24"
  network       = google_compute_network.vpc.name
}

resource "google_compute_address" "default" {
  name    = "digimap-static-ip-address"
  region  = var.region
  project = var.project_id
}

output "external_ip" {
  value       = google_compute_address.default.address
  description = "External IP of the digimap GKE cluster"
}
