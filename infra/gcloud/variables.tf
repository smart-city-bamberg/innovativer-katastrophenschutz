variable "project_id" {
  type        = string
  description = "The Google Cloud Project Id"
  default     = ""
}

variable "domain" {
  type        = string
  description = "Domain associated with the deployment (e.g. lagekarte-example.org)"
  default     = ""
}

variable "firebase_project_id" {
  type        = string
  description = "The firebase project id"
  default     = ""
}

variable "region" {
  type    = string
  default = "europe-west3"
}

output "region" {
  value       = var.region
  description = "Google Cloud Region"
}

output "project_id" {
  value       = var.project_id
  description = "Google Cloud Project Id"
}
