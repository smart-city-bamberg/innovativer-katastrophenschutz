resource "random_id" "bucket_prefix" {
  byte_length = 8
}

resource "google_storage_bucket" "default" {
  #  Bucket for storing the terraform state
  name          = "${random_id.bucket_prefix.hex}-bucket-terraformstate"
  force_destroy = false
  location      = var.region
  storage_class = "REGIONAL"
  versioning {
    enabled = true
  }
  public_access_prevention = "enforced"
}
