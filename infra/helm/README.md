# Installation

## Prequisites

Install the following:
- [helm](https://helm.sh/docs/intro/install/)
- [helmfile](https://github.com/helmfile/helmfile)

TLDR:
```shell
helmfile sync
helm install --create-namespace digimap
```
The operator must be installed in the namespace where you want to install DigiMap.
Otherwise, update the service account for each installation and follow these [instructions](https://www.mongodb.com/docs/kubernetes-operator/master/reference/helm-operator-settings/#helm-watch-namespace).

# Testing

Helm chart is automatically tested in CI.

## Linting

## Test Install

A [Kind](https://kind.sigs.k8s.io/docs/user/quick-start/) cluster is created and the chart is installed there.
As values the `ci/test-values.yaml` files is used. (more test values may be added in the format `ci/*-values.yaml`)
