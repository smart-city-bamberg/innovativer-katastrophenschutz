apiVersion: apps/v1
kind: Deployment
metadata:
  name: backend
  labels:
    app: backend
    {{- include "smart-city-digimap.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.backend.replicaCount }}
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      annotations:
        prometheus.io/scrape: "true"
        prometheus.io/path: /actuator/prometheus
        prometheus.io/port: "80"
      {{- with .Values.backend.podAnnotations }}
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        app: backend
    spec:
    {{- with .Values.backend.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      securityContext:
        {{- toYaml .Values.backend.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Values.backend.name }}
          securityContext:
            {{- toYaml .Values.backend.securityContext | nindent 12 }}
          image: "{{ .Values.backend.image.repository }}:{{ .Values.backend.image.tag | default "latest" }}"
          imagePullPolicy: {{ .Values.backend.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
          env:
            - name: SERVER_PORT
              value: "80"
            - name: SPRING_DATA_MONGODB_DATABASE
              value: {{ .Values.mongodb.credentials.db}}
            - name: SPRING_DATA_MONGODB_AUTHENTICATION_DATABASE
              value: "admin"
            - name: SPRING_DATA_MONGODB_PORT
              value: "27017"
            - name: SPRING_DATA_MONGODB_HOST
              value: "mongodb-svc"
            - name: SPRING_DATA_MONGODB_USERNAME
              value: {{ .Values.mongodb.credentials.username }}
            - name: SPRING_DATA_MONGODB_PASSWORD
              value: {{ .Values.mongodb.credentials.password }}
            - name: SPRING_SECURITY_USER_NAME
              value: {{ .Values.backend.ediBasicAuth.user }}
            - name: SPRING_SECURITY_USER_PASSWORD
              value: {{ .Values.backend.ediBasicAuth.password }}
            - name: DIGIMAP_EVENTSTORE_URL
              value: "esdb://eventstore-service:2113?tls=false"
            - name: DIGIMAP_MQTT_URL
              value: "ssl://{{.Values.gcp.brokerDomainName}}:{{ .Values.mqtt.service.ports.mqtts }}"
          livenessProbe:
            httpGet:
              path: /actuator/health/liveness
              port: http
          startupProbe:
            httpGet:
              path: /actuator/health
              port: http
            initialDelaySeconds: 10
            periodSeconds: 5
            failureThreshold: 10
          readinessProbe:
            httpGet:
              path: /actuator/health/readiness
              port: http
            initialDelaySeconds: 10
            periodSeconds: 10
            failureThreshold: 5
          resources:
            {{- toYaml .Values.backend.resources | nindent 12 }}
      {{- with .Values.backend.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.backend.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.backend.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
---
apiVersion: v1
kind: Service
metadata:
  name: {{ .Values.backend.name }}
  labels:
    {{- include "smart-city-digimap.labels" . | nindent 4 }}
spec:
  type: {{ .Values.backend.service.type }}
  ports:
    - port: {{ .Values.backend.service.port }}
      targetPort: http
      protocol: TCP
      name: http
  selector:
    app: backend
