#!/usr/bin/env bash
set -x

# Get the folder where this script is located in
SCRIPT_DIR=$(dirname $(realpath $0))

# Install mongodb and eventstore
helm repo add mongodb https://mongodb.github.io/helm-charts
helm install community-operator mongodb/community-operator
helm install digimap $SCRIPT_DIR
