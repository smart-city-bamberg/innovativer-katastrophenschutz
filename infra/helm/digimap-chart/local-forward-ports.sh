#!/usr/bin/env bash

# Forward ports from k8s to localhost
echo "Forwarding mongodb to localhost:27017"
kubectl port-forward services/mongodb-svc 27017:27017 &
echo "Forwarding eventstoreDB to localhost:1113 and 2113"
kubectl port-forward services/eventstore-service 1113:eventstore 2113:admin-ui &

kubectl port-forward services/mosquitto-broker-svc 1883:1883 8081:8081
