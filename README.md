# Innovativer Katastrophenschutz - Smart City Bamberg
Dieses Projekt entsteht im Rahmen des Smart CIty Projektes der Stadt Bamberg und wird durch das Förderprogramm **Modellprojekte Smart Cities (MPSC)** unterstützt.
Diese digitale Lagekarte erleichtert, die Koordination von Einheiten des Katastrophenschutzes, Feuerwehren,
THW und anderer Rettungsorganisationen.

### Dashboard
<img src="doc/img/Dashboard.jpg" alt="Dashboard" width="950"/>

### Lagekarte
<img src="doc/img/Lagekarte.jpg" alt="Dashboard" width="950"/>

## Lokale Ausführung

### Vorraussetzungen

* [Docker Desktop](https://www.docker.com/products/docker-desktop/)
* [kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl)
* [Helm](https://helm.sh/docs/intro/install/)
* [Terraform](https://developer.hashicorp.com/terraform/downloads) (nur für Google Cloud Deployments)

Das gesamte Projekt ist für die Ausführung in GKE konzipiert, kann aber auch lokal auf einem K8s-Cluster ausgeführt werden.
Sie können K8s lokal verwenden, indem Sie es in den Docker-Desktop-Einstellungen aktivieren (Kubernetes > Enable Kubernetes).

**Hinweis:** Dadurch wird Ihr kubectl-Kontext automatisch auf Ihren lokalen k8s-Cluster umgestellt.

### Einrichtung der lokalen Infrastruktur

Siehe [infra/helm/README.md](infra/helm/README.md).

1. Installieren Sie alle Software, die unter [Vorraussetzungen](#Vorraussetzungen) aufgelistet ist
2. Einrichten und Starten der Kubernetes-Ressourcen:
  
  ```shell
  ./infra/helm/digimap-chart/local-setup.sh
  ```

3. Forward the ports to localhost:
  
  ```shell
  ./infra/helm/digimap-chart/local-forward-ports.sh
  ```

4. Autorisieren Sie Google Identity Provider (2 Optionen):
  - Im Browser mit [Application Default Credentials](https://cloud.google.com/docs/authentication/provide-credentials-adc#local-dev):
  
  ```shell
  gcloud auth application-default login
  ```
  
  => speichert eine lokale Anmeldedaten-Datei, die als Standard-Authentifizierungsmethode auf Ihrem Rechner verwendet wird.
   - Verwendung eines [Dienstkonto-Schlüssels](https://cloud.google.com/docs/authentication/provide-credentials-adc#local-key): erzeugen Sie den privaten Schlüssel von [firebase-adminsdk](https://console.cloud.google.com/iam-admin/serviceaccounts/details/113912927122017768741/keys?project=smart-city-digimap)
  Dienstkontos in der Google Cloud Console, speichern Sie es lokal und setzen Sie dann die Umgebungsvariable `GOOGLE_APPLICATION_CREDENTIALS`, bevor Sie Backend ausführen (siehe unten).

### Ausführung des Backend (Spring Boot)

Installieren Sie die gesamte Infrastruktur auf einer lokalen Kubernetes Instanz und leiten Sie die mindestens die MongoDB-Ports weiter.

```shell
# only if local service account file is used - CAUTION: replace path so that it points to the stored file!
export GOOGLE_APPLICATION_CREDENTIALS="/Users/x/y/z/smart-city-digimap/etc/firebase-service-account.json"

cd backend
docker compose up
./gradlew bootRun
```
- **Swagger UI** ist erreichbar unter http://localhost:8089/swagger-ui/index.html.

### Run Frontend / PWA (Vue & Vite)

See [digimap-pwa/README.md](digimap-pwa/README.md)

## Dokumentation

Sämtliche Dokumentation der [Architektur](doc/Architecture%20Documentation.md) und der Schnittstellen findet sich im *doc* Verzeichnis in diesem Repository.

## Förderung

Dieses Projekt wurde gefördert durch das  Bundesministerium des Innern, für Bau und Heimat, die Kreditanstalt für Wiederaufbau und die Versicherungskammer Stiftung.

<p align="center">
  <img src="doc/img/KFW.png" alt="KFW" width="200" style="margin-right: 30px;"/>
  <img src="doc/img/Logo_BMI.jpg" alt="BMI" width="250" style="margin-right: 30px;"/>
  <img src="doc/img/VKS.png" alt="VKS" width="250"/>
</p>
