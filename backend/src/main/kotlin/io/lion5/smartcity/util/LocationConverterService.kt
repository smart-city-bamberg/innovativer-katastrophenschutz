/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.util

import io.lion5.smartcity.mongodb.model.location.GKLocation
import io.lion5.smartcity.mongodb.model.location.GPSLocation
import io.lion5.smartcity.mongodb.model.location.Location
import io.lion5.smartcity.mongodb.model.location.UTMLocation
import org.geotools.geometry.GeneralPosition
import org.geotools.referencing.CRS
import org.springframework.stereotype.Service

@Service
class LocationConverterService {
    /**
     * Converts a given Location to WGS84 GPS coordinates.
     *
     * @param location instance of Location
     */
    fun convertToGPS(location: Location): GPSLocation {
        var result: GPSLocation = GPSLocation(0.0, 0.0)
        when (location) {
            is GPSLocation -> result = location
            is GKLocation -> {
                val gaussKrugerCRS = CRS.decode(location.epsgId().string)
                val wgs84CRS = CRS.decode(GPSLocation(0.0, 0.0).epsgId().string)

                val gaussKrugerCoord = GeneralPosition(location.y, location.x)
                val transform = CRS.findMathTransform(gaussKrugerCRS, wgs84CRS)

                val wgs84Coords = transform.transform(gaussKrugerCoord, null)

                result = GPSLocation(wgs84Coords.getOrdinate(0), wgs84Coords.getOrdinate(1))
            }

            is UTMLocation -> {
                TODO("This is still missing because of the weird EPSGIDs for UTM")
            }
        }
        return result
    }
}
