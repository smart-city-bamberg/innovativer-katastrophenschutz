/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.util.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

class ImageNotFoundException(
    msg: String,
    cause: Exception?,
) : Exception(msg, cause)

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
class ImageStorageException(
    msg: String,
    cause: Exception?,
) : Exception(msg, cause)

class ImageResizeException(
    msg: String,
    cause: Exception?,
) : Exception(msg, cause)

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
class ImageTransformationException(
    msg: String,
    cause: Exception?,
) : Exception(msg, cause)

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
class FaviconGenerationException(
    msg: String,
    cause: Exception?,
) : Exception(msg, cause)

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
class ImageResolutionException(
    message: String,
) : RuntimeException(message)

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
class ImageReadException(
    message: String,
) : RuntimeException(message)
