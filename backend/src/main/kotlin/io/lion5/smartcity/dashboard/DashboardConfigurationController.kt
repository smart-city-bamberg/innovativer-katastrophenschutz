/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.dashboard

import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.auth.model.AuthorityRoles
import io.lion5.smartcity.dashboard.dto.DashboardConfigurationOption
import io.lion5.smartcity.dashboard.dto.DashboardConfigurationRequest
import io.lion5.smartcity.mongodb.model.dashboard.DashboardConfiguration
import io.swagger.v3.oas.annotations.Operation
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatusCode
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder

@RestController
@RequestMapping("dashboard-configurations")
@CrossOrigin
class DashboardConfigurationController(
    private val dashboardConfigurationService: DashboardConfigurationService,
) {
    private val log = KotlinLogging.logger {}

    @PreAuthorize("hasAnyRole('${AuthorityRoles.USER}','${AuthorityRoles.ADMIN}','${AuthorityRoles.SUPER_ADMIN}')")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Saves a new dashboard configuration")
    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun saveDashboardConfiguration(
        @RequestBody dashboardConfigurationRequest: DashboardConfigurationRequest,
    ): ResponseEntity<DashboardConfiguration> {
        log.debug { "Saving new dashboard configuration ${dashboardConfigurationRequest.name}" }
        try {
            val savedDashboardConfiguration = dashboardConfigurationService.saveNewDashboardConfiguration(dashboardConfigurationRequest)
            return ResponseEntity
                .created(
                    ServletUriComponentsBuilder
                        .fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(savedDashboardConfiguration.id)
                        .toUri(),
                ).body(
                    savedDashboardConfiguration,
                )
        } catch (iae: IllegalArgumentException) {
            return ResponseEntity.status(HttpStatusCode.valueOf(409)).build()
        }
    }

    @PreAuthorize("hasAnyRole('${AuthorityRoles.USER}','${AuthorityRoles.ADMIN}','${AuthorityRoles.SUPER_ADMIN}')")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Returns all dashboard configuration options")
    @GetMapping
    fun getAllDashboardConfigurationOptions(): ResponseEntity<List<DashboardConfigurationOption>> {
        log.debug { "Getting all dashboard configurations" }
        return ResponseEntity.ok().body(dashboardConfigurationService.getAllDashboardConfigurationOptions())
    }

    @PreAuthorize("hasAnyRole('${AuthorityRoles.USER}','${AuthorityRoles.ADMIN}','${AuthorityRoles.SUPER_ADMIN}')")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Returns a single dashboard configuration")
    @GetMapping("/{id}")
    fun getDashboardConfiguration(
        @PathVariable id: String,
    ): ResponseEntity<DashboardConfiguration> {
        log.debug { "Getting dashboard configuration with id: $id" }
        try {
            return ResponseEntity.ok().body(dashboardConfigurationService.getDashboardConfigurationById(id))
        } catch (e: IllegalArgumentException) {
            return ResponseEntity.notFound().build()
        }
    }

    @PreAuthorize("hasAnyRole('${AuthorityRoles.USER}','${AuthorityRoles.ADMIN}','${AuthorityRoles.SUPER_ADMIN}')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete a dashboard configuration")
    @DeleteMapping("/{id}")
    fun deleteDashboardConfiguration(
        @PathVariable id: String,
    ): ResponseEntity<Void> {
        log.debug { "Deleting dashboard configuration with id: $id" }
        try {
            dashboardConfigurationService.deleteDashboardConfiguration(id)
            return ResponseEntity.noContent().build()
        } catch (e: IllegalArgumentException) {
            return ResponseEntity.notFound().build()
        }
    }
}
