/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.dashboard

import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.dashboard.dto.DashboardConfigurationOption
import io.lion5.smartcity.dashboard.dto.DashboardConfigurationRequest
import io.lion5.smartcity.mongodb.model.dashboard.DashboardConfiguration
import io.lion5.smartcity.mongodb.repository.DashboardConfigurationRepository
import org.springframework.stereotype.Service

@Service
class DashboardConfigurationService(
    private val dashboardConfigurationRepository: DashboardConfigurationRepository,
) {
    private val log = KotlinLogging.logger {}

    fun getAllDashboardConfigurationOptions(): List<DashboardConfigurationOption> {
        log.debug { "Fetching all dashboard configuration options" }
        return dashboardConfigurationRepository.findAllDashboardConfigurationOptions()
    }

    fun saveNewDashboardConfiguration(dashboardConfigurationRequest: DashboardConfigurationRequest): DashboardConfiguration {
        dashboardConfigurationRepository.findByName(dashboardConfigurationRequest.name)?.let {
            throw IllegalArgumentException("Dashboard configuration with name ${dashboardConfigurationRequest.name} already exists")
        }
        val dashboardConfiguration =
            DashboardConfiguration(
                name = dashboardConfigurationRequest.name,
                widgetConfigurations = dashboardConfigurationRequest.widgetConfigurations,
            )
        log.debug { "Saving new dashboard configuration ${dashboardConfiguration.name}" }
        return dashboardConfigurationRepository.insert(dashboardConfiguration)
    }

    fun getDashboardConfigurationById(id: String): DashboardConfiguration =
        dashboardConfigurationRepository.findById(id).orElseThrow {
            throw IllegalArgumentException("Dashboard configuration with id $id not found")
        }

    fun deleteDashboardConfiguration(id: String) {
        log.debug { "Deleting dashboard configuration with id $id" }
        if (!dashboardConfigurationRepository.existsById(id)) {
            throw IllegalArgumentException("dashboard configuration with id $id does not exist")
        }
        dashboardConfigurationRepository.deleteById(id)
    }
}
