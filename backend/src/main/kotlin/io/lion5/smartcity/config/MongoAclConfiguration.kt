package io.lion5.smartcity.config

import org.springframework.cache.concurrent.ConcurrentMapCache
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.MongoDatabaseFactory
import org.springframework.data.mongodb.MongoTransactionManager
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import org.springframework.security.acls.AclPermissionEvaluator
import org.springframework.security.acls.dao.AclRepository
import org.springframework.security.acls.domain.AclAuthorizationStrategyImpl
import org.springframework.security.acls.domain.ConsoleAuditLogger
import org.springframework.security.acls.domain.DefaultPermissionGrantingStrategy
import org.springframework.security.acls.domain.SpringCacheBasedAclCache
import org.springframework.security.acls.jdbc.LookupStrategy
import org.springframework.security.acls.model.PermissionGrantingStrategy
import org.springframework.security.acls.mongodb.MongoDBBasicLookupStrategy
import org.springframework.security.acls.mongodb.MongoDBMutableAclService
import org.springframework.security.core.authority.SimpleGrantedAuthority

@Configuration
@EnableMongoRepositories(basePackageClasses = [AclRepository::class], basePackages = ["io.lion5.smartcity"])
class MongoAclConfiguration(
    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    private val aclRepository: AclRepository,
    private val mongoTemplate: MongoTemplate,
) {
    @Bean
    fun transactionManager(mongoDatabaseFactory: MongoDatabaseFactory): MongoTransactionManager =
        MongoTransactionManager(mongoDatabaseFactory)

    @Bean
    fun aclPermissionEvaluator(): AclPermissionEvaluator = AclPermissionEvaluator(aclService())

    @Bean
    fun aclService(): MongoDBMutableAclService = MongoDBMutableAclService(aclRepository, lookupStrategy(), aclCache())

    @Bean
    fun lookupStrategy(): LookupStrategy =
        MongoDBBasicLookupStrategy(
            mongoTemplate,
            aclCache(),
            aclAuthorizationStrategy(),
            permissionGrantingStrategy(),
        )

    @Bean
    fun aclCache(): SpringCacheBasedAclCache {
        val aclCache = ConcurrentMapCache("acl_cache")
        return SpringCacheBasedAclCache(aclCache, permissionGrantingStrategy(), aclAuthorizationStrategy())
    }

    @Bean
    fun aclAuthorizationStrategy(): AclAuthorizationStrategyImpl =
        AclAuthorizationStrategyImpl(
            SimpleGrantedAuthority("ADMIN"),
        )

    @Bean
    fun permissionGrantingStrategy(): PermissionGrantingStrategy = DefaultPermissionGrantingStrategy(ConsoleAuditLogger())
}
