/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.config

import io.lion5.smartcity.eventstore.EventstoreMissionService
import io.lion5.smartcity.mongodb.handler.EmergencyUnitEventHandler
import io.lion5.smartcity.mongodb.handler.MissionEventHandler
import io.lion5.smartcity.mongodb.model.Command
import io.lion5.smartcity.mongodb.model.DangerZone
import io.lion5.smartcity.mongodb.model.Department
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.model.PointOfInterest
import io.lion5.smartcity.mongodb.model.Report
import io.lion5.smartcity.mongodb.repository.AdhocJoinRequestRepository
import io.lion5.smartcity.mongodb.service.AuditService
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import io.lion5.smartcity.mongodb.service.MissionService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer
import org.springframework.http.HttpMethod
import org.springframework.web.servlet.config.annotation.CorsRegistry

/**
 * Configuration class for spring data rest repository configuration
 */
@Configuration
class RepositoryConfiguration : RepositoryRestConfigurer {
    @Bean
    fun emergencyUnitEventHandler(auditService: AuditService): EmergencyUnitEventHandler = EmergencyUnitEventHandler(auditService)

    @Bean
    fun missionEventHandler(
        missionService: MissionService,
        emergencyUnitService: EmergencyUnitService,
        eventstoreMissionService: EventstoreMissionService,
        adhocJoinRequestRepository: AdhocJoinRequestRepository,
    ): MissionEventHandler =
        MissionEventHandler(
            missionService = missionService,
            eventstoreMissionService = eventstoreMissionService,
            emergencyUnitService = emergencyUnitService,
            adhocJoinRequestRepository = adhocJoinRequestRepository,
        )

    override fun configureRepositoryRestConfiguration(
        config: RepositoryRestConfiguration,
        cors: CorsRegistry?,
    ) {
        // prevent hidden IDs in responses
        config.exposeIdsFor(
            EmergencyUnit::class.java,
            Mission::class.java,
            Report::class.java,
            Command::class.java,
            DangerZone::class.java,
            PointOfInterest::class.java,
        )

        // prevent PUT, POST and PATCH operation on Document associations because we write our own controller methods
        config.exposureConfiguration.withAssociationExposure { _, httpMethods ->
            httpMethods.disable(HttpMethod.PUT, HttpMethod.POST, HttpMethod.PATCH)
        }
        // disable the default PUT operation
        config.exposureConfiguration.forDomainType(Department::class.java).disablePutForCreation()
    }
}
