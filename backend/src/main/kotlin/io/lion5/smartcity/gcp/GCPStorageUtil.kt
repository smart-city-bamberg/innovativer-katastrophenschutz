/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.gcp
import com.google.cloud.storage.BlobInfo
import com.google.cloud.storage.Storage
import com.google.cloud.storage.StorageException
import io.lion5.smartcity.util.exception.ImageStorageException
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class GCPStorageUtil(
    @Value("\${tactical-sign-storage.bucket.assets.name}") val bucketName: String,
    val storage: Storage,
) {
    /**
     * Saves the given [image] to the gcp cloud storage using [blobName] as blob name and [contentType] (MimeType) as type.
     * The bucket is configured via env variable [bucketName].
     *
     * @return the [BlobInfo] that is returned from gcp
     * @throws ImageStorageException if the gcp sdk weren't able to store the image
     */
    fun saveImage(
        image: ByteArray,
        blobName: String,
        contentType: String,
    ): BlobInfo =
        try {
            storage
                .create(
                    BlobInfo
                        .newBuilder(bucketName, blobName)
                        .setContentType(contentType)
                        .build(),
                    image,
                ).asBlobInfo()
        } catch (e: StorageException) {
            throw ImageStorageException(
                "Image with upload file name $blobName could not be uploaded to bucket $bucketName.",
                e,
            )
        }

    /**
     * Deletes an image from the gcp storage using the image's [blobName] as identifier.
     * The bucket is configured via env variable [bucketName].
     *
     * @throws ImageStorageException if the gcp sdk weren't able to delete the image
     */
    @Throws(ImageStorageException::class)
    fun deleteImage(blobName: String) {
        try {
            storage.delete(bucketName, blobName)
        } catch (e: StorageException) {
            throw ImageStorageException("Image with blobName $blobName could not be deleted", e)
        }
    }
}
