/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.auth.model

import io.lion5.smartcity.auth.model.Role.ADMIN
import io.lion5.smartcity.auth.model.Role.EVENT_STORE
import io.lion5.smartcity.auth.model.Role.SUPER_ADMIN
import io.lion5.smartcity.auth.model.Role.USER
import io.lion5.smartcity.auth.model.Role.VEHICLE
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority

/**
 * Central role of a user within the application.
 * The following roles exist, with ascending number of rights (lower roles extend the rights of the upper roles):
 * - [VEHICLE]
 * - [USER]
 * - [ADMIN]
 * - [SUPER_ADMIN]
 * - [EVENT_STORE]
 *
 * Please see [AuthorityRoles] for the compile-time constant string version of the roles.
 */
enum class Role {
    /**
     * Can only publish its location.
     */
    VEHICLE,

    /**
     * All rights of [VEHICLE], plus:
     * - can publish (damage) reports or issue commands
     * - can view all units of the current mission
     */
    USER,

    /**
     * All rights of [USER], plus:
     * - can create missions or units
     * - can assign units to a mission
     */
    ADMIN,

    /**
     * All rights of [ADMIN], plus:
     * - can create users with any role
     */
    SUPER_ADMIN,

    /**
     * Special role for that is used for authorizing methods that are invoked by one of the eventstore listeners.
     *
     * This role is not meant to be assigned to a user.
     * The role will be removed again in the future, once we refactor the whole MQTT auth process.
     */
    EVENT_STORE,
    ;

    fun toGrantedAuthority(): GrantedAuthority = SimpleGrantedAuthority(AUTHORITY_PREFIX + this.name)

    companion object {
        const val AUTHORITY_PREFIX = "ROLE_"

        fun fromName(roleName: String): Role? = values().find { it.name == roleName }
    }
}
