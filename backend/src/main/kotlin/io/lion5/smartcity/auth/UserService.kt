/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.auth

import com.google.auth.oauth2.GoogleCredentials
import com.google.common.collect.ImmutableList
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.auth.UserRecord
import com.google.firebase.auth.UserRecord.CreateRequest
import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.auth.FirebaseAuthFilter.Companion.CLAIM_ROLE
import io.lion5.smartcity.auth.FirebaseAuthFilter.Companion.CLAIM_UNIT_ID
import io.lion5.smartcity.auth.acl.AclPermissionService
import io.lion5.smartcity.auth.exception.UserCreationException
import io.lion5.smartcity.auth.model.Role
import io.lion5.smartcity.auth.model.User
import io.lion5.smartcity.mongodb.model.DepartmentUser
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.model.EmergencyUnitUser
import io.lion5.smartcity.mongodb.repository.DepartmentRepository
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import io.lion5.smartcity.mongodb.service.MissionService
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.security.acls.domain.BasePermission
import org.springframework.security.acls.model.Permission
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.io.IOException

@Service
class UserService(
    @Value("\${spring.cloud.gcp.projectId}") googleCloudProjectId: String,
    private val departmentRepository: DepartmentRepository,
    private val aclPermissionService: AclPermissionService,
    private val emergencyUnitService: EmergencyUnitService,
    private val missionService: MissionService,
) {
    private val log = KotlinLogging.logger {}
    private lateinit var firebaseApp: FirebaseApp
    private lateinit var firebaseAuth: FirebaseAuth

    init {
        try {
            firebaseApp = FirebaseApp.getInstance()
            firebaseAuth = FirebaseAuth.getInstance(firebaseApp)
        } catch (stateException: IllegalStateException) {
            log.info { "Could not get FirebaseApp instance, needs to be initialized first" }
            try {
                val options: FirebaseOptions =
                    FirebaseOptions
                        .builder()
                        .setProjectId(googleCloudProjectId)
                        .setCredentials(GoogleCredentials.getApplicationDefault())
                        .build()
                firebaseAuth = FirebaseAuth.getInstance(FirebaseApp.initializeApp(options))
            } catch (ioException: IOException) {
                log.error(ioException) { "Failed to initialize FirebaseAuth instance" }
            }
        }
    }

    fun createUser(user: User): User {
        if (firebaseUserAlreadyExists(user.email)) {
            log.warn { "User could not be created - User with email ${user.email} already exists." }
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "User with email ${user.email} already exists.")
        }

        return try {
            val request =
                CreateRequest()
                    .setEmail(user.email)
                    .setPassword(user.password)
                    .setDisplayName(user.displayName)
                    .setDisabled(false)
                    .setEmailVerified(false)

            val userRecord: UserRecord = firebaseAuth.createUser(request)
            log.debug { "${"Successfully created new user"} $userRecord" }

            user.unitId?.let { addUserToEmergencyUnit(it, userRecord) }
            user.departments.forEach { department -> addUserToDepartment(department.departmentId, user.firebaseId) }

            setCustomClaims(userRecord, user)
            // we have to retrieve the updated user from firebase to get the new claims
            val updatedUserRecord = firebaseAuth.getUser(userRecord.uid)
            log.debug { "${"Added custom claims to userRecord"} $userRecord" }
            fromUserRecord(updatedUserRecord)
        } catch (authException: FirebaseAuthException) {
            log.error(authException) { "User could not be created - Firebase exception:" }
            throw UserCreationException("User could not be created", authException)
        } catch (iae: IllegalArgumentException) {
            log.error(iae) { "User could not be created - no password provided:" }
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, iae.toString())
        }
    }

    private fun firebaseUserAlreadyExists(email: String): Boolean =
        try {
            firebaseAuth.getUserByEmail(email)
            true
        } catch (e: FirebaseAuthException) {
            // if no user with email is found the FirebaseAuthException is thrown
            false
        }

    private fun setCustomClaims(
        userRecord: UserRecord,
        user: User,
    ) {
        val claims: MutableMap<String, Any> = hashMapOf()
        val role = user.role
        // set user role and assigned unitId as custom claims
        claims[CLAIM_ROLE] = role.name
        if (user.unitId != null) claims[CLAIM_UNIT_ID] = user.unitId
        firebaseAuth.setCustomUserClaims(userRecord.uid, claims)
    }

    private fun fromUserRecord(userRecord: UserRecord): User =
        User(
            firebaseId = userRecord.uid,
            displayName = userRecord.displayName,
            email = userRecord.email,
            role = Role.fromName(userRecord.customClaims[CLAIM_ROLE] as? String ?: Role.USER.name) ?: Role.USER,
            // Don't expose password
            password = null,
            unitId = userRecord.customClaims[CLAIM_UNIT_ID] as? String ?: "",
        )

    /**
     * Add a user to a department.
     * This method also sets the custom claims for the user and adds READ ACL permission for the user to all missions
     * associated with the department.
     * @param departmentId the id of the department
     * @param userId the id of the user
     * @throws ResponseStatusException if the department does not exist
     * @throws FirebaseAuthException if the user does not exist
     */
    fun addUserToDepartment(
        departmentId: String,
        userId: String,
        role: DepartmentUser.DepartmentRole = DepartmentUser.DepartmentRole.USER,
    ) {
        var department =
            departmentRepository.findById(departmentId).orElse(null) ?: throw ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "Department with id $departmentId not found.",
            )

        val departmentUser = DepartmentUser(userId, role)
        if (department.users.any { it.userId == userId && it.role == role }) {
            log.debug { "User with id $userId and role $role already exists in department with id $departmentId" }
            return
        } else if (department.users.any { it.userId == userId }) {
            // if the user already exists in the department, but with a different role, remove the user first
            department = department.copy(users = department.users.filter { it.userId != userId })
        }

        val updatedUsers =
            ImmutableList
                .builder<DepartmentUser>()
                .addAll(department.users)
                .add(departmentUser)
                .build()
        val updatedDepartment = department.copy(users = updatedUsers)
        departmentRepository.save(updatedDepartment)

        // check if user exists in firebase
        val user =
            try {
                val userRecord = firebaseAuth.getUser(userId)
                fromUserRecord(userRecord)
            } catch (e: FirebaseAuthException) {
                throw ResponseStatusException(HttpStatus.NOT_FOUND, "User with id $userId not found.")
            }

        // Add READ permission for the user to all missions associated with the department
        val associatedMissions = missionService.findMissionsByDepartmentId(departmentId)
        associatedMissions.forEach { mission ->

            // Add READ permission for the user to the mission
            aclPermissionService.addPermissionForUser(mission, BasePermission.READ, user.firebaseId)
        }

        // add permission for the user to the department
        val departmentPermission: Permission =
            role.let {
                when (it) {
                    DepartmentUser.DepartmentRole.ADMIN -> BasePermission.ADMINISTRATION
                    DepartmentUser.DepartmentRole.USER -> BasePermission.READ
                }
            }
        aclPermissionService.addPermissionForUser(department, departmentPermission, user.firebaseId)
    }

    /**
     * Add a user by email to a department.
     */
    fun addUserByEmailToDepartment(
        departmentId: String,
        email: String,
        role: DepartmentUser.DepartmentRole = DepartmentUser.DepartmentRole.USER,
    ) {
        val userRecord =
            firebaseAuth.getUserByEmail(email)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "User with email $email not found.")
        addUserToDepartment(departmentId, userRecord.uid, role)
    }

    /**
     * Remove a user from a department.
     */
    fun removeUserFromDepartment(
        departmentId: String,
        userId: String,
    ) {
        val department =
            departmentRepository.findById(departmentId).orElse(null) ?: throw ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "Department with id $departmentId not found.",
            )
        if (!department.users.any { it.userId == userId }) return

        val updatedUsers =
            ImmutableList
                .builder<DepartmentUser>()
                .addAll(department.users.filter { it.userId != userId })
                .build()
        val updatedDepartment = department.copy(users = updatedUsers)
        departmentRepository.save(updatedDepartment)

        // check if user exists in firebase
        try {
            firebaseAuth.getUser(userId)
        } catch (e: FirebaseAuthException) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "User with id $userId not found.")
        }

        // Remove READ permission for the user from all missions associated with the department
        val associatedMissions = missionService.findMissionsByDepartmentId(departmentId)
        associatedMissions.forEach { mission ->
            // Remove READ permission for the user from the mission
            aclPermissionService.deletePermissionForUser(mission, BasePermission.READ, userId)
        }

        // remove permission for the user from the department
        aclPermissionService.deletePermissionForUser(department, BasePermission.READ, userId)
        aclPermissionService.deletePermissionForUser(department, BasePermission.ADMINISTRATION, userId)
    }

    /**
     * Remove a user by email to a department.
     */
    fun removeUserByEmailFromDepartment(
        departmentId: String,
        email: String,
    ) {
        val userRecord =
            firebaseAuth.getUserByEmail(email)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "User with email $email not found.")
        removeUserFromDepartment(departmentId, userRecord.uid)
    }

    /**
     * Add a user by email to an emergency unit.
     */
    fun addUserByEmailToEmergencyUnit(
        unitId: String,
        email: String,
    ) {
        val userRecord =
            firebaseAuth.getUserByEmail(email)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "User with email $email not found.")
        addUserToEmergencyUnit(unitId, userRecord)
    }

    /**
     * Add a user to an emergency unit.
     * Adds the user to the department associated with the emergency unit too.
     */
    fun addUserToEmergencyUnit(
        unitId: String,
        user: UserRecord,
    ) {
        val emergencyUnit: EmergencyUnit =
            emergencyUnitService.getEmergencyUnitById(unitId).orElse(null) ?: throw ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "Emergency unit with id $unitId not found.",
            )
        if (emergencyUnit.user?.userId == user.uid) {
            log.debug { "User with id $user already exists in emergency unit with id $unitId" }
            return
        }

        // check if user exists in firebase
        try {
            val userRecord = firebaseAuth.getUser(user.uid)
            val firebaseUser = fromUserRecord(userRecord)
            val updatedUser = firebaseUser.copy(unitId = unitId)
            setCustomClaims(userRecord, updatedUser)
        } catch (e: FirebaseAuthException) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "User with id $user not found.")
        }

        // remove user from previous unit
        val previousUnit = emergencyUnitService.findByUserId(user.uid)
        previousUnit?.let {
            val updatedPreviousUnit = previousUnit.copy(user = null)
            emergencyUnitService.createOrUpdateEmergencyUnit(updatedPreviousUnit)
        }

        val updatedUnit = emergencyUnit.copy(user = EmergencyUnitUser(user.uid, user.displayName, user.email))
        emergencyUnitService.createOrUpdateEmergencyUnit(updatedUnit)

        // add user to department
        if (emergencyUnit.department != null) {
            addUserToDepartment(emergencyUnit.department!!.departmentId!!, user.uid)
        }
    }

    /**
     * Remove a user from an emergency unit.
     * Does not remove the user from the department too as in the add method.
     */
    fun removeUserFromEmergencyUnit(
        unitId: String,
        userId: String,
    ) {
        val emergencyUnit: EmergencyUnit =
            emergencyUnitService.getEmergencyUnitById(unitId).orElse(null) ?: throw ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "Emergency unit with id $unitId not found.",
            )
        if (emergencyUnit.user?.userId != userId) {
            log.debug { "User with id $userId does not exist in emergency unit with id $unitId" }
            return
        }

        // check if user exists in firebase
        try {
            val userRecord = firebaseAuth.getUser(userId)
            val user = fromUserRecord(userRecord)
            val updatedUser = user.copy(unitId = null)
            setCustomClaims(userRecord, updatedUser)
        } catch (e: FirebaseAuthException) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "User with id $userId not found.")
        }

        val updatedUnit = emergencyUnit.copy(user = null)
        emergencyUnitService.createOrUpdateEmergencyUnit(updatedUnit)
    }

    /**
     * Remove a user by email from an emergency unit.
     */
    fun removeUserByEmailFromEmergencyUnit(
        unitId: String,
        email: String,
    ) {
        val userRecord =
            firebaseAuth.getUserByEmail(email)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "User with email $email not found.")
        removeUserFromEmergencyUnit(unitId, userRecord.uid)
    }

    /**
     * Check if user is anonymous
     */
    fun isAnonymousUser(userId: String): Boolean {
        val user = firebaseAuth.getUser(userId)
        return user.providerData.isEmpty()
    }

    /**
     * Get all users from firebase
     **/
    fun getAllFirebaseUsers(): List<EmergencyUnitUser> {
        val firebaseUsers = mutableListOf<EmergencyUnitUser>()
        var page = FirebaseAuth.getInstance().listUsers(null)
        while (page != null) {
            for (user in page.values) {
                if (user.displayName != null) {
                    firebaseUsers.add(EmergencyUnitUser(user.uid, user.displayName, user.email))
                }
            }
            page = page.nextPage
        }
        return firebaseUsers.toList()
    }
}
