/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.auth

import io.lion5.smartcity.auth.dto.CreateUserRequest
import io.lion5.smartcity.auth.exception.UserCreationException
import io.lion5.smartcity.auth.model.AddUserToDepartmentRequest
import io.lion5.smartcity.auth.model.AddUserToEmergencyUnitRequest
import io.lion5.smartcity.auth.model.AuthorityRoles
import io.lion5.smartcity.auth.model.DeleteUserFromDepartmentRequest
import io.lion5.smartcity.auth.model.DeleteUserFromEmergencyUnitRequest
import io.lion5.smartcity.auth.model.FirebasePrincipal
import io.lion5.smartcity.auth.model.User
import io.lion5.smartcity.mongodb.model.AuditLogEntry
import io.lion5.smartcity.mongodb.model.EmergencyUnitUser
import io.lion5.smartcity.mongodb.service.AuditService
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import jakarta.validation.Valid
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal
import java.time.Instant

@RestController
@RequestMapping("users")
class UserController(
    val userService: UserService,
    private val auditService: AuditService,
) {
    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasRole('${AuthorityRoles.SUPER_ADMIN}')")
    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun createUser(
        @RequestBody createUserRequest: CreateUserRequest,
        @AuthenticationPrincipal principal: Principal,
    ): ResponseEntity<Any> =
        try {
            if (principal is FirebasePrincipal) {
                auditService.addNewLogEntry(
                    AuditLogEntry(
                        userMail = principal.name,
                        timestamp = Instant.now(),
                        operationPerformed = "Created User ${createUserRequest.email} with ROLE ${createUserRequest.role}",
                        entryId = null,
                    ),
                )
            }
            ResponseEntity.ok(userService.createUser(fromCreateUserRequest(createUserRequest)))
        } catch (e: UserCreationException) {
            ResponseEntity.badRequest().body(e)
        }

    private fun fromCreateUserRequest(request: CreateUserRequest): User =
        User(
            displayName = request.displayName,
            email = request.email,
            role = request.role,
            password = request.password,
            unitId = request.unitId,
            departments = request.departments,
        )

    /**
     * Adds a user as a member to a department.
     */
    @SecurityRequirement(name = "Bearer Authentication")
    @PutMapping("/departments", consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize(
        """hasAnyRole('${AuthorityRoles.ADMIN}','${AuthorityRoles.SUPER_ADMIN}')
        or hasPermission(#request.departmentId, 'ADMINISTRATION')""",
    )
    fun addUserToDepartment(
        @RequestBody request: AddUserToDepartmentRequest,
        @AuthenticationPrincipal principal: Principal,
    ): ResponseEntity<Any> =
        try {
            ResponseEntity.ok(
                userService.addUserByEmailToDepartment(email = request.email, departmentId = request.departmentId, role = request.role),
            )
        } catch (e: Exception) {
            ResponseEntity.badRequest().body(e)
        }

    /**
     * Removes a user as a member from a department.
     */
    @SecurityRequirement(name = "Bearer Authentication")
    @DeleteMapping("/departments", consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize(
        """hasAnyRole('${AuthorityRoles.ADMIN}','${AuthorityRoles.SUPER_ADMIN}')
        or hasPermission(#request.departmentId, 'ADMINISTRATION')""",
    )
    fun removeUserFromDepartment(
        @RequestBody request: DeleteUserFromDepartmentRequest,
        @AuthenticationPrincipal principal: Principal,
    ): ResponseEntity<Any> =
        try {
            ResponseEntity.ok(userService.removeUserByEmailFromDepartment(email = request.email, departmentId = request.departmentId))
        } catch (e: Exception) {
            ResponseEntity.badRequest().body(e)
        }

    /**
     * Add user to an emergency unit.
     */
    @SecurityRequirement(name = "Bearer Authentication")
    @PutMapping("/unit", consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize("hasAnyRole('${AuthorityRoles.ADMIN}','${AuthorityRoles.SUPER_ADMIN}')")
    fun addUserToEmergencyUnit(
        @Valid @RequestBody request: AddUserToEmergencyUnitRequest,
        @AuthenticationPrincipal principal: Principal,
    ): ResponseEntity<Any> =
        try {
            ResponseEntity.ok(userService.addUserByEmailToEmergencyUnit(email = request.email, unitId = request.unitId))
        } catch (e: Exception) {
            ResponseEntity.badRequest().body(e)
        }

    /**
     * Remove user from an emergency unit.
     */
    @SecurityRequirement(name = "Bearer Authentication")
    @DeleteMapping("/unit", consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize("hasAnyRole('${AuthorityRoles.ADMIN}','${AuthorityRoles.SUPER_ADMIN}')")
    fun removeUserFromEmergencyUnit(
        @Valid @RequestBody request: DeleteUserFromEmergencyUnitRequest,
        @AuthenticationPrincipal principal: Principal,
    ): ResponseEntity<Any> =
        try {
            ResponseEntity.ok(userService.removeUserByEmailFromEmergencyUnit(email = request.email, unitId = request.unitId))
        } catch (e: Exception) {
            ResponseEntity.badRequest().body(e)
        }

    @PreAuthorize("hasAnyRole('${AuthorityRoles.ADMIN}','${AuthorityRoles.SUPER_ADMIN}')")
    @GetMapping("/firebase", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getAllFirebaseUsers(
        @AuthenticationPrincipal principal: Principal,
    ): ResponseEntity<List<EmergencyUnitUser>> = ResponseEntity.ok(userService.getAllFirebaseUsers())
}
