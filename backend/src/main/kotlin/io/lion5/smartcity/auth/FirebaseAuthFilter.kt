/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.auth

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.auth.FirebaseToken
import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.auth.model.FirebasePrincipal
import io.lion5.smartcity.auth.model.Role
import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException

@Component
class FirebaseAuthFilter(
    @Value("\${spring.cloud.gcp.projectId}") googleCloudProjectId: String,
) : OncePerRequestFilter() {
    companion object {
        const val CLAIM_ROLE = "role"
        const val CLAIM_UNIT_ID = "unitId"
        const val BEARER_PREFIX = "Bearer "
    }

    private val log = KotlinLogging.logger {}
    private lateinit var firebaseApp: FirebaseApp
    private lateinit var firebaseAuth: FirebaseAuth

    init {
        try {
            // check if Firebase instance is already initialized
            firebaseApp = FirebaseApp.getInstance()
            firebaseAuth = FirebaseAuth.getInstance(firebaseApp)
        } catch (stateException: IllegalStateException) {
            log.info { "Could not get FirebaseApp instance, needs to be initialized first" }
            try {
                val options: FirebaseOptions =
                    FirebaseOptions
                        .builder()
                        .setProjectId(googleCloudProjectId)
                        .setCredentials(GoogleCredentials.getApplicationDefault())
                        .build()
                firebaseAuth = FirebaseAuth.getInstance(FirebaseApp.initializeApp(options))
            } catch (ioException: IOException) {
                log.error(ioException) { "Failed to initialize FirebaseAuth instance" }
            }
        }
    }

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain,
    ) {
        authorizeToken(request)
        filterChain.doFilter(request, response)
    }

    fun authorizeToken(request: HttpServletRequest) {
        log.debug { "Authorizing token for request $request" }
        val token = getBearerToken(request)
        if (token == null) {
            // suppress the log for the actuator path
            if (!request.requestURI.startsWith("/actuator")) {
                log.trace { "No token received, handing over to next filter" }
            }
            return
        }

        try {
            val idToken: FirebaseToken = firebaseAuth.verifyIdToken(token)
            val roleSet: Set<GrantedAuthority> = getRoleAsGrantedAuthoritySet(idToken)

            if (roleSet.isEmpty()) {
                idToken.email?.let {
                    log.warn { "No role associated with token, request blocked" }
                    return
                } // log warning and return only if not an anonymous user
            }

            val authentication =
                UsernamePasswordAuthenticationToken(
                    FirebasePrincipal(idToken.uid),
                    idToken,
                    roleSet,
                )
            authentication.details = WebAuthenticationDetailsSource().buildDetails(request)
            SecurityContextHolder.getContext().authentication = authentication
        } catch (e: FirebaseAuthException) {
            log.error(e) { "Failed to decode firebase token." }
        }
    }

    private fun getRoleAsGrantedAuthoritySet(token: FirebaseToken): Set<GrantedAuthority> {
        if (!token.claims.containsKey(CLAIM_ROLE)) {
            log.warn { "Firebase token does not include '$CLAIM_ROLE' claim, so no role was found" }
            return emptySet()
        }

        val roleName = token.claims[CLAIM_ROLE]

        if (roleName !is String) {
            log.warn { "Firebase token includes non-string role '$roleName'" }
            return emptySet()
        }

        val role = Role.fromName(roleName)
        if (role == null) {
            log.warn { "Firebase token includes unknown role '$roleName'" }
            return emptySet()
        }

        return setOf(role.toGrantedAuthority())
    }

    private fun getBearerToken(request: HttpServletRequest): String? {
        val authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION)
        if (authorizationHeader.isNullOrBlank()) {
            log.debug { "Authorization header is empty." }
            return null
        }
        if (!authorizationHeader.startsWith(BEARER_PREFIX)) {
            log.debug { "Authorization header has invalid scheme: $authorizationHeader" }
            return null
        }
        return authorizationHeader.substring(BEARER_PREFIX.length)
    }
}
