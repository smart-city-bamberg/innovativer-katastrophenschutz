/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.auth.acl

import io.lion5.smartcity.mongodb.model.IAclDocument
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.acls.domain.GrantedAuthoritySid
import org.springframework.security.acls.domain.ObjectIdentityImpl
import org.springframework.security.acls.domain.PrincipalSid
import org.springframework.security.acls.model.MutableAcl
import org.springframework.security.acls.model.NotFoundException
import org.springframework.security.acls.model.ObjectIdentity
import org.springframework.security.acls.model.Permission
import org.springframework.security.acls.model.Sid
import org.springframework.security.acls.mongodb.MongoDBMutableAclService
import org.springframework.stereotype.Service
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.TransactionStatus
import org.springframework.transaction.annotation.Transactional
import org.springframework.transaction.support.TransactionCallbackWithoutResult
import org.springframework.transaction.support.TransactionTemplate

@Service
@Transactional
class AclPermissionService {
    @Autowired
    private val aclService: MongoDBMutableAclService? = null

    @Autowired
    private val transactionManager: PlatformTransactionManager? = null

    fun addPermissionForUser(
        targetObj: IAclDocument,
        permission: Permission,
        username: String?,
    ) {
        val sid: Sid = PrincipalSid(username)
        addPermissionForSid(targetObj, permission, sid)
    }

    fun addPermissionForMultipleUsers(
        targetObj: IAclDocument,
        permission: Permission,
        usernames: List<String>,
    ) {
        val sids = usernames.map { PrincipalSid(it) }
        addPermissionsForMultipleSids(targetObj, permission, sids)
    }

    fun addPermissionForAuthority(
        targetObj: IAclDocument,
        permission: Permission,
        authority: String?,
    ) {
        val sid: Sid = GrantedAuthoritySid(authority)
        addPermissionForSid(targetObj, permission, sid)
    }

    private fun addPermissionForSid(
        targetObj: IAclDocument,
        permission: Permission,
        sid: Sid,
    ) {
        addPermissionsForMultipleSids(targetObj, permission, listOf(sid))
    }

    fun addPermissionsForMultipleSids(
        targetObj: IAclDocument,
        permission: Permission,
        sids: List<Sid>,
    ) {
        val tt = TransactionTemplate(transactionManager!!)
        tt.execute(
            object : TransactionCallbackWithoutResult() {
                override fun doInTransactionWithoutResult(status: TransactionStatus) {
                    val oi: ObjectIdentity = ObjectIdentityImpl(targetObj::class.java, targetObj.getId())
                    val acl: MutableAcl? =
                        try {
                            aclService?.readAclById(oi) as MutableAcl
                        } catch (nfe: NotFoundException) {
                            aclService?.createAcl(oi)
                        }

                    sids.forEach { sid ->
                        acl!!.insertAce(acl.entries.size, permission, sid, true)
                    }
                    aclService?.updateAcl(acl!!)
                }
            },
        )
    }

    fun deletePermissionForUser(
        targetObj: IAclDocument,
        permission: Permission,
        username: String?,
    ) {
        val sid: Sid = PrincipalSid(username)
        deletePermissionForSid(targetObj, permission, sid)
    }

    public fun deletePermissionForMultipleUsers(
        targetObj: IAclDocument,
        permission: Permission,
        usernames: List<String>,
    ) {
        val sids = usernames.map { PrincipalSid(it) }
        deletePermissionsForMultipleSids(targetObj, permission, sids)
    }

    fun deletePermissionForAuthority(
        targetObj: IAclDocument,
        permission: Permission,
        authority: String?,
    ) {
        val sid: Sid = GrantedAuthoritySid(authority)
        deletePermissionForSid(targetObj, permission, sid)
    }

    private fun deletePermissionForSid(
        targetObj: IAclDocument,
        permission: Permission,
        sid: Sid,
    ) {
        deletePermissionsForMultipleSids(targetObj, permission, listOf(sid))
    }

    public fun deletePermissionsForMultipleSids(
        targetObj: IAclDocument,
        permission: Permission,
        sids: List<Sid>,
    ) {
        val tt = TransactionTemplate(transactionManager!!)
        tt.execute(
            object : TransactionCallbackWithoutResult() {
                override fun doInTransactionWithoutResult(status: TransactionStatus) {
                    val oi: ObjectIdentity = ObjectIdentityImpl(targetObj::class.java, targetObj.getId())
                    val acl: MutableAcl? =
                        try {
                            aclService!!.readAclById(oi) as MutableAcl
                        } catch (nfe: NotFoundException) {
                            aclService!!.createAcl(oi)
                        }

                    sids.forEach { sid ->
                        // Find the correct ACE to delete
                        val it = acl!!.entries.iterator()
                        var index = 0
                        var found = false
                        while (it.hasNext()) {
                            val entry = it.next()
                            if (entry.sid == sid && entry.permission == permission) {
                                found = true
                                break
                            }
                            index++
                        }

                        if (found) {
                            acl.deleteAce(index)
                        }
                    }
                    aclService!!.updateAcl(acl!!)
                }
            },
        )
    }
}
