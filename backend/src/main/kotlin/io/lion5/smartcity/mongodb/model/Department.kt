/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Department(
    @Id
    @Indexed(unique = true)
    val departmentId: String?,
    @Indexed(unique = true)
    val name: String,
    @Indexed(unique = true)
    val slug: String,
    val state: DepartmentState = DepartmentState.PERMANENT,
    val users: List<DepartmentUser>,
) : IAclDocument {
    enum class DepartmentState {
        PERMANENT,
        TEMPORARY,
        TEMPORARY_CLOSED,
    }

    override fun getId(): String? = departmentId
}
