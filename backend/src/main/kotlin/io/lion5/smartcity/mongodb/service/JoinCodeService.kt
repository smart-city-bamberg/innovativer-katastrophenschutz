/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.service

import org.springframework.stereotype.Service
import java.util.Random

@Service
class JoinCodeService {
    private val random = Random()

    fun generateJoinCode(): String = generateRandomAlphanumeric(4) + "-" + generateRandomAlphanumeric(4)

    fun generateJoinPin(): String = (100000 + random.nextInt(900000)).toString()

    private fun generateRandomAlphanumeric(length: Int): String {
        val characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return (1..length)
            .map { characters[random.nextInt(characters.length)] }
            .joinToString("")
    }
}
