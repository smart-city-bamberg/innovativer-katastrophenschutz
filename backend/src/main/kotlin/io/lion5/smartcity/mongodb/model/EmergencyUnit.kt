/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.model

import com.fasterxml.jackson.annotation.JsonProperty
import io.lion5.smartcity.mongodb.model.location.GPSLocation
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.DocumentReference
import org.springframework.format.annotation.DateTimeFormat
import java.time.Instant

@Document(collection = "unit")
data class EmergencyUnit(
    @Id
    @Indexed(unique = true)
    val unitId: String?,
    val unitType: String? = "",
    val callSign: String = "",
    val tacticalSignPath: String,
    var gpsLocation: GPSLocation = GPSLocation(),
    /**
     * When was the last location update published from this unit
     */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    var lastLocationUpdate: Instant = Instant.EPOCH,
    @JsonProperty("etag")
    var version: Long = 0,
    @DocumentReference(lazy = false)
    var department: Department? = null,
    var user: EmergencyUnitUser? = null,
    var creationMethod: CreationMethod = CreationMethod.PERMANENT,
) {
    enum class CreationMethod {
        PERMANENT,
        TEMPORARY,
        TEMPORARY_DISABLED,
    }
}
