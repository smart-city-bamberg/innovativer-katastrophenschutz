/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.service

import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.eventstore.events.model.LocationUpdateEvent
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.model.location.GPSLocation
import io.lion5.smartcity.mongodb.repository.DepartmentRepository
import io.lion5.smartcity.mongodb.repository.EmergencyUnitRepository
import io.lion5.smartcity.util.ApplicationEventEnvelope
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.event.EventListener
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.rest.core.event.AfterCreateEvent
import org.springframework.data.rest.core.event.BeforeCreateEvent
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.util.Optional

@Service
class EmergencyUnitService(
    private val emergencyUnitRepository: EmergencyUnitRepository,
    private val departmentRepository: DepartmentRepository,
    @Value("\${digimap.eventstore.streamPrefixes.mission}") private val missionStreamPrefix: String = "default/missions/",
    private val applicationEventPublisher: ApplicationEventPublisher,
) {
    private val log = KotlinLogging.logger {}

    @EventListener
    fun projectLocationUpdateEventToMongoDb(envelope: ApplicationEventEnvelope<LocationUpdateEvent>) {
        // read unit and update location
        val event = envelope.data
        val emergencyUnit = emergencyUnitRepository.findById(event.senderUnitId).get()

        // check if our location event is newer than the already stored location and update it
        if (event.timestamp.isAfter(emergencyUnit.lastLocationUpdate)) {
            log.debug { "Got LocationUpdateEvent to write to MongoDB" }
            val updatedUnit =
                EmergencyUnit(
                    emergencyUnit.unitId,
                    emergencyUnit.unitType,
                    emergencyUnit.callSign,
                    emergencyUnit.tacticalSignPath,
                    GPSLocation(event.location.latitude, event.location.longitude),
                    event.timestamp,
                    event.eTag,
                    emergencyUnit.department,
                    emergencyUnit.user,
                )
            emergencyUnitRepository.save(updatedUnit)
        }
    }

    fun createEmergencyUnitWithUserDepartment(
        emergencyUnit: EmergencyUnit,
        userFirebaseId: String,
    ): EmergencyUnit {
        if (emergencyUnit.department == null) {
            departmentRepository.findFirstByUsersUserId(userFirebaseId)?.let { userDepartment ->
                emergencyUnit.department = userDepartment
            } ?: throw ResponseStatusException(
                org.springframework.http.HttpStatus.BAD_REQUEST,
                "No Department assigned for the Unit and creating user not part of any Department.",
            )
        }

        // ⚠️ these events are part of Spring Data Rest. We need to publish them manually because we overwrote the POST method.
        applicationEventPublisher.publishEvent(BeforeCreateEvent(emergencyUnit))
        val savedUnit = emergencyUnitRepository.save(emergencyUnit)
        applicationEventPublisher.publishEvent(AfterCreateEvent(savedUnit))
        return savedUnit
    }

    fun createOrUpdateEmergencyUnit(emergencyUnit: EmergencyUnit): EmergencyUnit = emergencyUnitRepository.save(emergencyUnit)

    fun getEmergencyUnitById(unitId: String): Optional<EmergencyUnit> = emergencyUnitRepository.findById(unitId)

    fun getEmergencyUnitsById(unitIds: List<String>): List<EmergencyUnit> {
        val units: MutableList<EmergencyUnit> = emptyList<EmergencyUnit>().toMutableList()
        unitIds.forEach {
            val optional = emergencyUnitRepository.findById(it)
            if (optional.isPresent) units.add(optional.get())
        }
        return units.toList()
    }

    private fun extractMissionIdFromStreamId(stream: String): String {
        log.debug { "Extracting missionID from $stream" }
        return stream.removePrefix(missionStreamPrefix)
    }

    fun findByCallSign(r: String): EmergencyUnit? = emergencyUnitRepository.findByCallSign(r)

    fun findByUserId(userId: String): EmergencyUnit? = emergencyUnitRepository.findByUserUserId(userId)

    fun findAllEmergencyUnits(pageable: Pageable): Page<EmergencyUnit> = emergencyUnitRepository.findAll(pageable)
}
