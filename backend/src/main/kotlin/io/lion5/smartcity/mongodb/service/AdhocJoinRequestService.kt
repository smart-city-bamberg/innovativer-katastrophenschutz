/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.service

import io.lion5.smartcity.auth.UserService
import io.lion5.smartcity.auth.acl.AclPermissionService
import io.lion5.smartcity.http.dto.AdhocJoinRequestCreateDto
import io.lion5.smartcity.http.dto.JoinRequestStatusResponseDto
import io.lion5.smartcity.http.dto.MissionJoinResponseDto
import io.lion5.smartcity.mongodb.model.AdhocJoinRequest
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.model.EmergencyUnitUser
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.repository.AdhocJoinRequestRepository
import io.lion5.smartcity.mongodb.repository.MissionRepository
import io.lion5.smartcity.mqtt.MqttGateway
import io.lion5.smartcity.mqtt.payload.outbound.OutboundAdhocJoinRequestPayload.Companion.toOutboundPayload
import org.bson.types.ObjectId
import org.springframework.context.ApplicationEventPublisher
import org.springframework.data.rest.core.event.AfterLinkSaveEvent
import org.springframework.data.rest.core.event.BeforeLinkSaveEvent
import org.springframework.http.HttpStatus
import org.springframework.security.acls.domain.BasePermission
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class AdhocJoinRequestService(
    private val missionRepository: MissionRepository,
    private val adhocJoinRequestRepository: AdhocJoinRequestRepository,
    private val mqttGateway: MqttGateway,
    private val aclPermissionService: AclPermissionService,
    private val emergencyUnitService: EmergencyUnitService,
    private val userService: UserService,
    private val applicationEventPublisher: ApplicationEventPublisher,
) {
    fun validateJoinCode(joinCode: String): MissionJoinResponseDto {
        val mission = missionRepository.findMissionByJoinCodeAndState(joinCode, Mission.State.RUNNING)
        mission?.let {
            return MissionJoinResponseDto(mission.missionNr)
        }
        throw ResponseStatusException(HttpStatus.NOT_FOUND, "Mission not found")
    }

    fun validateJoinPin(
        joinCode: String,
        joinPin: String,
    ): MissionJoinResponseDto {
        val mission = missionRepository.findMissionByJoinCodeAndState(joinCode, Mission.State.RUNNING)
        mission?.let {
            if (mission.joinPin == joinPin) {
                return MissionJoinResponseDto(mission.missionNr)
            } else {
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid join pin")
            }
        }
        throw ResponseStatusException(HttpStatus.NOT_FOUND, "Mission not found")
    }

    /**
     * Create an adhoc join request for a mission.
     * The request will be pending until it is approved or rejected.
     * If the join code or join pin is invalid, a [ResponseStatusException] will be thrown.
     * If the mission is not found, a [ResponseStatusException] will be thrown.
     * The adhoc join request will be published to the mqtt broker.
     *
     * @param userFirebaseId the firebase id of the user
     * @param adhocJoinRequestCreateDto the adhoc join request data
     * @return the created adhoc join request
     */
    fun createAdhocJoinRequest(
        userFirebaseId: String,
        adhocJoinRequestCreateDto: AdhocJoinRequestCreateDto,
    ): AdhocJoinRequest {
        if (!userService.isAnonymousUser(userFirebaseId)) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "User must be anonymous")
        }

        val mission =
            missionRepository.findMissionByJoinCodeAndState(adhocJoinRequestCreateDto.joinCode, Mission.State.RUNNING)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Mission not found")

        if (mission.joinPin != adhocJoinRequestCreateDto.joinPin) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid join pin")
        }

        adhocJoinRequestRepository
            .findFirstByUserFirebaseIdAndStatus(userFirebaseId, AdhocJoinRequest.AdhocJoinRequestStatus.PENDING)
            ?.let {
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "User already has a pending adhoc join request")
            }

        val adhocJoinRequest =
            AdhocJoinRequest(
                adhocJoinRequestId = null,
                userFirebaseId = userFirebaseId,
                userName = adhocJoinRequestCreateDto.userName,
                unitCallSign = adhocJoinRequestCreateDto.unitCallSign,
                unitTacticalSignPath = adhocJoinRequestCreateDto.unitTacticalSignPath,
                departmentName = adhocJoinRequestCreateDto.departmentName,
                mission = mission,
                status = AdhocJoinRequest.AdhocJoinRequestStatus.PENDING,
            )

        val savedAdhocJoinRequest = adhocJoinRequestRepository.save(adhocJoinRequest)

        // publish the adhoc join request. Frontend should check status to show it in waiting list
        mqttGateway.publishAdhocJoinRequest(savedAdhocJoinRequest.toOutboundPayload(), savedAdhocJoinRequest.mission.missionId!!)
        return savedAdhocJoinRequest
    }

    /**
     * Approve an adhoc join request.
     * The request must be in pending state.
     * The user will be granted read permission for the mission.
     * The updated adhoc join request will be published to the mqtt broker.
     */
    fun approveAdhocJoinRequest(adhocJoinRequestId: String): AdhocJoinRequest {
        val adhocJoinRequest =
            adhocJoinRequestRepository
                .findById(adhocJoinRequestId)
                .orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND, "Adhoc join request not found") }

        if (adhocJoinRequest.status != AdhocJoinRequest.AdhocJoinRequestStatus.PENDING) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Adhoc join request is already approved or rejected")
        }

        // create a temporary emergency unit for the user
        val temporaryEmergencyUnit =
            EmergencyUnit(
                unitId = null,
                unitType = null,
                callSign = adhocJoinRequest.unitCallSign,
                tacticalSignPath = adhocJoinRequest.unitTacticalSignPath,
                user = EmergencyUnitUser(adhocJoinRequest.userFirebaseId),
                creationMethod = EmergencyUnit.CreationMethod.TEMPORARY,
            )
        val savedTemporaryEmergencyUnit = emergencyUnitService.createOrUpdateEmergencyUnit(temporaryEmergencyUnit)

        // update the adhoc join request
        val updatedAdhocJoinRequest =
            adhocJoinRequest.copy(
                status = AdhocJoinRequest.AdhocJoinRequestStatus.APPROVED,
                createdUnit = savedTemporaryEmergencyUnit,
                approvalTime = java.util.Date(),
            )
        val savedAdhocJoinRequest = adhocJoinRequestRepository.save(updatedAdhocJoinRequest)

        aclPermissionService.addPermissionForUser(adhocJoinRequest.mission, BasePermission.READ, adhocJoinRequest.userFirebaseId)

        // publish the updated adhoc join request. Frontend should check status to remove it from waiting list
        mqttGateway.publishAdhocJoinRequest(savedAdhocJoinRequest.toOutboundPayload(), savedAdhocJoinRequest.mission.missionId!!)
        // publish the unit change event
        applicationEventPublisher.publishEvent(BeforeLinkSaveEvent(savedAdhocJoinRequest.mission, listOf(savedTemporaryEmergencyUnit)))
        // add the temporary emergency unit to the mission
        val mission = savedAdhocJoinRequest.mission.copy(units = savedAdhocJoinRequest.mission.units + savedTemporaryEmergencyUnit)
        val savedMission = missionRepository.save(mission)
        applicationEventPublisher.publishEvent(AfterLinkSaveEvent(savedMission, listOf(savedTemporaryEmergencyUnit)))

        return savedAdhocJoinRequest
    }

    /**
     * Reject an adhoc join request.
     * The request must be in pending state.
     * The updated adhoc join request will be published to the mqtt broker.
     */
    fun rejectAdhocJoinRequest(adhocJoinRequestId: String): AdhocJoinRequest {
        val adhocJoinRequest =
            adhocJoinRequestRepository
                .findById(adhocJoinRequestId)
                .orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND, "Adhoc join request not found") }

        if (adhocJoinRequest.status != AdhocJoinRequest.AdhocJoinRequestStatus.PENDING) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Adhoc join request is already approved, rejected or canceled")
        }

        val updatedAdhocJoinRequest = adhocJoinRequest.copy(status = AdhocJoinRequest.AdhocJoinRequestStatus.REJECTED)
        val savedAdhocJoinRequest = adhocJoinRequestRepository.save(updatedAdhocJoinRequest)

        // publish the updated adhoc join request. Frontend should check status to remove it from waiting list
        mqttGateway.publishAdhocJoinRequest(savedAdhocJoinRequest.toOutboundPayload(), savedAdhocJoinRequest.mission.missionId!!)
        return savedAdhocJoinRequest
    }

    fun cancelAdhocJoinRequest(
        adhocJoinRequestId: String,
        userFirebaseId: String,
    ): AdhocJoinRequest {
        if (!userService.isAnonymousUser(userFirebaseId)) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "User must be anonymous")
        }

        val adhocJoinRequest =
            adhocJoinRequestRepository
                .findById(adhocJoinRequestId)
                .orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND, "Adhoc join request not found") }

        if (adhocJoinRequest.status != AdhocJoinRequest.AdhocJoinRequestStatus.PENDING) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Adhoc join request is already approved, rejected or canceled")
        }

        if (adhocJoinRequest.userFirebaseId != userFirebaseId) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "User is not allowed to cancel this adhoc join request")
        }

        val updatedAdhocJoinRequest = adhocJoinRequest.copy(status = AdhocJoinRequest.AdhocJoinRequestStatus.CANCELED)
        val savedAdhocJoinRequest = adhocJoinRequestRepository.save(updatedAdhocJoinRequest)

        // publish the updated adhoc join request. Frontend should check status to remove it from waiting list
        mqttGateway.publishAdhocJoinRequest(savedAdhocJoinRequest.toOutboundPayload(), savedAdhocJoinRequest.mission.missionId!!)
        return savedAdhocJoinRequest
    }

    fun findAllPendingAdhocJoinRequests(): List<AdhocJoinRequest> =
        adhocJoinRequestRepository.findAllByStatus(AdhocJoinRequest.AdhocJoinRequestStatus.PENDING)

    fun findAllPendingAdhocJoinRequestsByMission(missionId: String): List<AdhocJoinRequest> {
        val mission =
            try {
                missionRepository.findMissionByMissionId(ObjectId(missionId))
                    ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Mission not found")
            } catch (e: Exception) {
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid mission id")
            }
        return adhocJoinRequestRepository.findAllByMissionAndStatus(mission, AdhocJoinRequest.AdhocJoinRequestStatus.PENDING)
    }

    fun getWaitingRoomStatus(userFirebaseId: String): JoinRequestStatusResponseDto {
        if (!userService.isAnonymousUser(userFirebaseId)) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "User must be anonymous")
        }

        val adhocJoinRequest = adhocJoinRequestRepository.findFirstByUserFirebaseIdOrderByCreationTimeDesc(userFirebaseId)
        return JoinRequestStatusResponseDto(
            adhocJoinRequest?.mission?.missionId,
            adhocJoinRequest?.adhocJoinRequestId,
            adhocJoinRequest?.status ?: AdhocJoinRequest.AdhocJoinRequestStatus.NOT_FOUND,
        )
    }
}
