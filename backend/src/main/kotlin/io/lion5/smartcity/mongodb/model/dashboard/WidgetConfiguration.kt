/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.model.dashboard

/**
 * A widget configuration for a dashboard
 * @param id the id of the widget
 * @param position the grid configuration of the widget
 * @param componentId the type of the widget
 */
data class WidgetConfiguration(
    val id: String,
    val position: GridWidget,
    val componentId: String,
    val settings: Map<String, Any>? = null,
)
