/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.repository

import io.lion5.smartcity.auth.model.AuthorityRoles
import io.lion5.smartcity.mongodb.model.Mission
import org.bson.types.ObjectId
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource
import org.springframework.security.access.prepost.PostAuthorize
import org.springframework.security.access.prepost.PostFilter
import org.springframework.web.bind.annotation.CrossOrigin
import java.util.Optional

@CrossOrigin
@RepositoryRestResource
interface MissionRepository : MongoRepository<Mission, String> {
    @RestResource(exported = false)
    override fun findAll(pageable: Pageable): Page<Mission>

    @PostAuthorize(
        """returnObject.isPresent() ? hasPermission(returnObject.get(), 'READ')
            or hasPermission(returnObject.get(), 'ADMINISTRATION')
            or hasAnyRole('${AuthorityRoles.SUPER_ADMIN}', '${AuthorityRoles.ADMIN}') : false""",
    )
    override fun findById(id: String): Optional<Mission>

    /**
     * Find missions by their state. e.g. get all running missions
     */
    @PostFilter(
        """hasPermission(filterObject, 'READ') or hasPermission(filterObject, 'ADMINISTRATION')
            or hasAnyRole('${AuthorityRoles.SUPER_ADMIN}', '${AuthorityRoles.ADMIN}')""",
    )
    fun findMissionsByState(stat: Mission.State): List<Mission>

    /**
     *  Find missions with the specified state where the specified unit is associated with.
     */
    @PostFilter(
        """hasPermission(filterObject, 'READ') or hasPermission(filterObject, 'ADMINISTRATION')
            or hasAnyRole('${AuthorityRoles.SUPER_ADMIN}', '${AuthorityRoles.ADMIN}', '${AuthorityRoles.EVENT_STORE}')""",
    )
    fun findMissionsByStateAndUnitsEquals(
        stat: Mission.State,
        unitId: ObjectId,
    ): List<Mission>

    /**
     * Retrieve a mission that has at least one unit with the given unitId. The unit needs to be assigned to the mission.
     * NOTE: this only works if the unitId was stored as an auto-generated ObjectId (e.g. 64107c0b2315d81978ace2d9),
     * not as a custom String.
     */
    @PostAuthorize(
        """hasPermission(returnObject, 'READ') or hasPermission(returnObject, 'ADMINISTRATION')
            or hasAnyRole('${AuthorityRoles.SUPER_ADMIN}', '${AuthorityRoles.ADMIN}')""",
    )
    fun findMissionByUnitsEquals(unitId: ObjectId): Mission?

    @PostAuthorize(
        """hasPermission(returnObject, 'READ') or hasPermission(returnObject, 'ADMINISTRATION')
            or hasAnyRole('${AuthorityRoles.SUPER_ADMIN}', '${AuthorityRoles.ADMIN}', '${AuthorityRoles.EVENT_STORE}')""",
    )
    fun findMissionByMissionId(missionId: ObjectId): Mission?

    fun findMissionByJoinCodeAndState(
        joinCode: String,
        state: Mission.State,
    ): Mission?
}
