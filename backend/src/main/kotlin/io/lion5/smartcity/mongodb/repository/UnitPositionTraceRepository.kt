package io.lion5.smartcity.mongodb.repository

import io.lion5.smartcity.mongodb.model.UnitPositionTrace
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.CrossOrigin

@CrossOrigin
@Repository
interface UnitPositionTraceRepository : MongoRepository<UnitPositionTrace, String> {
    fun findByUnitIdAndMissionId(
        unitId: String,
        missionId: String,
    ): UnitPositionTrace
}
