/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.handler

import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.auth.model.FirebasePrincipal
import io.lion5.smartcity.mongodb.model.AuditLogEntry
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.service.AuditService
import org.springframework.data.rest.core.annotation.HandleAfterCreate
import org.springframework.data.rest.core.annotation.HandleAfterDelete
import org.springframework.data.rest.core.annotation.HandleAfterSave
import org.springframework.data.rest.core.annotation.RepositoryEventHandler
import org.springframework.security.core.context.SecurityContextHolder
import java.time.Instant

/**
 * Event handler for all events emitted by spring data rest before and after saving unit entities to mongoDB
 */
@RepositoryEventHandler(EmergencyUnit::class)
class EmergencyUnitEventHandler(
    private val auditService: AuditService,
) {
    private val log = KotlinLogging.logger {}

    @HandleAfterCreate
    fun handleAfterCreate(emergencyUnit: EmergencyUnit) {
        createLogEntryFromString("Created Unit ${emergencyUnit.unitId}")
        log.debug { "Unit ${emergencyUnit.unitId} was created" }
    }

    @HandleAfterSave
    fun handleAfterUnitSave(emergencyUnit: EmergencyUnit) {
        createLogEntryFromString("Edited Unit ${emergencyUnit.unitId}")
        log.debug { "Unit ${emergencyUnit.unitId} was edited" }
    }

    @HandleAfterDelete
    fun handleBeforeUnitDelete(emergencyUnit: EmergencyUnit) {
        createLogEntryFromString("Deleted Unit $emergencyUnit")
        log.debug { "Unit ${emergencyUnit.unitId} was deleted" }
    }

    private fun createLogEntryFromString(message: String) {
        val authentication = SecurityContextHolder.getContext().authentication
        val principal = authentication.principal
        if (principal is FirebasePrincipal) {
            auditService.addNewLogEntry(
                AuditLogEntry(
                    entryId = null,
                    timestamp = Instant.now(),
                    operationPerformed = message,
                    userMail = principal.name,
                ),
            )
        }
    }
}
