package io.lion5.smartcity.mongodb.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "unitPositionTrace")
data class UnitPositionTrace(
    @Id
    @Indexed(unique = true)
    val traceId: String?,
    val unitId: String,
    val missionId: String,
    val traceLocations: Set<TraceLocation>,
)
