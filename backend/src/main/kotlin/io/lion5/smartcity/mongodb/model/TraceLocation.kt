package io.lion5.smartcity.mongodb.model

import io.lion5.smartcity.mongodb.model.location.GPSLocation
import org.springframework.format.annotation.DateTimeFormat
import java.time.Instant

class TraceLocation(
    val gpsLocation: GPSLocation,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val timestamp: Instant,
)
