/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.service.image

import io.lion5.smartcity.gcp.GCPStorageUtil
import io.lion5.smartcity.http.dto.ByteImage
import io.lion5.smartcity.mongodb.model.Image
import io.lion5.smartcity.mongodb.repository.ImageRepository
import io.lion5.smartcity.util.exception.ImageNotFoundException
import io.lion5.smartcity.util.exception.ImageReadException
import io.lion5.smartcity.util.exception.ImageStorageException
import io.lion5.smartcity.util.exception.ImageTransformationException
import org.slf4j.LoggerFactory
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.util.UUID
import javax.imageio.ImageIO

@Service
class ImageStorageService(
    private val gcpStorageUtil: GCPStorageUtil,
    private val imageRepository: ImageRepository,
) {
    companion object {
        private val log = LoggerFactory.getLogger(ImageStorageService::class.java)
    }

    /**
     * Saves the given ByteArray [image] with the given [blobName] and [mimeType] to the gcp cloud storage.
     * Thereafter, it saves the reference into the database.
     *
     * @return the created database [Image]
     * @throws ImageStorageException if the image cannot be saved to the cloud storage
     */
    fun saveImage(
        image: ByteArray,
        blobName: String,
        mimeType: String,
    ): Image {
        log.info("save image using blobName $blobName and mimeType $mimeType")
        val blobInfo = gcpStorageUtil.saveImage(image, blobName, mimeType)

        val imageEntity = imageRepository.findByBlobName(blobInfo.blobId.name)
        val updateImageEntity =
            if (imageEntity == null) {
                Image(
                    url = blobInfo.mediaLink,
                    blobName = blobInfo.blobId.name,
                )
            } else {
                Image(
                    imageEntity.id,
                    blobInfo.mediaLink,
                    imageEntity.blobName,
                )
            }
        return imageRepository.save(updateImageEntity)
    }

    /**
     * Saves the given BufferedImage [image] with the given [blobName] and [mimeType] to the gcp cloud storage.
     * Thereafter, it saves the reference into the database.
     *
     * @return the created database [Image]
     * @throws ImageStorageException if the image cannot be saved to the cloud storage
     */
    fun saveImage(
        image: BufferedImage,
        blobName: String,
        mimeType: MediaType,
    ): Image {
        val baos = ByteArrayOutputStream()
        ImageIO.write(image, mimeType.subtype, baos)
        val bytes: ByteArray = baos.toByteArray()
        return saveImage(bytes, blobName, mimeType.subtype)
    }

    /**
     * Saves [images] to the cloud storage.
     * The blobName is a random [UUID] prefixed with [fileNamePrefix].
     *
     * @return the created database [Image]
     * @throws ImageStorageException if the image cannot be saved to the cloud storage or the mime type cannot be found in the [MultipartFile] object
     */
    fun saveMultipartFiles(
        images: Array<MultipartFile>,
        fileNamePrefix: String,
    ): List<Image> {
        val byteImages =
            images.map {
                val extension: String = getFileExtension(it)
                val blobName = "$fileNamePrefix/${UUID.randomUUID()}.$extension"
                val mimeType =
                    it.contentType
                        ?: throw ImageStorageException(
                            "Content type of image ${it.originalFilename} cannot be retrieved.",
                            null,
                        )
                ByteImage(it.bytes, blobName, mimeType)
            }
        return byteImages.map { saveImage(it.image, it.blobName, it.mimeType) }
    }

    /**
     * Saves [image] to the cloud storage.
     * The blobName is [path] prefixed with [fileNamePrefix].
     *
     * @param image the image to be saved
     * @param path the path to the 'folder' in the cloud storage
     * @param fileNamePrefix the prefix of the file name (without extension). The extension is derived from multipart file.
     *
     * @return the created database [Image]
     * @throws ImageStorageException if the image cannot be saved to the cloud storage or the mime type cannot be found in the [MultipartFile] object
     */
    fun saveMultipartFile(
        image: MultipartFile,
        path: String,
        fileNamePrefix: String,
    ): Image {
        val extension: String = getFileExtension(image)
        val blobName = "$path/$fileNamePrefix.$extension"
        val mimeType =
            image.contentType
                ?: throw ImageStorageException(
                    "Content type of image ${image.originalFilename} cannot be retrieved.",
                    null,
                )
        return saveImage(image.bytes, blobName, mimeType)
    }

    fun getFileExtension(file: MultipartFile): String {
        val fileName = file.originalFilename ?: throw ImageReadException("File's name cannot be found")
        return File(fileName).extension
    }

    /**
     * Deletes an image using its [imageId] from the database and cloud storage.
     *
     * @throws ImageStorageException if the image cannot be deleted from the cloud storage
     * @throws ImageNotFoundException if the image cannot be found in the database
     */
    fun deleteImage(imageId: String) {
        val image =
            imageRepository.findByIdOrNull(imageId)
                ?: throw ImageNotFoundException("Image with id $imageId can not be found. Abort deletion.", null)
        gcpStorageUtil.deleteImage(image.blobName)
        imageRepository.delete(image)
    }

    /**
     * Deletes [images] from the database and cloud storage.
     *
     * Images that cannot be deleted are ignored. This is usually the case if the image was already deleted.
     */
    fun deleteImages(images: List<Image>) {
        for (image in images) {
            try {
                deleteImage(image.id!!)
            } catch (_: ImageNotFoundException) {
                // If image is uploaded the first time the image cannot be deleted
            } catch (e: ImageStorageException) {
                log.warn("Image ${image.blobName} cannot be deleted.")
            }
        }
    }

    fun getBufferedImage(image: MultipartFile): BufferedImage {
        try {
            return ImageIO.read(image.inputStream)
        } catch (e: IOException) {
            throw ImageTransformationException("Cannot convert multipart file to buffered image", e)
        }
    }
}
