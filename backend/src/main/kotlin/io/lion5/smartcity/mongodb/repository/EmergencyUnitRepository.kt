/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.repository

import io.lion5.smartcity.mongodb.model.EmergencyUnit
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource
import org.springframework.web.bind.annotation.CrossOrigin

@CrossOrigin
@RepositoryRestResource(path = "units")
interface EmergencyUnitRepository : MongoRepository<EmergencyUnit, String> {
    fun findByCallSign(
        @Param("callSign") callSign: String,
    ): EmergencyUnit?

    fun save(emergencyUnit: EmergencyUnit): EmergencyUnit

    fun findEmergencyUnitsByUnitIdIn(unitIds: List<String>): List<EmergencyUnit>

    fun findByUserUserId(userId: String): EmergencyUnit?

    @RestResource(exported = true)
    override fun findAll(pageable: Pageable): Page<EmergencyUnit>
}
