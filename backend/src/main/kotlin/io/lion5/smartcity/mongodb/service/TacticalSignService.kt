/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.service

import io.lion5.smartcity.mongodb.model.Image
import io.lion5.smartcity.mongodb.model.TacticalSign
import io.lion5.smartcity.mongodb.repository.TacticalSignRepository
import io.lion5.smartcity.mongodb.service.image.ImageResizeService
import io.lion5.smartcity.mongodb.service.image.ImageStorageService
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service
class TacticalSignService(
    @Value("\${tactical-sign-storage.imageSize}") val imageResizeSize: Int,
    private val imageStorageService: ImageStorageService,
    private val imageResizeService: ImageResizeService,
    private val tacticalSignRepository: TacticalSignRepository,
) {
    fun createTacticalSign(
        image: MultipartFile,
        name: String,
    ): TacticalSign {
        val blobNameTacticalSign = "tactical-sign/$name"
        val blobNameOriginal = "orig/$name"
        val mediaType = MediaType.parseMediaType(image.contentType!!)
        val img: Image

        if (mediaType == MediaType.IMAGE_JPEG || mediaType == MediaType.IMAGE_PNG) {
            val bufferedImage = imageStorageService.getBufferedImage(image)
            val resizedImage = imageResizeService.resizeImage(bufferedImage, imageResizeSize, imageResizeSize, mediaType)
            img = imageStorageService.saveImage(resizedImage, blobNameTacticalSign, mediaType)
            imageStorageService.saveImage(image.bytes, blobNameOriginal, mediaType.type + "/" + mediaType.subtype)
        } else {
            img =
                imageStorageService.saveImage(
                    image.bytes,
                    blobNameTacticalSign,
                    mediaType.type + "/" + mediaType.subtype,
                )
        }

        return tacticalSignRepository.save(TacticalSign(image = img))
    }

    fun getAllTacticalSigns(): List<TacticalSign> = tacticalSignRepository.findAll()

    fun deleteTacticalSign(id: String) {
        val tacticalSign = tacticalSignRepository.findById(id)
        if (tacticalSign.isPresent) {
            imageStorageService.deleteImage(tacticalSign.get().image.id!!)
            tacticalSignRepository.deleteById(id)
        } else {
            throw Exception("TacticalSign with id $id not found")
        }
    }
}
