/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.model.location

import org.springframework.data.annotation.Transient

/**
 * A Location represented in the Gauss-Krüger Coordinate System used by public authorities in Germany
 *
 * @param zone ID of the Gauss-Krüger Zone
 * @param x the northing coordinate in that zone (see EPSG:4530)
 * @param y the easting coordinate in that zone (see EPSG:4530)
 */
data class GKLocation(
    val x: Double,
    val y: Double,
) : Location {
    @Transient
    var zone: Int
        private set

    init {
        zone = extractZoneFromCoords(x)
    }

    override fun epsgId(): Location.EpsgId = Location.EpsgId.GAUS_KRUGER_ZONE_4

    private fun extractZoneFromCoords(x: Double): Int = x.toString().first().digitToInt()
}
