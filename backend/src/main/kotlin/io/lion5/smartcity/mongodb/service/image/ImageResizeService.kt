/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.service.image

import io.lion5.smartcity.util.exception.ImageReadException
import io.lion5.smartcity.util.exception.ImageResizeException
import net.coobird.thumbnailator.Thumbnails
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import java.io.IOException
import javax.imageio.ImageIO

@Service
class ImageResizeService {
    companion object {
        private val log = LoggerFactory.getLogger(ImageResizeService::class.java)
    }

    /**
     * Resizes the [originalImage] to the given [width] and [height] and convert it to the given [mimeType].
     *
     * For the supported [mimeType]'s see the [Thumbnailator supported output formats](https://coobird.github.io/thumbnailator/javadoc/0.4.13/net/coobird/thumbnailator/util/ThumbnailatorUtils.html#getSupportedOutputFormats())
     *
     * @throws ImageResizeException is thrown if
     * - aspect ratio of the [originalImage] differs from the aspect ratio given by [width] and [height]
     * - the given [mimeType] is not supported
     * - the [originalImage] cannot be read
     */
    fun resizeImage(
        originalImage: BufferedImage,
        width: Int,
        height: Int,
        mimeType: MediaType,
    ): BufferedImage {
        log.info("resize image using width $width, height $height and mimeType $mimeType")
        try {
            return Thumbnails
                .of(originalImage)
                .size(width, height)
                .keepAspectRatio(true)
                .outputFormat(mimeType.subtype)
                .asBufferedImage()
        } catch (e: IllegalStateException) {
            throw ImageResizeException("Cannot keep aspect ratio", e)
        } catch (e: IllegalArgumentException) {
            throw ImageResizeException("Format is not supported", e)
        } catch (e: IOException) {
            throw ImageResizeException("A problem occurred during the reading of the original image", e)
        }
    }

    /**
     * Resizes the given [originalImage] to the maximum width and height of [maxWidth] and [maxHeight].
     * @return the resized [BufferedImage]
     * @throws ImageReadException if the image data is invalid
     */
    internal fun resizeImageToMaxResolution(
        originalImage: MultipartFile,
        maxWidth: Int,
        maxHeight: Int,
        mimeType: MediaType,
    ): BufferedImage {
        val imageStream = ByteArrayInputStream(originalImage.bytes)
        val bufferedImage = ImageIO.read(imageStream) ?: throw ImageReadException("Invalid image data")
        if (bufferedImage.width < maxWidth && bufferedImage.height < maxHeight) {
            return bufferedImage
        }
        val aspectRatio = bufferedImage.width.toDouble() / bufferedImage.height.toDouble()
        val newHeight: Int
        val newWidth: Int
        if (aspectRatio > maxWidth / maxHeight) {
            newWidth = maxWidth
            newHeight = (newWidth / aspectRatio).toInt()
        } else {
            newHeight = maxHeight
            newWidth = (newHeight * aspectRatio).toInt()
        }
        val resizedBufferedImage = resizeImage(bufferedImage, newWidth, newHeight, mimeType)

        return resizedBufferedImage
    }
}
