package io.lion5.smartcity.mongodb.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.lion5.smartcity.eventstore.events.model.LocationUpdateEvent
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.model.TraceLocation
import io.lion5.smartcity.mongodb.model.UnitPositionTrace
import io.lion5.smartcity.mongodb.model.location.GPSLocation
import io.lion5.smartcity.mongodb.repository.MissionRepository
import io.lion5.smartcity.mongodb.repository.UnitPositionTraceRepository
import io.lion5.smartcity.util.ApplicationEventEnvelope
import org.bson.types.ObjectId
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service

@Service
class UnitPositionTraceService(
    private val unitPositionTraceRepository: UnitPositionTraceRepository,
    private val missionRepository: MissionRepository,
    private val objectMapper: ObjectMapper = jacksonObjectMapper(),
) {
    @EventListener
    fun projectLocationUpdateEventToMongoDb(envelope: ApplicationEventEnvelope<LocationUpdateEvent>) {
        // get missions containing this unit and the mission is running
        val event = envelope.data
        val missions = missionRepository.findMissionsByStateAndUnitsEquals(Mission.State.RUNNING, ObjectId(event.senderUnitId))
        if (missions.isEmpty()) {
            return
        }

        // add the location to the trace for each mission
        missions.map { mission ->
            val trace = unitPositionTraceRepository.findByUnitIdAndMissionId(event.senderUnitId, mission.missionId!!)

            val updatedLocations = trace.traceLocations.toMutableSet()
            updatedLocations.add(TraceLocation(GPSLocation(event.location.latitude, event.location.longitude), event.timestamp))
            val updatedUnitPositionTrace = UnitPositionTrace(trace.traceId, trace.unitId, trace.missionId, updatedLocations)
            unitPositionTraceRepository.save(updatedUnitPositionTrace)
        }
    }

    fun findByUnitIdAndMissionId(
        unitId: String,
        missionId: String,
    ): String {
        val trace = unitPositionTraceRepository.findByUnitIdAndMissionId(unitId, missionId)
        return createGeoJson(trace)
    }

    fun createGeoJson(unitPositionTrace: UnitPositionTrace): String {
        // Sort the traceLocations by timestamp
        val sortedTraceLocations = unitPositionTrace.traceLocations.sortedBy { it.timestamp }

        // Create a LineString for each pair of consecutive points
        val lineStringFeatures =
            sortedTraceLocations.zipWithNext().map { (traceLocation1, traceLocation2) ->
                val coordinates =
                    listOf(
                        listOf(traceLocation1.gpsLocation.longitude, traceLocation1.gpsLocation.latitude),
                        listOf(traceLocation2.gpsLocation.longitude, traceLocation2.gpsLocation.latitude),
                    )
                mapOf(
                    "type" to "Feature",
                    "geometry" to
                        mapOf(
                            "type" to "LineString",
                            "coordinates" to coordinates,
                        ),
                    "properties" to
                        mapOf(
                            "timestamp" to traceLocation1.timestamp,
                            "unitId" to unitPositionTrace.unitId,
                            "missionId" to unitPositionTrace.missionId,
                        ),
                )
            }

        // Create a Point for each traceLocation
        val pointFeatures =
            sortedTraceLocations.map { traceLocation ->
                mapOf(
                    "type" to "Feature",
                    "geometry" to
                        mapOf(
                            "type" to "Point",
                            "coordinates" to listOf(traceLocation.gpsLocation.longitude, traceLocation.gpsLocation.latitude),
                        ),
                    "properties" to
                        mapOf(
                            "timestamp" to traceLocation.timestamp,
                            "unitId" to unitPositionTrace.unitId,
                            "missionId" to unitPositionTrace.missionId,
                        ),
                )
            }

        val features = pointFeatures + lineStringFeatures

        val geoJson =
            mapOf(
                "type" to "FeatureCollection",
                "features" to features,
            )

        return objectMapper.writeValueAsString(geoJson)
    }
}
