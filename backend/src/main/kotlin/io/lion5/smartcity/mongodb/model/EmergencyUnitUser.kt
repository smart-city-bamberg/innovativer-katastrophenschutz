package io.lion5.smartcity.mongodb.model

data class EmergencyUnitUser(
    val userId: String,
    val displayName: String? = null,
    val email: String? = null,
)
