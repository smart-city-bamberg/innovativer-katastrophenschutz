/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.service

import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.auth.acl.AclPermissionService
import io.lion5.smartcity.auth.model.AuthorityRoles
import io.lion5.smartcity.eventstore.events.IMissionStreamRepository
import io.lion5.smartcity.eventstore.events.model.CommandEvent
import io.lion5.smartcity.eventstore.events.model.DangerZoneEvent
import io.lion5.smartcity.eventstore.events.model.MissionSectionEvent
import io.lion5.smartcity.eventstore.events.model.PoiEvent
import io.lion5.smartcity.eventstore.events.model.ReportEvent
import io.lion5.smartcity.eventstore.events.model.UnitAvailabilityChangedEvent
import io.lion5.smartcity.mongodb.model.Command
import io.lion5.smartcity.mongodb.model.DangerZone
import io.lion5.smartcity.mongodb.model.Department
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.model.PointOfInterest
import io.lion5.smartcity.mongodb.model.Report
import io.lion5.smartcity.mongodb.model.Section
import io.lion5.smartcity.mongodb.model.TraceLocation
import io.lion5.smartcity.mongodb.model.UnitPositionTrace
import io.lion5.smartcity.mongodb.model.location.GPSLocation
import io.lion5.smartcity.mongodb.repository.DepartmentRepository
import io.lion5.smartcity.mongodb.repository.MissionRepository
import io.lion5.smartcity.mongodb.repository.UnitPositionTraceRepository
import io.lion5.smartcity.util.ApplicationEventEnvelope
import io.lion5.smartcity.util.const.CrudOperation.CREATED
import io.lion5.smartcity.util.const.CrudOperation.DELETED
import io.lion5.smartcity.util.const.CrudOperation.UPDATED
import io.lion5.smartcity.util.const.UnitAvailability
import jakarta.validation.Validator
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Lazy
import org.springframework.context.event.EventListener
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation
import org.springframework.data.mongodb.core.aggregation.LookupOperation
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.rest.core.event.AfterCreateEvent
import org.springframework.data.rest.core.event.AfterLinkDeleteEvent
import org.springframework.data.rest.core.event.AfterLinkSaveEvent
import org.springframework.data.rest.core.event.BeforeCreateEvent
import org.springframework.data.rest.core.event.BeforeLinkDeleteEvent
import org.springframework.data.rest.core.event.BeforeLinkSaveEvent
import org.springframework.security.access.prepost.PostFilter
import org.springframework.security.acls.domain.BasePermission
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import java.util.Optional

@Service
class MissionService(
    private val missionRepository: MissionRepository,
    private val missionStreamRepository: IMissionStreamRepository,
    private val departmentRepository: DepartmentRepository,
    private val unitPositionTraceRepository: UnitPositionTraceRepository,
    private val emergencyUnitService: EmergencyUnitService,
    private val joinCodeService: JoinCodeService,
    private val validator: Validator,
    @Value("\${digimap.eventstore.streamPrefixes.mission}") private val missionStreamPrefix: String = "default/missions/",
    private val aclPermissionService: AclPermissionService,
    @Lazy private val mongoTemplate: MongoTemplate,
    private val applicationEventPublisher: ApplicationEventPublisher,
) {
    private val log = KotlinLogging.logger {}

    /**
     * Creates a new mission with the given data.
     *
     * This method generates a unique joinCode and joinPin for the mission and saves it to the database.
     * The authenticated principal is added as an ADMINISTRATION permission for the mission.
     * All users in the associated unit departments are added as READ permissions for the mission.
     * If the associated unit has no department, the userId of the unit is used to add the READ permission.
     *
     * @param mission the mission to create
     * @return the created mission
     */
    fun createNewMission(mission: Mission): Mission {
        // This part required to get the full userid list of the departments
        val unitsWithFullDepartments: Set<EmergencyUnit> =
            mission.units
                .map { unit ->
                    if (unit.department != null) {
                        val department: Department =
                            departmentRepository.findByDepartmentId(ObjectId(unit.department!!.departmentId))
                                ?: throw IllegalArgumentException("Department with id ${unit.department!!.departmentId} does not exist.")
                        unit.copy(department = department)
                    } else {
                        unit
                    }
                }.toSet()
        val missionWithFullDepartments = mission.copy(units = unitsWithFullDepartments)

        // ⚠️ these events are part of Spring Data Rest. We need to publish them manually because we overwrote the POST method.
        applicationEventPublisher.publishEvent(BeforeCreateEvent(missionWithFullDepartments))
        val missionWithCodes = generateJoinCodeAndPinAndSaveMission(missionWithFullDepartments)
        applicationEventPublisher.publishEvent(AfterCreateEvent(missionWithCodes))

        // pass the authenticated principal's username for addPermissionForUser method
        val username = SecurityContextHolder.getContext().authentication.name
        log.debug { "Adding ADMINISTRATION permission for user who created the mission: $missionWithCodes, ADMINISTRATION, $username" }
        aclPermissionService.addPermissionForUser(missionWithCodes, BasePermission.ADMINISTRATION, username)

        // Temporary Units for Adhoc Users may have null Departments
        updateAclPermissionsForMission(missionWithCodes, existingUnits = emptySet())

        return missionWithCodes
    }

    /**
     * Updates the ACL permissions for the given mission.
     *
     * This method adds READ permissions for all users in the associated unit departments.
     * If the associated unit has no department, the userId of the unit is used to add the READ permission.
     *
     * If the mission has existing units, the method will revoke READ permissions for users that are no longer associated with the units.
     *
     * @param mission the mission to update the ACL permissions for
     */
    private fun updateAclPermissionsForMission(
        mission: Mission,
        existingUnits: Set<EmergencyUnit>,
    ) {
        val usersInAssociatedUnitDepartments =
            mission.units
                .flatMap { unit ->
                    // if unit has department, get all users in the department. Otherwise, get the userId of the unit.
                    // If both department and the userId is null, return an empty list.
                    unit.department?.users?.map { user -> user.userId }
                        ?: listOfNotNull(unit.user?.userId).takeIf { it.isNotEmpty() }
                        ?: emptyList()
                }.distinct() // get all users in the associated unit departments

        if (existingUnits.isNotEmpty()) {
            val previousUsersInAssociatedUnitDepartments =
                existingUnits
                    .flatMap { unit ->
                        // if unit has department, get all users in the department. Otherwise, get the userId of the unit.
                        // If both department and the userId is null, return an empty list.
                        unit.department?.users?.map { user -> user.userId }
                            ?: listOfNotNull(unit.user?.userId).takeIf { it.isNotEmpty() }
                            ?: emptyList()
                    }.distinct() // get all users in the associated unit departments

            val usersToRevoke = previousUsersInAssociatedUnitDepartments.minus(usersInAssociatedUnitDepartments.toSet())

            if (usersToRevoke.isNotEmpty()) {
                log.debug { "Revoking READ permission for department with just created mission: $mission, READ, $usersToRevoke" }
                aclPermissionService.deletePermissionForMultipleUsers(
                    mission,
                    BasePermission.READ,
                    usersToRevoke,
                )
            }
        }
        log.debug { "Adding READ permission for department with just created mission: $mission, READ, $usersInAssociatedUnitDepartments" }
        if (usersInAssociatedUnitDepartments.isNotEmpty()) {
            aclPermissionService.addPermissionForMultipleUsers(
                mission,
                BasePermission.READ,
                usersInAssociatedUnitDepartments,
            )
        }
    }

    /**
     * Generates a unique joinCode and joinPin for the given mission and saves it to the database.
     * Mission is saved in this method to ensure that the generated joinCode and joinPin are unique.
     */
    private fun generateJoinCodeAndPinAndSaveMission(mission: Mission): Mission {
        val maxRetries = 5
        var retries = 0
        while (retries < maxRetries) {
            try {
                val joinCode = joinCodeService.generateJoinCode()
                val joinPin = joinCodeService.generateJoinPin()
                val missionWithCodes = mission.copy(joinCode = joinCode, joinPin = joinPin)
                val violations = validator.validate(missionWithCodes)
                if (violations.isNotEmpty()) {
                    throw IllegalArgumentException("Mission validation failed: $violations")
                }
                return missionRepository.save(missionWithCodes)
            } catch (e: Exception) {
                log.error { "Duplicate joinCode detected, retrying... (Attempt: ${retries + 1})" }
                retries++
                if (retries == maxRetries) {
                    log.error { "Maximum retry attempts reached, failing operation." }
                    throw IllegalStateException("Unable to generate a unique joinCode after $maxRetries attempts.")
                }
            }
        }
        throw IllegalStateException("Unexpected state: Mission JoinCode generation failed with reaching maximum retries.")
    }

    fun addUnitToMission(
        unitIds: List<String>,
        missionId: String,
    ): Mission {
        var mission =
            missionRepository.findMissionByMissionId(ObjectId(missionId))
                ?: throw IllegalArgumentException("Mission with id $missionId does not exist.")

        val emergencyUnitsToAdd: MutableList<EmergencyUnit> = mutableListOf()
        unitIds.forEach { unitId ->
            val unit = emergencyUnitService.getEmergencyUnitById(unitId)
            unit
                .orElseThrow {
                    IllegalArgumentException("Unit with id $unitId does not exist.")
                }.let {
                    emergencyUnitsToAdd.add(it)
                }
        }

        applicationEventPublisher.publishEvent(BeforeLinkSaveEvent(mission, emergencyUnitsToAdd))
        mission = mission.copy(units = mission.units.plus(emergencyUnitsToAdd))
        val savedMission = missionRepository.save(mission)
        applicationEventPublisher.publishEvent(AfterLinkSaveEvent(savedMission, emergencyUnitsToAdd))

        updateAclPermissionsForMission(savedMission, existingUnits = emptySet())
        return mission
    }

    fun removeUnitFromMission(
        missionId: String,
        unitId: String,
    ): Mission {
        val mission =
            missionRepository.findMissionByMissionId(ObjectId(missionId))
                ?: throw IllegalArgumentException("Mission with id $missionId does not exist.")
        val unit =
            emergencyUnitService
                .getEmergencyUnitById(unitId)
                .orElseThrow { IllegalArgumentException("Unit with id $unitId does not exist.") }
        val existingUnits = mission.units

        applicationEventPublisher.publishEvent(BeforeLinkDeleteEvent(mission, listOf(unit)))

        val updatedUnits = mission.units.filter { it.unitId != unit.unitId }
        val updatedMission = mission.copy(units = updatedUnits.toSet())

        val savedMission = missionRepository.save(updatedMission)
        applicationEventPublisher.publishEvent(AfterLinkDeleteEvent(savedMission, listOf(unit)))

        updateAclPermissionsForMission(savedMission, existingUnits)
        return savedMission
    }

    /**
     * Finds and returns a list of missions where the authenticated principal has READ permission.
     *
     * Uses this method to filter the list of missions where the authenticated principal has READ permission.
     * This @PostFilter annotation is not usable with @RepositoryRestResource, so this method is used to filter.
     *
     * @return a mutable list of missions where the authenticated principal has READ permission.
     */
    @PostFilter(
        "hasPermission(filterObject, 'READ') or hasPermission(filterObject, 'ADMINISTRATION') " +
            "or hasRole('SUPER_ADMIN') or hasRole('ADMIN')",
    )
    fun findAll(): List<Mission> = missionRepository.findAll()

    /**
     * Finds and returns a list of missions where at least one unit belongs to the specified department.
     *
     * This method uses MongoDB's aggregation framework to join the 'units' field in the 'mission' collection
     * with the 'unit' collection, and then filters the joined documents where the 'department.departmentId'
     * field matches the given 'departmentId'.
     *
     * @param departmentId the ID of the department to match units against. Can be null.
     * @return a mutable list of missions where at least one unit belongs to the specified department.
     *         If no matching missions are found, an empty list is returned.
     *         If the departmentId is null, all missions are returned.
     */
    fun findMissionsByDepartmentId(departmentId: String): MutableList<Mission> {
        val lookupUnits =
            LookupOperation
                .newLookup()
                .from("unit")
                .localField("units")
                .foreignField("_id")
                .`as`("unitsDetails")

        val lookupDepartments =
            LookupOperation
                .newLookup()
                .from("department")
                .localField("unitsDetails.department")
                .foreignField("_id")
                .`as`("unitsDetails.departmentDetails")

        val matchOperation =
            Aggregation.match(
                Criteria.where("unitsDetails.departmentDetails._id").`is`(ObjectId(departmentId)),
            )

        val groupOperation =
            Aggregation
                .group("missionId")
                .first(Aggregation.ROOT)
                .`as`("mission")

        val projectOperation =
            Aggregation
                .project()
                .and("\$mission")
                .`as`("mission")
                .andExclude("_id")

        val replaceRootOperation = Aggregation.replaceRoot("mission")

        val aggregation =
            Aggregation.newAggregation(
                lookupUnits,
                Aggregation.unwind("unitsDetails"),
                lookupDepartments,
                Aggregation.unwind("unitsDetails.departmentDetails"),
                matchOperation,
                groupOperation,
                projectOperation,
                replaceRootOperation,
            )

        return mongoTemplate.aggregate(aggregation, "mission", Mission::class.java).mappedResults.toMutableList()
    }

    @EventListener
    fun projectReportEventIntoMongoDb(envelope: ApplicationEventEnvelope<ReportEvent>) {
        val reportEvent = envelope.data

        log.debug { "Setting security context" }
        // FIXME: This is a workaround to fix the missing auth from our MQTT broker
        val authentication =
            UsernamePasswordAuthenticationToken("eventstore", null, listOf(SimpleGrantedAuthority("ROLE_${AuthorityRoles.EVENT_STORE}")))
        SecurityContextHolder.getContext().authentication = authentication

        log.debug { "Getting mission from event envelope" }
        // at this point we can be sure that this mission exists because otherwise the event could not have been written to the eventstore.
        val mission = getMissionFromEventEnvelope(envelope)

        log.debug { "Checking if report already exists" }
        // if report already exists then do nothing
        if (mission.reports.any { it.reportId == reportEvent.reportId }) {
            log.warn {
                "Received duplicate ReportEvent with id ${reportEvent.reportId} from EventStore. " +
                    "This may be caused by duplicated messages form the MQTT broker, the Eventstore or the PWA."
            }
            return
        }

        log.debug { "Getting gps location" }
        val gpsLocation =
            if (reportEvent.location != null) {
                GPSLocation(
                    reportEvent.location.latitude,
                    reportEvent.location.longitude,
                )
            } else {
                null
            }

        log.debug { "Getting sender unit" }
        val sender = emergencyUnitService.getEmergencyUnitById(reportEvent.senderUnitId).get()

        log.debug { "Constructing report entity" }
        // construct report entity
        val report =
            Report(
                senderUnitId = reportEvent.senderUnitId,
                report = reportEvent.report,
                createdAt = reportEvent.timestamp,
                tacticalSignPath = reportEvent.tacticalSignPath,
                receiverUnitIds = reportEvent.receiverUnitIds,
                referencingCommandId = reportEvent.referencingCommandId,
                images = reportEvent.images,
                location = gpsLocation,
                reportId = reportEvent.reportId,
                senderCallSign = sender.callSign,
                eTag = reportEvent.eTag.toInt(),
            )

        log.debug { "Updating mission object" }
        // update mission
        val mutableSet = mission.reports.toMutableSet()
        mutableSet.add(report)
        val newMission = mission.copy(reports = mutableSet.toSet())
        log.debug { "Saving mission" }
        missionRepository.save(newMission)
    }

    @EventListener
    fun projectCommandEventIntoMongoDb(envelope: ApplicationEventEnvelope<CommandEvent>) {
        val commandEvent = envelope.data
        // FIXME: This is a workaround to fix the missing auth from our MQTT broker
        val authentication =
            UsernamePasswordAuthenticationToken("eventstore", null, listOf(SimpleGrantedAuthority("ROLE_${AuthorityRoles.EVENT_STORE}")))
        SecurityContextHolder.getContext().authentication = authentication

        // at this point we can be sure that this mission exists because otherwise the event could not have been written to the eventstore.
        val mission = getMissionFromEventEnvelope(envelope)
        // if command already exists then do nothing
        if (mission.commands.any { it.commandId == commandEvent.commandId }) {
            log.warn {
                "Received duplicate CommandEvent with id ${commandEvent.commandId} from EventStore. " +
                    "This may be caused by duplicated messages form the MQTT broker, the Eventstore or the PWA."
            }
            return
        }
        val sender = emergencyUnitService.getEmergencyUnitById(commandEvent.senderUnitId).get()

        // construct command entity
        val command =
            Command(
                commandId = commandEvent.commandId,
                senderUnitId = commandEvent.senderUnitId,
                senderCallSign = sender.callSign,
                commandText = commandEvent.commandText,
                createdAt = commandEvent.timestamp,
                referencingReportId = commandEvent.referencingReportId,
                receiverUnitIds = commandEvent.receiverUnitIds,
                eTag = commandEvent.eTag.toInt(),
            )

        // update mission
        val mutableSet = mission.commands.toMutableSet()
        mutableSet.add(command)
        val newMission = mission.copy(commands = mutableSet.toSet())
        missionRepository.save(newMission)
    }

    @EventListener
    fun projectDangerZoneEventIntoMongoDb(envelope: ApplicationEventEnvelope<DangerZoneEvent>) {
        val dangerZoneEvent = envelope.data
        // FIXME: This is a workaround to fix the missing auth from our MQTT broker
        val authentication =
            UsernamePasswordAuthenticationToken("eventstore", null, listOf(SimpleGrantedAuthority("ROLE_${AuthorityRoles.EVENT_STORE}")))
        SecurityContextHolder.getContext().authentication = authentication

        val mission = getMissionFromEventEnvelope(envelope)
        val mutableDangerZoneSet = mission.dangerZones.toMutableSet()

        when (dangerZoneEvent.operation) {
            CREATED -> {
                if (mutableDangerZoneSet.find { it.dangerZoneId == dangerZoneEvent.dangerZoneId } != null) {
                    val ex =
                        IllegalArgumentException(
                            "DangerZone CREATE received for UUID ${dangerZoneEvent.dangerZoneId} that has already been created.",
                        )
                    log.error(ex) { "DangerZone CREATE received for UUID ${dangerZoneEvent.dangerZoneId} that has not yet been created." }
                    throw ex
                }

                val newDangerZone =
                    DangerZone(
                        dangerZoneId = dangerZoneEvent.dangerZoneId,
                        eTag = dangerZoneEvent.eTag.toInt(),
                        geoJson = dangerZoneEvent.geoJson,
                        senderUnitId = dangerZoneEvent.senderUnitId,
                    )
                mutableDangerZoneSet.add(newDangerZone)
            }

            UPDATED -> {
                val existingDangerZone = mutableDangerZoneSet.find { it.dangerZoneId == dangerZoneEvent.dangerZoneId }

                if (existingDangerZone == null) {
                    val ex =
                        IllegalArgumentException(
                            "DangerZone UPDATE received for UUID ${dangerZoneEvent.dangerZoneId} that has not yet been created.",
                        )
                    log.error(ex) { "DangerZone UPDATE received for UUID ${dangerZoneEvent.dangerZoneId} that has not yet been created." }
                    throw ex
                } else {
                    val updatedDangerZone =
                        existingDangerZone.copy(
                            geoJson = dangerZoneEvent.geoJson,
                            eTag = dangerZoneEvent.eTag.toInt(),
                        )
                    mutableDangerZoneSet.remove(existingDangerZone)
                    mutableDangerZoneSet.add(updatedDangerZone)
                }
            }

            DELETED -> {
                mutableDangerZoneSet
                    .filter { it.dangerZoneId == dangerZoneEvent.dangerZoneId }
                    .forEach { mutableDangerZoneSet.remove(it) }
            }
        }

        val updatedMission = mission.copy(dangerZones = mutableDangerZoneSet.toSet())
        missionRepository.save(updatedMission)
    }

    @EventListener
    fun projectMissionSectionEventIntoMongoDb(envelope: ApplicationEventEnvelope<MissionSectionEvent>) {
        val missionSectionEvent = envelope.data
        // FIXME: This is a workaround to fix the missing auth from our MQTT broker
        val authentication =
            UsernamePasswordAuthenticationToken("eventstore", null, listOf(SimpleGrantedAuthority("ROLE_${AuthorityRoles.EVENT_STORE}")))
        SecurityContextHolder.getContext().authentication = authentication

        val mission = getMissionFromEventEnvelope(envelope)
        val sectionMutableSet = mission.sections.toMutableSet()

        when (missionSectionEvent.operation) {
            CREATED -> {
                if (sectionMutableSet.find { it.sectionId == missionSectionEvent.sectionId } != null) {
                    val ex =
                        IllegalArgumentException(
                            "MissionSection CREATE received for UUID ${missionSectionEvent.sectionId} that has already been created.",
                        )
                    log.error(
                        ex,
                    ) { "MissionSection CREATE received for UUID ${missionSectionEvent.sectionId} that has not yet been created." }
                    throw ex
                }

                val section =
                    Section(
                        sectionId = missionSectionEvent.sectionId,
                        geoJson = missionSectionEvent.geoJson,
                        eTag = missionSectionEvent.eTag.toInt(),
                        createdAt = missionSectionEvent.timestamp,
                        senderUnitId = missionSectionEvent.senderUnitId,
                    )
                sectionMutableSet.add(section)
            }

            UPDATED -> {
                val existingMissionSection = sectionMutableSet.find { it.sectionId == missionSectionEvent.sectionId }

                if (existingMissionSection == null) {
                    val ex =
                        IllegalArgumentException(
                            "MissionSection UPDATE received for UUID ${missionSectionEvent.sectionId} that has not yet been created.",
                        )
                    log.error(
                        ex,
                    ) { "MissionSection UPDATE received for UUID ${missionSectionEvent.sectionId} that has not yet been created." }
                    throw ex
                } else {
                    val updatedMissionSection =
                        existingMissionSection.copy(
                            geoJson = missionSectionEvent.geoJson,
                            eTag = missionSectionEvent.eTag.toInt(),
                        )
                    sectionMutableSet.remove(existingMissionSection)
                    sectionMutableSet.add(updatedMissionSection)
                }
            }

            DELETED -> {
                sectionMutableSet
                    .filter { it.sectionId == missionSectionEvent.sectionId }
                    .forEach { sectionMutableSet.remove(it) }
            }
        }

        val updatedMission = mission.copy(sections = sectionMutableSet.toSet())
        missionRepository.save(updatedMission)
    }

    @EventListener
    fun projectUnitAvailabilityChangedEventToMongoDb(envelope: ApplicationEventEnvelope<UnitAvailabilityChangedEvent>) {
        val event = envelope.data
        log.debug { "Setting security context" }
        // FIXME: This is a workaround to fix the missing auth from our MQTT broker
        val authentication =
            UsernamePasswordAuthenticationToken("eventstore", null, listOf(SimpleGrantedAuthority("ROLE_${AuthorityRoles.EVENT_STORE}")))
        SecurityContextHolder.getContext().authentication = authentication

        log.debug { "Getting mission from event envelope" }
        // at this point we can be sure that this mission exists because otherwise the event could not have been written to the eventstore.
        val mission = getMissionFromEventEnvelope(envelope)
        log.debug { "Getting sender emergency unit" }
        val unitOptional = emergencyUnitService.getEmergencyUnitById(event.unitId)
        if (unitOptional.isEmpty) {
            throw IllegalArgumentException("Unit with this ID does not exist")
        }
        val emergencyUnit = unitOptional.get()
        val emergencyUnitMutableSet = mission.units.toMutableSet()
        val unitPositionTraceMutableSet = mission.unitPositionTrace.toMutableSet()
        when (event.unitAvailability) {
            UnitAvailability.JOINED -> {
                // updated the unit
                log.debug { "Unit joined" }
                val updatedUnit =
                    emergencyUnit.copy(gpsLocation = GPSLocation(event.location.latitude, event.location.longitude))
                emergencyUnitService.createOrUpdateEmergencyUnit(updatedUnit)
                // update the mission -> unit reference
                emergencyUnitMutableSet.remove(emergencyUnit)
                emergencyUnitMutableSet.add(updatedUnit)
                if (unitPositionTraceMutableSet.find { it.unitId == updatedUnit.unitId } == null) {
                    val traceLocation = GPSLocation(event.location.latitude, event.location.longitude)
                    val trace =
                        UnitPositionTrace(
                            traceId = null,
                            unitId = updatedUnit.unitId!!,
                            missionId = mission.missionId!!,
                            traceLocations =
                                setOf(
                                    TraceLocation(traceLocation, event.timestamp),
                                ),
                        )
                    val savedTrace = unitPositionTraceRepository.save(trace)
                    unitPositionTraceMutableSet.add(savedTrace)
                }
            }

            UnitAvailability.REMOVED -> {
                log.debug { "Unit removed" }
                emergencyUnitMutableSet.remove(emergencyUnit)
                unitPositionTraceMutableSet.removeIf { it.unitId == emergencyUnit.unitId }
            }
        }
        log.debug { "Saving updated mission" }
        val updatedMission = mission.copy(units = emergencyUnitMutableSet.toSet(), unitPositionTrace = unitPositionTraceMutableSet.toSet())
        missionRepository.save(updatedMission)
    }

    @EventListener
    fun projectPoiEventToMongoDb(envelope: ApplicationEventEnvelope<PoiEvent>) {
        val poiEvent = envelope.data
        // FIXME: This is a workaround to fix the missing auth from our MQTT broker
        val authentication =
            UsernamePasswordAuthenticationToken("eventstore", null, listOf(SimpleGrantedAuthority("ROLE_${AuthorityRoles.EVENT_STORE}")))
        SecurityContextHolder.getContext().authentication = authentication

        val mission = getMissionFromEventEnvelope(envelope)
        val pointOfInterestMutableSet = mission.pois.toMutableSet()

        when (poiEvent.operation) {
            CREATED -> {
                if (pointOfInterestMutableSet.find { it.poiId == poiEvent.poiId } != null) {
                    val ex =
                        IllegalArgumentException("PoiEvent CREATE received for UUID ${poiEvent.poiId} that has already been created.")
                    log.error(ex) { "PoiEvent CREATE received for UUID ${poiEvent.poiId} that has already been created." }
                    throw ex
                }

                val poi =
                    PointOfInterest(
                        poiId = poiEvent.poiId,
                        eTag = poiEvent.eTag.toInt(),
                        senderUnitId = poiEvent.senderUnitId,
                        location = GPSLocation(poiEvent.location.latitude, poiEvent.location.longitude),
                        createdAt = poiEvent.timestamp,
                        details = poiEvent.details,
                        tacticalSignPath = poiEvent.tacticalSignPath,
                        operation = poiEvent.operation,
                    )
                pointOfInterestMutableSet.add(poi)
            }

            UPDATED -> {
                val existingPoi = pointOfInterestMutableSet.find { it.poiId == poiEvent.poiId }

                if (existingPoi == null) {
                    val ex =
                        IllegalArgumentException("PoiEvent UPDATE received for UUID ${poiEvent.poiId} that has not yet been created.")
                    log.error(ex) { "PoiEvent UPDATE received for UUID ${poiEvent.poiId} that has not yet been created." }
                    throw ex
                } else {
                    val updatedPoi =
                        existingPoi.copy(
                            location = GPSLocation(poiEvent.location.latitude, poiEvent.location.longitude),
                            eTag = poiEvent.eTag.toInt(),
                            details = poiEvent.details,
                            tacticalSignPath = poiEvent.tacticalSignPath,
                        )
                    pointOfInterestMutableSet.remove(existingPoi)
                    pointOfInterestMutableSet.add(updatedPoi)
                }
            }

            DELETED -> {
                pointOfInterestMutableSet
                    .filter { it.poiId == poiEvent.poiId }
                    .forEach { pointOfInterestMutableSet.remove(it) }
            }
        }

        val updatedMission = mission.copy(pois = pointOfInterestMutableSet.toSet())
        missionRepository.save(updatedMission)
    }

    private fun getMissionFromEventEnvelope(envelope: ApplicationEventEnvelope<*>): Mission =
        missionRepository.findMissionByMissionId(ObjectId(extractMissionIdFromStreamId(envelope.originStream)))!!

    fun createEventStreamForMission(missionId: String): Boolean = missionStreamRepository.createNewStreamForMission(missionId)

    private fun extractMissionIdFromStreamId(stream: String): String {
        log.debug { "Extracting missionID from $stream" }
        return stream.removePrefix(missionStreamPrefix)
    }

    fun findMissionById(missionId: String): Optional<Mission> = missionRepository.findById(missionId)

    fun findByState(state: Mission.State): List<Mission> = missionRepository.findMissionsByState(state)
}
