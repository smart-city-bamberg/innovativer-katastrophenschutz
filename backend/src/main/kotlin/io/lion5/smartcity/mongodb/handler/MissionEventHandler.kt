/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.handler

import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.eventstore.EventstoreMissionService
import io.lion5.smartcity.mongodb.model.AdhocJoinRequest
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.repository.AdhocJoinRequestRepository
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import io.lion5.smartcity.mongodb.service.MissionService
import io.lion5.smartcity.mqtt.payload.LocationPayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundMissionStateChangedPayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundUnitAvailablityChangedPayload
import io.lion5.smartcity.util.const.UnitAvailability
import org.springframework.data.rest.core.annotation.HandleAfterCreate
import org.springframework.data.rest.core.annotation.HandleAfterSave
import org.springframework.data.rest.core.annotation.HandleBeforeLinkDelete
import org.springframework.data.rest.core.annotation.HandleBeforeLinkSave
import org.springframework.data.rest.core.annotation.HandleBeforeSave
import org.springframework.data.rest.core.annotation.RepositoryEventHandler
import org.springframework.messaging.Message
import org.springframework.messaging.support.MessageBuilder
import java.time.Instant

@RepositoryEventHandler(Mission::class)
class MissionEventHandler(
    private val missionService: MissionService,
    private val eventstoreMissionService: EventstoreMissionService,
    private val emergencyUnitService: EmergencyUnitService,
    private val adhocJoinRequestRepository: AdhocJoinRequestRepository,
) {
    private val log = KotlinLogging.logger {}

    @HandleBeforeSave
    fun handleBeforeSave(updatedMission: Mission) {
        log.debug { "Mission ${updatedMission.missionId} was updated -> updating event stream for it" }

        val mission = missionService.findMissionById(updatedMission.missionId!!).get()

        if (updatedMission.state == Mission.State.FINISHED && mission.state == Mission.State.RUNNING) {
            val message = createMissionStateChangedMessage(updatedMission)
            eventstoreMissionService.handleMissionStateChangedMessage(message)
        }
    }

    @HandleAfterCreate
    fun handleAfterCreate(mission: Mission) {
        log.debug { "New mission ${mission.missionId} was created -> creating event stream for it" }
        missionService.createEventStreamForMission(mission.missionId!!)
    }

    @HandleAfterSave
    fun handleAfterSave(updatedMission: Mission) {
        val mission = missionService.findMissionById(updatedMission.missionId!!).get()

        val units = mission.units

        if (mission.state != Mission.State.FINISHED) {
            return
        }

        // If the mission is finished, we need to disable all temporary units
        for (unit in units) {
            if (unit.creationMethod != EmergencyUnit.CreationMethod.TEMPORARY) {
                continue
            }
            unit.creationMethod = EmergencyUnit.CreationMethod.TEMPORARY_DISABLED
            emergencyUnitService.createOrUpdateEmergencyUnit(unit)
        }

        // If the mission is finished, we need to expire all pending adhoc join requests
        val adhocJoinRequests = adhocJoinRequestRepository.findAllByMission(mission)
        val expiredAdhocJoinRequests =
            adhocJoinRequests.map {
                it.status = AdhocJoinRequest.AdhocJoinRequestStatus.EXPIRED
                it
            }
        expiredAdhocJoinRequests.isNotEmpty().let { adhocJoinRequestRepository.saveAll(expiredAdhocJoinRequests) }
    }

    @HandleBeforeLinkSave
    fun handleBeforeLinkSaveEvent(
        updatedMission: Mission,
        units: Collection<EmergencyUnit>,
    ) {
        // In RepositoryConfiguration.kt it is configured that there can only be PATCH requests
        // that trigger this interceptor method
        log.debug { "New units are assigned to mission via REST API" }

        // get the current mission from the DB to find filter out units that are already part of the mission
        val mission = missionService.findMissionById(updatedMission.missionId!!).get()

        // only emmit a mqtt message and event log entry for those units that are not already assigned to the mission
        units
            .filter { unit -> !mission.units.contains(unit) }
            .forEach { unit ->
                val message =
                    createAvailabilityChangedMessage(
                        UnitAvailability.JOINED,
                        updatedMission.missionId.toString(),
                        unit.unitId!!,
                        LocationPayload(unit.gpsLocation.latitude, unit.gpsLocation.longitude),
                    )
                eventstoreMissionService.handleUnitAvailabilityChangedMessage(message)
            }
    }

    @HandleBeforeLinkDelete
    fun handleBeforeLinkDeleteEvent(
        updatedMission: Mission,
        units: Collection<EmergencyUnit>,
    ) {
        log.debug { "Units are unassigned from mission via REST API" }

        // get the current mission from the DB to find filter out units that are already part of the mission
        val mission = missionService.findMissionById(updatedMission.missionId!!).get()

        mission.units.subtract(updatedMission.units).forEach { unit ->
            val message =
                createAvailabilityChangedMessage(
                    UnitAvailability.REMOVED,
                    updatedMission.missionId.toString(),
                    unit.unitId!!,
                    LocationPayload(unit.gpsLocation.latitude, unit.gpsLocation.longitude),
                )
            eventstoreMissionService.handleUnitAvailabilityChangedMessage(message)
        }
    }

    private fun createAvailabilityChangedMessage(
        availability: UnitAvailability,
        missionId: String,
        unitId: String,
        locationPayload: LocationPayload,
    ): Message<InboundUnitAvailablityChangedPayload> =
        MessageBuilder
            .withPayload(
                InboundUnitAvailablityChangedPayload(
                    // This has to be set here otherwise we need to have the exception anyway
                    unitId = unitId,
                    createdAt = Instant.now(),
                    unitAvailability = availability,
                    location = locationPayload,
                ),
            ).setHeader("mission_id", missionId)
            .build()

    private fun createMissionStateChangedMessage(mission: Mission): Message<InboundMissionStateChangedPayload> =
        MessageBuilder
            .withPayload(
                InboundMissionStateChangedPayload(
                    missionState = mission.state,
                    createdAt = Instant.now(),
                ),
            ).setHeader("mission_id", mission.missionId)
            .build()
}
