/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.repository

import io.lion5.smartcity.mongodb.model.AdhocJoinRequest
import io.lion5.smartcity.mongodb.model.Mission
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface AdhocJoinRequestRepository : MongoRepository<AdhocJoinRequest, String> {
    fun findByUserFirebaseIdAndMissionMissionId(
        userFirebaseId: String,
        missionId: String,
    ): AdhocJoinRequest?

    fun findFirstByUserFirebaseIdAndStatus(
        userFirebaseId: String,
        adhocJoinRequestStatus: AdhocJoinRequest.AdhocJoinRequestStatus,
    ): AdhocJoinRequest?

    fun findByAdhocJoinRequestId(adhocJoinRequestId: String): AdhocJoinRequest?

    fun findAllByStatus(adhocJoinRequestStatus: AdhocJoinRequest.AdhocJoinRequestStatus): List<AdhocJoinRequest>

    fun findAllByMissionAndStatus(
        mission: Mission,
        adhocJoinRequestStatus: AdhocJoinRequest.AdhocJoinRequestStatus,
    ): List<AdhocJoinRequest>

    fun findAllByMission(mission: Mission): List<AdhocJoinRequest>

    fun findFirstByUserFirebaseIdOrderByCreationTimeDesc(userFirebaseId: String): AdhocJoinRequest?
}
