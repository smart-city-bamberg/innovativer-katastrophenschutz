/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.model

import io.lion5.smartcity.mongodb.model.location.GKLocation
import io.lion5.smartcity.mongodb.model.location.GPSLocation
import jakarta.validation.constraints.Pattern
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.DocumentReference
import org.springframework.format.annotation.DateTimeFormat
import java.time.Instant

@Document
data class Mission(
    @Id
    @Indexed(unique = true)
    val missionId: String?,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val alarmDate: Instant,
//    val List<Alarmierung> Alarmierungen,
    val missionNr: String,
    val missionType: Int,
    val missionTypeName: String,
    val gpsLocation: GPSLocation,
    val gkLocation: GKLocation,
    val missionAddress: Address,
    @DocumentReference(lazy = true)
    val units: Set<EmergencyUnit> = emptySet(),
    val radioGroup: String,
    val state: State,
    val reports: Set<Report> = emptySet(),
    val commands: Set<Command> = emptySet(),
    val dangerZones: Set<DangerZone> = emptySet(),
    val sections: Set<Section> = emptySet(),
    val pois: Set<PointOfInterest> = emptySet(),
    /**
     * Schlagwort
     */
    val keyword: String,
    /**
     * Stichwort
     */
    val cues: String,
    @Pattern(regexp = "^[A-Za-z0-9]{4}-[A-Za-z0-9]{4}$", message = "Invalid joinCode format")
    @Indexed(unique = true)
    val joinCode: String = "",
    @Pattern(regexp = "^\\d{6}$", message = "joinPin must be 6 digits")
    val joinPin: String = "",
    @DocumentReference(lazy = true)
    val unitPositionTrace: Set<UnitPositionTrace> = emptySet(),
) : IAclDocument {
    enum class State {
        RUNNING,
        FINISHED,
    }

    // Required for ACL
    override fun getId(): String? = missionId
}
