/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.DocumentReference
import java.util.Date

/**
 * Represents a request to join as an anonymous adhoc user.
 */
@Document
data class AdhocJoinRequest(
    @Id
    @Indexed(unique = true)
    val adhocJoinRequestId: String?,
    @Indexed(unique = true)
    val userFirebaseId: String,
    val userName: String,
    val unitCallSign: String,
    val unitTacticalSignPath: String,
    val departmentName: String,
    @DocumentReference
    val mission: Mission,
    var status: AdhocJoinRequestStatus = AdhocJoinRequestStatus.PENDING,
    val creationTime: Date = Date(),
    val approvalTime: Date? = null,
    @DocumentReference
    val createdUnit: EmergencyUnit? = null,
) {
    enum class AdhocJoinRequestStatus {
        PENDING,
        APPROVED,
        REJECTED,
        EXPIRED,
        CANCELED,
        NOT_FOUND,
    }
}
