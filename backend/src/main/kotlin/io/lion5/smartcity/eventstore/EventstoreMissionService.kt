/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.eventstore

import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.auth.model.AuthorityRoles
import io.lion5.smartcity.eventstore.events.IMissionStreamRepository
import io.lion5.smartcity.eventstore.events.model.CommandEvent
import io.lion5.smartcity.eventstore.events.model.DangerZoneEvent
import io.lion5.smartcity.eventstore.events.model.DigimapEvent
import io.lion5.smartcity.eventstore.events.model.EventGPSLocation
import io.lion5.smartcity.eventstore.events.model.LocationUpdateEvent
import io.lion5.smartcity.eventstore.events.model.MissionEvent
import io.lion5.smartcity.eventstore.events.model.MissionEventType
import io.lion5.smartcity.eventstore.events.model.MissionSectionEvent
import io.lion5.smartcity.eventstore.events.model.PoiEvent
import io.lion5.smartcity.eventstore.events.model.ReportEvent
import io.lion5.smartcity.eventstore.events.model.UnitAvailabilityChangedEvent
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.repository.MissionRepository
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import io.lion5.smartcity.mqtt.MqttGateway
import io.lion5.smartcity.mqtt.payload.LocationPayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundCommandPayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundDangerZonePayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundMissionSectionPayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundMissionStateChangedPayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundPoiPayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundReportPayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundUnitAvailablityChangedPayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundUnitLocationPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundCommandPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundDangerZonePayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundMissionSectionPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundMissionStateChangedPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundPoiPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundReportPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundUnitAvailabilityChangedPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundUnitLocationPayload
import io.lion5.smartcity.util.exception.WrongEtagException
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.bson.types.ObjectId
import org.springframework.integration.annotation.ServiceActivator
import org.springframework.messaging.Message
import org.springframework.messaging.MessageHeaders
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

/**
 * This service handles all events in a mission that need to be stored to the eventstore.
 */
@Service
class EventstoreMissionService(
    private val missionRepository: MissionRepository,
    private val unitService: EmergencyUnitService,
    private val mqttGateway: MqttGateway,
    private val missionStreamRepository: IMissionStreamRepository,
) {
    private val log = KotlinLogging.logger {}

    @ServiceActivator(inputChannel = "locationInboundChannel")
    fun handleLocationUpdateMqttMessage(locationMessage: Message<InboundUnitLocationPayload>) {
        val senderUnitId =
            extractIdAfterInboundFromTopicString(locationMessage.headers["mqtt_receivedTopic"].toString())

        // FIXME: This is a workaround to fix the missing auth from our MQTT broker
        val authentication =
            UsernamePasswordAuthenticationToken("eventstore", null, listOf(SimpleGrantedAuthority("ROLE_${AuthorityRoles.EVENT_STORE}")))
        SecurityContextHolder.getContext().authentication = authentication

        // persist to eventstore -> This will then trigger the projection to mongodb
        // get current running mission where this unit is associated with
        val runningMissions =
            missionRepository.findMissionsByStateAndUnitsEquals(Mission.State.RUNNING, ObjectId(senderUnitId))

        // send the location update message to the mission location topic
        if (runningMissions.isNotEmpty()) {
            // TODO: for demo and testing purposes we can associate a unit with multiple running missions.
            // This should not be the case for a productive system but this would also need to be validated in the fontend when manually assigning units.
            // If there is a mission then it also has an ID since we are reading from Mongo
            runningMissions.forEach {
                val missionId = it.missionId!!

                var nextEtag = missionStreamRepository.getNextEtag(missionId)
                val outboundLocation: OutboundUnitLocationPayload
                runBlocking {
                    launch {
                        var success = false
                        var retries = 1
                        while (!success && retries <= 10) {
                            try {
                                success =
                                    missionStreamRepository.appendDigimapEventToMissionStream(
                                        LocationUpdateEvent(
                                            senderUnitId,
                                            EventGPSLocation(
                                                locationMessage.payload.location.latitude,
                                                locationMessage.payload.location.longitude,
                                            ),
                                            locationMessage.payload.timestamp,
                                            nextEtag,
                                        ),
                                        missionId,
                                    )
                            } catch (ex: WrongEtagException) {
                                log.warn(ex) {
                                    """Tried $retries times to write to Eventstore but did not succeed because of wrong etag.
                                                Some other operation has appended to the stream before we could.
                                                Trying again with etag + 1."""
                                }
                                retries = ++retries
                                nextEtag = ++nextEtag
                            }
                        }
                    }
                    outboundLocation =
                        OutboundUnitLocationPayload(
                            nextEtag.toInt(),
                            LocationPayload(
                                locationMessage.payload.location.latitude,
                                locationMessage.payload.location.longitude,
                            ),
                            senderUnitId,
                            locationMessage.payload.timestamp,
                        )
                }

                mqttGateway.publishLocationUpdate(outboundLocation, missionId)
            }
        } else {
            log.info { "No Mission associated with Unit $senderUnitId exists. Skip sending update" }
        }
    }

    @ServiceActivator(inputChannel = "reportInboundChannel")
    fun handleReportMqttMessage(reportMessage: Message<InboundReportPayload>) {
        val missionId = extractMissionIdFromMessageHeaders(reportMessage.headers)
        var nextEtag = missionStreamRepository.getNextEtag(missionId)
        val reportEvent = reportMessage.payload.toReportEvent(nextEtag)

        runBlocking {
            launch {
                var success = false
                var retries = 1
                while (!success && retries <= 10) {
                    try {
                        success = missionStreamRepository.appendDigimapEventToMissionStream(reportEvent, missionId)
                    } catch (ex: WrongEtagException) {
                        log.warn(ex) {
                            """Tried $retries times to write to Eventstore but did not succeed because of wrong etag.
                                        Some other operation has appended to the stream before we could.
                                        Trying again with etag + 1."""
                        }
                        retries = ++retries
                        nextEtag = ++nextEtag
                    }
                }
            }
        }
        val sender = unitService.getEmergencyUnitById(reportEvent.senderUnitId).get()
        val outboundMessage = reportEvent.toOutboundReportPayload(sender.callSign)

        mqttGateway.publishReport(outboundMessage, missionId)
    }

    @ServiceActivator(inputChannel = "commandInboundChannel")
    fun handleCommandMqttMessage(reportMessage: Message<InboundCommandPayload>) {
        val missionId = extractMissionIdFromMessageHeaders(reportMessage.headers)
        var nextEtag = missionStreamRepository.getNextEtag(missionId)
        val commandEvent = reportMessage.payload.toCommandEvent(nextEtag)

        runBlocking {
            launch {
                var success = false
                var retries = 1
                while (!success && retries <= 10) {
                    try {
                        success = missionStreamRepository.appendDigimapEventToMissionStream(commandEvent, missionId)
                    } catch (ex: WrongEtagException) {
                        log.warn(ex) {
                            """Tried $retries times to write to Eventstore but did not succeed because of wrong etag.
                                        Some other operation has appended to the stream before we could.
                                        Trying again with etag + 1."""
                        }
                        retries = ++retries
                        nextEtag = ++nextEtag
                    }
                }
            }
        }
        val sender = unitService.getEmergencyUnitById(commandEvent.senderUnitId).get()
        val outboundMessage = commandEvent.toOutboundCommandPayload(sender.callSign)

        mqttGateway.publishCommand(outboundMessage, missionId)
    }

    @ServiceActivator(inputChannel = "dangerZoneInboundChannel")
    fun handleDangerZoneMqttMessage(dangerZoneMessage: Message<InboundDangerZonePayload>) {
        val missionId = extractMissionIdFromMessageHeaders(dangerZoneMessage.headers)
        var nextEtag = missionStreamRepository.getNextEtag(missionId)
        val dangerZoneEvent = dangerZoneMessage.payload.toDangerZoneEvent(nextEtag)

        runBlocking {
            launch {
                var success = false
                var retries = 1
                while (!success && retries <= 10) {
                    try {
                        success = missionStreamRepository.appendDigimapEventToMissionStream(dangerZoneEvent, missionId)
                    } catch (ex: WrongEtagException) {
                        log.warn(ex) {
                            """Tried $retries times to write to Eventstore but did not succeed because of wrong etag.
                                        Some other operation has appended to the stream before we could.
                                        Trying again with etag + 1."""
                        }
                        retries = ++retries
                        nextEtag = ++nextEtag
                    }
                }
            }
        }
        val outboundMessage = dangerZoneEvent.toOutboundDangerZonePayload()

        mqttGateway.publishDangerZone(outboundMessage, missionId)
    }

    @ServiceActivator(inputChannel = "missionSectionInboundChannel")
    fun handleMissionSectionMqttMessage(missionSectionMessage: Message<InboundMissionSectionPayload>) {
        val missionId = extractMissionIdFromMessageHeaders(missionSectionMessage.headers)
        var nextEtag = missionStreamRepository.getNextEtag(missionId)
        val missionSectionEvent = missionSectionMessage.payload.toMissionSectionEvent(nextEtag)

        runBlocking {
            launch {
                var success = false
                var retries = 1
                while (!success && retries <= 10) {
                    try {
                        success =
                            missionStreamRepository.appendDigimapEventToMissionStream(missionSectionEvent, missionId)
                    } catch (ex: WrongEtagException) {
                        log.warn(ex) {
                            """Tried $retries times to write to Eventstore but did not succeed because of wrong etag.
                                        Some other operation has appended to the stream before we could.
                                        Trying again with etag + 1."""
                        }
                        retries = ++retries
                        nextEtag = ++nextEtag
                    }
                }
            }
        }
        val outboundMessage = missionSectionEvent.toOutboundMissionSectionPayload()
        mqttGateway.publishMissionSection(outboundMessage, missionId)
    }

    @ServiceActivator(inputChannel = "missionStateChangedInboundChannel")
    fun handleMissionStateChangedMessage(missionStateChangedMessage: Message<InboundMissionStateChangedPayload>) {
        val missionId = extractMissionIdFromMessageHeaders(missionStateChangedMessage.headers)
        var nextEtag = missionStreamRepository.getNextEtag(missionId)

        val missionStateChangeEvent =
            MissionEvent(
                eventType = MissionEventType.FINISHED,
                timestamp = missionStateChangedMessage.payload.createdAt,
                eTag = nextEtag,
            )

        val outboundMissionStateChangedPayload: OutboundMissionStateChangedPayload
        runBlocking {
            launch {
                var success = false
                var retries = 1
                while (!success && retries <= 10) {
                    try {
                        success =
                            missionStreamRepository.appendDigimapEventToMissionStream(
                                missionStateChangeEvent,
                                missionId,
                            )
                    } catch (ex: WrongEtagException) {
                        log.warn(ex) {
                            """Tried $retries times to write to Eventstore but did not succeed because of wrong etag.
                                        Some other operation has appended to the stream before we could.
                                        Trying again with etag + 1."""
                        }
                        retries = ++retries
                        nextEtag = ++nextEtag
                    }
                }
            }

            val missionState =
                if (missionStateChangeEvent.eventType == MissionEventType.FINISHED) Mission.State.FINISHED else Mission.State.RUNNING

            outboundMissionStateChangedPayload =
                OutboundMissionStateChangedPayload(
                    missionState = missionState,
                    eTag = missionStateChangeEvent.eTag.toInt(),
                    createdAt = missionStateChangeEvent.timestamp,
                )
        }
        mqttGateway.publishMissionStateChanged(outboundMissionStateChangedPayload, missionId)
    }

    @ServiceActivator(inputChannel = "missionInboundChannel")
    fun handleUnitAvailabilityChangedMessage(unitAvailabilityChangedMessage: Message<InboundUnitAvailablityChangedPayload>) {
        val missionId = extractMissionIdFromMessageHeaders(unitAvailabilityChangedMessage.headers)
        var nextEtag = missionStreamRepository.getNextEtag(missionId)

        val emergencyUnit = unitService.getEmergencyUnitById(unitAvailabilityChangedMessage.payload.unitId).get()
        val unitAvailabilityChangedEvent =
            UnitAvailabilityChangedEvent(
                location =
                    EventGPSLocation(
                        unitAvailabilityChangedMessage.payload.location?.latitude ?: emergencyUnit.gpsLocation.latitude,
                        unitAvailabilityChangedMessage.payload.location?.longitude ?: emergencyUnit.gpsLocation.longitude,
                    ),
                eTag = nextEtag,
                timestamp = unitAvailabilityChangedMessage.payload.createdAt,
                unitAvailability = unitAvailabilityChangedMessage.payload.unitAvailability,
                unitId = unitAvailabilityChangedMessage.payload.unitId,
            )

        val outboundUnitAvailabilityChangedPayload: OutboundUnitAvailabilityChangedPayload
        runBlocking {
            launch {
                var success = false
                var retries = 1
                while (!success && retries <= 10) {
                    try {
                        success =
                            missionStreamRepository.appendDigimapEventToMissionStream(
                                unitAvailabilityChangedEvent,
                                missionId,
                            )
                    } catch (ex: WrongEtagException) {
                        log.warn(ex) {
                            """Tried $retries times to write to Eventstore but did not succeed because of wrong etag.
                                        Some other operation has appended to the stream before we could.
                                        Trying again with etag + 1."""
                        }
                        retries = ++retries
                        nextEtag = ++nextEtag
                    }
                }
            }
            outboundUnitAvailabilityChangedPayload =
                OutboundUnitAvailabilityChangedPayload(
                    eTag = unitAvailabilityChangedEvent.eTag.toInt(),
                    unitAvailability = unitAvailabilityChangedEvent.unitAvailability,
                    createdAt = unitAvailabilityChangedEvent.timestamp,
                    unitId = unitAvailabilityChangedEvent.unitId,
                    location =
                        LocationPayload(
                            unitAvailabilityChangedEvent.location.latitude,
                            unitAvailabilityChangedEvent.location.longitude,
                        ),
                    callSign = emergencyUnit.callSign,
                    tacticalSign = emergencyUnit.tacticalSignPath,
                )
        }
        mqttGateway.publishUnitAvailabilityChanged(outboundUnitAvailabilityChangedPayload, missionId)
    }

    @ServiceActivator(inputChannel = "poiInboundChannel")
    fun handlePoiMqttMessage(poiMessage: Message<InboundPoiPayload>) {
        val missionId = extractMissionIdFromMessageHeaders(poiMessage.headers)
        var nextEtag = missionStreamRepository.getNextEtag(missionId)
        val poiEvent = poiMessage.payload.toPoiEvent(nextEtag)

        runBlocking {
            launch {
                var success = false
                var retries = 1
                while (!success && retries <= 10) {
                    try {
                        success =
                            missionStreamRepository.appendDigimapEventToMissionStream(poiEvent, missionId)
                    } catch (ex: WrongEtagException) {
                        log.warn(ex) {
                            """Tried $retries times to write to Eventstore but did not succeed because of wrong etag.
                                        Some other operation has appended to the stream before we could.
                                        Trying again with etag + 1."""
                        }
                        retries = ++retries
                        nextEtag = ++nextEtag
                    }
                }
            }
        }
        val outboundMessage = poiEvent.toOutboundPoiPayload()
        mqttGateway.publishPoi(outboundMessage, missionId)
    }

    private fun extractMissionIdFromMessageHeaders(messageHeaders: MessageHeaders): String =
        if (messageHeaders.containsKey("mission_id")) {
            messageHeaders["mission_id"].toString()
        } else {
            extractIdAfterInboundFromTopicString(messageHeaders["mqtt_receivedTopic"].toString())
        }

    private fun extractIdAfterInboundFromTopicString(topicString: String): String {
        val stringList = topicString.split("/")
        return stringList[stringList.indexOf("inbound") + 1]
    }

    private fun ReportEvent.toOutboundReportPayload(senderCallSign: String): OutboundReportPayload =
        OutboundReportPayload(
            eTag = eTag.toInt(),
            report = report,
            tacticalSignPath = tacticalSignPath,
            senderUnitId = senderUnitId,
            images = images,
            referencingCommandId = referencingCommandId,
            location = location,
            createdAt = timestamp,
            reportId = reportId,
            senderCallSign = senderCallSign,
        )

    private fun InboundReportPayload.toReportEvent(eTag: Long): ReportEvent =
        ReportEvent(
            report = report,
            eTag = eTag,
            location = location,
            timestamp = createdAt,
            images = images,
            receiverUnitIds = receiverUnitIds,
            referencingCommandId = referencingCommandId,
            senderUnitId = senderUnitId,
            tacticalSignPath = tacticalSignPath,
            reportId = reportId,
        )

    private fun CommandEvent.toOutboundCommandPayload(senderCallSign: String): OutboundCommandPayload =
        OutboundCommandPayload(
            commandId = commandId,
            commandText = commandText,
            senderUnitId = senderUnitId,
            receiverUnitIds = receiverUnitIds,
            eTag = eTag.toInt(),
            createdAt = timestamp,
            senderCallSign = senderCallSign,
            referencingReportId = referencingReportId,
        )

    private fun InboundCommandPayload.toCommandEvent(eTag: Long): CommandEvent =
        CommandEvent(
            timestamp = createdAt,
            eTag = eTag,
            senderUnitId = senderUnitId,
            commandText = commandText,
            receiverUnitIds = receiverUnitIds,
            commandId = commandId,
            referencingReportId = referencingReportId,
        )

    fun readEventsFromStreamForMission(missionId: String): List<DigimapEvent> =
        missionStreamRepository.readEventsFromStreamForMission(missionId)

    private fun InboundDangerZonePayload.toDangerZoneEvent(eTag: Long): DangerZoneEvent =
        DangerZoneEvent(
            eTag = eTag,
            dangerZoneId = dangerZoneId,
            timestamp = createdAt,
            geoJson = geoJson,
            operation = operation,
            senderUnitId = senderUnitId,
        )

    private fun DangerZoneEvent.toOutboundDangerZonePayload(): OutboundDangerZonePayload =
        OutboundDangerZonePayload(
            dangerZoneId = dangerZoneId,
            operation = operation,
            geoJson = geoJson,
            eTag = eTag.toInt(),
            createdAt = timestamp,
        )

    private fun InboundMissionSectionPayload.toMissionSectionEvent(eTag: Long): MissionSectionEvent =
        MissionSectionEvent(
            sectionId = sectionId,
            senderUnitId = senderUnitId,
            timestamp = createdAt,
            eTag = eTag,
            geoJson = geoJson,
            operation = operation,
        )

    private fun MissionSectionEvent.toOutboundMissionSectionPayload(): OutboundMissionSectionPayload =
        OutboundMissionSectionPayload(
            sectionId = sectionId,
            geoJson = geoJson,
            createdAt = timestamp,
            eTag = eTag.toInt(),
            operation = operation,
        )

    private fun InboundPoiPayload.toPoiEvent(eTag: Long): PoiEvent =
        PoiEvent(
            details = details,
            eTag = eTag,
            timestamp = createdAt,
            poiId = poiId,
            senderUnitId = senderUnitId,
            tacticalSignPath = tacticalSignPath,
            operation = operation,
            location = EventGPSLocation(location.latitude, location.longitude),
        )

    private fun PoiEvent.toOutboundPoiPayload(): OutboundPoiPayload =
        OutboundPoiPayload(
            poiId = poiId,
            operation = operation,
            tacticalSignPath = tacticalSignPath,
            senderUnitId = senderUnitId,
            eTag = eTag.toInt(),
            details = details,
            createdAt = timestamp,
            location = LocationPayload(location.latitude, location.longitude),
        )
}
