/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.eventstore.subscription

import com.eventstore.dbclient.CreatePersistentSubscriptionToStreamOptions
import com.eventstore.dbclient.EventStoreDBPersistentSubscriptionsClient
import com.eventstore.dbclient.PersistentSubscription
import com.eventstore.dbclient.PersistentSubscriptionListener
import com.eventstore.dbclient.ResolvedEvent
import com.fasterxml.jackson.databind.ObjectMapper
import io.github.oshai.kotlinlogging.KotlinLogging
import org.springframework.context.ApplicationEventPublisher
import org.springframework.retry.support.RetryTemplate

abstract class AbstractPersistentEventSubscription(
    private val eventStorePersistentSubscriptionsClient: EventStoreDBPersistentSubscriptionsClient,
    private val eventPublisher: ApplicationEventPublisher,
    private val objectMapper: ObjectMapper,
    private val subscriptionGroupName: String,
) : PersistentSubscriptionListener() {
    var isRunning = false
        protected set
    protected val log = KotlinLogging.logger {}
    protected var persistentSubscription: PersistentSubscription? = null

    init {
        // create subscription group if it does not exist create it.
        eventStorePersistentSubscriptionsClient.createToStream(
            LocationEventSubscription.LOCATION_EVENT_STREAM,
            subscriptionGroupName,
            CreatePersistentSubscriptionToStreamOptions.get().resolveLinkTos(true).fromStart(),
        )
    }

    protected val retryTemplate =
        RetryTemplate
            .builder()
            .infiniteRetry()
            .exponentialBackoff(100, 2.0, 5000)
            .build()

    abstract fun start()

    abstract fun stop()

    abstract override fun onEvent(
        subscription: PersistentSubscription,
        retryCount: Int,
        event: ResolvedEvent,
    )
}
