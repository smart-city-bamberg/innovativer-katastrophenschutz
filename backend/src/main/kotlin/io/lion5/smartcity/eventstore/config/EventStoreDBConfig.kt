/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.eventstore.config

import com.eventstore.dbclient.ConnectionStringParsingException
import com.eventstore.dbclient.EventStoreDBClient
import com.eventstore.dbclient.EventStoreDBClientSettings
import com.eventstore.dbclient.EventStoreDBConnectionString
import com.eventstore.dbclient.EventStoreDBPersistentSubscriptionsClient
import com.fasterxml.jackson.databind.ObjectMapper
import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.eventstore.EventStoreDBSubscriptionBackgroundWorker
import io.lion5.smartcity.eventstore.events.EventStoreDBMissionStreamRepository
import io.lion5.smartcity.eventstore.events.IMissionStreamRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope

/**
 * Configure settings and access to EventStore DB
 */
@Configuration
class EventStoreDBConfig(
    @Value("\${digimap.eventstore.url}") private val connectionString: String,
    @Value("\${digimap.eventstore.streamPrefixes.mission}") private val missionStreamPrefix: String,
    @Value("\${digimap.eventstore.subscriptionGroup}") private val subscriptionGroupName: String,
) {
    @Autowired
    private lateinit var applicationEventPublisher: ApplicationEventPublisher

    @Autowired
    private lateinit var objectMapper: ObjectMapper
    private val log = KotlinLogging.logger {}

    private val settings: EventStoreDBClientSettings =
        try {
            EventStoreDBConnectionString.parseOrThrow(connectionString)
        } catch (e: ConnectionStringParsingException) {
            log.error(e) { "Could not parse EventstoreDB connectionString -> You configured it wrong! Check application.yaml or env vars" }
            throw RuntimeException(e)
        }

    @Bean
    @Scope("singleton")
    fun eventStoreDBClient(): EventStoreDBClient = EventStoreDBClient.create(settings)

    @Bean
    @Scope("singleton")
    fun eventStoreDBPersistentSubscriptionClient(): EventStoreDBPersistentSubscriptionsClient =
        EventStoreDBPersistentSubscriptionsClient.create(settings)

    @Bean
    fun locationEventRepository(eventStore: EventStoreDBClient): IMissionStreamRepository =
        EventStoreDBMissionStreamRepository(
            eventStore,
            objectMapper,
            missionStreamPrefix,
        )

    @Bean
    fun eventStoreDBSubscriptionBackgroundWorker(
        eventStore: EventStoreDBClient,
        eventStorePersistentSubscriptionsClient: EventStoreDBPersistentSubscriptionsClient,
    ): EventStoreDBSubscriptionBackgroundWorker =
        EventStoreDBSubscriptionBackgroundWorker(
            eventStorePersistentSubscriptionsClient,
            applicationEventPublisher,
            objectMapper,
            subscriptionGroupName,
        )
}
