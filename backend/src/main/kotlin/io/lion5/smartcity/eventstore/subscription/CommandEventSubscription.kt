/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.eventstore.subscription

import com.eventstore.dbclient.CreatePersistentSubscriptionToStreamOptions
import com.eventstore.dbclient.EventStoreDBPersistentSubscriptionsClient
import com.eventstore.dbclient.NackAction
import com.eventstore.dbclient.PersistentSubscription
import com.eventstore.dbclient.ResolvedEvent
import com.fasterxml.jackson.databind.ObjectMapper
import io.lion5.smartcity.eventstore.events.model.CommandEvent
import io.lion5.smartcity.util.ApplicationEventEnvelope
import io.lion5.smartcity.util.exception.SubscriptionException
import org.springframework.context.ApplicationEventPublisher
import org.springframework.retry.RetryContext

class CommandEventSubscription(
    private val eventStorePersistentSubscriptionsClient: EventStoreDBPersistentSubscriptionsClient,
    private val eventPublisher: ApplicationEventPublisher,
    private val objectMapper: ObjectMapper,
    private val subscriptionGroupName: String,
) : AbstractPersistentEventSubscription(
        eventStorePersistentSubscriptionsClient,
        eventPublisher,
        objectMapper,
        subscriptionGroupName,
    ) {
    companion object {
        const val COMMAND_EVENT_STREAM = "\$et-CommandEvent"
    }

    init {
        // create subscription group if it does not exist create it.
        eventStorePersistentSubscriptionsClient.createToStream(
            COMMAND_EVENT_STREAM,
            subscriptionGroupName,
            CreatePersistentSubscriptionToStreamOptions.get().resolveLinkTos(true).fromStart(),
        )
    }

    override fun start() {
        try {
            // try to subscribe to the stream
            retryTemplate.execute<Any?, java.lang.Exception> { _: RetryContext ->
                persistentSubscription =
                    eventStorePersistentSubscriptionsClient
                        .subscribeToStream(
                            COMMAND_EVENT_STREAM,
                            subscriptionGroupName,
                            this,
                        ).get()
                log.info { "Subscribing to all ${persistentSubscription?.subscriptionId}" }
                null
            }
        } catch (ex: Exception) {
            throw SubscriptionException("Error while starting subscription", ex)
        }
        isRunning = true
    }

    override fun stop() {
        if (isRunning) {
            persistentSubscription?.stop()
            isRunning = false
            log.debug { "Stopped location event subscription" }
        }
    }

    override fun onEvent(
        subscription: PersistentSubscription,
        retryCount: Int,
        event: ResolvedEvent,
    ) {
        log.debug {
            "CommandSubscriptionListener received event ${event.originalEvent.revision}@${event.originalEvent.streamId} " +
                "that references ${event.event.revision}@${event.event.streamId}"
        }
        try {
            val json = String(event.event.eventData, Charsets.UTF_8)
            val commandEvent = objectMapper.reader().readValue(json, CommandEvent::class.java)
            eventPublisher.publishEvent(
                ApplicationEventEnvelope<CommandEvent>(
                    commandEvent,
                    event.event.streamId,
                ),
            )
            subscription.ack(event)
        } catch (ex: Exception) {
            // we retry 5 times to process the message. If it still fails the message gets parked for debugging
            log.error(ex) { "Could not process CommandEvent for the $retryCount time." }
            val reason = ex.message ?: "Runtime Exception occurred while processing this event"
            if (retryCount <= 5) {
                subscription.nack(NackAction.Retry, reason, event)
            } else {
                subscription.nack(NackAction.Park, reason, event)
            }
        }
    }
}
