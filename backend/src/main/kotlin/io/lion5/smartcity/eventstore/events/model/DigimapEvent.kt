/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.eventstore.events.model

import java.time.Instant

/**
 * Interface for all events that get stored to the Eventstore DB
 */
interface DigimapEvent {
    /**
     * When this event was created by the client.
     */
    val timestamp: Instant
    val eTag: Long
}

/**
 * Interface for all events that are created while a mission is live and in process.
 *
 */
interface UnitEvent : DigimapEvent {
    val senderUnitId: String
}

/**
 * Interface for all audit events. Those events are created when a user preforms CRUD operations
 * on any of the mission, unit or user data.
 */
interface AuditEvent : DigimapEvent {
    val userId: String
}
