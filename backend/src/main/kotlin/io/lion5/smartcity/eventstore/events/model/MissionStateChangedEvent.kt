package io.lion5.smartcity.eventstore.events.model

import io.lion5.smartcity.mongodb.model.Mission
import org.springframework.format.annotation.DateTimeFormat
import java.time.Instant

data class MissionStateChangedEvent(
    val missionState: Mission.State,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    override val timestamp: Instant,
    override val eTag: Long,
) : DigimapEvent
