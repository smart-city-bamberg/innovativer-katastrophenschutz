/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.eventstore

import com.eventstore.dbclient.EventStoreDBPersistentSubscriptionsClient
import com.fasterxml.jackson.databind.ObjectMapper
import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.eventstore.subscription.CommandEventSubscription
import io.lion5.smartcity.eventstore.subscription.DangerZoneEventSubscription
import io.lion5.smartcity.eventstore.subscription.LocationEventSubscription
import io.lion5.smartcity.eventstore.subscription.MissionSectionEventSubscription
import io.lion5.smartcity.eventstore.subscription.PoiEventSubscription
import io.lion5.smartcity.eventstore.subscription.ReportEventSubscription
import io.lion5.smartcity.eventstore.subscription.UnitAvailabilityChangedEventSubscription
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.SmartLifecycle

/**
 * A worker managing the lifecycle for all the subscriptions to the eventstore db.
 *
 * TODO: Refactor to more elegant subscription handling
 */
class EventStoreDBSubscriptionBackgroundWorker(
    private val eventStorePersistentSubscriptionsClient: EventStoreDBPersistentSubscriptionsClient,
    private val applicationEventPublisher: ApplicationEventPublisher,
    private val objectMapper: ObjectMapper,
    private val subscriptionGroupName: String,
) : SmartLifecycle {
    private var locationEventSubscription =
        LocationEventSubscription(
            eventStorePersistentSubscriptionsClient,
            applicationEventPublisher,
            objectMapper,
            subscriptionGroupName,
        )
    private var reportEventSubscription =
        ReportEventSubscription(
            eventStorePersistentSubscriptionsClient,
            applicationEventPublisher,
            objectMapper,
            subscriptionGroupName,
        )
    private var commandEventSubscription =
        CommandEventSubscription(
            eventStorePersistentSubscriptionsClient,
            applicationEventPublisher,
            objectMapper,
            subscriptionGroupName,
        )

    private var dangerZoneEventSubscription =
        DangerZoneEventSubscription(
            eventStorePersistentSubscriptionsClient,
            applicationEventPublisher,
            objectMapper,
            subscriptionGroupName,
        )

    private var missionSectionEventSubscription =
        MissionSectionEventSubscription(
            eventStorePersistentSubscriptionsClient,
            applicationEventPublisher,
            objectMapper,
            subscriptionGroupName,
        )

    private var unitAvailabilityChangedEventSubscription =
        UnitAvailabilityChangedEventSubscription(
            eventStorePersistentSubscriptionsClient,
            applicationEventPublisher,
            objectMapper,
            subscriptionGroupName,
        )
    private var poiEventSubscription =
        PoiEventSubscription(
            eventStorePersistentSubscriptionsClient,
            applicationEventPublisher,
            objectMapper,
            subscriptionGroupName,
        )
    private val log = KotlinLogging.logger {}

    override fun start() {
        log.debug { "EventStoreDBSubscriptionBackgroundWorker started" }
        try {
            locationEventSubscription =
                LocationEventSubscription(
                    eventStorePersistentSubscriptionsClient,
                    applicationEventPublisher,
                    objectMapper,
                    subscriptionGroupName,
                )
            locationEventSubscription.start()
        } catch (e: Throwable) {
            log.error(e) { "Failed to start subscription to to all location events from Eventstore" }
        }
        try {
            reportEventSubscription =
                ReportEventSubscription(
                    eventStorePersistentSubscriptionsClient,
                    applicationEventPublisher,
                    objectMapper,
                    subscriptionGroupName,
                )
            reportEventSubscription.start()
        } catch (e: Throwable) {
            log.error(e) { "Failed to start subscription to to all report events from Eventstore" }
        }
        try {
            commandEventSubscription =
                CommandEventSubscription(
                    eventStorePersistentSubscriptionsClient,
                    applicationEventPublisher,
                    objectMapper,
                    subscriptionGroupName,
                )
            commandEventSubscription.start()
        } catch (e: Throwable) {
            log.error(e) { "Failed to start subscription to to all command events from Eventstore" }
        }
        try {
            dangerZoneEventSubscription =
                DangerZoneEventSubscription(
                    eventStorePersistentSubscriptionsClient,
                    applicationEventPublisher,
                    objectMapper,
                    subscriptionGroupName,
                )
            dangerZoneEventSubscription.start()
        } catch (e: Throwable) {
            log.error(e) { "Failed to start subscription to to all DangerZone events from Eventstore" }
        }
        try {
            missionSectionEventSubscription =
                MissionSectionEventSubscription(
                    eventStorePersistentSubscriptionsClient,
                    applicationEventPublisher,
                    objectMapper,
                    subscriptionGroupName,
                )
            missionSectionEventSubscription.start()
        } catch (e: Throwable) {
            log.error(e) { "Failed to start subscription to to all MissionSection events from Eventstore" }
        }
        try {
            unitAvailabilityChangedEventSubscription =
                UnitAvailabilityChangedEventSubscription(
                    eventStorePersistentSubscriptionsClient,
                    applicationEventPublisher,
                    objectMapper,
                    subscriptionGroupName,
                )
            unitAvailabilityChangedEventSubscription.start()
        } catch (e: Throwable) {
            log.error(e) { "Failed to start subscription to to all AvailabilityChanged events from Eventstore" }
        }
        try {
            poiEventSubscription =
                PoiEventSubscription(
                    eventStorePersistentSubscriptionsClient,
                    applicationEventPublisher,
                    objectMapper,
                    subscriptionGroupName,
                )
            poiEventSubscription.start()
        } catch (e: Throwable) {
            log.error(e) { "Failed to start subscription to to all POI events from Eventstore" }
        }
    }

    override fun stop() = stop {}

    override fun isRunning(): Boolean = locationEventSubscription.isRunning

    override fun stop(callback: Runnable) {
        if (isRunning) {
            log.debug { "EventStoreDBSubscriptionBackgroundWorker stopped" }
            locationEventSubscription.stop()
            reportEventSubscription.stop()
            commandEventSubscription.stop()
            dangerZoneEventSubscription.stop()
            missionSectionEventSubscription.stop()
            unitAvailabilityChangedEventSubscription.stop()
            poiEventSubscription.stop()
            callback.run()
        }
    }

    override fun getPhase(): Int = Int.MAX_VALUE

    override fun isAutoStartup(): Boolean = true
}
