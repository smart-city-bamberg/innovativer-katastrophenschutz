/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.eventstore.events

import io.lion5.smartcity.eventstore.events.model.DigimapEvent

/**
 * Interface for writing and reading events to a mission stream from eventstoreDB
 */
interface IMissionStreamRepository {
    /**
     * Query a mission stream for the current eTag
     *
     * @return latest eTag written to this repository. Returns 0 if the stream does not exist
     */
    fun getCurrentEtag(missionId: String): Long

    fun getNextEtag(missionId: String): Long = getCurrentEtag(missionId) + 1

    /**
     * Write unit event to the eventstore topic of the mission
     *
     * @param digimapEvent event that will be appended to the event stream
     * @param missionId the missionId of the mission of which event stream the event shall be appended to
     * @return true if append was successful
     */
    suspend fun appendDigimapEventToMissionStream(
        digimapEvent: DigimapEvent,
        missionId: String,
    ): Boolean

    fun createNewStreamForMission(missionId: String): Boolean

    fun readEventsFromStreamForMission(missionId: String): List<DigimapEvent>
}
