/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.eventstore.events

import com.eventstore.dbclient.AppendToStreamOptions
import com.eventstore.dbclient.EventData
import com.eventstore.dbclient.EventStoreDBClient
import com.eventstore.dbclient.ExpectedRevision
import com.eventstore.dbclient.ReadResult
import com.eventstore.dbclient.ReadStreamOptions
import com.eventstore.dbclient.StreamNotFoundException
import com.eventstore.dbclient.WrongExpectedVersionException
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.TypeAdapterFactory
import com.google.gson.reflect.TypeToken
import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.eventstore.events.model.CommandEvent
import io.lion5.smartcity.eventstore.events.model.DangerZoneEvent
import io.lion5.smartcity.eventstore.events.model.DigimapEvent
import io.lion5.smartcity.eventstore.events.model.LocationUpdateEvent
import io.lion5.smartcity.eventstore.events.model.MissionEvent
import io.lion5.smartcity.eventstore.events.model.MissionEventType
import io.lion5.smartcity.eventstore.events.model.MissionSectionEvent
import io.lion5.smartcity.eventstore.events.model.PoiEvent
import io.lion5.smartcity.eventstore.events.model.ReportEvent
import io.lion5.smartcity.eventstore.events.model.UnitAvailabilityChangedEvent
import io.lion5.smartcity.util.exception.WrongEtagException
import java.time.Instant
import java.util.concurrent.ExecutionException

class EventStoreDBMissionStreamRepository(
    private val eventStore: EventStoreDBClient,
    private val objectMapper: ObjectMapper,
    private val missionStreamPrefix: String,
) : IMissionStreamRepository {
    private val log = KotlinLogging.logger {}

    override fun getCurrentEtag(missionId: String): Long {
        val readAllOptions =
            ReadStreamOptions
                .get()
                .backwards()
                .fromEnd()
                .maxCount(1)
        try {
            val result = eventStore.readStream("$missionStreamPrefix$missionId", readAllOptions).get()
            return result.lastStreamPosition
        } catch (ex: ExecutionException) {
            if (ex.cause is StreamNotFoundException) {
                // If the stream does not exist the next eTag is 0
                log.info { "Stream does not exist: $" }
            }
        }
        return 0
    }

    override suspend fun appendDigimapEventToMissionStream(
        digimapEvent: DigimapEvent,
        missionId: String,
    ): Boolean {
        val eventData = buildEventDataFromDigimapEvent(digimapEvent)
        val options = AppendToStreamOptions.get().expectedRevision(digimapEvent.eTag - 1)
        val completableFuture = eventStore.appendToStream("$missionStreamPrefix$missionId", options, eventData)

        try {
            completableFuture.get()
            if (completableFuture.isCompletedExceptionally) {
                log.error { "massive error" }
                return false
            }
        } catch (ex: ExecutionException) {
            if (ex.cause is WrongExpectedVersionException) {
                throw WrongEtagException(
                    "Could not write to eventstream $missionStreamPrefix$missionId because message with eTag already exists.",
                    ex.cause as WrongExpectedVersionException,
                )
            }
        }
        return true
    }

    override fun createNewStreamForMission(missionId: String): Boolean {
        val missionEvent = MissionEvent(Instant.now(), MissionEventType.START, 0)
        val eventData = buildEventDataFromMissionEvent(missionEvent)
        val options = AppendToStreamOptions.get().expectedRevision(ExpectedRevision.noStream())
        val completableFuture = eventStore.appendToStream("$missionStreamPrefix$missionId", options, eventData)

        try {
            completableFuture.get()
            if (completableFuture.isCompletedExceptionally) {
                log.error { "massive error" }
                return false
            }
        } catch (ex: ExecutionException) {
            if (ex.cause is WrongExpectedVersionException) {
                throw WrongEtagException(
                    "Could not write to eventstream $missionStreamPrefix$missionId because message with eTag already exists.",
                    ex.cause as WrongExpectedVersionException,
                )
            }
        }
        return true
    }

    private fun buildEventDataFromMissionEvent(missionEvent: MissionEvent): EventData {
        val json = toJson(missionEvent)
        return EventData.builderAsBinary("MissionEvent", json.toByteArray()).build()
    }

    private fun buildEventDataFromDigimapEvent(digimapEvent: DigimapEvent): EventData {
        var type = "NONE"
        when (digimapEvent) {
            is LocationUpdateEvent -> type = "LocationUpdateEvent"
            is ReportEvent -> type = "ReportEvent"
            is CommandEvent -> type = "CommandEvent"
            is DangerZoneEvent -> type = "DangerZoneEvent"
            is MissionSectionEvent -> type = "MissionSectionEvent"
            is UnitAvailabilityChangedEvent -> type = "UnitAvailabilityChangedEvent"
            is PoiEvent -> type = "PoiEvent"
            is MissionEvent -> type = "MissionEvent"
        }

        val json = toJson(digimapEvent)
        return EventData.builderAsBinary(type, json.toByteArray()).build()
    }

    private fun toJson(digimapEvent: DigimapEvent): String {
        // we need to do explicit JSON marshalling since the EventData Builder cannot convert java.time.Instant correctly.
        // So we use the default ObjectMapper provided by Spring
        return try {
            objectMapper.writer().writeValueAsString(digimapEvent)
        } catch (ex: JsonProcessingException) {
            log.error(ex) { "${"failed conversion: $digimapEvent object to Json"}" }
            throw ex
        }
    }

    override fun readEventsFromStreamForMission(missionId: String): List<DigimapEvent> {
        val options =
            ReadStreamOptions
                .get()
                .forwards()
                .fromStart()

        val result: ReadResult = eventStore.readStream("$missionStreamPrefix$missionId", options).get()
        val gson = GsonBuilder().registerTypeAdapterFactory(InstantTypeAdapterFactory()).create()
        val streamEventList = arrayListOf<DigimapEvent>()
        for (resolvedEvent in result.events) {
            val recordedEvent = resolvedEvent.originalEvent
            when (recordedEvent.eventType) {
                "LocationUpdateEvent" ->
                    streamEventList.add(
                        gson.fromJson(String(recordedEvent.eventData, Charsets.UTF_8), LocationUpdateEvent::class.java),
                    )

                "ReportEvent" ->
                    streamEventList.add(
                        gson.fromJson(String(recordedEvent.eventData, Charsets.UTF_8), ReportEvent::class.java),
                    )

                "CommandEvent" ->
                    streamEventList.add(
                        gson.fromJson(String(recordedEvent.eventData, Charsets.UTF_8), CommandEvent::class.java),
                    )

                "MissionEvent" ->
                    streamEventList.add(
                        gson.fromJson(String(recordedEvent.eventData, Charsets.UTF_8), MissionEvent::class.java),
                    )

                "PoiEvent" ->
                    streamEventList.add(
                        gson.fromJson(String(recordedEvent.eventData, Charsets.UTF_8), PoiEvent::class.java),
                    )

                "UnitAvailabilityChangedEvent" ->
                    streamEventList.add(
                        gson.fromJson(
                            String(recordedEvent.eventData, Charsets.UTF_8),
                            UnitAvailabilityChangedEvent::class.java,
                        ),
                    )

                "MissionSectionEvent" ->
                    streamEventList.add(
                        gson.fromJson(String(recordedEvent.eventData, Charsets.UTF_8), MissionSectionEvent::class.java),
                    )

                "DangerZoneEvent" ->
                    streamEventList.add(
                        gson.fromJson(String(recordedEvent.eventData, Charsets.UTF_8), DangerZoneEvent::class.java),
                    )
            }
        }
        return streamEventList
    }
}

class InstantTypeAdapter : TypeAdapter<Instant>() {
    override fun write(
        out: com.google.gson.stream.JsonWriter?,
        value: Instant?,
    ) {
        if (value != null) {
            out!!.value(value.toString())
        } else {
            out!!.nullValue()
        }
    }

    override fun read(`in`: com.google.gson.stream.JsonReader?): Instant = Instant.parse(`in`!!.nextString())
}

class InstantTypeAdapterFactory : TypeAdapterFactory {
    override fun <T : Any?> create(
        gson: Gson,
        type: TypeToken<T>,
    ): TypeAdapter<T>? {
        if (type.rawType == Instant::class.java) {
            return InstantTypeAdapter() as TypeAdapter<T>
        }
        return null
    }
}
