/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.http

import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.http.dto.EdiMissionRequest
import io.lion5.smartcity.mongodb.model.Address
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.model.location.GKLocation
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import io.lion5.smartcity.mongodb.service.MissionService
import io.lion5.smartcity.util.LocationConverterService
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder

@RestController
class EdiController(
    val missionService: MissionService,
    val unitService: EmergencyUnitService,
    val locationConverterService: LocationConverterService,
) {
    private val log = KotlinLogging.logger {}

    // TODO: which role(s) can do this?
    @PostMapping(value = ["/missions/edi"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun postEdiInformation(
        @RequestBody ediResource: EdiMissionRequest,
    ): ResponseEntity<Mission> {
        log.debug { "Received POST for new edi mission: $ediResource" }

        val gkLocation = GKLocation(ediResource.mission.EoX, ediResource.mission.EoY)
        val gpsLocation = locationConverterService.convertToGPS(gkLocation)

        val units =
            ediResource.mission.Resources
                .map { r -> r.FunkRufname }
                .mapNotNull { r -> unitService.findByCallSign(r) }
                .toSet()

        val mission =
            Mission(
                missionId = ediResource.missionId.toString(),
                alarmDate = ediResource.mission.AlarmDate,
                missionNr = ediResource.mission.EinsatzNr,
                missionType = ediResource.mission.EinsatztypId,
                missionTypeName = ediResource.mission.EinsatztypName,
                gpsLocation = gpsLocation,
                gkLocation = gkLocation,
                missionAddress =
                    Address(
                        ediResource.mission.EoStrasse,
                        ediResource.mission.EoHausNr.toString() + ediResource.mission.EoHausNrZusatz,
                        Integer.parseInt(ediResource.mission.EoPlz),
                        ediResource.mission.EoOrt,
                    ),
                units = units,
                radioGroup = ediResource.mission.Tetra,
                state = Mission.State.RUNNING,
                keyword = ediResource.mission.Schlagwort,
                // filter all null & empty string out
                cues =
                    ediResource.mission.Stichwoerter
                        .filterNotNull()
                        .map { it.toFormattedString() }
                        .firstOrNull { it != "" }
                        ?: "",
            )

        val savedMission = missionService.createNewMission(mission)

        val location =
            ServletUriComponentsBuilder
                .fromCurrentServletMapping()
                .path("/missions/{id}")
                .buildAndExpand(savedMission.missionId)
                .toUri()

        return ResponseEntity.created(location).build()
    }
}
