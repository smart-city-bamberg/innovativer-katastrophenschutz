/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

@file:Suppress("ktlint:standard:property-naming")

package io.lion5.smartcity.http.dto

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.Instant

/**
 * This represents a mission in EDI format and is taken as input
 *
 * @property AlarmDate The alarm date and time in the format "yyyy-MM-dd'T'HH:mm:ssX".
 * @property AlarmInfo The list of alarm information.
 * @property Blaulicht Indicates if the bluelight is on.
 * @property DispoList The map of dispatch lists.
 * @property EinsatzNr The mission number.
 * @property Einsatzplannr The mission plan number.
 * @property EinsatztypId The mission type ID.
 * @property EinsatztypName The mission type name.
 * @property EoAbschnitt The address section of the mission request.
 * @property EoHausNr The house number of the mission request.
 * @property EoHausNrKz The additional house number information of the mission request.
 * @property EoHausNrZusatz The additional house number information of the mission request.
 * @property EoKreuzung The intersection information of the mission request.
 * @property EoObjekte The list of objects in the mission request.
 * @property EoOrt The city of the mission request address.
 * @property EoOrtsteil The district of the mission request address.
 * @property EoPlz The postal code of the mission request address.
 * @property EoStrasse The street of the mission request address.
 * @property EoX The x-coordinate of the mission request location.
 * @property EoY The y-coordinate of the mission request location.
 * @property Freitext The free text associated with the mission request.
 * @property Melder The list of reporters.
 * @property Prioritaet The priority of the mission request.
 * @property Resources The list of resources assigned to the mission request.
 * @property Schlagwort The keyword associated with the mission request.
 * @property Status The status of the mission request.
 * @property StatusText The status text of the mission request.
 * @property Stichwoerter The list of keywords associated with the mission request.
 * @property Tetra The Tetra information of the mission request.
 */
class EDIMissionDto {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssX")
    lateinit var AlarmDate: Instant
    lateinit var AlarmInfo: ArrayList<String>

    @JsonProperty("Blaulicht")
    var Blaulicht: Boolean = false
    lateinit var DispoList: Map<String, String>
    lateinit var EinsatzNr: String
    lateinit var Einsatzplannr: String

    @JsonProperty("EinsatztypId")
    var EinsatztypId: Int = 0
    lateinit var EinsatztypName: String
    lateinit var EoAbschnitt: String

    @JsonProperty("EoHausNr")
    var EoHausNr: Int = 0

    @JsonProperty("EoHausNrKz")
    var EoHausNrKz: String? = null

    @JsonProperty("EoHausNrZusatz")
    var EoHausNrZusatz: String? = null

    @JsonProperty("EoKreuzung")
    var EoKreuzung: String? = null
    lateinit var EoObjekte: ArrayList<EoObjekte>
    lateinit var EoOrt: String
    lateinit var EoOrtsteil: String
    lateinit var EoPlz: String
    lateinit var EoStrasse: String

    @JsonProperty("EoX")
    var EoX: Double = 0.0

    @JsonProperty("EoY")
    var EoY: Double = 0.0
    lateinit var Freitext: String
    lateinit var Melder: ArrayList<String>

    @JsonProperty("Prioritaet")
    var Prioritaet: Int = 0
    lateinit var Resources: ArrayList<Resources>
    lateinit var Schlagwort: String

    @JsonProperty("Status")
    var Status: Int = 0
    lateinit var StatusText: String
    lateinit var Stichwoerter: ArrayList<Stichwort>
    lateinit var Tetra: String
}
