package io.lion5.smartcity.http.dto

data class AddUnitToMissionRequest(
    val unitIds: List<String>,
)
