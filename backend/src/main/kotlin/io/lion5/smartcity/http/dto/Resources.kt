/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

@file:Suppress("ktlint:standard:property-naming")

package io.lion5.smartcity.http.dto

import com.fasterxml.jackson.annotation.JsonProperty

class Resources {
    @JsonProperty("FunkRufname")
    lateinit var FunkRufname: String

    @JsonProperty("FunkRufnameKurz")
    var FunkRufnameKurz: String? = null

    @JsonProperty("Name")
    var Name: String? = null

    @JsonProperty("Status")
    var Status: String? = null

    @JsonProperty("ZeitAbD")
    var ZeitAbD: String? = null

    @JsonProperty("ZeitAbE")
    var ZeitAbE: String? = null

    @JsonProperty("ZeitAbZ")
    var ZeitAbZ: String? = null

    @JsonProperty("ZeitAbgezogen")
    var ZeitAbgezogen: String? = null

    @JsonProperty("ZeitAl")
    var ZeitAl: String? = null

    @JsonProperty("ZeitAnD")
    var ZeitAnD: String? = null

    @JsonProperty("ZeitAnE")
    var ZeitAnE: String? = null

    @JsonProperty("ZeitAnZ")
    var ZeitAnZ: String? = null

    @JsonProperty("ZeitEinsatzBeginn")
    var ZeitEinsatzBeginn: String? = null

    @JsonProperty("ZeitEinsatzEnde")
    var ZeitEinsatzEnde: String? = null

    @JsonProperty("ZeitErstAl")
    var ZeitErstAl: String? = null

    @JsonProperty("Zeit_1")
    var Zeit1: String? = null

    @JsonProperty("Zeit_2")
    var Zeit2: String? = null
}
