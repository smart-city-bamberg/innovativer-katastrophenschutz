/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.http

import io.lion5.smartcity.auth.model.AuthorityRoles
import io.lion5.smartcity.http.dto.CreateDepartmentDTO
import io.lion5.smartcity.http.dto.UpdateDepartmentDTO
import io.lion5.smartcity.mongodb.model.Department
import io.lion5.smartcity.mongodb.service.DepartmentService
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import org.springframework.data.rest.webmvc.RepositoryRestController
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody

@RepositoryRestController
class DepartmentController(
    private var departmentService: DepartmentService,
) {
    @GetMapping("/departments")
    @PreAuthorize("""hasAnyRole('${AuthorityRoles.SUPER_ADMIN}', '${AuthorityRoles.ADMIN}', '${AuthorityRoles.USER}')""")
    fun findAllDepartments(): ResponseEntity<List<Department>> {
        val department = departmentService.findAllDepartments()
        return ResponseEntity.ok(department)
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("""hasAnyRole('${AuthorityRoles.SUPER_ADMIN}', '${AuthorityRoles.ADMIN}')""")
    @PostMapping("/departments")
    fun createDepartment(
        @RequestBody createDepartmentDTO: CreateDepartmentDTO,
    ): ResponseEntity<Department> {
        val department =
            departmentService.createDepartment(
                createDepartmentDTO.name,
                createDepartmentDTO.slug,
                createDepartmentDTO.state,
            )
        return ResponseEntity.ok(department)
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize(
        "hasRole('${AuthorityRoles.SUPER_ADMIN}') " +
            "or hasRole('${AuthorityRoles.ADMIN}') " +
            "or hasPermission(#departmentId, 'ADMINISTRATION')",
    )
    @PutMapping("/departments/{departmentId}")
    fun updateDepartment(
        @PathVariable departmentId: String,
        @RequestBody updateDepartmentDTO: UpdateDepartmentDTO,
    ): ResponseEntity<Department> {
        val updatedDepartment =
            departmentService.updateDepartment(
                departmentId,
                updateDepartmentDTO.name,
                updateDepartmentDTO.slug,
                updateDepartmentDTO.state,
            )
        return updatedDepartment?.let { ResponseEntity.ok(it) } ?: ResponseEntity.notFound().build()
    }
}
