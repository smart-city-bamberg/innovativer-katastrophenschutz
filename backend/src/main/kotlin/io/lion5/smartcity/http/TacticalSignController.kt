/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.http

import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.auth.model.AuthorityRoles
import io.lion5.smartcity.mongodb.model.TacticalSign
import io.lion5.smartcity.mongodb.service.TacticalSignService
import io.swagger.v3.oas.annotations.Operation
import jakarta.validation.constraints.NotNull
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestPart
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.support.ServletUriComponentsBuilder

@RestController
@RequestMapping("/tactical-signs")
@CrossOrigin
class TacticalSignController(
    private val tacticalSignService: TacticalSignService,
) {
    private val log = KotlinLogging.logger {}

    @PreAuthorize("hasAnyRole('${AuthorityRoles.ADMIN}','${AuthorityRoles.SUPER_ADMIN}')")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Creates a new tactical sign.")
    @PostMapping
    fun postTacticalSign(
        @NotNull @RequestPart("name") name: String,
        @NotNull @RequestPart("image") image: MultipartFile,
    ): ResponseEntity<TacticalSign> {
        log.info { "Creating tactical sign" }
        val tacticalSign = tacticalSignService.createTacticalSign(image, name)
        val location =
            ServletUriComponentsBuilder
                .fromCurrentServletMapping()
                .path("/tactical-sign/{id}")
                .buildAndExpand(tacticalSign.id)
                .toUri()

        return ResponseEntity.created(location).body(tacticalSign)
    }

    @PreAuthorize("isAuthenticated()")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get all tactical signs.")
    @GetMapping
    fun getTacticalSigns(): List<TacticalSign> {
        log.info { "Getting all tactical signs" }
        return tacticalSignService.getAllTacticalSigns()
    }

    @PreAuthorize("hasAnyRole('${AuthorityRoles.ADMIN}','${AuthorityRoles.SUPER_ADMIN}')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete a tactical sign.")
    @DeleteMapping("/{id}")
    fun deleteTacticalSign(
        @NotNull @PathVariable("id") id: String,
    ) {
        log.info { "Deleting tactical sign with id $id" }
        tacticalSignService.deleteTacticalSign(id)
    }
}
