/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.http

import io.lion5.smartcity.auth.model.AuthorityRoles
import io.lion5.smartcity.auth.model.FirebasePrincipal
import io.lion5.smartcity.http.dto.AdhocJoinRequestCreateDto
import io.lion5.smartcity.http.dto.JoinRequestStatusResponseDto
import io.lion5.smartcity.http.dto.MissionJoinResponseDto
import io.lion5.smartcity.mongodb.model.AdhocJoinRequest
import io.lion5.smartcity.mongodb.service.AdhocJoinRequestService
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
@RequestMapping("/adhoc-join")
@CrossOrigin
class AdhocJoinRequestController(
    private val adhocJoinRequestService: AdhocJoinRequestService,
) {
    @GetMapping
    fun validateJoinCode(
        @RequestParam joinCode: String,
    ): ResponseEntity<MissionJoinResponseDto> = ResponseEntity.ok(adhocJoinRequestService.validateJoinCode(joinCode))

    @GetMapping("/validate")
    fun validateJoinPin(
        @RequestParam joinCode: String,
        @RequestParam joinPin: String,
    ): ResponseEntity<MissionJoinResponseDto> = ResponseEntity.ok(adhocJoinRequestService.validateJoinPin(joinCode, joinPin))

    @PostMapping
    fun createAdhocJoinRequest(
        @RequestBody adhocJoinRequestCreateDto: AdhocJoinRequestCreateDto,
        @AuthenticationPrincipal principal: Principal,
    ): ResponseEntity<AdhocJoinRequest> {
        if (principal !is FirebasePrincipal) {
            throw IllegalStateException("Principal is not a FirebasePrincipal")
        }
        return ResponseEntity.ok(
            adhocJoinRequestService.createAdhocJoinRequest(
                userFirebaseId = principal.name,
                adhocJoinRequestCreateDto,
            ),
        )
    }

    @PreAuthorize("hasAnyRole('${AuthorityRoles.SUPER_ADMIN}', '${AuthorityRoles.ADMIN}')")
    @PatchMapping("/{adhocJoinRequestId}/approve")
    fun approveAdhocJoinRequest(
        @PathVariable adhocJoinRequestId: String,
    ): ResponseEntity<AdhocJoinRequest> = ResponseEntity.ok(adhocJoinRequestService.approveAdhocJoinRequest(adhocJoinRequestId))

    @PreAuthorize("hasAnyRole('${AuthorityRoles.SUPER_ADMIN}', '${AuthorityRoles.ADMIN}')")
    @PatchMapping("/{adhocJoinRequestId}/reject")
    fun rejectAdhocJoinRequest(
        @PathVariable adhocJoinRequestId: String,
    ): ResponseEntity<AdhocJoinRequest> = ResponseEntity.ok(adhocJoinRequestService.rejectAdhocJoinRequest(adhocJoinRequestId))

    @PatchMapping("/{adhocJoinRequestId}/cancel")
    fun cancelAdhocJoinRequest(
        @PathVariable adhocJoinRequestId: String,
        @AuthenticationPrincipal principal: Principal,
    ): ResponseEntity<AdhocJoinRequest> {
        if (principal !is FirebasePrincipal) {
            throw IllegalStateException("Principal is not a FirebasePrincipal")
        }
        return ResponseEntity.ok(adhocJoinRequestService.cancelAdhocJoinRequest(adhocJoinRequestId, principal.name))
    }

    @PreAuthorize("hasAnyRole('${AuthorityRoles.SUPER_ADMIN}', '${AuthorityRoles.ADMIN}')")
    @GetMapping("/waiting-room")
    fun getWaitingRoom(): ResponseEntity<List<AdhocJoinRequest>> =
        ResponseEntity.ok(adhocJoinRequestService.findAllPendingAdhocJoinRequests())

    @PreAuthorize("hasAnyRole('${AuthorityRoles.SUPER_ADMIN}', '${AuthorityRoles.ADMIN}')")
    @GetMapping("/waiting-room/{missionId}")
    fun getWaitingRoom(
        @PathVariable missionId: String,
    ): ResponseEntity<List<AdhocJoinRequest>> =
        ResponseEntity.ok(adhocJoinRequestService.findAllPendingAdhocJoinRequestsByMission(missionId))

    @GetMapping("/waiting-room/status")
    fun getWaitingRoomStatus(
        @AuthenticationPrincipal principal: Principal,
    ): ResponseEntity<JoinRequestStatusResponseDto> {
        if (principal !is FirebasePrincipal) {
            throw IllegalStateException("Principal is not a FirebasePrincipal")
        }
        return ResponseEntity.ok(adhocJoinRequestService.getWaitingRoomStatus(principal.name))
    }
}
