/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.http

import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.http.dto.AddUnitToMissionRequest
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.service.MissionService
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.rest.webmvc.RepositoryRestController
import org.springframework.hateoas.EntityModel
import org.springframework.hateoas.IanaLinkRelations
import org.springframework.hateoas.Link
import org.springframework.hateoas.PagedModel
import org.springframework.hateoas.PagedModel.PageMetadata
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.util.stream.Collectors

@RepositoryRestController
@CrossOrigin
class MissionController(
    private val missionService: MissionService,
) {
    val log = KotlinLogging.logger {}

    /**
     * Controller method to get all missions.
     * Created custom controller method because @RepositoryRestResource does not support ACL @PostFilter annotation
     * as it uses Paginated findAll() repository method.
     *
     * However, the List of mission is returned paginated and with HATEOAS to not break on the client side.
     */
    @GetMapping("/missions")
    fun getAllMissions(
        @RequestParam(value = "page", defaultValue = "0") page: Int,
        @RequestParam(value = "size", defaultValue = "20") size: Int,
    ): ResponseEntity<PagedModel<EntityModel<Mission>>> {
        val missionsList = missionService.findAll()
        val missions =
            missionsList
                .stream()
                .map { mission: Mission ->
                    val selfLink =
                        Link.of(
                            ServletUriComponentsBuilder
                                .fromCurrentRequest()
                                .path("/{id}")
                                .buildAndExpand(mission.missionId)
                                .toUriString(),
                            IanaLinkRelations.SELF,
                        )
                    val missionLink =
                        Link.of(
                            ServletUriComponentsBuilder
                                .fromCurrentRequest()
                                .path("/{id}")
                                .buildAndExpand(mission.missionId)
                                .toUriString(),
                            "mission",
                        )
                    val unitsLink =
                        Link.of(
                            ServletUriComponentsBuilder
                                .fromCurrentRequest()
                                .path("/{id}/units")
                                .buildAndExpand(mission.missionId)
                                .toUriString(),
                            "units",
                        )
                    EntityModel.of(mission, selfLink, missionLink, unitsLink)
                }.collect(Collectors.toList())

        val pageOfMissions: Page<Mission> =
            PageImpl(missionsList, PageRequest.of(0, missionsList.size), missionsList.size.toLong())

        val pageMetadata =
            PageMetadata(
                pageOfMissions.size.toLong(),
                pageOfMissions.number.toLong(),
                pageOfMissions.totalElements,
                pageOfMissions.totalPages.toLong(),
            )
        val pagedModel = PagedModel.of(missions, pageMetadata)

        val selfLink =
            Link.of(
                ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .replacePath("/missions")
                    .build()
                    .toUriString(),
                "self",
            )
        val profileLink =
            Link.of(
                ServletUriComponentsBuilder.fromCurrentContextPath().path("/profile/missions").toUriString(),
                "profile",
            )
        val searchLink =
            Link.of(
                ServletUriComponentsBuilder.fromCurrentContextPath().path("/missions/search").toUriString(),
                "search",
            )

        pagedModel.add(selfLink, profileLink, searchLink)

        return ResponseEntity.ok(pagedModel)
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PostMapping("/missions")
    fun createMission(
        @RequestBody mission: Mission,
    ): ResponseEntity<Mission> {
        val savedMission = missionService.createNewMission(mission)
        val location =
            ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedMission.missionId)
                .toUri()
        return ResponseEntity.created(location).body(savedMission)
    }

    @DeleteMapping("/missions/{missionId}/units/{unitId}")
    fun removeUnitFromMission(
        @PathVariable missionId: String,
        @PathVariable unitId: String,
    ): ResponseEntity<Mission> {
        val mission = missionService.removeUnitFromMission(missionId, unitId)
        return ResponseEntity.ok(mission)
    }

    @PatchMapping("/missions/{missionId}/units")
    fun addUnitToMission(
        @RequestBody addUnitToMissionRequest: AddUnitToMissionRequest,
        @PathVariable missionId: String,
    ): ResponseEntity<Mission> {
        val mission = missionService.addUnitToMission(addUnitToMissionRequest.unitIds, missionId)
        return ResponseEntity.ok(mission)
    }
}
