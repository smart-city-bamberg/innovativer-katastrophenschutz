/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.http

import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.auth.model.AuthorityRoles
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.rest.webmvc.RepositoryRestController
import org.springframework.hateoas.EntityModel
import org.springframework.hateoas.Link
import org.springframework.hateoas.PagedModel
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.security.Principal
import java.util.stream.Collectors

@RepositoryRestController
@CrossOrigin
class EmergencyUnitController(
    private var emergencyUnitService: EmergencyUnitService,
) {
    private val log = KotlinLogging.logger {}

    /**
     * Create a new EmergencyUnit.
     * Include HATEOAS links for navigation.
     */
    @PreAuthorize("""hasAnyRole('${AuthorityRoles.SUPER_ADMIN}', '${AuthorityRoles.ADMIN}', '${AuthorityRoles.USER}')""")
    @PostMapping("/units")
    @SecurityRequirement(name = "Bearer Authentication")
    fun createEmergencyUnit(
        @RequestBody emergencyUnit: EmergencyUnit,
        @AuthenticationPrincipal principal: Principal,
    ): ResponseEntity<EntityModel<EmergencyUnit>> {
        log.debug { "called fun createEmergencyUnit " }
        val createdEmergencyUnit =
            emergencyUnitService.createEmergencyUnitWithUserDepartment(
                emergencyUnit,
                userFirebaseId = principal.name,
            )

        val selfLink =
            linkTo(
                methodOn(EmergencyUnitController::class.java).createEmergencyUnit(
                    emergencyUnit,
                    principal,
                ),
            ).slash(createdEmergencyUnit.unitId).withSelfRel()
        val emergencyUnitLink =
            linkTo(
                methodOn(EmergencyUnitController::class.java).createEmergencyUnit(emergencyUnit, principal),
            ).slash(createdEmergencyUnit.unitId).withRel("emergencyUnit")
        val departmentLink =
            linkTo(
                methodOn(EmergencyUnitController::class.java).createEmergencyUnit(emergencyUnit, principal),
            ).slash(createdEmergencyUnit.unitId).slash("department").withRel("department")

        val entityModel = EntityModel.of(createdEmergencyUnit, selfLink, emergencyUnitLink, departmentLink)

        return ResponseEntity.ok(entityModel)
    }

    /**
     * Paginated request to get all EmergencyUnits.
     * Include HATEOAS links for navigation.
     *
     * TODO: change this for all units within department & mission
     */
    @PreAuthorize("""isAuthenticated()""")
    @GetMapping("/units")
    fun getEmergencyUnits(
        @RequestParam(defaultValue = "0") page: Int,
        @RequestParam(defaultValue = "20") size: Int,
    ): ResponseEntity<PagedModel<EntityModel<EmergencyUnit>>> {
        val pageable: Pageable = PageRequest.of(page, size)
        val emergencyUnitsPage: Page<EmergencyUnit> = emergencyUnitService.findAllEmergencyUnits(pageable)

        val emergencyUnits =
            emergencyUnitsPage
                .stream()
                .map { emergencyUnit ->
                    val selfLink =
                        Link.of(
                            ServletUriComponentsBuilder
                                .fromCurrentRequest()
                                .path("/{id}")
                                .buildAndExpand(emergencyUnit.unitId)
                                .toUriString(),
                            "self",
                        )
                    val emergencyUnitLink =
                        Link.of(
                            ServletUriComponentsBuilder
                                .fromCurrentRequest()
                                .path("/{id}")
                                .buildAndExpand(emergencyUnit.unitId)
                                .toUriString(),
                            "emergencyUnit",
                        )
                    val departmentLink =
                        Link.of(
                            ServletUriComponentsBuilder
                                .fromCurrentRequest()
                                .path("/{id}/department")
                                .buildAndExpand(emergencyUnit.unitId)
                                .toUriString(),
                            "department",
                        )
                    EntityModel.of(emergencyUnit, selfLink, emergencyUnitLink, departmentLink)
                }.collect(Collectors.toList())

        val pageMetadata =
            PagedModel.PageMetadata(
                emergencyUnitsPage.size.toLong(),
                emergencyUnitsPage.number.toLong(),
                emergencyUnitsPage.totalElements,
                emergencyUnitsPage.totalPages.toLong(),
            )

        val pagedModel = PagedModel.of(emergencyUnits, pageMetadata)

        val baseUri =
            ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .replacePath(null)
                .build()
                .toUriString()
        val selfLink =
            Link.of(
                ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .replaceQueryParam("page", page)
                    .replaceQueryParam("size", size)
                    .toUriString(),
                "self",
            )
        val profileLink = Link.of("$baseUri/profile/units", "profile")
        val searchLink = Link.of("$baseUri/units/search", "search")
        pagedModel.add(selfLink, profileLink, searchLink)

        return ResponseEntity.ok(pagedModel)
    }
}
