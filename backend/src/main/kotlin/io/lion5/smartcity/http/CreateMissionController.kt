/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.http

import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.auth.model.AuthorityRoles
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import io.lion5.smartcity.mongodb.service.MissionService
import io.swagger.v3.oas.annotations.Operation
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.util.Optional

@RestController
class CreateMissionController(
    private val missionService: MissionService,
    private val emergencyUnitService: EmergencyUnitService,
) {
    private val log = KotlinLogging.logger {}

    @PreAuthorize("hasAnyRole('${AuthorityRoles.ADMIN}','${AuthorityRoles.SUPER_ADMIN}')")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Creates a new mission. Optionally creates the given units if no id is provided")
    @PostMapping(value = ["/missions/bulk-create"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun bulkCreateMission(
        @RequestBody missionResource: Mission,
    ): ResponseEntity<Mission> {
        log.info { "Bulk creating mission ${missionResource.missionNr}" }
        val savedUnits =
            missionResource.units
                .map {
                    var emergencyUnit: Optional<EmergencyUnit>
                    if (it.unitId != null) {
                        log.info { "Getting unit with id: ${it.unitId}" }
                        emergencyUnit = emergencyUnitService.getEmergencyUnitById(it.unitId!!)

                        if (emergencyUnit.isPresent) {
                            emergencyUnit.get()
                        } else {
                            // unit with given id does not exist yet
                            log.info { "Creating unit, because it was not found: $it" }
                            emergencyUnitService.createOrUpdateEmergencyUnit(it)
                        }
                    } else {
                        log.info { "Creating unit: $it" }
                        emergencyUnitService.createOrUpdateEmergencyUnit(it)
                    }
                }.toSet()
        val savedMission =
            missionService.createNewMission(missionResource.copy(units = savedUnits))

        missionService.createEventStreamForMission(savedMission.missionId!!)

        val location =
            ServletUriComponentsBuilder
                .fromCurrentServletMapping()
                .path("/missions/{id}")
                .buildAndExpand(savedMission.missionId)
                .toUri()

        return ResponseEntity.created(location).body(savedMission)
    }
}
