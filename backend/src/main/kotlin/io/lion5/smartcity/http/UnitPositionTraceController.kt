package io.lion5.smartcity.http

import io.lion5.smartcity.mongodb.service.UnitPositionTraceService
import org.springframework.data.rest.webmvc.RepositoryRestController
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@RepositoryRestController
@CrossOrigin
class UnitPositionTraceController(
    private var unitPositionTraceService: UnitPositionTraceService,
) {
    @GetMapping("/unitPositionTraces")
    fun getUnitPositionTraces(
        @RequestParam unitId: String,
        @RequestParam missionId: String,
    ): ResponseEntity<String> {
        val trace =
            unitPositionTraceService.findByUnitIdAndMissionId(
                unitId,
                missionId,
            )
        return ResponseEntity.ok(trace)
    }
}
