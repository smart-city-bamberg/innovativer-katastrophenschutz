/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.document

import io.lion5.smartcity.auth.model.AuthorityRoles
import io.swagger.v3.oas.annotations.Operation
import org.springframework.core.io.InputStreamResource
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.io.File
import java.io.FileInputStream

@RestController
@RequestMapping("protocols")
@CrossOrigin
class ProtocolDocumentController(
    private val protocolDocumentService: ProtocolDocumentService,
) {
    @PreAuthorize("hasAnyRole('${AuthorityRoles.SUPER_ADMIN}','${AuthorityRoles.ADMIN}','${AuthorityRoles.USER}')")
    @GetMapping("/{missionId}", produces = [MediaType.APPLICATION_PDF_VALUE])
    @Operation(summary = "Create a mission report.")
    fun downloadDocument(
        @PathVariable missionId: String,
        @RequestParam includeLocationEvents: Boolean = true,
    ): ByteArray {
        val file2Upload = File(protocolDocumentService.createPdfProtocolFromMissionId(missionId, includeLocationEvents))
        val resource = InputStreamResource(FileInputStream(file2Upload))
        return resource.contentAsByteArray
    }
}
