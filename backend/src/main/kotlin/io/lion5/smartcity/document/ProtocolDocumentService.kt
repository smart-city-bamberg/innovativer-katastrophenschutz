/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.document

import io.lion5.smartcity.eventstore.EventstoreMissionService
import io.lion5.smartcity.eventstore.events.model.DigimapEvent
import io.lion5.smartcity.eventstore.events.model.LocationUpdateEvent
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import io.lion5.smartcity.mongodb.service.MissionService
import org.springframework.stereotype.Service

/**
 * This service creates a mission protocol from all events in a mission eventstream
 */
@Service
class ProtocolDocumentService(
    private val emergencyUnitService: EmergencyUnitService,
    private val pdfProtocolBuilder: PdfProtocolBuilder,
    private val missionService: MissionService,
    private val eventStoreMissionService: EventstoreMissionService,
) {
    fun createPdfProtocolAndUploadToCloudStorage(
        mission: Mission,
        digimapEvents: List<DigimapEvent>,
    ): String =
        pdfProtocolBuilder.generatePdf(
            mission.toMissionEntry(),
            digimapEvents.toEventLogEntries(emergencyUnitService),
        )

    fun createPdfProtocolFromMissionId(
        missionId: String,
        includeLocationEvents: Boolean,
    ): String {
        val mission = missionService.findMissionById(missionId)
        if (mission.isPresent) {
            val digimapEvents = eventStoreMissionService.readEventsFromStreamForMission(missionId)
            if (includeLocationEvents) {
                return createPdfProtocolAndUploadToCloudStorage(mission.get(), digimapEvents)
            }
            val filteredEvents: List<DigimapEvent> = digimapEvents.filter { it !is LocationUpdateEvent }
            return createPdfProtocolAndUploadToCloudStorage(mission.get(), filteredEvents)
        } else {
            return "Mission not found with the given mission id $missionId"
        }
    }
}
