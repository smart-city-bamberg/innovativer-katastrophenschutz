/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.document

import org.springframework.stereotype.Service
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import org.thymeleaf.templatemode.TemplateMode
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver
import org.w3c.tidy.Tidy
import org.xhtmlrenderer.pdf.ITextRenderer
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.FileOutputStream
import java.io.OutputStream
import java.nio.file.FileSystems

@Service
class PdfProtocolBuilder {
    companion object {
        private const val OUTPUT_FILE = "protocol.pdf"
        private const val UTF_8 = "UTF-8"
    }

    fun generatePdf(
        mission: MissionEntry,
        eventList: List<EventLogEntry>,
    ): String {
        // We set-up a Thymeleaf rendering engine. All Thymeleaf templates
        // are HTML-based files located under "src/test/resources". Beside
        // of the main HTML file, we also have partials like a footer or
        // a header. We can re-use those partials in different documents.
        val templateResolver = ClassLoaderTemplateResolver()
        templateResolver.prefix = "/"
        templateResolver.suffix = ".html"
        templateResolver.templateMode = TemplateMode.HTML
        templateResolver.characterEncoding = UTF_8
        val templateEngine = TemplateEngine()
        templateEngine.setTemplateResolver(templateResolver)

        // The data in our Thymeleaf templates is not hard-coded. Instead,
        // we use placeholders in our templates. We fill these placeholders
        // with actual data by passing in an object. In this example, we will
        // write a letter to "John Doe".
        //
        // Note that we could also read this data from a JSON file, a database
        // a web service or whatever.
        val context = Context()
        context.setVariable("mission", mission)
        context.setVariable("eventLogs", eventList.sortedBy { it.timestamp })

        // Flying Saucer needs XHTML - not just normal HTML. To make our life
        // easy, we use JTidy to convert the rendered Thymeleaf template to
        // XHTML. Note that this might not work for very complicated HTML. But
        // it's good enough for a simple letter.
        val renderedHtmlContent = templateEngine.process("protocol-pdf", context)
        val xHtml = convertToXhtml(renderedHtmlContent)
        val renderer = ITextRenderer()
//        renderer.fontResolver.addFont("Code39.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED)

        // FlyingSaucer has a working directory. If you run this test, the working directory
        // will be the root folder of your project. However, all files (HTML, CSS, etc.) are
        // located under "/src/main/resources". So we want to use this folder as the working
        // directory.
        val baseUrl =
            FileSystems
                .getDefault()
                .getPath("src", "main", "resources")
                .toUri()
                .toURL()
                .toString()
        renderer.setDocumentFromString(xHtml, baseUrl)
        renderer.layout()

        // And finally, we create the PDF:
        val outputStream: OutputStream = FileOutputStream(OUTPUT_FILE)
        renderer.createPDF(outputStream)
        outputStream.close()
        val path = System.getProperty("user.dir")
        return "$path/$OUTPUT_FILE"
    }

    private fun convertToXhtml(html: String): String {
        val tidy = Tidy()
        tidy.inputEncoding = UTF_8
        tidy.outputEncoding = UTF_8
        tidy.xhtml = true
        val inputStream = ByteArrayInputStream(html.toByteArray(charset(UTF_8)))
        val outputStream = ByteArrayOutputStream()
        tidy.parseDOM(inputStream, outputStream)
        return outputStream.toString(UTF_8)
    }
}
