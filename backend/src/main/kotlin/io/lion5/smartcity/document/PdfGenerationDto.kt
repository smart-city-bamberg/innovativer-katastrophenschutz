/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.document

import io.lion5.smartcity.eventstore.events.model.CommandEvent
import io.lion5.smartcity.eventstore.events.model.DigimapEvent
import io.lion5.smartcity.eventstore.events.model.LocationUpdateEvent
import io.lion5.smartcity.eventstore.events.model.MissionEvent
import io.lion5.smartcity.eventstore.events.model.ReportEvent
import io.lion5.smartcity.eventstore.events.model.UnitEvent
import io.lion5.smartcity.mongodb.model.Address
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import java.time.ZoneId
import java.time.ZonedDateTime

data class EventLogEntry(
    val timestamp: ZonedDateTime,
    val senderUnit: String,
    val receiverUnit: String,
    val description: String,
)

data class MissionEntry(
    val alarmDate: ZonedDateTime,
    val missionNr: String,
    val missionAddress: Address,
    val units: List<String>,
)

fun List<DigimapEvent>.toEventLogEntries(emergencyUnitService: EmergencyUnitService): List<EventLogEntry> =
    this.map {
        it.toEventLogEntry(emergencyUnitService)
    }

fun DigimapEvent.toEventLogEntry(emergencyUnitService: EmergencyUnitService): EventLogEntry =
    EventLogEntry(
        this.timestamp.atZone(ZoneId.systemDefault()),
        getEventSender(this, emergencyUnitService),
        getEventReceiver(this, emergencyUnitService),
        getEventDescription(this),
    )

fun Mission.toMissionEntry(): MissionEntry =
    MissionEntry(
        this.alarmDate.atZone(ZoneId.systemDefault()),
        this.missionNr,
        this.missionAddress,
        // TODO: Use all units, not just the latest ones
        this.units.map { it.callSign },
    )

private fun getEventSender(
    digimapEvent: DigimapEvent,
    emergencyUnitService: EmergencyUnitService,
): String =
    when (digimapEvent) {
        is UnitEvent -> emergencyUnitService.getEmergencyUnitById(digimapEvent.senderUnitId).get().callSign
        else -> ""
    }

private fun getEventReceiver(
    digimapEvent: DigimapEvent,
    emergencyUnitService: EmergencyUnitService,
): String {
    return when (digimapEvent) {
        is CommandEvent ->
            emergencyUnitService
                .getEmergencyUnitsById(digimapEvent.receiverUnitIds)
                .joinToString(", ", transform = { it.callSign })

        is ReportEvent -> {
            return if (digimapEvent.receiverUnitIds != null) {
                emergencyUnitService
                    .getEmergencyUnitsById(digimapEvent.receiverUnitIds)
                    .joinToString(separator = ", ", transform = { it.callSign })
            } else {
                ""
            }
        }

        else -> ""
    }
}

private fun getEventDescription(digimapEvent: DigimapEvent): String =
    when (digimapEvent) {
        is MissionEvent -> "${digimapEvent.eventType}"
        is LocationUpdateEvent -> "Ankunft bei Position (${digimapEvent.location.latitude},${digimapEvent.location.longitude})"
        is ReportEvent ->
            "Lagemeldung ${digimapEvent.reportId} bei ${digimapEvent.location?.latitude}, " +
                "${digimapEvent.location?.longitude}: ${digimapEvent.report}"
        is CommandEvent -> "Befehl ${digimapEvent.commandId}: ${digimapEvent.commandText}"
        else -> ""
    }
