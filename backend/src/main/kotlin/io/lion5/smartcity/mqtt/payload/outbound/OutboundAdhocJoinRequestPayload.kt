/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mqtt.payload.outbound

import io.lion5.smartcity.mongodb.model.AdhocJoinRequest
import io.lion5.smartcity.mongodb.model.AdhocJoinRequest.AdhocJoinRequestStatus

class OutboundAdhocJoinRequestPayload(
    val adhocJoinRequestId: String,
    val userFirebaseId: String,
    val userName: String,
    val unitCallSign: String,
    val unitTacticalSignPath: String,
    val departmentName: String,
    val missionId: String,
    val missionNr: String,
    val status: AdhocJoinRequestStatus = AdhocJoinRequestStatus.PENDING,
) {
    companion object {
        fun AdhocJoinRequest.toOutboundPayload(): OutboundAdhocJoinRequestPayload =
            OutboundAdhocJoinRequestPayload(
                adhocJoinRequestId = this.adhocJoinRequestId!!,
                userFirebaseId = this.userFirebaseId,
                userName = this.userName,
                unitCallSign = this.unitCallSign,
                unitTacticalSignPath = this.unitTacticalSignPath,
                departmentName = this.departmentName,
                missionId = this.mission.missionId!!,
                missionNr = this.mission.missionNr,
                status = this.status,
            )
    }
}
