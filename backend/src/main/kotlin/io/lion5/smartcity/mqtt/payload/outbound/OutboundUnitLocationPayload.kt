/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mqtt.payload.outbound

import com.fasterxml.jackson.annotation.JsonProperty
import io.lion5.smartcity.mqtt.payload.LocationPayload
import org.springframework.format.annotation.DateTimeFormat
import java.time.Instant

/**
 * Location update that is send to all active clients.
 *
 * ### Note:
 * This payload is defined in the asyncapi.yaml. Please update the documentation if you change something.
 *
 * @param eTag event tag. The client may discard this message if it has already received a message with a higher eTag number
 * @param location the updated location
 * @param unitId Id of the unit that updated its location
 * @param timestamp when did the unit sent this location update
 */
data class OutboundUnitLocationPayload(
    @JsonProperty("etag")
    val eTag: Int,
    val location: LocationPayload,
    val unitId: String,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val timestamp: Instant,
)
