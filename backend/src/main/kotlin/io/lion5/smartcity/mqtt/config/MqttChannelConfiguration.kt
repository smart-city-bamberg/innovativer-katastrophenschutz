/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

@file:Suppress("ktlint:standard:max-line-length")

package io.lion5.smartcity.mqtt.config

import io.lion5.smartcity.mqtt.payload.inbound.InboundCommandPayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundDangerZonePayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundMissionSectionPayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundPoiPayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundReportPayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundUnitAvailablityChangedPayload
import io.lion5.smartcity.mqtt.payload.inbound.InboundUnitLocationPayload
import org.eclipse.paho.mqttv5.client.IMqttAsyncClient
import org.eclipse.paho.mqttv5.client.MqttConnectionOptions
import org.eclipse.paho.mqttv5.client.persist.MqttDefaultFilePersistence
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.expression.spel.standard.SpelExpressionParser
import org.springframework.integration.config.EnableIntegration
import org.springframework.integration.dsl.IntegrationFlow
import org.springframework.integration.json.JsonToObjectTransformer
import org.springframework.integration.json.ObjectToJsonTransformer
import org.springframework.integration.mqtt.core.ClientManager
import org.springframework.integration.mqtt.core.Mqttv5ClientManager
import org.springframework.integration.mqtt.inbound.Mqttv5PahoMessageDrivenChannelAdapter
import org.springframework.integration.mqtt.outbound.Mqttv5PahoMessageHandler
import java.util.UUID
import javax.net.ssl.SSLSocketFactory

@Configuration
@EnableIntegration
class MqttChannelConfiguration(
    @Value("\${digimap.mqtt.url}")
    val brokerUrl: String,
    @Value("\${digimap.mqtt.username}")
    private val username: String,
    @Value("\${digimap.mqtt.password}")
    private val password: String,
) {
    companion object {
        private val CLIENT_ID = "digimap-backend-${UUID.randomUUID().toString().subSequence(0, 7)}"
        private const val TOPIC_PREFIX = "digimap/"
    }

    @Bean
    fun clientManager(): ClientManager<IMqttAsyncClient, MqttConnectionOptions> {
        val connectionOptions = MqttConnectionOptions()
        connectionOptions.serverURIs = arrayOf(brokerUrl)
        connectionOptions.userName = username
        connectionOptions.password = password.toByteArray()
        if (brokerUrl.startsWith("ssl")) {
            connectionOptions.socketFactory = SSLSocketFactory.getDefault()
        }
        connectionOptions.connectionTimeout = 30000
        connectionOptions.maxReconnectDelay = 1000
        connectionOptions.isAutomaticReconnect = true
        val clientManager = Mqttv5ClientManager(connectionOptions, CLIENT_ID)
        clientManager.setPersistence(MqttDefaultFilePersistence())

        return clientManager
    }

    @Bean
    fun locationInFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val channelAdapter =
            Mqttv5PahoMessageDrivenChannelAdapter(
                clientManager,
                "${TOPIC_PREFIX}units/v1/inbound/+/location",
            )
        channelAdapter.setQos(1)

        return IntegrationFlow
            .from(channelAdapter)
            .transform(JsonToObjectTransformer(InboundUnitLocationPayload::class.java))
            .channel("locationInboundChannel")
            .get()
    }

    @Bean
    fun locationOutFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val handler = Mqttv5PahoMessageHandler(clientManager)
        handler.setDefaultTopic("${TOPIC_PREFIX}units/v1/outbound/{missionId}/location")
        handler.setAsync(true)
        handler.setDefaultQos(1)

        val topicExpression =
            SpelExpressionParser().parseExpression(
                "headers.containsKey('mission_id') ? \"${TOPIC_PREFIX}units/v1/outbound/\" + headers['mission_id'] + \"/location\"  : \"$TOPIC_PREFIX/failed-delivery\"",
            )
        handler.setTopicExpression(topicExpression)

        return IntegrationFlow
            .from("locationOutboundChannel")
            .transform(ObjectToJsonTransformer())
            .handle(handler)
            .get()
    }

    @Bean
    fun reportInFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val channelAdapter =
            Mqttv5PahoMessageDrivenChannelAdapter(
                clientManager,
                "${TOPIC_PREFIX}units/v1/inbound/+/report",
            )
        channelAdapter.setQos(1)

        return IntegrationFlow
            .from(channelAdapter)
            .transform(JsonToObjectTransformer(InboundReportPayload::class.java))
            .channel("reportInboundChannel")
            .get()
    }

    @Bean
    fun reportOutFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val handler = Mqttv5PahoMessageHandler(clientManager)
        handler.setDefaultTopic("${TOPIC_PREFIX}units/v1/outbound/{missionId}/report")
        handler.setAsync(true)
        handler.setDefaultQos(1)

        val topicExpression =
            SpelExpressionParser().parseExpression(
                "headers.containsKey('mission_id') ? \"${TOPIC_PREFIX}units/v1/outbound/\" + headers['mission_id'] + \"/report\"  : \"$TOPIC_PREFIX/failed-delivery\"",
            )
        handler.setTopicExpression(topicExpression)

        return IntegrationFlow
            .from("reportOutboundChannel")
            .transform(ObjectToJsonTransformer())
            .handle(handler)
            .get()
    }

    @Bean
    fun commandInFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val channelAdapter =
            Mqttv5PahoMessageDrivenChannelAdapter(
                clientManager,
                "${TOPIC_PREFIX}units/v1/inbound/+/command",
            )
        channelAdapter.setQos(1)

        return IntegrationFlow
            .from(channelAdapter)
            .transform(JsonToObjectTransformer(InboundCommandPayload::class.java))
            .channel("commandInboundChannel")
            .get()
    }

    @Bean
    fun commandOutFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val handler = Mqttv5PahoMessageHandler(clientManager)
        handler.setDefaultTopic("${TOPIC_PREFIX}units/v1/outbound/{missionId}/command")
        handler.setAsync(true)
        handler.setDefaultQos(1)

        val topicExpression =
            SpelExpressionParser().parseExpression(
                "headers.containsKey('mission_id') ? \"${TOPIC_PREFIX}units/v1/outbound/\" + headers['mission_id'] + \"/command\"  : \"$TOPIC_PREFIX/failed-delivery\"",
            )
        handler.setTopicExpression(topicExpression)

        return IntegrationFlow
            .from("commandOutboundChannel")
            .transform(ObjectToJsonTransformer())
            .handle(handler)
            .get()
    }

    @Bean
    fun dangerZoneInFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val channelAdapter =
            Mqttv5PahoMessageDrivenChannelAdapter(
                clientManager,
                "${TOPIC_PREFIX}units/v1/inbound/+/dangerZone",
            )
        channelAdapter.setQos(1)

        return IntegrationFlow
            .from(channelAdapter)
            .transform(JsonToObjectTransformer(InboundDangerZonePayload::class.java))
            .channel("dangerZoneInboundChannel")
            .get()
    }

    @Bean
    fun dangerZoneOutFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val handler = Mqttv5PahoMessageHandler(clientManager)
        handler.setDefaultTopic("${TOPIC_PREFIX}units/v1/outbound/{missionId}/dangerZone")
        handler.setAsync(true)
        handler.setDefaultQos(1)

        val topicExpression =
            SpelExpressionParser().parseExpression(
                "headers.containsKey('mission_id') ? \"${TOPIC_PREFIX}units/v1/outbound/\" + headers['mission_id'] + \"/dangerZone\"  : \"$TOPIC_PREFIX/failed-delivery\"",
            )
        handler.setTopicExpression(topicExpression)

        return IntegrationFlow
            .from("dangerZoneOutboundChannel")
            .transform(ObjectToJsonTransformer())
            .handle(handler)
            .get()
    }

    @Bean
    fun missionSectionInFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val channelAdapter =
            Mqttv5PahoMessageDrivenChannelAdapter(
                clientManager,
                "${TOPIC_PREFIX}units/v1/inbound/+/section",
            )
        channelAdapter.setQos(1)

        return IntegrationFlow
            .from(channelAdapter)
            .transform(JsonToObjectTransformer(InboundMissionSectionPayload::class.java))
            .channel("missionSectionInboundChannel")
            .get()
    }

    @Bean
    fun missionSectionOutFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val handler = Mqttv5PahoMessageHandler(clientManager)
        handler.setDefaultTopic("${TOPIC_PREFIX}units/v1/outbound/{missionId}/section")
        handler.setAsync(true)
        handler.setDefaultQos(1)

        val topicExpression =
            SpelExpressionParser().parseExpression(
                "headers.containsKey('mission_id') ? \"${TOPIC_PREFIX}units/v1/outbound/\" + headers['mission_id'] + \"/section\"  : \"$TOPIC_PREFIX/failed-delivery\"",
            )
        handler.setTopicExpression(topicExpression)

        return IntegrationFlow
            .from("missionSectionOutboundChannel")
            .transform(ObjectToJsonTransformer())
            .handle(handler)
            .get()
    }

    @Bean
    fun missionInFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val channelAdapter =
            Mqttv5PahoMessageDrivenChannelAdapter(
                clientManager,
                "${TOPIC_PREFIX}missions/v1/inbound/+",
            )
        channelAdapter.setQos(1)

        return IntegrationFlow
            .from(channelAdapter)
            .transform(JsonToObjectTransformer(InboundUnitAvailablityChangedPayload::class.java))
            .channel("missionInboundChannel")
            .get()
    }

    @Bean
    fun missionOutFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val handler = Mqttv5PahoMessageHandler(clientManager)
        handler.setDefaultTopic("${TOPIC_PREFIX}missions/v1/outbound/{missionId}")
        handler.setAsync(true)
        handler.setDefaultQos(1)

        val topicExpression =
            SpelExpressionParser().parseExpression(
                "headers.containsKey('mission_id') ? \"${TOPIC_PREFIX}missions/v1/outbound/\" + headers['mission_id'] : \"$TOPIC_PREFIX/failed-delivery\"",
            )
        handler.setTopicExpression(topicExpression)

        return IntegrationFlow
            .from("missionOutboundChannel")
            .transform(ObjectToJsonTransformer())
            .handle(handler)
            .get()
    }

    @Bean
    fun poiInFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val channelAdapter =
            Mqttv5PahoMessageDrivenChannelAdapter(
                clientManager,
                "${TOPIC_PREFIX}missions/v1/inbound/+/poi",
            )
        channelAdapter.setQos(1)

        return IntegrationFlow
            .from(channelAdapter)
            .transform(JsonToObjectTransformer(InboundPoiPayload::class.java))
            .channel("poiInboundChannel")
            .get()
    }

    @Bean
    fun poiOutFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val handler = Mqttv5PahoMessageHandler(clientManager)
        handler.setDefaultTopic("${TOPIC_PREFIX}missions/v1/outbound/{missionId}/poi")
        handler.setAsync(true)
        handler.setDefaultQos(1)

        val topicExpression =
            SpelExpressionParser().parseExpression(
                "headers.containsKey('mission_id') ? \"${TOPIC_PREFIX}missions/v1/outbound/\" + headers['mission_id'] + \"/poi\"   : \"$TOPIC_PREFIX/failed-delivery\"",
            )
        handler.setTopicExpression(topicExpression)

        return IntegrationFlow
            .from("poiOutboundChannel")
            .transform(ObjectToJsonTransformer())
            .handle(handler)
            .get()
    }

    @Bean
    fun adhocJoinRequestOutFlow(clientManager: ClientManager<IMqttAsyncClient, MqttConnectionOptions>): IntegrationFlow {
        val handler = Mqttv5PahoMessageHandler(clientManager)
        handler.setDefaultTopic("${TOPIC_PREFIX}adhoc-join-requests/v1/outbound")
        handler.setAsync(true)
        handler.setDefaultQos(1)

        val topicExpression =
            SpelExpressionParser().parseExpression(
                "headers.containsKey('mission_id') ? \"${TOPIC_PREFIX}adhoc-join-requests/v1/outbound/\" + headers['mission_id'] : \"${TOPIC_PREFIX}failed-delivery\"",
            )
        handler.setTopicExpression(topicExpression)

        return IntegrationFlow
            .from("adhocJoinRequestOutboundChannel")
            .transform(ObjectToJsonTransformer())
            .handle(handler)
            .get()
    }
}
