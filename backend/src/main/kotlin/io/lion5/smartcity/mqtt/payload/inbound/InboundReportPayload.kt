/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mqtt.payload.inbound

import io.lion5.smartcity.mqtt.payload.LocationPayload
import org.springframework.format.annotation.DateTimeFormat
import java.time.Instant
import java.util.UUID

/**
 * A report from a unit to a leader mostly referencing a specific command given before by that leader.
 *
 * ### Note:
 * This payload is defined in the asyncapi.yaml. Please update the documentation if you change something.
 *
 * @param senderUnitId Id of the unit that created this report
 * @param tacticalSignPath Tactical sign that should be show by the website to visualize this report in the UI
 * @param report The actual report as a String. This includes multi
 * @param receiverUnitIds Optionally a report can be addressed to one or multiple receiver Units.
 *                        If no receiver is specified the report is targeted to all.
 * @param createdAt the timestamp when this report was created by the client
 * @param referencingCommandId the ID of the command that this report is referencing
 * @param images A List of image paths that are relative to the configured cloud storage bucket.
 * @param location Usually a report may include a location so the website can show it on the map.
 */
data class InboundReportPayload(
    val reportId: UUID,
    val senderUnitId: String,
    val tacticalSignPath: String?,
    val report: String,
    val receiverUnitIds: List<String>?,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val createdAt: Instant,
    val referencingCommandId: UUID?,
    val images: List<String>?,
    val location: LocationPayload?,
)
