/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mqtt

import io.lion5.smartcity.mqtt.payload.outbound.OutboundAdhocJoinRequestPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundCommandPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundDangerZonePayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundMissionSectionPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundMissionStateChangedPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundPoiPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundReportPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundUnitAvailabilityChangedPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundUnitLocationPayload
import org.springframework.integration.annotation.Gateway
import org.springframework.integration.annotation.MessagingGateway
import org.springframework.messaging.handler.annotation.Header
import org.springframework.messaging.handler.annotation.Payload

@MessagingGateway
interface MqttGateway {
    @Gateway(requestChannel = "locationOutboundChannel")
    fun publishLocationUpdate(
        @Payload payload: OutboundUnitLocationPayload,
        @Header("mission_id") missionId: String,
    )

    @Gateway(requestChannel = "reportOutboundChannel")
    fun publishReport(
        @Payload payload: OutboundReportPayload,
        @Header("mission_id") missionId: String,
    )

    @Gateway(requestChannel = "commandOutboundChannel")
    fun publishCommand(
        @Payload payload: OutboundCommandPayload,
        @Header("mission_id") missionId: String,
    )

    @Gateway(requestChannel = "dangerZoneOutboundChannel")
    fun publishDangerZone(
        @Payload payload: OutboundDangerZonePayload,
        @Header("mission_id") missionId: String,
    )

    @Gateway(requestChannel = "missionSectionOutboundChannel")
    fun publishMissionSection(
        @Payload payload: OutboundMissionSectionPayload,
        @Header("mission_id") missionId: String,
    )

    @Gateway(requestChannel = "missionOutboundChannel")
    fun publishUnitAvailabilityChanged(
        @Payload payload: OutboundUnitAvailabilityChangedPayload,
        @Header("mission_id") missionId: String,
    )

    @Gateway(requestChannel = "missionOutboundChannel")
    fun publishMissionStateChanged(
        @Payload payload: OutboundMissionStateChangedPayload,
        @Header("mission_id") missionId: String,
    )

    @Gateway(requestChannel = "poiOutboundChannel")
    fun publishPoi(
        @Payload payload: OutboundPoiPayload,
        @Header("mission_id") missionId: String,
    )

    @Gateway(requestChannel = "adhocJoinRequestOutboundChannel")
    fun publishAdhocJoinRequest(
        @Payload outboundAdhocJoinRequestPayload: OutboundAdhocJoinRequestPayload,
        @Header("mission_id") missionId: String,
    )
}
