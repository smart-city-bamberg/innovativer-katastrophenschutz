/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mqtt.payload.inbound

import io.lion5.smartcity.mqtt.payload.LocationPayload
import io.lion5.smartcity.util.const.UnitAvailability
import org.springframework.format.annotation.DateTimeFormat
import java.time.Instant

data class InboundUnitAvailablityChangedPayload(
    val unitId: String,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val createdAt: Instant,
    val unitAvailability: UnitAvailability,
    val location: LocationPayload?,
)
