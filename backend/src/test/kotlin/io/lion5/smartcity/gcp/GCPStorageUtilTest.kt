/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.gcp

import com.google.cloud.storage.Blob
import com.google.cloud.storage.BlobInfo
import com.google.cloud.storage.Storage
import com.google.cloud.storage.StorageException
import io.lion5.smartcity.util.exception.ImageStorageException
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.unmockkAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.http.MediaType
import org.springframework.util.MimeTypeUtils
import java.io.IOException

class GCPStorageUtilTest {
    @MockK
    private lateinit var storage: Storage

    private lateinit var gcpStorageUtil: GCPStorageUtil

    private val bucketName = "my-test-bucket"
    private val testImage = "test image".toByteArray()

    @BeforeEach
    fun setUp() {
        MockKAnnotations.init(this)
        gcpStorageUtil = GCPStorageUtil(bucketName, storage)
    }

    @AfterEach
    fun `Remove all mocks`() {
        unmockkAll()
    }

    @Test
    fun `saveImage - returns blobInfo`() {
        val blobName = "my-special-test-file"
        val expectedContentType = MediaType.IMAGE_PNG_VALUE
        val expectedBlobInfoMock = mockk<BlobInfo>()
        val blobMock = mockk<Blob>()
        every { blobMock.asBlobInfo() }.returns(expectedBlobInfoMock)

        every {
            storage.create(
                BlobInfo.newBuilder(bucketName, blobName).setContentType(expectedContentType).build(),
                testImage,
            )
        }.returns(blobMock)

        val blobInfo = gcpStorageUtil.saveImage(testImage, blobName, expectedContentType)
        assertEquals(expectedBlobInfoMock, blobInfo)
    }

    @Test
    fun `saveImage - throws ImageStorageException if gcpStorageService saveImage throws ImageStorageException`() {
        val blobName = "my-special-test-file"
        val expectedException = ImageStorageException("test saveImage excpetion", null)

        every {
            storage.create(any(), testImage)
        }.throws(expectedException)

        val exception =
            assertThrows<ImageStorageException> { gcpStorageUtil.saveImage(testImage, blobName, MimeTypeUtils.IMAGE_JPEG_VALUE) }
        assertEquals(expectedException.message, exception.message)
    }

    @Test
    fun `deleteImage - deletes image`() {
        val blobName = "my-special-test-file"
        every { storage.delete(bucketName, blobName) }.returns(true)

        gcpStorageUtil.deleteImage(blobName)
    }

    @Test
    fun `deleteImage - throws ImageStorageException if storage delete throws StorageException`() {
        val blobName = "my-special-test-file"
        every { storage.delete(bucketName, blobName) }.throws(StorageException(IOException("cannot delete image")))

        val exception = assertThrows<ImageStorageException> { gcpStorageUtil.deleteImage(blobName) }
        assertEquals("Image with blobName $blobName could not be deleted", exception.message)
    }
}
