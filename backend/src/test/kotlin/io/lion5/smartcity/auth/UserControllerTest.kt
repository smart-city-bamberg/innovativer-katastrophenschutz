/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.auth

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.lion5.smartcity.auth.model.AuthorityRoles
import io.lion5.smartcity.auth.model.User
import io.lion5.smartcity.auth.util.UserFixture
import io.lion5.smartcity.auth.util.WithMockFirebaseUser
import io.lion5.smartcity.config.SecurityConfiguration
import io.lion5.smartcity.mongodb.service.AuditService
import io.mockk.every
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.security.acls.AclPermissionEvaluator
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.post

@WebMvcTest(UserController::class)
@Import(SecurityConfiguration::class)
@MockkBean(AclPermissionEvaluator::class)
internal class UserControllerTest(
    @Autowired val mockMvc: MockMvc,
) {
    @MockkBean
    lateinit var userService: UserService

    @MockkBean(relaxed = true)
    lateinit var auditService: AuditService

    private val objectMapper = ObjectMapper()

    @Test
    @WithMockFirebaseUser(role = AuthorityRoles.SUPER_ADMIN)
    fun `user can be created by SUPER_ADMIN role`() {
        val user = UserFixture.user(password = "abc")
        val expectedUser = UserFixture.user(password = null)
        val userJson = objectMapper.writeValueAsString(user)

        val userCaptor = slot<User>()
        every { userService.createUser(capture(userCaptor)) } returns expectedUser

        mockMvc
            .post("/users") {
                content = userJson
                contentType = MediaType.APPLICATION_JSON
            }.andExpect {
                status { isOk() }
                content { json(objectMapper.writeValueAsString(expectedUser)) }
            }

        verify { userService.createUser(userCaptor.captured) }
    }

    @Test
    @WithMockFirebaseUser(role = AuthorityRoles.ADMIN)
    fun `user cannot be created by ADMIN role`() {
        val user = UserFixture.user(password = "abc")
        val expectedUser = UserFixture.user(password = null)
        val userJson = objectMapper.writeValueAsString(user)

        every { userService.createUser(any()) } returns expectedUser

        mockMvc
            .post("/users") {
                content = userJson
                contentType = MediaType.APPLICATION_JSON
            }.andExpect {
                status { isForbidden() }
            }

        verify(inverse = true) { userService.createUser(any()) }
    }

    @Test
    @WithMockFirebaseUser(role = AuthorityRoles.USER)
    fun `user cannot be created by USER role`() {
        val user = UserFixture.user(password = "abc")
        val expectedUser = UserFixture.user(password = null)
        val userJson = objectMapper.writeValueAsString(user)

        every { userService.createUser(any()) } returns expectedUser

        mockMvc
            .post("/users") {
                content = userJson
                contentType = MediaType.APPLICATION_JSON
            }.andExpect {
                status { isForbidden() }
            }

        verify(inverse = true) { userService.createUser(any()) }
    }

    @Test
    @WithMockFirebaseUser(role = AuthorityRoles.VEHICLE)
    fun `user cannot be created by VEHICLE role`() {
        val user = UserFixture.user(password = "abc")
        val expectedUser = UserFixture.user(password = null)
        val userJson = objectMapper.writeValueAsString(user)

        every { userService.createUser(any()) } returns expectedUser

        mockMvc
            .post("/users") {
                content = userJson
                contentType = MediaType.APPLICATION_JSON
            }.andExpect {
                status { isForbidden() }
            }

        verify(inverse = true) { userService.createUser(any()) }
    }
}
