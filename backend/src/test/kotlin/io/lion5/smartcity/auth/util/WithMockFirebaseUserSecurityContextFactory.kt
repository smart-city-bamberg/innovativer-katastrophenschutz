/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.auth.util

import io.lion5.smartcity.auth.model.FirebasePrincipal
import io.lion5.smartcity.auth.model.Role
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.test.context.support.WithSecurityContextFactory

class WithMockFirebaseUserSecurityContextFactory : WithSecurityContextFactory<WithMockFirebaseUser> {
    override fun createSecurityContext(annotation: WithMockFirebaseUser): SecurityContext {
        val context = SecurityContextHolder.createEmptyContext()
        val principal = FirebasePrincipal(annotation.email)
        val role =
            Role.fromName(annotation.role)
                ?: throw IllegalArgumentException("No valid role provided: ${annotation.role}")
        val auth: Authentication =
            UsernamePasswordAuthenticationToken(
                principal,
                "test.token",
                listOf(role.toGrantedAuthority()),
            )
        context.authentication = auth
        return context
    }
}
