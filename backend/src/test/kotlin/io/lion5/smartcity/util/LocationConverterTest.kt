/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.util

import io.lion5.smartcity.mongodb.model.location.GKLocation
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class LocationConverterTest {
    private val converterService = LocationConverterService()
    private val gpsDelta = 0.00001

    @Test
    fun `Convert GK4 to GPS`() {
        // Test Location Leitstelle Bamberg https://geoportal.bayern.de/bayernatlas/?lang=de&topic=ba&catalogNodes=11&bgLayer=atkis&E=635744.35&N=5526392.42&zoom=14
        var gkLocation = GKLocation(4420263.94, 5527041.07)
        var gpsLocation = converterService.convertToGPS(gkLocation)

        assertEquals(49.87455, gpsLocation.latitude, gpsDelta)
        assertEquals(10.88925, gpsLocation.longitude, gpsDelta)

        // Leitstelle München https://geoportal.bayern.de/bayernatlas/?lang=de&topic=ba&catalogNodes=11&bgLayer=atkis&E=690026&N=5336837&zoom=14.426666666666664&crosshair=marker
        gkLocation = GKLocation(4467018.0, 5335467.0)
        gpsLocation = converterService.convertToGPS(gkLocation)

        assertEquals(48.15637, gpsLocation.latitude, gpsDelta)
        assertEquals(11.55523, gpsLocation.longitude, gpsDelta)
    }

    @Test
    fun `Test GK zone detection`() {
        var gKLocation = GKLocation(4420263.94, 5527041.07)
        assertEquals(4, gKLocation.zone)
        gKLocation = GKLocation(5457190.967, 5733462.400)
        assertEquals(5, gKLocation.zone)
    }
}
