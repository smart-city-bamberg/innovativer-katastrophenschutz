/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.service.image

import io.lion5.smartcity.util.exception.ImageResizeException
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.SpyK
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.slot
import io.mockk.unmockkStatic
import io.mockk.verify
import net.coobird.thumbnailator.Thumbnails
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import java.io.IOException
import javax.imageio.ImageIO

class ImageResizeServiceTest {
    @SpyK
    @InjectMockKs
    private lateinit var imageResizeService: ImageResizeService

    @BeforeEach
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @AfterEach
    fun `Remove all mocks`() {
        unmockkStatic(Thumbnails::class)
    }

    @Test
    fun `resizeImage - returns buffered image`() {
        val expectedWidth = 4
        val expectedHeight = 5
        val expectedContentType = MediaType.IMAGE_PNG
        val originalImageMock = mockk<BufferedImage>()
        mockkStatic(Thumbnails::class)

        every {
            Thumbnails
                .of(originalImageMock)
                .size(expectedWidth, expectedHeight)
                .keepAspectRatio(true)
                .outputFormat(expectedContentType.subtype)
                .asBufferedImage()
        }.returns(originalImageMock)
        imageResizeService.resizeImage(originalImageMock, expectedWidth, expectedHeight, expectedContentType)
    }

    @Test
    fun `resizeImage - throws ImageResizeException on thumbnailator IllegalStateException`() {
        val originalImageMock = mockk<BufferedImage>()
        mockkStatic(Thumbnails::class)

        every {
            Thumbnails.of(originalImageMock)
        }.throws(IllegalStateException("test exception"))
        val exception = assertThrows<ImageResizeException> { imageResizeService.resizeImage(originalImageMock, 1, 2, MediaType.IMAGE_PNG) }
        assertEquals("Cannot keep aspect ratio", exception.message)
    }

    @Test
    fun `resizeImage - throws ImageResizeException on thumbnailator IllegalArgumentException`() {
        val originalImageMock = mockk<BufferedImage>()
        mockkStatic(Thumbnails::class)

        every {
            Thumbnails.of(originalImageMock)
        }.throws(IllegalArgumentException("test exception"))
        val exception = assertThrows<ImageResizeException> { imageResizeService.resizeImage(originalImageMock, 1, 2, MediaType.IMAGE_PNG) }
        assertEquals("Format is not supported", exception.message)
    }

    @Test
    fun `resizeImage - throws ImageResizeException on thumbnailator IOException`() {
        val originalImageMock = mockk<BufferedImage>()
        mockkStatic(Thumbnails::class)

        every {
            Thumbnails.of(originalImageMock)
        }.throws(IOException("test exception"))
        val exception = assertThrows<ImageResizeException> { imageResizeService.resizeImage(originalImageMock, 1, 2, MediaType.IMAGE_PNG) }
        assertEquals("A problem occurred during the reading of the original image", exception.message)
    }

    @ParameterizedTest
    @CsvSource("200, 100, 400, 50, 200, 25", "200, 100, 100, 200, 50, 100")
    fun `resizeImageToMaxResolution - resizes to max width and max height while keeping aspect ratio`(
        maxWidth: Int,
        maxHeight: Int,
        originalWidth: Int,
        originalHeight: Int,
        expectedNewWidth: Int,
        expectedNewHeight: Int,
    ) {
        val originalImageMock = mockk<BufferedImage>()
        every { originalImageMock.width } returns originalWidth
        every { originalImageMock.height } returns originalHeight

        val widthSlot = slot<Int>()
        val heightSlot = slot<Int>()

        every { imageResizeService.resizeImage(any(), capture(widthSlot), capture(heightSlot), MediaType.IMAGE_JPEG) } answers {
            mockk<BufferedImage>().apply {
                every { width } returns widthSlot.captured
                every { height } returns heightSlot.captured
            }
        }

        val imageBytes = "image data".toByteArray()
        val imageMock = MockMultipartFile("image", "original.jpg", MediaType.IMAGE_JPEG_VALUE, imageBytes)

        mockkStatic(ImageIO::class)
        every { ImageIO.read(any<ByteArrayInputStream>()) } returns originalImageMock

        imageResizeService.resizeImageToMaxResolution(imageMock, maxWidth, maxHeight, MediaType.IMAGE_JPEG)

        verify { imageResizeService.resizeImage(any(), eq(expectedNewWidth), eq(expectedNewHeight), eq(MediaType.IMAGE_JPEG)) }

        assertEquals(expectedNewWidth, widthSlot.captured)
        assertEquals(expectedNewHeight, heightSlot.captured)
    }
}
