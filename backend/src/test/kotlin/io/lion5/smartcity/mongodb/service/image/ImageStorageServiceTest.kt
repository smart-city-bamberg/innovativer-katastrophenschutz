/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.service.image

import com.google.cloud.storage.BlobInfo
import io.lion5.smartcity.gcp.GCPStorageUtil
import io.lion5.smartcity.mongodb.model.Image
import io.lion5.smartcity.mongodb.repository.ImageRepository
import io.lion5.smartcity.util.exception.ImageNotFoundException
import io.lion5.smartcity.util.exception.ImageStorageException
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.SpyK
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkAll
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.MediaType
import org.springframework.util.MimeTypeUtils
import org.springframework.web.multipart.MultipartFile
import java.util.UUID

class ImageStorageServiceTest {
    @MockK
    private lateinit var gcpStorageUtil: GCPStorageUtil

    @MockK
    private lateinit var imageRepository: ImageRepository

    @SpyK(recordPrivateCalls = true)
    @InjectMockKs
    private lateinit var imageStorageService: ImageStorageService

    @BeforeEach
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @AfterEach
    fun `Remove all mocks`() {
        unmockkAll()
    }

    @Test
    fun `saveImage - returns image entity using byteArray when image entity already exists`() {
        val expectedFileName = "my-special-test-file"
        val expectedImage = "test image".toByteArray()
        val expectedContentType = MediaType.IMAGE_PNG_VALUE
        val blobInfoMock = mockk<BlobInfo>()
        every { blobInfoMock.mediaLink }.returns("https://my-special-blob-link")
        every { blobInfoMock.blobId.name }.returns("my-special-blob")
        val existingImageEntity = Image(url = "https://old-image-url", blobName = blobInfoMock.blobId.name)
        val expectedImageEntity = Image(url = blobInfoMock.mediaLink, blobName = blobInfoMock.blobId.name)

        every { gcpStorageUtil.saveImage(expectedImage, expectedFileName, expectedContentType) }.returns(blobInfoMock)
        every { imageRepository.findByBlobName("my-special-blob") }.returns(existingImageEntity)
        every { imageRepository.save(any()) }.returns(expectedImageEntity)

        val imageEntity = imageStorageService.saveImage(expectedImage, expectedFileName, expectedContentType)
        assertEquals(expectedImageEntity, imageEntity)
    }

    @Test
    fun `saveImage - returns image entity using byteArray when image entity not exists`() {
        val expectedFileName = "my-special-test-file"
        val expectedImage = "test image".toByteArray()
        val expectedContentType = MediaType.IMAGE_PNG_VALUE
        val blobInfoMock = mockk<BlobInfo>()
        every { blobInfoMock.mediaLink }.returns("https://my-special-blob-link")
        every { blobInfoMock.blobId.name }.returns("my-special-blob")
        val expectedImageEntity = Image(url = blobInfoMock.mediaLink, blobName = blobInfoMock.blobId.name)

        every { gcpStorageUtil.saveImage(expectedImage, expectedFileName, expectedContentType) }.returns(blobInfoMock)
        every { imageRepository.findByBlobName("my-special-blob") }.returns(null)
        every { imageRepository.save(any()) }.returns(expectedImageEntity)

        val imageEntity = imageStorageService.saveImage(expectedImage, expectedFileName, expectedContentType)
        assertEquals(expectedImageEntity, imageEntity)
    }

    @Test
    fun `saveImage - throws ImageStorageException if gcpStorageService saveImage throws ImageStorageException`() {
        val expectedFileName = "my-special-test-file"
        val expectedImage = "test image".toByteArray()
        val imageStorageException = ImageStorageException("test saveImage exception", null)

        every { gcpStorageUtil.saveImage(expectedImage, expectedFileName, MediaType.IMAGE_PNG_VALUE) }.throws(
            imageStorageException,
        )

        val exception =
            assertThrows<ImageStorageException> {
                imageStorageService.saveImage(
                    expectedImage,
                    expectedFileName,
                    MediaType.IMAGE_PNG_VALUE,
                )
            }
        assertEquals(imageStorageException.message, exception.message)
    }

    @Test
    fun `saveMultipartFiles - returns ImageEntities`() {
        mockkStatic(UUID::class)
        val uuid = UUID.randomUUID()
        every { UUID.randomUUID() }.returns(uuid)

        val expectedFilePrefix = "test-prefix"

        val bytesImage1 = "image-1".toByteArray()
        val multipartFile1 = mockk<MultipartFile>()
        every { multipartFile1.bytes }.returns(bytesImage1)
        every { multipartFile1.contentType }.returns(MimeTypeUtils.IMAGE_PNG_VALUE)
        every { multipartFile1.originalFilename }.returns("image-1.png")
        val imageEntity1 = Image("1", "https://example.com/image-1", "image-1")
        val blobInfo1 = mockk<BlobInfo>()
        every { blobInfo1.blobId.name }.returns(imageEntity1.blobName)
        every { blobInfo1.mediaLink }.returns(imageEntity1.url)

        val bytesImage2 = "image-2".toByteArray()
        val multipartFile2 = mockk<MultipartFile>()
        every { multipartFile2.bytes }.returns(bytesImage2)
        every { multipartFile2.contentType }.returns(MimeTypeUtils.IMAGE_JPEG_VALUE)
        every { multipartFile2.originalFilename }.returns("image-2.jpeg")
        val imageEntity2 = Image("2", "https://example.com/image-2", "image-2")
        val blobInfo2 = mockk<BlobInfo>()
        every { blobInfo2.blobId.name }.returns(imageEntity2.blobName)
        every { blobInfo2.mediaLink }.returns(imageEntity2.url)

        val bytesImage3 = "image-3".toByteArray()
        val multipartFile3 = mockk<MultipartFile>()
        every { multipartFile3.bytes }.returns(bytesImage3)
        every { multipartFile3.contentType }.returns(MimeTypeUtils.IMAGE_JPEG_VALUE)
        every { multipartFile3.originalFilename }.returns("image-3.jpg")
        val imageEntity3 = Image("3", "https://example.com/image-3", "image-3")
        val blobInfo3 = mockk<BlobInfo>()
        every { blobInfo3.blobId.name }.returns(imageEntity3.blobName)
        every { blobInfo3.mediaLink }.returns(imageEntity3.url)

        every {
            imageStorageService.saveImage(
                bytesImage1,
                "$expectedFilePrefix/$uuid.png",
                MimeTypeUtils.IMAGE_PNG_VALUE,
            )
        }.returns(imageEntity1)
        every {
            imageStorageService.saveImage(
                bytesImage2,
                "$expectedFilePrefix/$uuid.jpeg",
                MimeTypeUtils.IMAGE_JPEG_VALUE,
            )
        }.returns(imageEntity2)
        every {
            imageStorageService.saveImage(
                bytesImage3,
                "$expectedFilePrefix/$uuid.jpg",
                MimeTypeUtils.IMAGE_JPEG_VALUE,
            )
        }.returns(imageEntity3)

        val imageEntities =
            imageStorageService.saveMultipartFiles(
                arrayOf(multipartFile1, multipartFile2, multipartFile3),
                expectedFilePrefix,
            )

        assertThat(imageEntities[0]).usingRecursiveComparison().isEqualTo(imageEntity1)
        assertThat(imageEntities[1]).usingRecursiveComparison().isEqualTo(imageEntity2)
        assertThat(imageEntities[2]).usingRecursiveComparison().isEqualTo(imageEntity3)
    }

    @Test
    fun `saveMultipartFiles - throws ImageStorageException if mimeType of at least one multipart file is not set`() {
        mockkStatic(UUID::class)
        val uuid = UUID.randomUUID()
        every { UUID.randomUUID() }.returns(uuid)

        val filePrefix = "test-prefix"
        val bytesImage1 = "image-1".toByteArray()
        val multipartFile1 = mockk<MultipartFile>()
        every { multipartFile1.bytes }.returns(bytesImage1)
        every { multipartFile1.contentType }.returns(MimeTypeUtils.IMAGE_PNG_VALUE)
        every { multipartFile1.originalFilename }.returns("image-1.png")
        val imageEntity1 = Image("1", "https://example.com/image-1", "image-1")
        val blobInfo1 = mockk<BlobInfo>()
        every { blobInfo1.blobId.name }.returns(imageEntity1.blobName)
        every { blobInfo1.mediaLink }.returns(imageEntity1.url)

        val bytesImage2 = "image-2".toByteArray()
        val multipartFile2 = mockk<MultipartFile>()
        every { multipartFile2.bytes }.returns(bytesImage2)
        every { multipartFile2.contentType }.returns(null)
        every { multipartFile2.originalFilename }.returns("image-2.jpeg")
        val imageEntity2 = Image("2", "https://example.com/image-2", "image-2")
        val blobInfo2 = mockk<BlobInfo>()
        every { blobInfo2.blobId.name }.returns(imageEntity2.blobName)
        every { blobInfo2.mediaLink }.returns(imageEntity2.url)

        val bytesImage3 = "image-3".toByteArray()
        val multipartFile3 = mockk<MultipartFile>()
        every { multipartFile3.bytes }.returns(bytesImage3)
        every { multipartFile3.contentType }.returns(MimeTypeUtils.IMAGE_PNG_VALUE)
        every { multipartFile3.originalFilename }.returns("image-3.png")
        val imageEntity3 = Image("3", "https://example.com/image-3", "image-3")
        val blobInfo3 = mockk<BlobInfo>()
        every { blobInfo3.blobId.name }.returns(imageEntity3.blobName)
        every { blobInfo3.mediaLink }.returns(imageEntity3.url)

        every {
            imageStorageService.saveImage(
                bytesImage1,
                "$filePrefix/$uuid.png",
                MimeTypeUtils.IMAGE_PNG_VALUE,
            )
        }.returns(imageEntity1)
        every {
            imageStorageService.saveImage(
                bytesImage2,
                "$filePrefix/$uuid.jpeg",
                MimeTypeUtils.IMAGE_JPEG_VALUE,
            )
        }.returns(imageEntity2)
        every {
            imageStorageService.saveImage(
                bytesImage3,
                "$filePrefix/$uuid.png",
                MimeTypeUtils.IMAGE_PNG_VALUE,
            )
        }.returns(imageEntity3)

        val exception =
            assertThrows<ImageStorageException> {
                imageStorageService.saveMultipartFiles(
                    arrayOf(multipartFile1, multipartFile2, multipartFile3),
                    filePrefix,
                )
            }

        assertEquals("Content type of image image-2.jpeg cannot be retrieved.", exception.message)
    }

    @Test
    fun `deleteImage - deletes image`() {
        val expectedImageId = "435"
        val expectedImageEntity = Image(url = "https://my-special-blob-link", blobName = "my-special-blob")

        every { imageRepository.findByIdOrNull(expectedImageId) }.returns(expectedImageEntity)
        every { gcpStorageUtil.deleteImage(expectedImageEntity.blobName) }.answers { }
        every { imageRepository.delete(expectedImageEntity) }.answers { }

        imageStorageService.deleteImage(expectedImageId)

        verify { gcpStorageUtil.deleteImage(expectedImageEntity.blobName) }
        verify { imageRepository.delete(expectedImageEntity) }
    }

    @Test
    fun `deleteImage - throws ImageNotFoundException if image does not exists`() {
        val imageId = "23"

        every { imageRepository.findByIdOrNull(imageId) }.returns(null)

        val exception = assertThrows<ImageNotFoundException> { imageStorageService.deleteImage(imageId) }
        assertEquals("Image with id $imageId can not be found. Abort deletion.", exception.message)
    }

    @Test
    fun `deleteImage - throws ImageStorageException if gcpStorageService deleteImage throws ImageStorageException`() {
        val expectedImageId = "435"
        val expectedImageEntity = Image(url = "https://my-special-blob-link", blobName = "my-special-blob")
        val imageStorageException = ImageStorageException("test exception", null)

        every { imageRepository.findByIdOrNull(expectedImageId) }.returns(expectedImageEntity)
        every { gcpStorageUtil.deleteImage(expectedImageEntity.blobName) }.throws(imageStorageException)

        val exception = assertThrows<ImageStorageException> { imageStorageService.deleteImage(expectedImageId) }
        assertEquals(imageStorageException.message, exception.message)
    }

    @Test
    fun `deleteImages - delete three images`() {
        val imageEntity1 = Image("1", "https://example.com/image-1", "image-1")
        val imageEntity2 = Image("2", "https://example.com/image-2", "image-2")
        val imageEntity3 = Image("3", "https://example.com/image-3", "image-3")
        val images = listOf(imageEntity1, imageEntity2, imageEntity3)

        every { imageRepository.findByIdOrNull(imageEntity1.id) }.returns(imageEntity1)
        every { imageRepository.findByIdOrNull(imageEntity2.id) }.returns(imageEntity2)
        every { imageRepository.findByIdOrNull(imageEntity3.id) }.returns(imageEntity3)
        every { gcpStorageUtil.deleteImage(imageEntity1.blobName) }.answers { }
        every { gcpStorageUtil.deleteImage(imageEntity2.blobName) }.answers { }
        every { gcpStorageUtil.deleteImage(imageEntity3.blobName) }.answers { }
        every { imageRepository.delete(imageEntity1) }.answers { }
        every { imageRepository.delete(imageEntity2) }.answers { }
        every { imageRepository.delete(imageEntity3) }.answers { }

        imageStorageService.deleteImages(images)

        verify { gcpStorageUtil.deleteImage(imageEntity1.blobName) }
        verify { imageRepository.delete(imageEntity1) }
        verify { gcpStorageUtil.deleteImage(imageEntity2.blobName) }
        verify { imageRepository.delete(imageEntity2) }
        verify { gcpStorageUtil.deleteImage(imageEntity3.blobName) }
        verify { imageRepository.delete(imageEntity3) }
    }

    @Test
    fun `deleteImages - passes if just two of three images can be deleted`() {
        val imageEntity1 = Image("1", "https://example.com/image-1", "image-1")
        val imageEntity2 = Image("2", "https://example.com/image-2", "image-2")
        val imageEntity3 = Image("3", "https://example.com/image-3", "image-3")
        val images = listOf(imageEntity1, imageEntity2, imageEntity3)

        every { imageRepository.findByIdOrNull(imageEntity1.id) }.returns(imageEntity1)
        every { imageRepository.findByIdOrNull(imageEntity2.id) }.returns(null)
        every { imageRepository.findByIdOrNull(imageEntity3.id) }.returns(imageEntity3)
        every { gcpStorageUtil.deleteImage(imageEntity1.blobName) }.answers { }
        every { gcpStorageUtil.deleteImage(imageEntity2.blobName) }.answers { }
        every { gcpStorageUtil.deleteImage(imageEntity3.blobName) }.answers { }
        every { imageRepository.delete(imageEntity1) }.answers { }
        every { imageRepository.delete(imageEntity2) }.answers { }
        every { imageRepository.delete(imageEntity3) }.answers { }

        imageStorageService.deleteImages(images)

        verify { gcpStorageUtil.deleteImage(imageEntity1.blobName) }
        verify { imageRepository.delete(imageEntity1) }
        verify(exactly = 0) { gcpStorageUtil.deleteImage(imageEntity2.blobName) }
        verify(exactly = 0) { imageRepository.delete(imageEntity2) }
        verify { gcpStorageUtil.deleteImage(imageEntity3.blobName) }
        verify { imageRepository.delete(imageEntity3) }
    }
}
