/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.handler

import com.ninjasquad.springmockk.MockkBean
import io.lion5.smartcity.eventstore.EventstoreMissionService
import io.lion5.smartcity.mongodb.model.Address
import io.lion5.smartcity.mongodb.model.AdhocJoinRequest
import io.lion5.smartcity.mongodb.model.Department
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.model.location.GKLocation
import io.lion5.smartcity.mongodb.model.location.GPSLocation
import io.lion5.smartcity.mongodb.repository.AdhocJoinRequestRepository
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import io.lion5.smartcity.mongodb.service.MissionService
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.slot
import io.mockk.verify
import org.bson.types.ObjectId
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.Instant
import java.util.Optional

@ExtendWith(SpringExtension::class)
class MissionEventHandlerTest {
    @MockkBean
    private lateinit var missionServiceMock: MissionService

    @MockkBean
    private lateinit var eventstoreMissionServiceMock: EventstoreMissionService

    @MockkBean
    private lateinit var emergencyUnitServiceMock: EmergencyUnitService

    @MockkBean
    private lateinit var adhocJoinRequestRepositoryMock: AdhocJoinRequestRepository

    private lateinit var missionEventHandler: MissionEventHandler

    private val missionId = ObjectId.get().toHexString()

    private val mission: Mission =
        Mission(
            missionId = missionId,
            missionNr = "1122",
            alarmDate = Instant.EPOCH,
            missionType = 1,
            state = Mission.State.RUNNING,
            missionTypeName = "Test",
            missionAddress = Address("Teststr.", "4711", 78712, "Teststadt"),
            radioGroup = "Radio Test",
            keyword = "keyword",
            cues = "cue",
            gpsLocation = GPSLocation(1.0, 1.0),
            gkLocation = GKLocation(0.0, 0.0),
            joinCode = "ABCD-1234",
            joinPin = "123456",
        )
    private val units: List<EmergencyUnit> =
        listOf(
            EmergencyUnit(
                unitId = "aaa111",
                unitType = "Type 1",
                callSign = "Test 11",
                tacticalSignPath = "sign/path/1",
                department = Department("123", "Feuerwehr", "FW", state = Department.DepartmentState.PERMANENT, users = emptyList()),
            ),
            EmergencyUnit(
                unitId = "bbb222",
                unitType = "Type 2",
                callSign = "Test 22",
                tacticalSignPath = "sign/path/2",
                department = Department("123", "Feuerwehr", "FW", state = Department.DepartmentState.PERMANENT, users = emptyList()),
            ),
        )

    @BeforeEach
    fun setUp() {
        missionEventHandler =
            MissionEventHandler(missionServiceMock, eventstoreMissionServiceMock, emergencyUnitServiceMock, adhocJoinRequestRepositoryMock)

        every { eventstoreMissionServiceMock.handleUnitAvailabilityChangedMessage(any()) } returns Unit
        every { eventstoreMissionServiceMock.handleMissionStateChangedMessage(any()) } returns Unit
    }

    @AfterEach
    fun tearDown() {
        clearMocks(missionServiceMock, eventstoreMissionServiceMock)
    }

    @Test
    fun `handleAfterCreate should create an event stream for the new mission`() {
        every { missionServiceMock.createEventStreamForMission(missionId) } returns true

        missionEventHandler.handleAfterCreate(mission)

        verify { missionServiceMock.createEventStreamForMission(mission.missionId!!) }
    }

    @Test
    fun `handleBeforeSave should send a mission state changed message if the state changes to finished`() {
        val updatedMission = mission.copy(state = Mission.State.FINISHED)
        every { missionServiceMock.findMissionById(missionId) } returns Optional.of(mission)

        missionEventHandler.handleBeforeSave(updatedMission)

        verify { eventstoreMissionServiceMock.handleMissionStateChangedMessage(any()) }
    }

    @Test
    fun `handleBeforeSave should not send a mission state changed message if the state does not change`() {
        val updatedMission = mission.copy()
        every { missionServiceMock.findMissionById(missionId) } returns Optional.of(mission)

        missionEventHandler.handleBeforeSave(updatedMission)

        verify(exactly = 0) { eventstoreMissionServiceMock.handleMissionStateChangedMessage(any()) }
    }

    @Test
    fun `handleAfterSave should update the creation method of temporary units`() {
        val temporaryUnit = units[0].copy(creationMethod = EmergencyUnit.CreationMethod.TEMPORARY)
        val permanentUnit = units[1].copy(creationMethod = EmergencyUnit.CreationMethod.PERMANENT)
        val missionWithUnits = mission.copy(units = setOf(temporaryUnit, permanentUnit), state = Mission.State.FINISHED)

        every { missionServiceMock.findMissionById(missionId) } returns Optional.of(missionWithUnits)
        every { emergencyUnitServiceMock.createOrUpdateEmergencyUnit(any()) } returns temporaryUnit
        every { adhocJoinRequestRepositoryMock.findAllByMission(any()) } returns emptyList()
        every { adhocJoinRequestRepositoryMock.saveAll(any<List<AdhocJoinRequest>>()) } returns emptyList()
        missionEventHandler.handleAfterSave(mission)

        verify(exactly = 1) { emergencyUnitServiceMock.createOrUpdateEmergencyUnit(temporaryUnit) }
        verify(exactly = 0) { emergencyUnitServiceMock.createOrUpdateEmergencyUnit(permanentUnit) }
    }

    @Test
    fun `handleAfterSave should update the status of all adhoc join requests to expired`() {
        val adhocJoinRequest =
            AdhocJoinRequest(
                adhocJoinRequestId = ObjectId.get().toHexString(),
                userFirebaseId = "123",
                mission = mission,
                status = AdhocJoinRequest.AdhocJoinRequestStatus.PENDING,
                departmentName = "TestDepartment",
                userName = "Test",
                unitTacticalSignPath = "test/path",
                unitCallSign = "testSign",
            )
        val missionWithUnits = mission.copy(units = setOf(units[0], units[1]), state = Mission.State.FINISHED)

        every { missionServiceMock.findMissionById(missionId) } returns Optional.of(missionWithUnits)
        every { emergencyUnitServiceMock.createOrUpdateEmergencyUnit(any()) } returns units[0]
        every { adhocJoinRequestRepositoryMock.findAllByMission(any()) } returns listOf(adhocJoinRequest)

        val slot = slot<List<AdhocJoinRequest>>()
        every { adhocJoinRequestRepositoryMock.saveAll(capture(slot)) } answers { slot.captured }

        missionEventHandler.handleAfterSave(mission)

        // capture the updated adhoc join request and verify that it was changed to EXPIRED
        val capturedRequests = slot.captured
        assertTrue(capturedRequests.all { it.status == AdhocJoinRequest.AdhocJoinRequestStatus.EXPIRED })
    }

    @Test
    fun `handleBeforeLinkSaveEvent must send a unit availability changed message`() {
        every { missionServiceMock.findMissionById(missionId) } returns Optional.of(mission)

        missionEventHandler.handleBeforeLinkSaveEvent(mission, units)

        // verify that eventstoreMissionService.handleUnitAvailabilityChangedMessage was called for each unit
        verify(exactly = units.size) { eventstoreMissionServiceMock.handleUnitAvailabilityChangedMessage(any()) }
    }

    @Test
    fun `handleBeforeLinkDeleteEvent must send a unit availability changed message`() {
        val missionWithUnits = mission.copy(units = units.toSet())
        every { missionServiceMock.findMissionById(missionId) } returns
            Optional.of(
                missionWithUnits,
            )

        // The handle before link delete function is a little strange because it is
        // invoked with list of units that is assigned to the mission after the delete operation is already successful
        // We cannot do anything about it. It's just how SPRING DATA REST works.
        val unitsRemainingAfterDelete = listOf(units[1])
        val updatedMission = mission.copy(units = unitsRemainingAfterDelete.toSet())
        // empty set is on purpose because it is always empty on DELETE
        missionEventHandler.handleBeforeLinkDeleteEvent(updatedMission, emptySet())

        // verify that eventstoreMissionService.handleUnitAvailabilityChangedMessage was called for each unit
        verify(exactly = unitsRemainingAfterDelete.size) {
            eventstoreMissionServiceMock.handleUnitAvailabilityChangedMessage(
                any(),
            )
        }
    }
}
