package io.lion5.smartcity.mongodb.service

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.lion5.smartcity.mongodb.model.TraceLocation
import io.lion5.smartcity.mongodb.model.UnitPositionTrace
import io.lion5.smartcity.mongodb.model.location.GPSLocation
import io.lion5.smartcity.mongodb.repository.MissionRepository
import io.lion5.smartcity.mongodb.repository.UnitPositionTraceRepository
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.Instant

@ExtendWith(MockKExtension::class, SpringExtension::class)
class UnitPositionTraceServiceTest {
    @MockK(relaxed = true)
    private lateinit var missionRepository: MissionRepository

    @MockK(relaxed = true)
    private lateinit var unitPositionTraceRepository: UnitPositionTraceRepository

    @InjectMockKs
    private lateinit var unitPositionTraceService: UnitPositionTraceService

    private val objectMapper =
        jacksonObjectMapper().apply {
            registerModule(JavaTimeModule())
            configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        }

    @Test
    fun `createGeoJson - handle geoJSON creation`() {
        // Create dummy data
        val traceLocations =
            setOf(
                TraceLocation(GPSLocation(10.0, 20.0), Instant.parse("2023-01-01T00:00:00Z")),
                TraceLocation(GPSLocation(30.0, 40.0), Instant.parse("2023-01-01T01:00:00Z")),
            )
        val unitPositionTrace =
            UnitPositionTrace(
                traceId = "traceId",
                unitId = "unitId",
                missionId = "missionId",
                traceLocations = traceLocations,
            )

        // Generate GeoJSON
        val geoJson = unitPositionTraceService.createGeoJson(unitPositionTrace)

        // Expected GeoJSON
        val expectedGeoJson =
            """
            {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Point",
                            "coordinates": [20.0, 10.0]
                        },
                        "properties": {
                            "timestamp": "2023-01-01T00:00:00Z",
                            "unitId": "unitId",
                            "missionId": "missionId"
                        }
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "Point",
                            "coordinates": [40.0, 30.0]
                        },
                        "properties": {
                            "timestamp": "2023-01-01T01:00:00Z",
                            "unitId": "unitId",
                            "missionId": "missionId"
                        }
                    },
                    {
                        "type": "Feature",
                        "geometry": {
                            "type": "LineString",
                            "coordinates": [
                                [20.0, 10.0],
                                [40.0, 30.0]
                            ]
                        },
                        "properties": {
                            "timestamp": "2023-01-01T00:00:00Z",
                            "unitId": "unitId",
                            "missionId": "missionId"
                        }
                    }
                ]
            }
            """.trimIndent()

        // Verify the generated GeoJSON
        assertEquals(objectMapper.readTree(expectedGeoJson), objectMapper.readTree(geoJson))
    }
}
