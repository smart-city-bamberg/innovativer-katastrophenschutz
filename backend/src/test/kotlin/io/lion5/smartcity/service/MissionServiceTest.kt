/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.service

import io.lion5.smartcity.auth.acl.AclPermissionService
import io.lion5.smartcity.eventstore.events.IMissionStreamRepository
import io.lion5.smartcity.mongodb.model.Address
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.model.location.GKLocation
import io.lion5.smartcity.mongodb.model.location.GPSLocation
import io.lion5.smartcity.mongodb.repository.DepartmentRepository
import io.lion5.smartcity.mongodb.repository.MissionRepository
import io.lion5.smartcity.mongodb.repository.UnitPositionTraceRepository
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import io.lion5.smartcity.mongodb.service.JoinCodeService
import io.lion5.smartcity.mongodb.service.MissionService
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.SpyK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import jakarta.validation.ConstraintViolation
import jakarta.validation.Validator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.context.ApplicationEventPublisher
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.Instant

@ExtendWith(MockKExtension::class, SpringExtension::class)
class MissionServiceTest {
    @SpyK
    var joinCodeService: JoinCodeService = JoinCodeService()

    @MockK(relaxed = true)
    private lateinit var missionRepository: MissionRepository

    @MockK(relaxed = true)
    private lateinit var missionStreamRepository: IMissionStreamRepository

    @MockK(relaxed = true)
    private lateinit var emergencyUnitService: EmergencyUnitService

    @MockK(relaxed = true)
    private lateinit var unitPositionTraceRepository: UnitPositionTraceRepository

    @MockK(relaxed = true)
    private lateinit var departmentRepository: DepartmentRepository

    @MockK(relaxed = true)
    private lateinit var aclPermissionService: AclPermissionService

    @MockK(relaxed = true)
    private lateinit var mongoTemplate: MongoTemplate

    @MockK(relaxed = true)
    private lateinit var validator: Validator

    @MockK(relaxed = true)
    private lateinit var applicationEventPublisher: ApplicationEventPublisher

    @MockK
    private lateinit var securityContext: SecurityContext

    @MockK
    private lateinit var authentication: Authentication

    @InjectMockKs
    private lateinit var missionService: MissionService

    @BeforeEach
    fun setup() {
        // Setup expected behavior
        every { securityContext.authentication } returns authentication
        every { authentication.name } returns "mockedUser"

        // Install the mock into the SecurityContextHolder
        SecurityContextHolder.setContext(securityContext)
    }

    @Test
    fun `createNewMission assigns new joinCode and joinPin`() {
        val mission =
            Mission(
                missionId = "abc-def",
                alarmDate = Instant.now(),
                missionNr = "123",
                missionType = 1,
                missionTypeName = "FF",
                gpsLocation = GPSLocation(0.0, 1.0),
                gkLocation = GKLocation(0.0, 1.0),
                missionAddress = Address("TestStraße", "42", 12345, "Bamberg"),
                units = emptySet(),
                radioGroup = "TestGruppe",
                state = Mission.State.RUNNING,
                keyword = "",
                cues = "",
            )
        every { missionRepository.save(any()) } answers { firstArg() }
        every { validator.validate(any<Mission>()) } returns emptySet<ConstraintViolation<Mission>>()
        every { aclPermissionService.addPermissionForMultipleUsers(any(), any(), any()) } returns Unit
        every { aclPermissionService.addPermissionForUser(any(), any(), any()) } returns Unit

        val savedMission = missionService.createNewMission(mission)

        assertTrue(savedMission.joinCode.matches(Regex("[A-Z0-9]{4}-[A-Z0-9]{4}")))
        assertTrue(savedMission.joinPin.matches(Regex("\\d{6}")))
        verify(exactly = 1) { missionRepository.save(any()) }
        verify(exactly = 1) { missionRepository.save(any()) }
    }

    @Test
    fun `createNewMission handles duplicate join codes`() {
        val mission =
            Mission(
                missionId = "abc-def",
                alarmDate = Instant.now(),
                missionNr = "123",
                missionType = 1,
                missionTypeName = "FF",
                gpsLocation = GPSLocation(0.0, 1.0),
                gkLocation = GKLocation(0.0, 1.0),
                missionAddress = Address("TestStraße", "42", 12345, "Bamberg"),
                units = emptySet(),
                radioGroup = "TestGruppe",
                state = Mission.State.RUNNING,
                keyword = "",
                cues = "",
            )
        val newJoinCode = "DUPLICATE"
        val newJoinPin = "NEWPIN"
        every { joinCodeService.generateJoinCode() } returns newJoinCode andThen "UNIQUECODE"
        every { joinCodeService.generateJoinPin() } returns newJoinPin
        every { validator.validate(any<Mission>()) } returns emptySet<ConstraintViolation<Mission>>()
        every { missionRepository.save(any()) } throws Exception("duplicate key error") andThenAnswer { firstArg<Mission>() }
        every { aclPermissionService.addPermissionForMultipleUsers(any(), any(), any()) } returns Unit
        every { aclPermissionService.addPermissionForUser(any(), any(), any()) } returns Unit

        val savedMission = missionService.createNewMission(mission)

        assertEquals("UNIQUECODE", savedMission.joinCode)
        assertEquals(newJoinPin, savedMission.joinPin)
        verify(exactly = 2) { missionRepository.save(any()) } // One for the duplicate, one for the unique
    }
}
