/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mongodb.service

import io.lion5.smartcity.mongodb.model.Department
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.model.EmergencyUnitUser
import io.lion5.smartcity.mongodb.repository.DepartmentRepository
import io.lion5.smartcity.mongodb.repository.EmergencyUnitRepository
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationEventPublisher
import org.springframework.web.server.ResponseStatusException
import java.time.Instant

@ExtendWith(MockKExtension::class)
class EmergencyUnitServiceTest {
    @MockK
    private lateinit var emergencyUnitRepository: EmergencyUnitRepository

    @MockK
    private lateinit var departmentRepository: DepartmentRepository

    @MockK(relaxed = true)
    private lateinit var applicationEventPublisher: ApplicationEventPublisher

    @Value("\${digimap.eventstore.streamPrefixes.mission}")
    private lateinit var missionStreamPrefix: String

    @InjectMockKs
    private lateinit var emergencyUnitService: EmergencyUnitService

    @Test
    fun `createEmergencyUnitWithUserDepartment should assign department if null and user belongs to a department`() {
        // Arrange
        val department = Department(departmentId = "dept1", name = "Department 1", slug = "dept1", users = listOf())
        val emergencyUnit =
            EmergencyUnit(
                unitId = "unit1",
                unitType = "Type1",
                callSign = "CallSign1",
                tacticalSignPath = "Path1",
                lastLocationUpdate = Instant.EPOCH,
                department = null,
                user = EmergencyUnitUser("user1"),
            )
        val userFirebaseId = "user1"
        val slot = slot<EmergencyUnit>()

        every { departmentRepository.findFirstByUsersUserId(userFirebaseId) } returns department
        every { emergencyUnitRepository.save(capture(slot)) } answers { slot.captured }

        // Act
        val result = emergencyUnitService.createEmergencyUnitWithUserDepartment(emergencyUnit, userFirebaseId)

        // Assert
        assertNotNull(result.department)
        assertEquals(department, result.department)
        verify { emergencyUnitRepository.save(any()) }
    }

    @Test
    fun `createEmergencyUnitWithUserDepartment should not assign department if not null`() {
        // Arrange
        val department1 = Department(departmentId = "dept1", name = "Department 1", slug = "dept1", users = listOf())
        val department2 = Department(departmentId = "dept2", name = "Department 2", slug = "dept1", users = listOf())
        val emergencyUnit =
            EmergencyUnit(
                unitId = "unit1",
                unitType = "Type1",
                callSign = "CallSign1",
                tacticalSignPath = "Path1",
                lastLocationUpdate = Instant.EPOCH,
                department = department1,
                user = EmergencyUnitUser("user1"),
            )
        val userFirebaseId = "user1"
        val slot = slot<EmergencyUnit>()

        every { emergencyUnitRepository.save(capture(slot)) } answers { slot.captured }
        every { departmentRepository.findFirstByUsersUserId(userFirebaseId) } returns department2

        // Act
        val result = emergencyUnitService.createEmergencyUnitWithUserDepartment(emergencyUnit, userFirebaseId)

        // Assert
        assertNotNull(result.department)
        assertEquals(department1, result.department)
        verify { emergencyUnitRepository.save(any()) }
    }

    @Test
    fun `createEmergencyUnitWithUserDepartment should throw exception if department is null and user does not belong to a department`() {
        // Arrange
        val emergencyUnit =
            EmergencyUnit(
                unitId = "unit1",
                unitType = "Type1",
                callSign = "CallSign1",
                tacticalSignPath = "Path1",
                lastLocationUpdate = Instant.EPOCH,
                department = null,
                user = EmergencyUnitUser("user1"),
            )
        val userFirebaseId = "user1"

        every { departmentRepository.findFirstByUsersUserId(userFirebaseId) } returns null

        // Act & Assert
        val exception =
            assertThrows<ResponseStatusException> {
                emergencyUnitService.createEmergencyUnitWithUserDepartment(emergencyUnit, userFirebaseId)
            }
        assertEquals(
            "No Department assigned for the Unit and creating user not part of any Department.",
            exception.reason,
        )
    }
}
