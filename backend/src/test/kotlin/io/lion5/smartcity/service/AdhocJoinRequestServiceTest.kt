/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.service

import io.lion5.smartcity.auth.UserService
import io.lion5.smartcity.auth.acl.AclPermissionService
import io.lion5.smartcity.http.dto.AdhocJoinRequestCreateDto
import io.lion5.smartcity.http.dto.MissionJoinResponseDto
import io.lion5.smartcity.mongodb.model.Address
import io.lion5.smartcity.mongodb.model.AdhocJoinRequest
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.model.location.GKLocation
import io.lion5.smartcity.mongodb.model.location.GPSLocation
import io.lion5.smartcity.mongodb.repository.AdhocJoinRequestRepository
import io.lion5.smartcity.mongodb.repository.MissionRepository
import io.lion5.smartcity.mongodb.service.AdhocJoinRequestService
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import io.lion5.smartcity.mqtt.MqttGateway
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.context.ApplicationEventPublisher
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import java.time.Instant
import java.util.Optional

class AdhocJoinRequestServiceTest {
    private lateinit var adhocJoinRequestService: AdhocJoinRequestService
    private lateinit var missionRepository: MissionRepository
    private lateinit var adhocJoinRequestRepository: AdhocJoinRequestRepository
    private lateinit var mqttGateway: MqttGateway
    private lateinit var aclPermissionService: AclPermissionService
    private lateinit var emergencyUnitService: EmergencyUnitService
    private lateinit var userService: UserService
    private lateinit var applicationEventPublisher: ApplicationEventPublisher

    val mission =
        Mission(
            missionId = "testId",
            alarmDate = Instant.now(),
            missionNr = "123",
            missionType = 1,
            missionTypeName = "FF",
            gpsLocation = GPSLocation(0.0, 1.0),
            gkLocation = GKLocation(0.0, 1.0),
            missionAddress = Address("TestStraße", "42", 12345, "Bamberg"),
            units = emptySet(),
            radioGroup = "TestGruppe",
            state = Mission.State.RUNNING,
            keyword = "",
            cues = "",
            joinCode = "ABCD-1234",
            joinPin = "123456",
        )

    @BeforeEach
    fun setup() {
        missionRepository = mockk(relaxed = true)
        adhocJoinRequestRepository = mockk(relaxed = true)
        mqttGateway = mockk(relaxed = true)
        aclPermissionService = mockk(relaxed = true)
        emergencyUnitService = mockk(relaxed = true)
        userService = mockk(relaxed = true)
        applicationEventPublisher = mockk(relaxed = true)

        adhocJoinRequestService =
            AdhocJoinRequestService(
                missionRepository,
                adhocJoinRequestRepository,
                mqttGateway,
                aclPermissionService,
                emergencyUnitService,
                userService,
                applicationEventPublisher,
            )
    }

    @Test
    fun `validateJoinCode should return true if mission is found`() {
        // given
        val joinCode = "ABCD-1234"
        every { missionRepository.findMissionByJoinCodeAndState(joinCode, Mission.State.RUNNING) } returns mission
        every { aclPermissionService.addPermissionForUser(any(), any(), any()) } returns Unit

        // when
        val result = adhocJoinRequestService.validateJoinCode(joinCode)

        // then
        verify(exactly = 1) { missionRepository.findMissionByJoinCodeAndState(joinCode, Mission.State.RUNNING) }
        assertEquals(result, MissionJoinResponseDto(mission.missionNr))
    }

    @Test
    fun `validateJoinCode throws ResponseStatusException if mission is not found`() {
        // given
        val joinCode = "ABCD-1234"
        every { missionRepository.findMissionByJoinCodeAndState(joinCode, Mission.State.RUNNING) } returns null

        // when
        val exception =
            assertThrows(ResponseStatusException::class.java) {
                adhocJoinRequestService.validateJoinCode(joinCode)
            }

        // then
        verify(exactly = 1) { missionRepository.findMissionByJoinCodeAndState(joinCode, Mission.State.RUNNING) }
        assertEquals(exception.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(exception.reason, "Mission not found")
    }

    @Test
    fun `validateJoinPin should return true if mission is found and joinPin is correct`() {
        // given
        val joinCode = "ABCD-1234"
        val joinPin = "123456"
        every { missionRepository.findMissionByJoinCodeAndState(joinCode, Mission.State.RUNNING) } returns mission

        // when
        val result = adhocJoinRequestService.validateJoinPin(joinCode, joinPin)

        // then
        verify(exactly = 1) { missionRepository.findMissionByJoinCodeAndState(joinCode, Mission.State.RUNNING) }
        assertEquals(result, MissionJoinResponseDto(mission.missionNr))
    }

    @Test
    fun `validateJoinPin throws ResponseStatusException if mission is not found`() {
        // given
        val joinCode = "ABCD-1234"
        val joinPin = "123456"
        every { missionRepository.findMissionByJoinCodeAndState(joinCode, Mission.State.RUNNING) } returns null

        // when
        val exception =
            assertThrows(ResponseStatusException::class.java) {
                adhocJoinRequestService.validateJoinPin(joinCode, joinPin)
            }

        // then
        verify(exactly = 1) { missionRepository.findMissionByJoinCodeAndState(joinCode, Mission.State.RUNNING) }
        assertEquals(exception.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(exception.reason, "Mission not found")
    }

    @Test
    fun `validateJoinPin throws ResponseStatusException if joinPin is incorrect`() {
        // given
        val joinCode = "ABCD-1234"
        val joinPin = "1234567"
        every { missionRepository.findMissionByJoinCodeAndState(joinCode, Mission.State.RUNNING) } returns mission

        // when
        val exception =
            assertThrows(ResponseStatusException::class.java) {
                adhocJoinRequestService.validateJoinPin(joinCode, joinPin)
            }

        // then
        verify(exactly = 1) { missionRepository.findMissionByJoinCodeAndState(joinCode, Mission.State.RUNNING) }
        assertEquals(exception.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(exception.reason, "Invalid join pin")
    }

    @Test
    fun `createAdhocJoinRequest should return adhocJoinRequest if mission is found and joinPin is correct`() {
        // given
        val userFirebaseId = "userFirebaseId"
        val adhocJoinRequestCreateDto =
            AdhocJoinRequestCreateDto(
                joinCode = "ABCD-1234",
                joinPin = "123456",
                userName = "userName",
                unitCallSign = "unitName",
                unitTacticalSignPath = "unitTacticalSignPath",
                departmentName = "departmentName",
            )
        every { missionRepository.findMissionByJoinCodeAndState(adhocJoinRequestCreateDto.joinCode, Mission.State.RUNNING) } returns mission
        every { adhocJoinRequestRepository.save(any()) } returns
            AdhocJoinRequest(
                adhocJoinRequestId = "testId",
                userFirebaseId = userFirebaseId,
                userName = adhocJoinRequestCreateDto.userName,
                unitCallSign = adhocJoinRequestCreateDto.unitCallSign,
                unitTacticalSignPath = adhocJoinRequestCreateDto.unitTacticalSignPath,
                departmentName = adhocJoinRequestCreateDto.departmentName,
                mission = mission,
                status = AdhocJoinRequest.AdhocJoinRequestStatus.PENDING,
            )
        every { mqttGateway.publishAdhocJoinRequest(any(), any()) } returns Unit
        every { userService.isAnonymousUser(userFirebaseId) } returns true
        every { adhocJoinRequestRepository.findFirstByUserFirebaseIdAndStatus(any(), any()) } returns null

        // when
        adhocJoinRequestService.createAdhocJoinRequest(userFirebaseId, adhocJoinRequestCreateDto)

        // then
        verify(exactly = 1) { adhocJoinRequestRepository.save(any()) }
    }

    @Test
    fun `createAdhocJoinRequest throws ResponseStatusException if mission is not found`() {
        // given
        val userFirebaseId = "userFirebaseId"
        val adhocJoinRequestCreateDto =
            AdhocJoinRequestCreateDto(
                joinCode = "ABCD-1234",
                joinPin = "123456",
                userName = "userName",
                unitCallSign = "unitName",
                unitTacticalSignPath = "unitTacticalSignPath",
                departmentName = "departmentName",
            )
        every { missionRepository.findMissionByJoinCodeAndState(adhocJoinRequestCreateDto.joinCode, Mission.State.RUNNING) } returns null
        every { userService.isAnonymousUser(userFirebaseId) } returns true

        // when
        val exception =
            assertThrows(ResponseStatusException::class.java) {
                adhocJoinRequestService.createAdhocJoinRequest(userFirebaseId, adhocJoinRequestCreateDto)
            }

        // then
        verify(exactly = 0) { adhocJoinRequestRepository.save(any()) }
        assertEquals(exception.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(exception.reason, "Mission not found")
    }

    @Test
    fun `createAdhocJoinRequest throws ResponseStatusException if joinPin is incorrect`() {
        // given
        val userFirebaseId = "userFirebaseId"
        val adhocJoinRequestCreateDto =
            AdhocJoinRequestCreateDto(
                joinCode = "ABCD-1234",
                joinPin = "1234567",
                userName = "userName",
                unitCallSign = "unitName",
                unitTacticalSignPath = "unitTacticalSignPath",
                departmentName = "departmentName",
            )
        every { missionRepository.findMissionByJoinCodeAndState(adhocJoinRequestCreateDto.joinCode, Mission.State.RUNNING) } returns mission
        every { userService.isAnonymousUser(userFirebaseId) } returns true

        // when
        val exception =
            assertThrows(ResponseStatusException::class.java) {
                adhocJoinRequestService.createAdhocJoinRequest(userFirebaseId, adhocJoinRequestCreateDto)
            }

        // then
        verify(exactly = 0) { adhocJoinRequestRepository.save(any()) }
        assertEquals(exception.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(exception.reason, "Invalid join pin")
    }

    @Test
    fun `createAdhocJoinRequest throws ResponseStatusException if User is not Anonymous`() {
        // given
        val userFirebaseId = "userFirebaseId"
        val adhocJoinRequestCreateDto =
            AdhocJoinRequestCreateDto(
                joinCode = "ABCD-1234",
                joinPin = "123456",
                userName = "userName",
                unitCallSign = "unitName",
                unitTacticalSignPath = "unitTacticalSignPath",
                departmentName = "departmentName",
            )
        every { missionRepository.findMissionByJoinCodeAndState(adhocJoinRequestCreateDto.joinCode, Mission.State.RUNNING) } returns mission
        every { userService.isAnonymousUser(userFirebaseId) } returns false

        // when
        val exception =
            assertThrows(ResponseStatusException::class.java) {
                adhocJoinRequestService.createAdhocJoinRequest(userFirebaseId, adhocJoinRequestCreateDto)
            }

        // then
        verify(exactly = 0) { adhocJoinRequestRepository.save(any()) }
        assertEquals(exception.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(exception.reason, "User must be anonymous")
    }

    @Test
    fun `approveAdhocJoinRequest should return adhocJoinRequest with status APPROVED if mission is found`() {
        // given
        val adhocJoinRequestId = "testId"
        val adhocJoinRequest =
            AdhocJoinRequest(
                adhocJoinRequestId = adhocJoinRequestId,
                userFirebaseId = "userFirebaseId",
                userName = "userName",
                unitCallSign = "unitName",
                unitTacticalSignPath = "unitTacticalSignPath",
                departmentName = "departmentName",
                mission = mission,
                status = AdhocJoinRequest.AdhocJoinRequestStatus.PENDING,
            )
        every { userService.isAnonymousUser(adhocJoinRequest.userFirebaseId) } returns false
    }

    @Test
    fun `approveAdhocJoinRequest creates temporary EmergencyUnit and assigns to AdhocJoinRequest`() {
        // given
        val adhocJoinRequestId = "testId"
        val adhocJoinRequest =
            AdhocJoinRequest(
                adhocJoinRequestId = adhocJoinRequestId,
                userFirebaseId = "userFirebaseId",
                userName = "userName",
                unitCallSign = "unitName",
                unitTacticalSignPath = "unitTacticalSignPath",
                departmentName = "departmentName",
                mission = mission,
                status = AdhocJoinRequest.AdhocJoinRequestStatus.PENDING,
            )
        every { adhocJoinRequestRepository.findById(adhocJoinRequestId) } returns Optional.of(adhocJoinRequest)
        every { mqttGateway.publishAdhocJoinRequest(any(), any()) } returns Unit
        every { missionRepository.save(any()) } answers { mission }

        // Define a slot to capture the EmergencyUnit passed to createOrUpdateEmergencyUnit()
        val slotEmergencyUnit = slot<EmergencyUnit>()
        every { emergencyUnitService.createOrUpdateEmergencyUnit(capture(slotEmergencyUnit)) } answers { slotEmergencyUnit.captured }

        // Define a slot to capture the AdhocJoinRequest passed to save()
        val slotAdhocJoinRequest = slot<AdhocJoinRequest>()
        every { adhocJoinRequestRepository.save(capture(slotAdhocJoinRequest)) } answers { slotAdhocJoinRequest.captured }

        // when
        adhocJoinRequestService.approveAdhocJoinRequest(adhocJoinRequestId)

        // then
        verify(exactly = 1) { emergencyUnitService.createOrUpdateEmergencyUnit(any()) }
        assertEquals(slotAdhocJoinRequest.captured.createdUnit, slotEmergencyUnit.captured)
        assertEquals(slotEmergencyUnit.captured.creationMethod, EmergencyUnit.CreationMethod.TEMPORARY)
    }

    @Test
    fun `approveAdhocJoinRequest throws ResponseStatusException if adhocJoinRequest is not found`() {
        // given
        val adhocJoinRequestId = "testId"
        every { adhocJoinRequestRepository.findById(adhocJoinRequestId) } returns Optional.empty()

        // when
        val exception =
            assertThrows(ResponseStatusException::class.java) {
                adhocJoinRequestService.approveAdhocJoinRequest(adhocJoinRequestId)
            }

        // then
        verify(exactly = 1) { adhocJoinRequestRepository.findById(adhocJoinRequestId) }
        assertEquals(exception.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(exception.reason, "Adhoc join request not found")
    }

    @Test
    fun `approveAdhocJoinRequest throws ResponseStatusException if adhocJoinRequest is already approved or rejected`() {
        // given
        val adhocJoinRequestId = "testId"
        val adhocJoinRequest =
            AdhocJoinRequest(
                adhocJoinRequestId = adhocJoinRequestId,
                userFirebaseId = "userFirebaseId",
                userName = "userName",
                unitCallSign = "unitName",
                unitTacticalSignPath = "unitTacticalSignPath",
                departmentName = "departmentName",
                mission = mission,
                status = AdhocJoinRequest.AdhocJoinRequestStatus.APPROVED,
            )
        every { adhocJoinRequestRepository.findById(adhocJoinRequestId) } returns Optional.of(adhocJoinRequest)

        // when
        val exception =
            assertThrows(ResponseStatusException::class.java) {
                adhocJoinRequestService.approveAdhocJoinRequest(adhocJoinRequestId)
            }

        // then
        verify(exactly = 1) { adhocJoinRequestRepository.findById(adhocJoinRequestId) }
        assertEquals(exception.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(exception.reason, "Adhoc join request is already approved or rejected")
    }

    @Test
    fun `rejectAdhocJoinRequest should return adhocJoinRequest with status REJECTED if mission is found`() {
        // given
        val adhocJoinRequestId = "testId"
        val adhocJoinRequest =
            AdhocJoinRequest(
                adhocJoinRequestId = adhocJoinRequestId,
                userFirebaseId = "userFirebaseId",
                userName = "userName",
                unitCallSign = "unitName",
                unitTacticalSignPath = "unitTacticalSignPath",
                departmentName = "departmentName",
                mission = mission,
                status = AdhocJoinRequest.AdhocJoinRequestStatus.PENDING,
            )
        every { adhocJoinRequestRepository.findById(adhocJoinRequestId) } returns Optional.of(adhocJoinRequest)
        every { mqttGateway.publishAdhocJoinRequest(any(), any()) } returns Unit

        // Define a slot to capture the AdhocJoinRequest passed to save()
        val slot = slot<AdhocJoinRequest>()
        every { adhocJoinRequestRepository.save(capture(slot)) } answers { slot.captured }

        // when
        val result = adhocJoinRequestService.rejectAdhocJoinRequest(adhocJoinRequestId)

        // then
        verify(exactly = 1) { adhocJoinRequestRepository.findById(adhocJoinRequestId) }
        verify(exactly = 1) { adhocJoinRequestRepository.save(any()) }
        assertEquals(AdhocJoinRequest.AdhocJoinRequestStatus.REJECTED, slot.captured.status)
        assertEquals(result, slot.captured)
    }

    @Test
    fun `rejectAdhocJoinRequest throws ResponseStatusException if adhocJoinRequest is not found`() {
        // given
        val adhocJoinRequestId = "testId"
        every { adhocJoinRequestRepository.findById(adhocJoinRequestId) } returns Optional.empty()

        // when
        val exception =
            assertThrows(ResponseStatusException::class.java) {
                adhocJoinRequestService.rejectAdhocJoinRequest(adhocJoinRequestId)
            }

        // then
        verify(exactly = 1) { adhocJoinRequestRepository.findById(adhocJoinRequestId) }
        assertEquals(exception.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(exception.reason, "Adhoc join request not found")
    }

    @Test
    fun `rejectAdhocJoinRequest throws ResponseStatusException if adhocJoinRequest is already approved or rejected`() {
        // given
        val adhocJoinRequestId = "testId"
        val adhocJoinRequest =
            AdhocJoinRequest(
                adhocJoinRequestId = adhocJoinRequestId,
                userFirebaseId = "userFirebaseId",
                userName = "userName",
                unitCallSign = "unitName",
                unitTacticalSignPath = "unitTacticalSignPath",
                departmentName = "departmentName",
                mission = mission,
                status = AdhocJoinRequest.AdhocJoinRequestStatus.APPROVED,
            )
        every { adhocJoinRequestRepository.findById(adhocJoinRequestId) } returns Optional.of(adhocJoinRequest)

        // when
        val exception =
            assertThrows(ResponseStatusException::class.java) {
                adhocJoinRequestService.rejectAdhocJoinRequest(adhocJoinRequestId)
            }

        // then
        verify(exactly = 1) { adhocJoinRequestRepository.findById(adhocJoinRequestId) }
        assertEquals(exception.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(exception.reason, "Adhoc join request is already approved, rejected or canceled")
    }

    @Test
    fun `cancelAdhocJoinRequest should return adhocJoinRequest with status CANCELED if mission is found`() {
        // given
        val adhocJoinRequestId = "testId"
        val userFirebaseId = "userFirebaseId"
        val adhocJoinRequest =
            AdhocJoinRequest(
                adhocJoinRequestId = adhocJoinRequestId,
                userFirebaseId = userFirebaseId,
                userName = "userName",
                unitCallSign = "unitName",
                unitTacticalSignPath = "unitTacticalSignPath",
                departmentName = "departmentName",
                mission = mission,
                status = AdhocJoinRequest.AdhocJoinRequestStatus.PENDING,
            )
        every { adhocJoinRequestRepository.findById(adhocJoinRequestId) } returns Optional.of(adhocJoinRequest)
        every { mqttGateway.publishAdhocJoinRequest(any(), any()) } returns Unit
        every { userService.isAnonymousUser(userFirebaseId) } returns true

        // Define a slot to capture the AdhocJoinRequest passed to save()
        val slot = slot<AdhocJoinRequest>()
        every { adhocJoinRequestRepository.save(capture(slot)) } answers { slot.captured }

        // when
        val result = adhocJoinRequestService.cancelAdhocJoinRequest(adhocJoinRequestId, userFirebaseId)

        // then
        verify(exactly = 1) { adhocJoinRequestRepository.findById(adhocJoinRequestId) }
        verify(exactly = 1) { adhocJoinRequestRepository.save(any()) }
        assertEquals(AdhocJoinRequest.AdhocJoinRequestStatus.CANCELED, slot.captured.status)
        assertEquals(result, slot.captured)
    }

    @Test
    fun `cancelAdhocJoinRequest throws ResponseStatusException if adhocJoinRequest is not found`() {
        // given
        val adhocJoinRequestId = "testId"
        val userFirebaseId = "userFirebaseId"
        every { adhocJoinRequestRepository.findById(adhocJoinRequestId) } returns Optional.empty()
        every { userService.isAnonymousUser(userFirebaseId) } returns true

        // when
        val exception =
            assertThrows(ResponseStatusException::class.java) {
                adhocJoinRequestService.cancelAdhocJoinRequest(adhocJoinRequestId, userFirebaseId)
            }

        // then
        verify(exactly = 1) { adhocJoinRequestRepository.findById(adhocJoinRequestId) }
        assertEquals(exception.statusCode, HttpStatus.NOT_FOUND)
        assertEquals(exception.reason, "Adhoc join request not found")
    }

    @Test
    fun `cancelAdhocJoinRequest throws ResponseStatusException if User is not Anonymous`() {
        // given
        val adhocJoinRequestId = "testId"
        val userFirebaseId = "userFirebaseId"
        val adhocJoinRequest =
            AdhocJoinRequest(
                adhocJoinRequestId = adhocJoinRequestId,
                userFirebaseId = userFirebaseId,
                userName = "userName",
                unitCallSign = "unitName",
                unitTacticalSignPath = "unitTacticalSignPath",
                departmentName = "departmentName",
                mission = mission,
                status = AdhocJoinRequest.AdhocJoinRequestStatus.PENDING,
            )
        every { adhocJoinRequestRepository.findById(adhocJoinRequestId) } returns Optional.of(adhocJoinRequest)
        every { userService.isAnonymousUser(userFirebaseId) } returns false

        // when
        val exception =
            assertThrows(ResponseStatusException::class.java) {
                adhocJoinRequestService.cancelAdhocJoinRequest(adhocJoinRequestId, userFirebaseId)
            }

        // then
        verify(exactly = 0) { adhocJoinRequestRepository.save(any()) }
        assertEquals(exception.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(exception.reason, "User must be anonymous")
    }

    @Test
    fun `cancelAdhocJoinRequest throws ResponseStatusException if adhocJoinRequest is already approved or rejected`() {
        // given
        val adhocJoinRequestId = "testId"
        val userFirebaseId = "userFirebaseId"
        val adhocJoinRequest =
            AdhocJoinRequest(
                adhocJoinRequestId = adhocJoinRequestId,
                userFirebaseId = userFirebaseId,
                userName = "userName",
                unitCallSign = "unitName",
                unitTacticalSignPath = "unitTacticalSignPath",
                departmentName = "departmentName",
                mission = mission,
                status = AdhocJoinRequest.AdhocJoinRequestStatus.APPROVED,
            )
        every { adhocJoinRequestRepository.findById(adhocJoinRequestId) } returns Optional.of(adhocJoinRequest)
        every { userService.isAnonymousUser(userFirebaseId) } returns true

        // when
        val exception =
            assertThrows(ResponseStatusException::class.java) {
                adhocJoinRequestService.cancelAdhocJoinRequest(adhocJoinRequestId, userFirebaseId)
            }

        // then
        verify(exactly = 1) { adhocJoinRequestRepository.findById(adhocJoinRequestId) }
        assertEquals(exception.statusCode, HttpStatus.BAD_REQUEST)
        assertEquals(exception.reason, "Adhoc join request is already approved, rejected or canceled")
    }

    @Test
    fun `getWaitingRoom should return all pending adhocJoinRequests`() {
        // given
        val adhocJoinRequest1 =
            AdhocJoinRequest(
                adhocJoinRequestId = "testId1",
                userFirebaseId = "userFirebaseId1",
                userName = "userName1",
                unitCallSign = "unitName1",
                unitTacticalSignPath = "unitTacticalSignPath1",
                departmentName = "departmentName1",
                mission = mission,
                status = AdhocJoinRequest.AdhocJoinRequestStatus.PENDING,
            )
        val adhocJoinRequest2 =
            AdhocJoinRequest(
                adhocJoinRequestId = "testId2",
                userFirebaseId = "userFirebaseId2",
                userName = "userName2",
                unitCallSign = "unitName2",
                unitTacticalSignPath = "unitTacticalSignPath2",
                departmentName = "departmentName2",
                mission = mission,
                status = AdhocJoinRequest.AdhocJoinRequestStatus.PENDING,
            )
        every {
            adhocJoinRequestRepository.findAllByStatus(AdhocJoinRequest.AdhocJoinRequestStatus.PENDING)
        } returns listOf(adhocJoinRequest1, adhocJoinRequest2)

        // when
        val result = adhocJoinRequestService.findAllPendingAdhocJoinRequests()

        // then
        verify(exactly = 1) { adhocJoinRequestRepository.findAllByStatus(AdhocJoinRequest.AdhocJoinRequestStatus.PENDING) }
        assertEquals(result, listOf(adhocJoinRequest1, adhocJoinRequest2))
    }
}
