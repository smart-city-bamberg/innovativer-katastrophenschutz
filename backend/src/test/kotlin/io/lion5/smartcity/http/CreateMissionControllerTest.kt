/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.http

import com.ninjasquad.springmockk.MockkBean
import io.lion5.smartcity.mongodb.model.Address
import io.lion5.smartcity.mongodb.model.Department
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.model.location.GKLocation
import io.lion5.smartcity.mongodb.model.location.GPSLocation
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import io.lion5.smartcity.mongodb.service.MissionService
import io.mockk.every
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.security.acls.AclPermissionEvaluator
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.time.Instant
import java.util.Optional

@WebMvcTest(CreateMissionController::class)
@AutoConfigureMockMvc(addFilters = false)
@MockkBean(AclPermissionEvaluator::class)
class CreateMissionControllerTest(
    @Autowired val mockMvc: MockMvc,
) {
    @MockkBean
    lateinit var missionService: MissionService

    @MockkBean
    lateinit var emergencyUnitService: EmergencyUnitService

    private var emergencyUnit0: EmergencyUnit =
        EmergencyUnit(
            null,
            "test0",
            "Florian Musterdorf 30/1",
            "foo/bar.png",
            GPSLocation(0.0, 1.0),
            department = Department("123", "Feuerwehr", "FW", state = Department.DepartmentState.PERMANENT, users = emptyList()),
        )

    private val mission0 =
        Mission(
            missionId = "abc-def",
            alarmDate = Instant.now(),
            missionNr = "123",
            missionType = 1,
            missionTypeName = "FF",
            gpsLocation = GPSLocation(0.0, 1.0),
            gkLocation = GKLocation(0.0, 1.0),
            missionAddress = Address("TestStraße", "42", 12345, "Bamberg"),
            units = emptySet(),
            radioGroup = "TestGruppe",
            state = Mission.State.RUNNING,
            keyword = "",
            cues = "",
            joinCode = "ABCD-1234",
            joinPin = "123456",
        )

    private var missionJsonNoId =
        """
                {
          "missionId": "12345",
          "alarmDate": "2023-03-03T08:23:19Z",
          "missionNr": "FooNumber",
          "missionType": 4712,
          "missionTypeName": "Hochwasser Brand",
          "gpsLocation": {
            "latitude": 49.91602,
            "longitude": 10.88347
          },
          "gkLocation": {
            "x": 4419918.0,
            "y": 5531661.0
          },
          "missionAddress": {
            "street": "Aufsesshöflein",
            "houseNumber": "321",
            "zipCode": 96049,
            "locality": "Aufsesshöflein"
          },
          "units": [
          {
            "callSign": "unit-1",
            "tacticalSignPath": "tac/tical/path",
            "gpsLocation": {
              "latitude": 48.15637,
              "longitude": 11.55523
            },
            "department": {
              "departmentId": "123",
              "name": "Feuerwehr",
              "slug": "FW",
              "state": "PERMANENT",
              "users": []
            }
          }
          ],
          "radioGroup": "Tetra-4711",
          "state": "RUNNING",
          "keyword": "",
          "cues": ""
        }
                "
        """.trimIndent()

    private var missionJsonWithId =
        """
                {
          "missionId": "12345",
          "alarmDate": "2023-03-03T08:23:19Z",
          "missionNr": "FooNumber",
          "missionType": 4712,
          "missionTypeName": "Hochwasser Brand",
          "gpsLocation": {
            "latitude": 49.91602,
            "longitude": 10.88347
          },
          "gkLocation": {
            "x": 4419918.0,
            "y": 5531661.0
          },
          "missionAddress": {
            "street": "Aufsesshöflein",
            "houseNumber": "321",
            "zipCode": 96049,
            "locality": "Aufsesshöflein"
          },
          "units": [
          {
            "unitId": "Unit0",
            "callSign": "unit-1",
            "tacticalSignPath": "tac/tical/path",
            "gpsLocation": {
              "latitude": 48.15637,
              "longitude": 11.55523
            },
            "department": {
              "departmentId": "123",
              "name": "Feuerwehr",
              "slug": "FW",
              "state": "PERMANENT",
              "users": []
            }
          }
          ],
          "radioGroup": "Tetra-4711",
          "state": "RUNNING",
          "keyword": "",
          "cues": ""
        }
        """.trimIndent()

    @Test
    fun `bulk create mission including unit without id`() {
        every { missionService.createNewMission(any()) } returns mission0
        every { emergencyUnitService.createOrUpdateEmergencyUnit(any()) } returns emergencyUnit0
        every { missionService.createEventStreamForMission(any()) } returns true

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post("/missions/bulk-create")
                    .content(missionJsonNoId)
                    .contentType("application/json; charset=utf-8"),
            ).andExpect(MockMvcResultMatchers.status().isCreated)
            .andExpect(MockMvcResultMatchers.header().string("Location", "http://localhost/missions/abc-def"))
        verify { missionService.createNewMission(any()) }
        verify { emergencyUnitService.createOrUpdateEmergencyUnit(any()) }
    }

    @Test
    fun `bulk create mission including unit with id`() {
        every { missionService.createNewMission(any()) } returns mission0
        every { emergencyUnitService.getEmergencyUnitById(any()) } returns Optional.of(emergencyUnit0)
        every { missionService.createEventStreamForMission(any()) } returns true

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post("/missions/bulk-create")
                    .content(missionJsonWithId)
                    .contentType("application/json; charset=utf-8"),
            ).andExpect(MockMvcResultMatchers.status().isCreated)
            .andExpect(MockMvcResultMatchers.header().string("Location", "http://localhost/missions/abc-def"))
        verify { missionService.createNewMission(any()) }
        verify { emergencyUnitService.getEmergencyUnitById("Unit0") }
    }

    @Test
    fun `bulkCreateMission creates non-existing units`() {
        val nonExistingUnit =
            EmergencyUnit(
                unitId = "Unit0",
                unitType = "",
                callSign = "unit-1",
                tacticalSignPath = "tac/tical/path",
                gpsLocation = GPSLocation(48.15637, 11.55523),
                department = Department("123", "Feuerwehr", "FW", state = Department.DepartmentState.PERMANENT, users = emptyList()),
            )

        every { missionService.createNewMission(any()) } returns mission0
        every { emergencyUnitService.getEmergencyUnitById(any()) } returns Optional.empty()
        every { emergencyUnitService.createOrUpdateEmergencyUnit(any()) } returns nonExistingUnit
        every { missionService.createEventStreamForMission(any()) } returns true

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post("/missions/bulk-create")
                    .content(missionJsonWithId)
                    .contentType("application/json; charset=utf-8"),
            ).andExpect(MockMvcResultMatchers.status().isCreated)
            .andExpect(MockMvcResultMatchers.header().string("Location", "http://localhost/missions/abc-def"))
        verify { missionService.createNewMission(any()) }
        verify { emergencyUnitService.createOrUpdateEmergencyUnit(nonExistingUnit) }
    }

    @Test
    fun `bulk create mission starts event stream with mission id`() {
        every { missionService.createNewMission(any()) } returns mission0
        every { emergencyUnitService.createOrUpdateEmergencyUnit(any()) } returns emergencyUnit0
        every { missionService.createEventStreamForMission(mission0.missionId!!) } returns true

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post("/missions/bulk-create")
                    .content(missionJsonNoId)
                    .contentType("application/json; charset=utf-8"),
            ).andExpect(MockMvcResultMatchers.status().isCreated)
            .andExpect(MockMvcResultMatchers.header().string("Location", "http://localhost/missions/abc-def"))
        verify { missionService.createNewMission(any()) }
        verify { missionService.createEventStreamForMission(mission0.missionId!!) }
    }
}
