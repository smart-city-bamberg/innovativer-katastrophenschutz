/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.http

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.lion5.smartcity.http.dto.EdiMissionRequest
import io.lion5.smartcity.http.dto.Stichwort
import io.lion5.smartcity.mongodb.model.Address
import io.lion5.smartcity.mongodb.model.Department
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.model.location.GKLocation
import io.lion5.smartcity.mongodb.model.location.GPSLocation
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import io.lion5.smartcity.mongodb.service.MissionService
import io.lion5.smartcity.util.LocationConverterService
import io.mockk.every
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.header
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.Instant

@WebMvcTest(EdiController::class)
@AutoConfigureMockMvc(addFilters = false)
class EdiControllerTest(
    @Autowired val mockMvc: MockMvc,
) {
    @MockkBean
    lateinit var missionService: MissionService

    @MockkBean
    lateinit var unitService: EmergencyUnitService

    @MockkBean
    lateinit var locationConverterService: LocationConverterService

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    private var emergencyUnit0: EmergencyUnit =
        EmergencyUnit(
            null,
            "test0",
            "Florian Musterdorf 30/1",
            "foo/bar.png",
            GPSLocation(0.0, 1.0),
            department = Department("123", "Feuerwehr", "FW", state = Department.DepartmentState.PERMANENT, users = emptyList()),
        )

    private var emergencyUnit1: EmergencyUnit =
        EmergencyUnit(
            null,
            "test1",
            "Florian Musterdorf 40/1",
            "foo/bar2.png",
            GPSLocation(0.0, 1.0),
            department = Department("123", "Feuerwehr", "FW", state = Department.DepartmentState.PERMANENT, users = emptyList()),
        )

    @BeforeEach
    fun setUp() {
        every { unitService.findByCallSign("Florian Musterdorf 30/1") } returns emergencyUnit0
        every { unitService.findByCallSign("Florian Musterdorf 40/1") } returns emergencyUnit1
        every { locationConverterService.convertToGPS(any()) } returns GPSLocation(0.0, 1.0)
        every { missionService.createNewMission(any()) } returns
            Mission(
                "abc-def",
                Instant.now(),
                "123",
                1,
                "FF",
                GPSLocation(0.0, 1.0),
                GKLocation(0.0, 1.0),
                Address("TestStraße", "42", 12345, "Bamberg"),
                emptySet(),
                "TestGruppe",
                Mission.State.RUNNING,
                keyword = "#B1412#Landwirtschaft#Stall / Scheune ",
                cues = Stichwort(Name = "B 4", Type = "Brand").toString(),
                joinCode = "ABCD-1234",
                joinPin = "123456",
            )
    }

    @Test
    fun parseEdi() {
        Assertions.assertDoesNotThrow { objectMapper.readValue(ediJson, EdiMissionRequest::class.javaObjectType) }
    }

    @Test
    fun postEdiInformation() {
        mockMvc
            .perform(
                post("/missions/edi")
                    .content(ediJson)
                    .contentType("application/json; charset=utf-8"),
            ).andExpect(status().isCreated)
            .andExpect(header().string("Location", "http://localhost/missions/abc-def"))
    }

    private var ediJson: String = """
        {
          "Mission": {
            "AlarmDate": "2021-08-09T15:05:45Z",
            "AlarmInfo": [
              "K462-12345"
            ],
            "Blaulicht": true,
            "DispoList": {
              "4.1.3 FO FL Musterdorf 41/1": "Über-/Hochdrucklüfter",
              "4.1.3 BA-L FL Bamberg Land 1 Ziegmann": ""
            },
            "EinsatzNr": "B 4.1 2108XX XXX",
            "Einsatzplannr": "Plan Nr. 201",
            "EinsatztypId": 2,
            "EinsatztypName": "Brand",
            "EoAbschnitt": "Haupstraße",
            "EoHausNr": 1,
            "EoHausNrKz": null,
            "EoHausNrZusatz": null,
            "EoKreuzung": null,
            "EoObjekte": [
              {
                "Name": "4.1.1 BA-S ILS Bamberg-Forchheim",
                "Nr": null,
                "NrAlpha": null
              }
            ],
            "EoOrt": "Musterdorf Oberfr",
            "EoOrtsteil": "Musterdorf Oberfr",
            "EoPlz": "91000",
            "EoStrasse": "Paradiesweg",
            "EoX": 4420263.94,
            "EoY": 5527041.07,
            "Freitext": "gegen\u00fcber HS NR 25  brennt eine Halle",
            "Melder": [],
            "Prioritaet": 1,
            "Resources": [
              {
                "FunkRufname": "Florian Musterdorf 30/1",
                "FunkRufnameKurz": "FL MUSTER 30/1",
                "Name": "4.1.3 FO FL Musterdorf 30/1",
                "Status": null,
                "ZeitAbD": "2021-08-09T15:11:08Z",
                "ZeitAbE": null,
                "ZeitAbZ": "2021-08-09T15:29:25Z",
                "ZeitAbgezogen": null,
                "ZeitAl": "2021-08-09T15:05:46Z",
                "ZeitAnD": "2021-08-09T15:43:17Z",
                "ZeitAnE": "2021-08-09T15:16:44Z",
                "ZeitAnZ": null,
                "ZeitEinsatzBeginn": "2021-08-09T15:11:08Z",
                "ZeitEinsatzEnde": "2021-08-09T15:43:17Z",
                "ZeitErstAl": "2021-08-09T15:05:46Z",
                "Zeit_1": null,
                "Zeit_2": null
              },
              {
                "FunkRufname": "Florian Musterdorf 40/1",
                "FunkRufnameKurz": "FL MUSTER 40/1",
                "Name": "4.1.2 BA-L FL Musterdorf 40/1",
                "Status": null,
                "ZeitAbD": "2021-08-09T15:10:04Z",
                "ZeitAbE": null,
                "ZeitAbZ": "2021-08-09T15:48:14Z",
                "ZeitAbgezogen": null,
                "ZeitAl": "2021-08-09T15:05:48Z",
                "ZeitAnD": "2021-08-09T15:52:44Z",
                "ZeitAnE": "2021-08-09T15:16:19Z",
                "ZeitAnZ": null,
                "ZeitEinsatzBeginn": "2021-08-09T15:10:04Z",
                "ZeitEinsatzEnde": "2021-08-09T15:52:44Z",
                "ZeitErstAl": "2021-08-09T15:05:48Z",
                "Zeit_1": null,
                "Zeit_2": null
              }
            ],
            "Schlagwort": "#B1412#Landwirtschaft#Stall / Scheune ",
            "Status": 0,
            "StatusText": "LAUFEND",
            "Stichwoerter": [
              null,
              {
                "Category": null,
                "Name": "B 4",
                "Type": "Brand"
              },
              null,
              null,
              null,
              null,
              null
            ],
            "Tetra": "SoG_1_BA"
          },
          "MissionId": 14800000
        }
        """"
}
