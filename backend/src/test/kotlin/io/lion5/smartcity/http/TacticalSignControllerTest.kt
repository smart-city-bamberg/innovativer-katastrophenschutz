/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.http

import com.ninjasquad.springmockk.MockkBean
import io.lion5.smartcity.mongodb.model.Image
import io.lion5.smartcity.mongodb.model.TacticalSign
import io.lion5.smartcity.mongodb.service.TacticalSignService
import io.mockk.every
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.multipart
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.testcontainers.shaded.org.bouncycastle.asn1.cms.CMSAttributes.contentType

@WebMvcTest(TacticalSignController::class)
@AutoConfigureMockMvc(addFilters = false)
class TacticalSignControllerTest(
    @Autowired val mockMvc: MockMvc,
) {
    @MockkBean
    lateinit var tacticalSignService: TacticalSignService

    private val tacticalSign0 =
        TacticalSign(
            "abc-def",
            Image("ghi-jkl", "foo/bar.png", "image/png"),
        )

    private val tacticalSign1 =
        TacticalSign(
            "mno-pqr",
            Image("stu-vwx", "foo/bar.png", "image/png"),
        )

    private val tacticalSign2 =
        TacticalSign(
            "yza-bcd",
            Image("efg-hij", "foo/bar.png", "image/png"),
        )

    private val tacticalSigns = arrayOf(tacticalSign0, tacticalSign1, tacticalSign2)

    @Test
    fun `create tacticalSign`() {
        every { tacticalSignService.createTacticalSign(any(), any()) } returns tacticalSign0
        val imageContent = this::class.java.getResource("/test-tactical-sign.svg")?.readBytes()
        val name = MockMultipartFile("name", "name", "text/plain", "FileName".toByteArray())
        val image = MockMultipartFile("image", "test.svg", "image/svg+xml", imageContent)

        mockMvc
            .multipart(HttpMethod.POST, "/tactical-signs") {
                with(user("test"))
                contentType = MediaType.MULTIPART_FORM_DATA
                file(name)
                file(image)
            }.andExpect {
                status { MockMvcResultMatchers.status().isCreated }
                header { MockMvcResultMatchers.header().string("Location", "http://localhost/tactical-sign/abc-def") }
                verify { tacticalSignService.createTacticalSign(any(), any()) }
            }
    }

    @Test
    fun `get all tacticalSigns`() {
        // returns all tactical signs
        every { tacticalSignService.getAllTacticalSigns() } returns tacticalSigns.toList()

        mockMvc
            .perform(
                MockMvcRequestBuilders.get("/tactical-signs"),
            ).andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(
                MockMvcResultMatchers.content().json(
                    """
                    [
                        {
                            "id": "abc-def",
                            "image": {
                                "id": "ghi-jkl",
                                "url": "foo/bar.png",
                                "blobName": "image/png"
                            }
                        },
                        {
                            "id": "mno-pqr",
                            "image": {
                                "id": "stu-vwx",
                                "url": "foo/bar.png",
                                "blobName": "image/png"
                            }
                        },
                        {
                            "id": "yza-bcd",
                            "image": {
                                "id": "efg-hij",
                                "url": "foo/bar.png",
                                "blobName": "image/png"
                            }
                        }
                    ]
                    """.trimIndent(),
                ),
            )
    }

    @Test
    fun `delete tacticalSign`() {
        every { tacticalSignService.deleteTacticalSign(any()) } returns Unit

        mockMvc
            .perform(MockMvcRequestBuilders.delete("/tactical-signs/{id}", "abc-def"))
            .andExpect(status().isNoContent)
        verify { tacticalSignService.deleteTacticalSign("abc-def") }
    }
}
