/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.mqtt

import io.github.oshai.kotlinlogging.KotlinLogging
import io.lion5.smartcity.config.LocalSecurityConfiguration
import io.lion5.smartcity.mqtt.config.MqttChannelConfiguration
import io.lion5.smartcity.mqtt.payload.LocationPayload
import io.lion5.smartcity.mqtt.payload.outbound.OutboundUnitLocationPayload
import io.lion5.smartcity.testcontainer.MosquittoContainer
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallback
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import java.time.Instant

@SpringBootTest(classes = [MqttChannelConfiguration::class, LocalSecurityConfiguration::class])
@Testcontainers
@ComponentScan("io.lion5.smartcity.mqtt")
@Import(MqttGateway::class)
class MqttBrokerIntegrationTest {
    @Autowired
    private lateinit var locationGateway: MqttGateway

    companion object {
        private val log = KotlinLogging.logger {}

        @Container
        val broker = MosquittoContainer()

        @JvmStatic
        @DynamicPropertySource
        fun mqttBrokerProperties(registry: DynamicPropertyRegistry) {
            registry.add("digimap.mqtt.url", broker::getBrokerUrl)
            registry.add("digimap.mqtt.username", broker::username)
            registry.add("digimap.mqtt.password", broker::password)
        }
    }

    @Test
    fun `handle inbound location update message`() {
        val mqttClient = MqttClient(broker.getBrokerUrl(), MqttClient.generateClientId())
        mqttClient.connect()
        val testPayload =
            """
            {
              "location": {
                "lat": 49.91454,
                "lon": 10.88511
              },
              "timestamp": "2019-08-24T14:15:22Z"
            }
            """.trimIndent()
        mqttClient.publish("digimap/units/v1/inbound/testUnitId4711/location", MqttMessage(testPayload.toByteArray()))
        log.debug { "waiting for handler" }
        mqttClient.disconnect()
    }

    @Test
    fun `publish outbound location update message`() {
        val mqttClient = MqttClient(broker.getBrokerUrl(), MqttClient.generateClientId())
        mqttClient.setCallback(
            object : MqttCallback {
                override fun connectionLost(cause: Throwable?) {
                    TODO("Not yet implemented")
                }

                override fun messageArrived(
                    topic: String?,
                    message: MqttMessage?,
                ) {
                    log.debug { "Received message from $topic with content $message" }
                }

                override fun deliveryComplete(token: IMqttDeliveryToken?) {
                    TODO("Not yet implemented")
                }
            },
        )
        mqttClient.connect()
        mqttClient.subscribe("#")

        log.debug { "preparing" }
        locationGateway.publishLocationUpdate(
            OutboundUnitLocationPayload(
                eTag = 1,
                location = LocationPayload(12.12345, 4.123415),
                unitId = "unit1",
                timestamp = Instant.now(),
            ),
            "mission4321",
        )
        log.debug { "waiting for message to be published back" }
        mqttClient.disconnect()
    }
}
