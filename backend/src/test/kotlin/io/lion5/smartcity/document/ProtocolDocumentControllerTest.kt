/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.document

import com.ninjasquad.springmockk.MockkBean
import io.lion5.smartcity.auth.model.AuthorityRoles
import io.lion5.smartcity.auth.util.WithMockFirebaseUser
import io.lion5.smartcity.config.SecurityConfiguration
import io.mockk.every
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.Import
import org.springframework.security.acls.AclPermissionEvaluator
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import java.io.File
import java.nio.file.Files
import java.nio.file.Path

@WebMvcTest(ProtocolDocumentController::class)
@Import(SecurityConfiguration::class)
@MockkBean(AclPermissionEvaluator::class)
class ProtocolDocumentControllerTest(
    @Autowired val mockMvc: MockMvc,
) {
    @MockkBean
    lateinit var protocolDocumentService: ProtocolDocumentService

    @Test
    @WithMockFirebaseUser(role = AuthorityRoles.SUPER_ADMIN)
    fun `pdf is generated and downloadable`() {
        val rootPath = System.getProperty("user.dir")
        val filePath = "$rootPath/protocol.pdf"
        every { protocolDocumentService.createPdfProtocolFromMissionId("1", true) } answers {
            if (!File(filePath).isFile) {
                Files.write(Path.of("$rootPath/protocol.pdf"), byteArrayOf())
            }
            "$rootPath/protocol.pdf"
        }

        mockMvc
            .get("/protocols/1?includeLocationEvents=true") {
            }.andExpect {
                status { isOk() }
                header {
                    string("Content-Type", "application/pdf")
                }
            }
    }
}
