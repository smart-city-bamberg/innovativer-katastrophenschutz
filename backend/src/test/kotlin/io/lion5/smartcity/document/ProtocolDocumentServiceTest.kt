/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.document

import io.lion5.smartcity.eventstore.EventstoreMissionService
import io.lion5.smartcity.eventstore.events.model.DigimapEvent
import io.lion5.smartcity.eventstore.events.model.EventGPSLocation
import io.lion5.smartcity.eventstore.events.model.LocationUpdateEvent
import io.lion5.smartcity.eventstore.events.model.MissionEvent
import io.lion5.smartcity.eventstore.events.model.MissionEventType
import io.lion5.smartcity.mongodb.model.Address
import io.lion5.smartcity.mongodb.model.Department
import io.lion5.smartcity.mongodb.model.EmergencyUnit
import io.lion5.smartcity.mongodb.model.Mission
import io.lion5.smartcity.mongodb.model.location.GKLocation
import io.lion5.smartcity.mongodb.model.location.GPSLocation
import io.lion5.smartcity.mongodb.service.EmergencyUnitService
import io.lion5.smartcity.mongodb.service.MissionService
import io.lion5.smartcity.util.LocationConverterService
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.time.Instant
import java.util.Optional

class ProtocolDocumentServiceTest {
    private lateinit var protocolDocumentService: ProtocolDocumentService

    private val converterService = LocationConverterService()
    private val gkLocation = GKLocation(4420263.94, 5527041.07)
    private val missionServiceMock = mockk<MissionService>()
    private val eventstoreMissionServiceMock = mockk<EventstoreMissionService>()
    private val emergencyUnitServiceMock = mockk<EmergencyUnitService>()

    private val testMission =
        Mission(
            "12898asd012f912bbc6aaa",
            Instant.parse("2021-08-09T15:05:45Z"),
            "B 4.1 2108XX XXX",
            2,
            "Brand",
            converterService.convertToGPS(gkLocation),
            gkLocation,
            Address("Paradiesweg", "1", 91000, "Musterdorf Oberfr"),
            setOf(
                EmergencyUnit(
                    unitId = null,
                    callSign = "Einsatzkraft1",
                    tacticalSignPath = "",
                    gpsLocation = GPSLocation(1.0, 2.0),
                    department = Department("123", "Feuerwehr", "FW", state = Department.DepartmentState.PERMANENT, users = emptyList()),
                ),
                EmergencyUnit(
                    unitId = null,
                    callSign = "Einsatzkraft2",
                    tacticalSignPath = "",
                    gpsLocation = GPSLocation(1.0, 2.0),
                    department = Department("123", "Feuerwehr", "FW", state = Department.DepartmentState.PERMANENT, users = emptyList()),
                ),
                EmergencyUnit(
                    unitId = null,
                    callSign = "Einsatzkraft3",
                    tacticalSignPath = "",
                    gpsLocation = GPSLocation(1.0, 2.0),
                    department = Department("123", "Feuerwehr", "FW", state = Department.DepartmentState.PERMANENT, users = emptyList()),
                ),
            ),
            "SoG_1_BA",
            Mission.State.FINISHED,
            keyword = "",
            cues = "",
            joinCode = "ABCD-1234",
            joinPin = "123456",
        )

    private val testEventList = mutableListOf<DigimapEvent>()

    init {
        // create mission start event
        testEventList.add(MissionEvent(Instant.parse("2019-08-09T15:05:45Z"), MissionEventType.START, 0))
        // create some location updates
        testEventList.add(
            LocationUpdateEvent(
                "unit1",
                EventGPSLocation(49.87413, 10.88821),
                Instant.parse("2021-08-09T15:10:45Z"),
                1,
            ),
        )
        testEventList.add(
            LocationUpdateEvent(
                "unit2",
                EventGPSLocation(49.87413, 10.88842),
                Instant.parse("2021-08-09T15:11:45Z"),
                2,
            ),
        )
        testEventList.add(
            LocationUpdateEvent(
                "unit1",
                EventGPSLocation(49.87526, 10.88701),
                Instant.parse("2021-08-09T15:11:55Z"),
                3,
            ),
        )
        testEventList.add(
            LocationUpdateEvent(
                "unit1",
                EventGPSLocation(49.87413, 10.88821),
                Instant.parse("2021-08-09T15:14:45Z"),
                4,
            ),
        )
        testEventList.add(
            LocationUpdateEvent(
                "unit2",
                EventGPSLocation(49.87413, 10.88821),
                Instant.parse("2021-12-09T15:15:45Z"),
                5,
            ),
        )
        testEventList.add(
            LocationUpdateEvent(
                "unit1",
                EventGPSLocation(49.87413, 10.88821),
                Instant.parse("2021-08-09T15:17:45Z"),
                7,
            ),
        )
        testEventList.add(
            LocationUpdateEvent(
                "unit1",
                EventGPSLocation(49.87413, 10.88821),
                Instant.parse("2021-08-09T15:18:45Z"),
                8,
            ),
        )
        testEventList.add(
            LocationUpdateEvent(
                "unit2",
                EventGPSLocation(49.87413, 10.88821),
                Instant.parse("2021-08-09T15:26:45Z"),
                11,
            ),
        )
        testEventList.add(
            LocationUpdateEvent(
                "unit1",
                EventGPSLocation(49.87413, 10.88821),
                Instant.parse("2021-08-09T15:20:45Z"),
                9,
            ),
        )
        testEventList.add(
            LocationUpdateEvent(
                "unit1",
                EventGPSLocation(49.87413, 10.88821),
                Instant.parse("2021-08-09T15:16:45Z"),
                6,
            ),
        )
        testEventList.add(
            LocationUpdateEvent(
                "unit2",
                EventGPSLocation(49.87413, 10.88821),
                Instant.parse("2021-08-09T15:25:45Z"),
                10,
            ),
        )
        testEventList.add(
            LocationUpdateEvent(
                "unit2",
                EventGPSLocation(49.87739, 10.88425),
                Instant.parse("2020-08-09T15:27:45Z"),
                12,
            ),
        )
        testEventList.add(
            LocationUpdateEvent(
                "unit1",
                EventGPSLocation(49.87413, 10.88821),
                Instant.parse("2021-08-09T15:30:45Z"),
                13,
            ),
        )
        testEventList.add(
            LocationUpdateEvent(
                "unit1",
                EventGPSLocation(49.87413, 10.88821),
                Instant.parse("2021-02-09T15:31:45Z"),
                14,
            ),
        )
        testEventList.add(
            LocationUpdateEvent(
                "unit1",
                EventGPSLocation(49.87413, 10.88821),
                Instant.parse("2021-08-09T15:33:45Z"),
                15,
            ),
        )
    }

    @BeforeEach
    fun beforeEach() {
        protocolDocumentService =
            ProtocolDocumentService(
                emergencyUnitServiceMock,
                PdfProtocolBuilder(),
                missionServiceMock,
                eventstoreMissionServiceMock,
            )
    }

    @Test
    fun `get event stream from mission id and generate pdf document`() {
        every { emergencyUnitServiceMock.getEmergencyUnitById(any()) } answers {
            Optional.of(testMission.units.first())
        }
        every { missionServiceMock.findMissionById(any()) } answers {
            Optional.of(testMission)
        }
        every { eventstoreMissionServiceMock.readEventsFromStreamForMission(any()) } answers {
            testEventList
        }
        val fileLocation = protocolDocumentService.createPdfProtocolFromMissionId(testMission.missionId!!, true)
        assert(File(fileLocation).isFile)
    }
}
