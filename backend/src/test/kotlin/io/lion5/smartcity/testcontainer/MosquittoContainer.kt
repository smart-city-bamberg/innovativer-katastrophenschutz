/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.testcontainer

import org.testcontainers.containers.BindMode
import org.testcontainers.containers.GenericContainer

class MosquittoContainer : GenericContainer<MosquittoContainer>("eclipse-mosquitto:2.0.15") {
    // These values need to be in sync with the content of the USER_RESOURCE_PATH file
    val username = "user"
    val password = "user"

    companion object {
        private const val MOSQUITTO_CONF_PATH = "/mosquitto/config/mosquitto.conf"
        private const val CONFIG_RESOURCE_PATH = "testcontainers/mosquitto.conf"

        private const val MOSQUITTO_USER_CONFIG = "/passwd/mosquitto_user"
        private const val USER_RESOURCE_PATH = "testcontainers/mosquitto_user"
    }

    init {
        withExposedPorts(1883)
        withClasspathResourceMapping(CONFIG_RESOURCE_PATH, MOSQUITTO_CONF_PATH, BindMode.READ_ONLY)
        withClasspathResourceMapping(USER_RESOURCE_PATH, MOSQUITTO_USER_CONFIG, BindMode.READ_ONLY)
    }

    /**
     * Return the Broker URL incl. the protocol and port
     */
    fun getBrokerUrl(): String = "tcp://$host:${getMappedPort(1883)}"
}
