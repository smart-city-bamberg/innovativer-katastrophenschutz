/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.dashboard

import com.ninjasquad.springmockk.MockkBean
import io.lion5.smartcity.dashboard.dto.DashboardConfigurationOption
import io.lion5.smartcity.dashboard.dto.DashboardConfigurationRequest
import io.lion5.smartcity.mongodb.model.dashboard.DashboardConfiguration
import io.lion5.smartcity.mongodb.model.dashboard.GridWidget
import io.lion5.smartcity.mongodb.model.dashboard.WidgetConfiguration
import io.mockk.every
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@WebMvcTest(DashboardConfigurationController::class)
@AutoConfigureMockMvc(addFilters = false)
class DashboardConfigurationControllerTest(
    @Autowired val mockMvc: MockMvc,
) {
    @MockkBean
    private lateinit var dashboardConfigurationService: DashboardConfigurationService

    private val widgetConfiguration = WidgetConfiguration("widgetId1", GridWidget(0, 0, 2, 2), "type1")
    private val dashboardConfigurationRequest = DashboardConfigurationRequest("TestDashboard", listOf(widgetConfiguration))
    private val dashboardConfiguration = DashboardConfiguration("1", "TestDashboard", listOf(widgetConfiguration))
    private val dashboardConfigurationOption = DashboardConfigurationOption("1", "TestDashboard")

    @Test
    fun `save new dashboard configuration`() {
        every { dashboardConfigurationService.saveNewDashboardConfiguration(any()) } returns dashboardConfiguration

        mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post("/dashboard-configurations")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(
                        """
                        {
                            "name": "${dashboardConfigurationRequest.name}",
                            "widgetConfigurations": [
                                {
                                    "id": "${widgetConfiguration.id}",
                                    "position": {
                                        "x": ${widgetConfiguration.position.x},
                                        "y": ${widgetConfiguration.position.y},
                                        "width": ${widgetConfiguration.position.width},
                                        "height": ${widgetConfiguration.position.height}
                                    },
                                    "componentId": "${widgetConfiguration.componentId}"
                                }
                            ]
                        }
                        """.trimIndent(),
                    ),
            ).andExpect(MockMvcResultMatchers.status().isCreated)
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(dashboardConfiguration.id))
            .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(dashboardConfiguration.name))
    }

    @Test
    fun `get all dashboard configuration options`() {
        every { dashboardConfigurationService.getAllDashboardConfigurationOptions() } returns listOf(dashboardConfigurationOption)

        mockMvc
            .perform(MockMvcRequestBuilders.get("/dashboard-configurations"))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(dashboardConfigurationOption.id))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(dashboardConfigurationOption.name))
    }

    @Test
    fun `get dashboard configuration by id`() {
        every { dashboardConfigurationService.getDashboardConfigurationById(any()) } returns dashboardConfiguration

        mockMvc
            .perform(MockMvcRequestBuilders.get("/dashboard-configurations/${dashboardConfiguration.id}"))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(dashboardConfiguration.id))
            .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(dashboardConfiguration.name))
    }

    @Test
    fun `delete dashboard configuration by id`() {
        every { dashboardConfigurationService.deleteDashboardConfiguration(any()) } returns Unit

        mockMvc
            .perform(MockMvcRequestBuilders.delete("/dashboard-configurations/${dashboardConfiguration.id}"))
            .andExpect(MockMvcResultMatchers.status().isNoContent)

        verify { dashboardConfigurationService.deleteDashboardConfiguration("1") }
    }

    @Test
    fun `delete dashboard configuration by id, not found`() {
        every { dashboardConfigurationService.deleteDashboardConfiguration(any()) } throws IllegalArgumentException("test")

        mockMvc
            .perform(MockMvcRequestBuilders.delete("/dashboard-configurations/${dashboardConfiguration.id}"))
            .andExpect(MockMvcResultMatchers.status().isNotFound)

        verify { dashboardConfigurationService.deleteDashboardConfiguration("1") }
    }
}
