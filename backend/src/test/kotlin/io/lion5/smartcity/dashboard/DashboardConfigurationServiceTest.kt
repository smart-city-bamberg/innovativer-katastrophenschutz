/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the European Public License, Version 1.2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: EUPL-1.2
 */

package io.lion5.smartcity.dashboard

import io.lion5.smartcity.dashboard.dto.DashboardConfigurationOption
import io.lion5.smartcity.dashboard.dto.DashboardConfigurationRequest
import io.lion5.smartcity.mongodb.model.dashboard.DashboardConfiguration
import io.lion5.smartcity.mongodb.model.dashboard.GridWidget
import io.lion5.smartcity.mongodb.model.dashboard.WidgetConfiguration
import io.lion5.smartcity.mongodb.repository.DashboardConfigurationRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.Optional

class DashboardConfigurationServiceTest {
    private lateinit var dashboardConfigurationService: DashboardConfigurationService
    private val dashboardConfigurationRepository = mockk<DashboardConfigurationRepository>()

    private val testWidgetConfiguration = WidgetConfiguration("widgetId1", GridWidget(0, 0, 2, 2), "type1")
    private val testDashboardConfiguration =
        DashboardConfiguration("1", "TestDashboard", listOf(testWidgetConfiguration))

    @BeforeEach
    fun setUp() {
        dashboardConfigurationService = DashboardConfigurationService(dashboardConfigurationRepository)
    }

    @Test
    fun `save new dashboard configuration`() {
        val dashboardConfigurationRequest =
            DashboardConfigurationRequest("TestDashboard", listOf(testWidgetConfiguration))
        every { dashboardConfigurationRepository.insert(any<DashboardConfiguration>()) } returns testDashboardConfiguration
        every { dashboardConfigurationRepository.findByName("TestDashboard") } returns null

        val savedDashboardConfiguration =
            dashboardConfigurationService.saveNewDashboardConfiguration(dashboardConfigurationRequest)

        verify { dashboardConfigurationRepository.insert(any<DashboardConfiguration>()) }
        assertEquals(testDashboardConfiguration.id, savedDashboardConfiguration.id)
        assertEquals(testDashboardConfiguration.name, savedDashboardConfiguration.name)
        assertEquals(testDashboardConfiguration.widgetConfigurations, savedDashboardConfiguration.widgetConfigurations)
    }

    @Test
    fun `save new dashboard configuration with existing name`() {
        val dashboardConfigurationRequest = DashboardConfigurationRequest("TestDashboard", listOf(testWidgetConfiguration))
        every { dashboardConfigurationRepository.findByName("TestDashboard") } returns testDashboardConfiguration

        try {
            dashboardConfigurationService.saveNewDashboardConfiguration(dashboardConfigurationRequest)
        } catch (iae: IllegalArgumentException) {
            assertEquals("Dashboard configuration with name TestDashboard already exists", iae.message)
        }
    }

    @Test
    fun `get all dashboard configuration options`() {
        val dashboardConfigurationOption = DashboardConfigurationOption("1", "TestDashboard")
        every { dashboardConfigurationRepository.findAllDashboardConfigurationOptions() } returns
            listOf(
                dashboardConfigurationOption,
            )

        val dashboardConfigurationOptions = dashboardConfigurationService.getAllDashboardConfigurationOptions()

        verify { dashboardConfigurationRepository.findAllDashboardConfigurationOptions() }
        assertTrue(dashboardConfigurationOptions.contains(dashboardConfigurationOption))
    }

    @Test
    fun `get dashboard configuration by id`() {
        every { dashboardConfigurationRepository.findById("1") } returns Optional.of(testDashboardConfiguration)

        val dashboardConfiguration = dashboardConfigurationService.getDashboardConfigurationById("1")

        verify { dashboardConfigurationRepository.findById("1") }
        assertEquals(testDashboardConfiguration.id, dashboardConfiguration.id)
        assertEquals(testDashboardConfiguration.name, dashboardConfiguration.name)
        assertEquals(testDashboardConfiguration.widgetConfigurations, dashboardConfiguration.widgetConfigurations)
    }

    @Test
    fun `delete dashboard configuration by id`() {
        every { dashboardConfigurationRepository.existsById("1") } returns true
        every { dashboardConfigurationRepository.deleteById("1") } returns Unit

        dashboardConfigurationService.deleteDashboardConfiguration("1")

        verify { dashboardConfigurationRepository.existsById("1") }
        verify { dashboardConfigurationRepository.deleteById("1") }
    }

    @Test
    fun `delete dashboard configuration by id, not found`() {
        every { dashboardConfigurationRepository.existsById("1") } returns false

        val exception =
            assertThrows<IllegalArgumentException> {
                dashboardConfigurationService.deleteDashboardConfiguration("1")
            }

        assertEquals("dashboard configuration with id 1 does not exist", exception.message)

        verify { dashboardConfigurationRepository.existsById("1") }
        verify(exactly = 0) { dashboardConfigurationRepository.deleteById("1") }
    }
}
