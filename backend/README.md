# Digimap Backend

## Running the backend

Please refer to the main [README.md](../README.md)

## API Documentation

The backend features two different types of APIs

1. REST API for static data
2. MQTT API for dynamic and real time data

### REST API Documentation

The REST API is documented with [OpenAPI](https://spec.openapis.org/oas/v3.0.3).
You can access the OpenAPI file via this link http://localhost:8089/v3/api-docs after starting the backend.
A Swagger UI visualizing the API endpoints is accessible via http://localhost:8089/swagger-ui/index.html.

### MQTT API Documentation

The MQTT API is documented using [AsyncAPI](https://github.com/asyncapi/spec/blob/v2.6.0/spec/asyncapi.md).
You find the documentation in the [asyncapi.yaml](../doc/mqtt-api/asyncapi.yaml)
