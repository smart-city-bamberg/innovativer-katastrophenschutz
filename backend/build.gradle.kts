import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "3.3.1"
    id("io.spring.dependency-management") version "1.1.6"
    id("com.google.cloud.tools.jib") version "3.4.3"
    id("jacoco")
    id("test-report-aggregation")
    id("jacoco-report-aggregation")
    id("com.diffplug.spotless") version "6.25.0"
    kotlin("jvm") version "2.0.0"
    kotlin("plugin.spring") version "2.0.0"
    kotlin("plugin.allopen") version "2.0.0"
}

group = "io.lion5.smart-city"
version = "0.4.1-SNAPSHOT"
java.targetCompatibility = JavaVersion.VERSION_21

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "21"
    }
}

repositories {
    mavenLocal()
    maven {
        url = uri("https://repo.osgeo.org/repository/release/")
    }
    mavenCentral()
    maven {
        name = "GitHubPackages"
        url = uri("https://maven.pkg.github.com/lion5/spring-security-acl-mongodb") // GitHub Packages URL for your repo
        credentials {
            username = System.getenv("GITHUB_ACTOR")
            password = System.getenv("GITHUB_TOKEN")
        }
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-data-rest")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("io.micrometer:micrometer-registry-prometheus:1.13.2")

    // Mongo-ACL
    implementation("org.springframework:spring-security-acl-mongodb-kotlin:0.0.1-SNAPSHOT") // From Mongo ACL fork repo

    // ACL
    implementation("org.springframework:spring-context-support")
    implementation("org.springframework.security:spring-security-acl")
    implementation("org.ehcache:ehcache:3.10.8")
    implementation("javax.cache:cache-api:1.1.1")

    implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.6.0")
    implementation("org.springdoc:springdoc-openapi-starter-common:2.6.0")

    implementation("io.github.oshai:kotlin-logging-jvm:7.0.0")

    implementation("com.eventstore:db-client-java:5.4.0")

    implementation("org.springframework.boot:spring-boot-starter-integration")
    implementation("org.springframework.integration:spring-integration-mqtt")
    implementation("org.eclipse.paho:org.eclipse.paho.mqttv5.client:1.2.5")

    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.8.1")

    implementation("org.geotools:gt-epsg-hsql:31.2")

    implementation("com.google.firebase:firebase-admin:9.3.0")
    implementation("com.google.code.gson:gson:2.11.0")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.17.2")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.14.0")

    implementation("org.thymeleaf:thymeleaf:3.1.2.RELEASE")
    implementation("org.xhtmlrenderer:flying-saucer-pdf:9.8.0")
    implementation("net.sf.jtidy:jtidy:r938")

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
        exclude(group = "org.mockito", module = "mockito-core")
    }
    testImplementation("org.springframework.boot:spring-boot-testcontainers")
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("com.ninja-squad:springmockk:4.0.2")
    testImplementation("org.testcontainers:junit-jupiter")
    testImplementation("org.testcontainers:mongodb")

    implementation("net.coobird:thumbnailator:0.4.20")

    implementation("com.google.cloud:spring-cloud-gcp-starter-storage:5.5.0")
}

spotless {
    kotlin {
        // by default the target is every '.kt' and '.kts` file in the java sourcesets
        ktfmt("0.50").kotlinlangStyle()
        ktlint("1.3.0").setEditorConfigPath("$projectDir/../.editorconfig")
    }
    kotlinGradle {
        ktlint("1.3.0")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.check {
    dependsOn("jacocoTestReport")
}

jib {
    from {
        image = "eclipse-temurin:21.0.1_12-jre-alpine"
        platforms {
            platform {
                architecture = "amd64"
                os = "linux"
            }
        }
    }
    to {
        image = "europe-west3-docker.pkg.dev/smart-city-digimap/docker/backend:$version"
        tags = arrayOf("latest").toSet()
    }
    container {
        format = com.google.cloud.tools.jib.api.buildplan.ImageFormat.OCI
    }
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
        csv.required.set(false)
        html.required.set(true)
    }
}

tasks.bootRun {
    args = listOf("--spring.profiles.active=local")
}

// https://stackoverflow.com/questions/56095909/dbreflazy-true-with-a-kotlin-data-class-throwing-cannot-subclass-final-clas
// otherwise we have this problem with mission <-> emergencyUnit
allOpen {
    annotation("org.springframework.data.mongodb.core.mapping.Document")
}
