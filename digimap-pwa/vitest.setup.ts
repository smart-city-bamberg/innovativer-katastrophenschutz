import { afterAll, beforeAll, vi } from 'vitest'

beforeAll(() => {
  //Dialog patch
  // TODO: remove when JSDom supports HTMLDialogElement https://github.com/jsdom/jsdom/issues/3294
  HTMLDialogElement.prototype.show = vi.fn()
  HTMLDialogElement.prototype.showModal = vi.fn()
  HTMLDialogElement.prototype.close = vi.fn()
})
afterAll(() => {
  vi.clearAllMocks()
})
