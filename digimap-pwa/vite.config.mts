/// <reference types="vitest" />
import { fileURLToPath, URL } from 'node:url'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from 'vite-plugin-pwa'

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => ({
  plugins: [
    vue(),
    // for tests, we do not need PWA / service worker
    ...(mode === 'ci-testing'
      ? []
      : [
          VitePWA({
            registerType: 'autoUpdate',
            devOptions: {
              enabled: true,
            },
            includeAssets: ['**/svg/**/*.svg'],
            manifest: {
              name: 'DigiMap',
              short_name: 'DigiMap',
              description: 'Smarter Katastrophenschutz für Smart City Bamberg',
              theme_color: '#9BD4F4',
              id: '/',
              icons: [
                {
                  src: 'svg/icons/favicon.svg',
                  sizes: '32x32',
                  type: 'image/svg+xml',
                },
                {
                  src: 'favicon-16x16.png',
                  sizes: '16x16',
                  type: 'image/png',
                },
                {
                  src: 'favicon-32x32.png',
                  sizes: '32x32',
                  type: 'image/png',
                },
                {
                  src: 'apple-touch-icon.png',
                  sizes: '180x180',
                  type: 'image/png',
                },
                {
                  src: 'mstile-150x150.png',
                  sizes: '150x150',
                  type: 'image/png',
                },
                {
                  src: 'pwa-192x192.png',
                  sizes: '192x192',
                  type: 'image/png',
                },
                {
                  src: 'pwa-512x512.png',
                  sizes: '512x512',
                  type: 'image/png',
                  purpose: 'any',
                },
                {
                  // https://web.dev/maskable-icon/
                  src: 'pwa-512x512-maskable.png',
                  sizes: '512x512',
                  type: 'image/png',
                  purpose: 'maskable',
                },
              ],
            },
          }),
        ]),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      mqtt: 'mqtt/dist/mqtt.js',
    },
  },
    test: {
        globals: true,
        environment: 'jsdom',
        css: true,
        server: {
            deps: {
                // Let the gridstack component library be handled by vite to get rid of module is not in commonjs format error
                inline: ['@lion5/component-library-gridstack']
            }
        },
        setupFiles: './vitest.setup.ts',
    },
}))
