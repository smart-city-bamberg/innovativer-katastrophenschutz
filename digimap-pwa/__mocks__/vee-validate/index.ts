import type { Mock } from 'vitest'
import { vi } from 'vitest'

export const useForm: Mock = vi.fn().mockReturnValue({
  handleSubmit: vi.fn(),
  meta: { value: { dirty: false, valid: true } },
})
export const useField: Mock = vi.fn().mockReturnValue({
  validate: vi.fn().mockReturnValue({
    errors: [],
    valid: true,
  }),
  errorMessage: { value: undefined },
})
