import { defineConfig } from 'cypress'

/* eslint-env node */
module.exports = defineConfig({
  e2e: {
    specPattern: 'cypress/e2e/**/*.{cy,spec}.{js,jsx,ts,tsx}',
    baseUrl: 'http://localhost:5173', // NOTE: must be same as in test:e2e / test:e2e:ui scripts in package.json
  },
})
