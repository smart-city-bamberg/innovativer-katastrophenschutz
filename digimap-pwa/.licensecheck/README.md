This folder contains all data related to the requirement that we have to display both used third-party components and
their licenses in our applications *we deliver to the customer*. In our case the latter means that only runtime
dependencies (and their transitive dependencies!) of web components are affected.

The tooling is based on https://www.npmjs.com/package/license-checker-rseidelsohn.

```sh
license-checker-rseidelsohn --production --json --customPath fields.json
```

To always be up-to-date, the package list is generated dynamically in our CI pipeline for all deployments. In order to
match the expected output format (list of objects, so we can iterate through the list in our UI), `jq` is required to
map from the default `license-checker` output (`map(.)`).
