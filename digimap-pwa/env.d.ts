/// <reference types="vite/client" />
import { useAccountStore } from '@/account/stores/account'
import { Router } from 'vue-router'
import { ContextMenuItem, Control, Map } from 'leaflet'

declare global {
  interface Window {
    Cypress: boolean
    __accountStore__: ReturnType<typeof useAccountStore>
    __router__: Router
    appReady: boolean
  }

  // additional types for leaflet.contextmenu (@types/leaflet-contextmenu is lacking them)
  interface MapWithCustomProps extends Map {
    contextmenu: ContextMenu
    locateControl: Control.Locate
  }

  interface ContextMenu {
    addItem: (item: ContextMenuItem) => void
    _enabled: boolean
  }
}
