import { MqttJsClient } from '@/mqtt/mqttClient'

export interface CommandResponse {
  reportId: string //uuid
  senderUnitId: string
  report: string
  createdAt: string // ISO date time string
  referencingCommandId: string
}

export function useCommandResponse() {
  const publishCommandResponse = async (
    missionId: string,
    commandResponse: CommandResponse
  ) => {
    // publish mqtt message to mission topic
    const topic = `units/v1/inbound/${missionId}/report`
    MqttJsClient.getInstance().publish(topic, commandResponse)
  }
  return { publishReportResponse: publishCommandResponse }
}
