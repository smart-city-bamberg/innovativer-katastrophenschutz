import { MqttJsClient } from '@/mqtt/mqttClient'
import type { GenericMqttPayload } from '@/mqtt/iMqttClient'
import { ref } from 'vue'
import { SubCommand } from '@/command/models/subCommand'
import { PubCommand } from '@/command/models/pubCommand'
import { useMissionsStore } from '@/mission/stores/missions'

const subscribedTopic = ref<string>('')

export function useCommand() {
  const mqttClient: MqttJsClient = MqttJsClient.getInstance()

  const _getSubscriptionTopic = (missionId: string) => {
    return `units/v1/outbound/${missionId}/command`
  }

  const subscribe = (
    missionId: string,
    callback: (command: SubCommand) => void
  ) => {
    const relativeTopic = _getSubscriptionTopic(missionId)
    if (subscribedTopic.value === relativeTopic) {
      // topic of this mission is already subscribed
      return
    }
    mqttClient.subscribe(
      relativeTopic,
      (_topic, payload: GenericMqttPayload) => {
        try {
          const command = SubCommand.fromMqtt(payload)
          callback(command)
        } catch (e) {
          console.error('Cannot execute callback. Unknown error', e)
        }
      }
    )
    subscribedTopic.value = relativeTopic
  }

  const unsubscribe = (missionId: string) => {
    const topic = _getSubscriptionTopic(missionId)
    if (subscribedTopic.value !== topic) return
    mqttClient.unsubscribe(topic)
    subscribedTopic.value = ''
  }

  const publish = (command: PubCommand) => {
    const store = useMissionsStore()
    // TODO: change mission id to function param
    const missionId = store.currentMission?.missionId
    if (!missionId) {
      throw new Error(
        'Kein Einsatz ausgewählt. Bitte wählen Sie einen Einsatz in der Lagekarte.'
      )
    }
    // publish mqtt message to mission topic
    const topic = `units/v1/inbound/${missionId}/command`
    MqttJsClient.getInstance().publish(topic, command.toMqtt())
  }

  return { publish, subscribe, unsubscribe }
}
