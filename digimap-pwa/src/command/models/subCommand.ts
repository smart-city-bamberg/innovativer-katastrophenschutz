import type { GenericMqttPayload } from '@/mqtt/iMqttClient'

export interface ISubCommand {
  etag: number
  senderUnitId: string
  senderCallSign: string
  commandId: string // uuid
  commandText: string
  createdAt: number | string
  receiverUnitIds?: string[] // uuid[]
  referencingReportId?: string // uuid
}

export class SubCommand {
  readonly etag: number
  readonly senderUnitId: string
  readonly senderCallSign: string
  readonly commandId: string
  readonly commandText: string
  readonly createdAt: Date
  readonly receiverUnitIds: string[]
  readonly referencingReportId?: string
  readonly commandResponses: string[] = []

  constructor(
    etag: number,
    senderUnitId: string,
    senderCallSign: string,
    commandId: string,
    commandText: string,
    createdAt: Date,
    receiverUnitIds: string[],
    referencingReportId?: string
  ) {
    this.etag = etag
    this.senderUnitId = senderUnitId
    this.senderCallSign = senderCallSign
    this.commandId = commandId
    this.commandText = commandText
    this.createdAt = createdAt
    this.receiverUnitIds = receiverUnitIds
    this.referencingReportId = referencingReportId
  }

  static fromMqtt(payload: GenericMqttPayload) {
    const typedPayload = payload as ISubCommand
    return new SubCommand(
      typedPayload.etag,
      typedPayload.senderUnitId,
      typedPayload.senderCallSign,
      typedPayload.commandId,
      typedPayload.commandText,
      typeof typedPayload.createdAt === 'string'
        ? new Date(typedPayload.createdAt)
        : new Date(typedPayload.createdAt * 1000), // TODO: fix unix time stamp conversion
      typedPayload.receiverUnitIds || [],
      typedPayload.referencingReportId
    )
  }

  isCompleted() {
    return this.commandResponses.length > 0
  }

  clone() {
    return new SubCommand(
      this.etag,
      this.senderUnitId,
      this.senderCallSign,
      this.commandId,
      this.commandText,
      new Date(this.createdAt),
      [...this.receiverUnitIds],
      this.referencingReportId
    )
  }
}
