export interface IPubCommand {
  senderUnitId: string
  receiverUnitIds: string[] // uuid[]
  commandId: string // uuid
  commandText: string
  createdAt: string
  referencingReportId?: string // uuid
}

export class PubCommand {
  readonly senderUnitId: string
  readonly receiverUnitIds: string[]
  readonly commandId: string
  readonly commandText: string
  readonly createdAt: Date
  readonly referencingReportId?: string

  constructor(
    senderUnitId: string,
    receiverUnitIds: string[],
    commandId: string,
    commandText: string,
    createdAt: Date,
    referencingReportId?: string
  ) {
    this.senderUnitId = senderUnitId
    this.receiverUnitIds = receiverUnitIds
    this.commandId = commandId
    this.commandText = commandText
    this.createdAt = createdAt
    this.referencingReportId = referencingReportId
  }

  toMqtt(): IPubCommand {
    return {
      senderUnitId: this.senderUnitId,
      receiverUnitIds: this.receiverUnitIds,
      commandId: this.commandId,
      commandText: this.commandText,
      createdAt: this.createdAt.toISOString(),
      referencingReportId: this.referencingReportId,
    }
  }
}
