import type { ISubscriptionGrant, Packet } from 'mqtt'
import MQTT, { MqttClient } from 'mqtt'
import type {
  GenericMqttPayload,
  IMqttClient,
  PublishOptions,
  QosLevel,
  SubscribeOptions,
} from '@/mqtt/iMqttClient'

/**
 * Singleton that implements MQTT-based communication via the WSS
 * (WebSockets over SSL/TLS) protocol. It acts as a client to a broker.
 * By default, a QoS of 0 (At-most-once) is chosen, but can be changed for
 * each subscription / publishing.
 */
export class MqttJsClient implements IMqttClient {
  readonly defaultQos: QosLevel = 1
  readonly brokerUrl = import.meta.env.VITE_DIGIMAP_MQTT_BROKER_URL
  readonly brokerUserName: string = import.meta.env
    .VITE_DIGIMAP_MQTT_BROKER_USERNAME
  readonly brokerPassword: string = import.meta.env
    .VITE_DIGIMAP_MQTT_BROKER_PASSWORD
  readonly topicPrefix: string = 'digimap'

  private static instance: MqttJsClient

  private readonly client: MqttClient

  private topicHandlers: {
    [topic: string]: (topic: string, payload: Buffer) => void
  } = {}

  private constructor() {
    this.client = MQTT.connect(this.brokerUrl, {
      username: this.brokerUserName,
      password: this.brokerPassword,
    })
    this.onConnect(() => {
      console.log(
        'MqttClient connected successfully to Broker URL:',
        this.brokerUrl
      )
    })
  }

  static getInstance() {
    if (!MqttJsClient.instance) {
      MqttJsClient.instance = new MqttJsClient()
    }

    return MqttJsClient.instance
  }

  _getTopic(relativeTopic: string) {
    return `${this.topicPrefix}/${relativeTopic}`
  }

  onConnect(callback: () => void) {
    this.client.on('connect', callback)
  }

  onMessage(callback: (topic: string, payload: GenericMqttPayload) => void) {
    this.client.on('message', (topic: string, payload: Buffer) => {
      const payloadObject = this.parsePayloadFromBuffer(payload)
      payloadObject && callback(topic, payloadObject)
    })
  }

  publish(
    relativeTopic: string,
    payload: GenericMqttPayload,
    options: PublishOptions = { qos: this.defaultQos }
  ) {
    const prefixedTopic = this._getTopic(relativeTopic)
    this.client.publish(
      prefixedTopic,
      this.parsePayloadToString(payload),
      options,
      (error: Error | undefined, packet: Packet | undefined) => {
        if (error) {
          console.error('Could not publish message', error)
        } else {
          console.log('Message published', {
            prefixedTopic,
            payload,
            packet,
            qos: options.qos,
          })
        }
      }
    )
  }

  subscribe(
    subscriptionTopic: string | string[],
    messageCallback: (topic: string, payload: GenericMqttPayload) => void,
    options: SubscribeOptions = { qos: this.defaultQos }
  ) {
    const topic =
      typeof subscriptionTopic === 'string'
        ? this._getTopic(subscriptionTopic)
        : subscriptionTopic.map((relativeTopic) =>
            this._getTopic(relativeTopic)
          )

    this.client.subscribe(
      topic,
      options,
      (error: Error, granted: ISubscriptionGrant[]) => {
        if (error) {
          console.error(error)
        } else {
          console.debug('Subscription successful', {
            topic,
            granted,
          })
        }
      }
    )

    const messageHandler = (receivedTopic: string, payload: Buffer) => {
      if (
        receivedTopic === this._getTopic(subscriptionTopic as string) ||
        (Array.isArray(subscriptionTopic) &&
          subscriptionTopic.includes(receivedTopic))
      ) {
        const payloadObject = this.parsePayloadFromBuffer(payload)
        payloadObject && messageCallback(receivedTopic, payloadObject)
      }
    }

    this.topicHandlers[subscriptionTopic as string] = messageHandler
    this.client.on('message', messageHandler)
  }

  unsubscribe(relativeTopic: string) {
    const topic = this._getTopic(relativeTopic)
    this.client.unsubscribe(topic, (error: Error) => {
      if (error) {
        console.error('Could not unsubscribe', error)
      } else {
        console.debug('Unsubscribed successfully', relativeTopic)
      }
    })

    if (this.topicHandlers[relativeTopic]) {
      this.client.removeListener('message', this.topicHandlers[relativeTopic])
      delete this.topicHandlers[relativeTopic]
    }
  }

  parsePayloadToString(payload: GenericMqttPayload): string {
    return typeof payload === 'object' ? JSON.stringify(payload) : payload
  }

  parsePayloadFromBuffer(payload: Buffer): GenericMqttPayload | null {
    const payloadString = payload.toString()
    let payloadJson: object
    try {
      // throws exception if invalid JSON
      payloadJson = JSON.parse(payloadString)
    } catch (e: unknown) {
      // payload is plain string
      console.error('Could not parse payload: No valid JSON' + payloadString)
      return null
    }
    // payload is JSON object represented as string
    return payloadJson
  }
}
