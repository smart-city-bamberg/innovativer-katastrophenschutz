/**
 * Represents a generic payload of a MQTT packet (message).
 * {@link object Objects} will be represented as `string`s using `JSON.stringify(obj)`.
 * Hence, incoming object messages will be parsed using `JSON.parse(payload.toString())`.
 */
export type GenericMqttPayload = object

export type QosLevel = 0 | 1 | 2

export interface PublishOptions {
  qos?: QosLevel
}

export interface SubscribeOptions {
  qos: QosLevel
}

export interface IMqttClient {
  brokerUrl: string
  brokerUserName: string
  brokerPassword: string
  defaultQos: QosLevel
  /**
   * Publishes a message to the given topic.
   * @param topic The topic to which the message should be published.
   * @param payload The payload to be published, see {@link GenericMqttPayload} for information on parsing.
   * @param options Options to configure the publishing, such as the QoS.
   */
  publish(
    topic: string,
    payload: GenericMqttPayload,
    options: PublishOptions
  ): void

  /**
   * Subscribes to a topic or a list of topics. The
   * Subscriptions should be done after the client connected to the broker,
   * {@see onConnect}.
   * @param topic The topic(s) to subscribe to.
   * @param messageCallback The callback to be called when a message was received
   * with the topic. Includes the `topic` of the received message, and the
   * `message` payload as params. See {@link GenericMqttPayload} for message parsing.
   * @param options Options to configure the subscription, such as the QoS.
   */
  subscribe(
    topic: string | string[],
    messageCallback: (topic: string, payload: GenericMqttPayload) => void,
    options: SubscribeOptions
  ): void

  /**
   * Unsubscribe the MQTT client from the given topic(s), meaning that all message
   * handlers should be removed.
   * @param topic Topic(s) to be unsubscribed.
   */
  unsubscribe(topic: string | string[]): void

  /**
   * Adds a callback that is called when the client successfully (re-)connected
   * to the broker.
   */
  onConnect(callback: () => void): void

  /**
   * Adds a callback that is called when the client received a message from
   * the broker, i.e., by subscribing to a topic.
   */
  onMessage(
    callback: (topic: string, payload: GenericMqttPayload) => void
  ): void
}
