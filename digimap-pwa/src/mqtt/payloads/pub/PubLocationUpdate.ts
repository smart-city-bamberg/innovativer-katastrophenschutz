import { GpsLocation } from '@lion5/component-library-leaflet'

export class PubLocationUpdate {
  /**
   * Current location of the unit.
   */
  readonly location: GpsLocation
  /**
   * ISO datetime string of the timestamp when the message was sent.
   */
  readonly timestamp: string = new Date().toISOString()

  constructor(location: GpsLocation) {
    this.location = location
  }
}
