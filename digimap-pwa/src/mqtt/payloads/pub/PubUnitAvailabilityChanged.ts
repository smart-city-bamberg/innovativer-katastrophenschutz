import { GpsLocation } from '@lion5/component-library-leaflet'
import { UnitAvailability } from '@/mqtt/payloads/UnitAvailability'

export class PubUnitAvailabilityChanged {
  /**
   * Whether the unit was added or removed from the mission.
   */
  readonly unitAvailability: UnitAvailability
  /**
   * ID of the unit to be positioned.
   */
  readonly unitId: string
  /**
   * Location that should be set for the unit with {@link unitId}.
   */
  readonly location?: GpsLocation
  /**
   * ISO datetime string of the timestamp when the message was sent.
   */
  readonly createdAt: string = new Date().toISOString()

  constructor(
    unitAvailability: UnitAvailability,
    unitId: string,
    location?: GpsLocation
  ) {
    this.unitAvailability = unitAvailability
    this.unitId = unitId
    this.location = location
  }
}
