import type { ApiGpsLocation } from '@lion5/component-library-leaflet'

export class PubReport {
  reportId: string
  senderUnitId: string
  report: string
  tacticalSignPath: string
  createdAt: string
  receiverUnitIds?: string[]
  referencingCommandId?: string
  images?: string[]
  location?: ApiGpsLocation

  constructor(
    reportId: string,
    senderUnitId: string,
    report: string,
    tacticalSignPath: string,
    createdAt: string,
    images?: string[],
    location?: ApiGpsLocation,
    receiverUnitIds?: string[],
    referencingCommandId?: string
  ) {
    this.reportId = reportId
    this.senderUnitId = senderUnitId
    this.receiverUnitIds = receiverUnitIds
    this.report = report
    this.tacticalSignPath = tacticalSignPath
    this.createdAt = createdAt
    this.referencingCommandId = referencingCommandId
    this.images = images
    this.location = location
  }
}
