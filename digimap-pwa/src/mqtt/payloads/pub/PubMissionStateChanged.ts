import type { MissionState } from '@/mqtt/payloads/MissionState'

export class PubMissionStateChanged {
  /**
   * Whether the unit was added or removed from the mission.
   */
  readonly missionState: MissionState
  /**
   * etag of the mission
   */
  readonly etag: number
  /**
   * ISO datetime string of the timestamp when the message was sent.
   */
  readonly createdAt: string = new Date().toISOString()

  constructor(missionState: MissionState, etag: number) {
    this.missionState = missionState
    this.etag = etag
  }
}
