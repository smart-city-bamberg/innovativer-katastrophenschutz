/**
 * Can be used in a message payload of an asynchronous APIs (e.g. MQTT) to
 * determine whether a new object is created, an existing one is updated, or an
 * object is deleted. The object is identified by the accompanied
 * message payload properties (e.g. a UUID).
 */
export enum AsyncCrudOperation {
  CREATED = 'CREATED',
  UPDATED = 'UPDATED',
  DELETED = 'DELETED',
}
