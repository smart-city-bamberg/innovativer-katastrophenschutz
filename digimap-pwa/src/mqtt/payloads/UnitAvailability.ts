export enum UnitAvailability {
  JOINED = 'JOINED',
  REMOVED = 'REMOVED',
}
