import type { UnitAvailability } from '@/mqtt/payloads/UnitAvailability'
import type { ApiGpsLocation } from '@lion5/component-library-leaflet'

export interface ISubUnitAvailabilityChanged {
  etag: number
  unitAvailability: UnitAvailability
  createdAt: string
  unitId: string
  callSign: string
  tacticalSign: string
  location?: ApiGpsLocation
  messageType: string
}
