import type { MissionState } from '@/mqtt/payloads/MissionState'

export interface ISubMissionStateChanged {
  etag: number
  missionState: MissionState
  createdAt: string
  messageType: string
}
