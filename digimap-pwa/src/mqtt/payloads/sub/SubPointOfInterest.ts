import { AsyncCrudOperation } from '@/mqtt/payloads/AsyncCrudOperation'
import type { ApiGpsLocation } from '@lion5/component-library-leaflet'

export interface SubPointOfInterest {
  poiId: string
  etag: number
  senderUnitId: string
  details: string
  tacticalSignPath: string
  createdAt: number | string
  operation: AsyncCrudOperation
  location: ApiGpsLocation
}
