import type { ApiGpsLocation } from '@lion5/component-library-leaflet'
import { GpsLocation } from '@lion5/component-library-leaflet'
import type { GenericMqttPayload } from '@/mqtt/iMqttClient'

export interface ISubLocationUpdate {
  etag: number
  unitId: string
  location: ApiGpsLocation
  timestamp: string
}

export class SubLocationUpdate {
  readonly eTag: number
  readonly unitId: string
  readonly location: GpsLocation
  readonly timestamp: Date

  constructor(
    eTag: number,
    unitId: string,
    location: GpsLocation,
    timestamp: Date
  ) {
    this.eTag = eTag
    this.unitId = unitId
    this.location = location
    this.timestamp = timestamp
  }

  public static fromMqtt(payload: GenericMqttPayload) {
    const typedPayload = payload as ISubLocationUpdate
    return new SubLocationUpdate(
      typedPayload.etag,
      typedPayload.unitId,
      GpsLocation.fromApi(typedPayload.location),
      new Date(typedPayload.timestamp)
    )
  }
}
