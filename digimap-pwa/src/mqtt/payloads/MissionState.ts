export enum MissionState {
  RUNNING = 'RUNNING',
  FINISHED = 'FINISHED',
}
