import { mount } from '@vue/test-utils'
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest'
import type { defineComponent } from 'vue'
import UnitsView from '@/unit/pages/UnitsView.vue'
import { createTestingPinia } from '@pinia/testing'
import { useRouter } from 'vue-router'
import { ApprovalModal } from '@lion5/component-library'
import { MockData } from '@/assets/mocks/data'

vi.mock('vue-router', async () => {
  const originalRouter = await vi.importActual('vue-router')
  return {
    ...originalRouter,
    useRouter: vi.fn().mockReturnValue({
      push: vi.fn(),
    }),
  }
})

describe('UnitsView', () => {
  let wrapper: ReturnType<typeof defineComponent>

  const units = MockData.getAllUnits()
  const pinia = createTestingPinia({
    initialState: {
      positionUnit: {
        allUnits: units,
      },
    },
  })

  beforeEach(() => {
    wrapper = mount(UnitsView, {
      global: {
        plugins: [pinia],
        stubs: ['RouterLink'],
      },
      attachTo: document.body,
    })
  })

  afterEach(() => {
    wrapper.unmount()
    vi.clearAllMocks()
  })

  it('should render the units', () => {
    const rows = wrapper.findAll('tr')
    expect(rows).toHaveLength(units.length + 1) // +1 for the header row
  })

  it('should render the correct unit data', () => {
    const rows = wrapper.findAll('tr')
    const firstRow = rows[1]
    const secondRow = rows[2]
    expect(firstRow.text()).toContain(units[0].callSign)
    expect(firstRow.html()).toContain(units[0].tacticalSign.path)
    expect(secondRow.text()).toContain(units[1].callSign)
    expect(secondRow.html()).toContain(units[1].tacticalSign.path)
  })

  it('should render the correct tactical sign', () => {
    const rows = wrapper.findAll('tr')
    const firstRow = rows[1]
    const secondRow = rows[2]
    const firstRowTacticalSign = firstRow.find('img')
    const secondRowTacticalSign = secondRow.find('img')
    expect(firstRowTacticalSign.attributes('src')).toBe(
      units[0].tacticalSign.path
    )
    expect(secondRowTacticalSign.attributes('src')).toBe(
      units[1].tacticalSign.path
    )
  })

  it('should filter the units by call sign', async () => {
    const input = wrapper.find('input')
    await input.setValue('euerwehr')
    const rows = wrapper.findAll('tr')
    expect(rows).toHaveLength(5)
    expect(rows[1].text()).toContain(units[0].callSign)
    expect(rows[2].text()).toContain(units[1].callSign)
    expect(rows[3].text()).toContain(units[2].callSign)
    expect(rows[4].text()).toContain(units[3].callSign)

    await input.setValue('2')
    const filteredRows = wrapper.findAll('tr')
    expect(filteredRows).toHaveLength(2)
    expect(filteredRows[1].text()).toContain(units[1].callSign)
  })

  it('should filter the units by assignment', async () => {
    const assignedButton = wrapper.find('.assigned-button')
    await assignedButton.trigger('click')
    const assignedRows = wrapper.findAll('tr')
    expect(assignedRows).toHaveLength(4)

    const unassignedButton = wrapper.find('.not-assigned-button')
    await unassignedButton.trigger('click')
    const unassignedRows = wrapper.findAll('tr')
    expect(unassignedRows).toHaveLength(3)
  })

  it('should open the right modal if clicking on the delete button', async () => {
    const rows = wrapper.findAll('tr')
    const firstRow = rows[1]
    const deleteButton = firstRow.find('.delete-button')
    const approvalModal = wrapper.findComponent(ApprovalModal)
    expect(approvalModal.props('modalDisplayed')).toBe(false)
    await deleteButton.trigger('click')
    expect(approvalModal.props('modalDisplayed')).toBe(true)
  })

  it.skip('should open the right modal if clicking on the unassign button', async () => {
    const rows = wrapper.findAll('tr')
    const firstRow = rows[1]
    const unassignButton = firstRow.find('.unassign-button')
    await unassignButton.trigger('click')
    //find ApprovalModal with test-id="unassign-modal" & class="unassign-modal"
    const unassignApprovalModal = wrapper.find(
      '[data-test-id="unassign-modal"]'
    )

    console.log(unassignApprovalModal.html())
    expect(unassignApprovalModal.props('modalDisplayed')).toBe(true)
  })

  it('should route to the correct page if clicking on the edit button', async () => {
    const mockedRouter = useRouter()
    const rows = wrapper.findAll('tr')
    const firstRow = rows[1]
    const editButton = firstRow.find('.edit-button')
    expect(editButton.exists()).toBe(true)
    await editButton.trigger('click')
    expect(mockedRouter.push).toHaveBeenCalledTimes(1)
    expect(mockedRouter.push).toHaveBeenCalledWith(`/unit/edit/${units[0].id}`)
  })

  it('should route to the correct page if clicking on the create unit button', async () => {
    const mockedRouter = useRouter()
    const button = wrapper.find('.button.unit')
    expect(button.exists()).toBe(true)
    await button.trigger('click')
    expect(mockedRouter.push).toHaveBeenCalledTimes(1)
    expect(mockedRouter.push).toHaveBeenCalledWith('/unit/create')
  })
})
