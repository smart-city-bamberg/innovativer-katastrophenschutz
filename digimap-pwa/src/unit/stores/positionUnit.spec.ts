import { beforeAll, describe, expect, it, vi } from 'vitest'
import type { PositionUnitForm } from '@/unit/stores/positionUnit'
import { usePositionUnitStore } from '@/unit/stores/positionUnit'
import { MockData } from '@/assets/mocks/data'
import { createPinia, setActivePinia, storeToRefs } from 'pinia'
import * as useUnitExports from '@/unit/services/useUnit'
import { useMissionsStore } from '@/mission/stores/missions'
import { GpsLocation } from '@lion5/component-library-leaflet'
import * as useLocationServiceExports from '@/base/composables/useLocationService'
import { useLocationService } from '@/base/composables/useLocationService'
import { ref } from 'vue'
import * as useUnitAvailabilityExports from '@/unit/composables/useUnitAvailability'
import { useUnitAvailability } from '@/unit/composables/useUnitAvailability'
import { UnitAvailability } from '@/mqtt/payloads/UnitAvailability'

const mission = MockData.getMissions()[0]
const units = MockData.getAllUnits()

vi.spyOn(useUnitExports, 'useUnit').mockReturnValue({
  getAllUnits: vi.fn().mockResolvedValue(units),
  getUnitByUserId: vi.fn(),
  createUnit: vi.fn(),
  deleteUnit: vi.fn(),
  patchUnit: vi.fn(),
  getFirebaseUsers: vi.fn(),
  addUserToUnit: vi.fn(),
  removeUserFromUnit: vi.fn(),
})
vi.spyOn(useUnitAvailabilityExports, 'useUnitAvailability').mockReturnValue({
  publishUnitAvailabilityChanged: vi.fn(),
})
vi.spyOn(useLocationServiceExports, 'useLocationService').mockReturnValue({
  currentLocation: ref<GpsLocation>(),
  unprocessedUpdates: [],
  startContinuousUnitLocationUpdates: vi.fn(),
  publishUnitLocationUpdate: vi.fn(),
  getCurrentUserPosition: vi.fn(),
  handleInboundLocationUpdates: vi.fn(),
})

describe('usePositionUnitStore', () => {
  beforeAll(() => {
    setActivePinia(createPinia())
  })

  describe('fetchUnits', () => {
    it('should set allUnits to retrieved units', async () => {
      const store = usePositionUnitStore()
      const { allUnits } = storeToRefs(store)

      await store.fetchUnits()

      expect(allUnits.value).toStrictEqual(units)
    })
  })

  describe('getUnitById', () => {
    it('should return correct unit', () => {
      const store = usePositionUnitStore()
      const { allUnits } = storeToRefs(store)
      const expectedUnit = units[0]
      allUnits.value = units

      const result = store.getUnitById(expectedUnit.id)

      expect(result).toStrictEqual(expectedUnit)
    })
    it('should return undefined if unit does not exist', () => {
      const store = usePositionUnitStore()
      const { allUnits } = storeToRefs(store)
      const nonExistingUnitId = 'doesNotExist'
      allUnits.value = units

      const result = store.getUnitById(nonExistingUnitId)

      expect(result).toBeUndefined()
    })
  })

  describe('positionUnit', () => {
    it('should throw error if unitId is empty', () => {
      const store = usePositionUnitStore()
      const formValues = { unitId: '' } as PositionUnitForm

      expect(() => store.positionUnit(formValues)).rejects.toThrowError(
        'Keine Einheit zugewiesen. Bitte kontaktieren Sie einen System-Administrator.'
      )
    })
    it('should throw error if currentMission is undefined', () => {
      const store = usePositionUnitStore()
      const formValues = { unitId: units[0].id } as PositionUnitForm

      expect(() => store.positionUnit(formValues)).rejects.toThrowError(
        'Kein Einsatz ausgewählt. Bitte wählen Sie einen Einsatz in den Einstellungen oder auf der Lagekarte.'
      )
    })
    it('should throw error if location is undefined', () => {
      const store = usePositionUnitStore()
      const formValues = { unitId: units[0].id } as PositionUnitForm
      const { currentMission } = storeToRefs(useMissionsStore())
      currentMission.value = mission

      expect(() => store.positionUnit(formValues)).rejects.toThrowError(
        'Ort der Einheit konnte nicht ermittelt werden. Bitte setzen Sie den Ort neu.'
      )
    })
    it('should update unit location and publish update if unit belongs to mission', async () => {
      const store = usePositionUnitStore()
      const unitId = units[0].id
      const formValues = { unitId } as PositionUnitForm
      const missionsStore = useMissionsStore()
      const { currentMission } = storeToRefs(missionsStore)
      currentMission.value = mission
      const { publishUnitLocationUpdate } = useLocationService()
      const updatedLocation = new GpsLocation(1.0, 1.0)
      store.setUnitLocation(updatedLocation)
      missionsStore.updateUnitLocationById = vi.fn()
      const { allUnits } = storeToRefs(store)
      allUnits.value = units

      await store.positionUnit(formValues)

      expect(publishUnitLocationUpdate).toHaveBeenCalledWith(
        unitId,
        updatedLocation
      )
      expect(missionsStore.updateUnitLocationById).toHaveBeenCalledWith(
        unitId,
        updatedLocation
      )
    })
    it('should add unit to current mission and publish availability change if unit is new to mission', async () => {
      const store = usePositionUnitStore()
      // use unitId from other mission
      const otherUnit = MockData.getMissions()[1].units[0]
      const unitId = otherUnit.id
      const formValues = { unitId } as PositionUnitForm
      const missionsStore = useMissionsStore()
      const { currentMission } = storeToRefs(missionsStore)
      currentMission.value = mission
      const updatedLocation = new GpsLocation(1.0, 1.0)
      store.setUnitLocation(updatedLocation)
      missionsStore.addUnit = vi.fn()
      const { allUnits } = storeToRefs(store)
      allUnits.value = units
      const { publishUnitAvailabilityChanged } = useUnitAvailability()

      await store.positionUnit(formValues)

      expect(mission.hasUnit(otherUnit)).toBe(false)
      expect(missionsStore.addUnit).toHaveBeenCalledWith(otherUnit)
      expect(publishUnitAvailabilityChanged).toHaveBeenCalledWith(
        UnitAvailability.JOINED,
        unitId,
        mission.missionId,
        updatedLocation
      )
    })
  })
})
