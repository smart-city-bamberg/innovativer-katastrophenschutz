import { createPinia, setActivePinia } from 'pinia'
import { beforeEach, describe, expect, it, vi } from 'vitest'
import { GpsLocation } from '@lion5/component-library'
import type { CreateUnitForm } from '@/unit/stores/createUnit'
import { useCreateUnitStore } from '@/unit/stores/createUnit'
import type { CreateApiUnit, UpdateApiUnit } from '@/unit/models/unit'
import { usePositionUnitStore } from '@/unit/stores/positionUnit'
import { MockData } from '@/assets/mocks/data'

const units = MockData.getAllUnits()

describe('useCreateUnitStore', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
    vi.clearAllMocks()
  })
  const mockDateIsoString = '2024-07-16T10:18:53.183Z'
  const mockDate = new Date(mockDateIsoString)
  vi.spyOn(global, 'Date').mockImplementation(() => mockDate)
  vi.spyOn(mockDate, 'toISOString').mockReturnValue(mockDateIsoString)

  describe('publishUnit', () => {
    const expectedCreateApiUnit: CreateApiUnit = {
      unitType: units[0].unitType,
      callSign: units[0].callSign,
      tacticalSignPath: units[0].tacticalSign.relativePath,
      gpsLocation: new GpsLocation(0, 0),
      lastLocationUpdate: mockDateIsoString,
    }

    it('publishes correct payload', async () => {
      const formValues = {
        callSign: units[0].callSign,
        unitType: units[0].unitType,
        tacticalSignPath: units[0].tacticalSign.path,
        user: units[0].user,
      } as CreateUnitForm

      const createUnitStore = useCreateUnitStore()
      const positionUnitStore = usePositionUnitStore()
      vi.spyOn(positionUnitStore, 'addUserToUnit').mockResolvedValue()
      vi.spyOn(positionUnitStore, 'createUnit').mockReturnValue(
        Promise.resolve(units[0])
      )

      await createUnitStore.publishUnit(formValues)

      expect(positionUnitStore.createUnit).toHaveBeenCalledOnce()
      expect(positionUnitStore.createUnit).toHaveBeenCalledWith(
        expectedCreateApiUnit
      )
      expect(positionUnitStore.addUserToUnit).toHaveBeenCalledOnce()
      expect(positionUnitStore.addUserToUnit).toHaveBeenCalledWith(
        units[0].id,
        units[0].user
      )
    })

    it('create - does not add user to unit if user is undefined', async () => {
      const formValues = {
        callSign: units[0].callSign,
        unitType: units[0].unitType,
        tacticalSignPath: units[0].tacticalSign.path,
      } as CreateUnitForm

      const createUnitStore = useCreateUnitStore()
      const positionUnitStore = usePositionUnitStore()
      vi.spyOn(positionUnitStore, 'addUserToUnit').mockResolvedValue()
      vi.spyOn(positionUnitStore, 'createUnit').mockReturnValue(
        Promise.resolve(units[0])
      )

      await createUnitStore.publishUnit(formValues)

      expect(positionUnitStore.createUnit).toHaveBeenCalledOnce()
      expect(positionUnitStore.createUnit).toHaveBeenCalledWith(
        expectedCreateApiUnit
      )
      expect(positionUnitStore.addUserToUnit).not.toHaveBeenCalled()
    })
  })

  describe('editUnit', () => {
    const expectedUpdateApiUnit: UpdateApiUnit = {
      unitType: units[0].unitType,
      callSign: units[0].callSign,
      tacticalSignPath: units[0].tacticalSign.relativePath,
    }

    it('publishes correct payload', async () => {
      const formValues = {
        unitId: units[0].id,
        callSign: units[0].callSign,
        unitType: units[0].unitType,
        tacticalSignPath: units[0].tacticalSign.path,
        user: units[0].user,
      } as CreateUnitForm

      const createUnitStore = useCreateUnitStore()
      const positionUnitStore = usePositionUnitStore()
      vi.spyOn(positionUnitStore, 'addUserToUnit').mockResolvedValue()
      vi.spyOn(positionUnitStore, 'patchUnit').mockReturnValue(
        Promise.resolve(units[0])
      )

      await createUnitStore.editUnit(formValues)

      expect(positionUnitStore.patchUnit).toHaveBeenCalledOnce()
      expect(positionUnitStore.patchUnit).toHaveBeenCalledWith(
        units[0].id,
        expectedUpdateApiUnit
      )
      expect(positionUnitStore.addUserToUnit).toHaveBeenCalledOnce()
      expect(positionUnitStore.addUserToUnit).toHaveBeenCalledWith(
        units[0].id,
        units[0].user
      )
    })

    it('edit - does not add user to unit if user is undefined', async () => {
      const formValues = {
        unitId: units[0].id,
        callSign: units[0].callSign,
        unitType: units[0].unitType,
        tacticalSignPath: units[0].tacticalSign.path,
      } as CreateUnitForm

      const createUnitStore = useCreateUnitStore()
      const positionUnitStore = usePositionUnitStore()
      vi.spyOn(positionUnitStore, 'addUserToUnit').mockResolvedValue()
      vi.spyOn(positionUnitStore, 'patchUnit').mockReturnValue(
        Promise.resolve(units[0])
      )

      await createUnitStore.editUnit(formValues)

      expect(positionUnitStore.patchUnit).toHaveBeenCalledOnce()
      expect(positionUnitStore.patchUnit).toHaveBeenCalledWith(
        units[0].id,
        expectedUpdateApiUnit
      )
      expect(positionUnitStore.addUserToUnit).not.toHaveBeenCalled()
    })
  })
})
