import { defineStore } from 'pinia'
import { ref } from 'vue'
import { object, string } from 'yup'
import { GpsLocation } from '@lion5/component-library'
import type {
  CreateApiUnit,
  EmergencyUnitUser,
  UpdateApiUnit,
} from '@/unit/models/unit'
import { TacticalSign } from '@/base/models/tacticalSign'
import { usePositionUnitStore } from '@/unit/stores/positionUnit'
import { useTacticalSignStore } from '@/tactical-sign/stores/customTacticalSign'

export interface CreateUnitForm {
  unitId: string
  callSign: string
  unitType: string
  tacticalSignPath: string
  user: EmergencyUnitUser
}

export const useCreateUnitStore = defineStore('create-unit', () => {
  const callSign = ref<string>()
  const unitType = ref<string>()
  const tacticalSign = ref<TacticalSign>()
  const user = ref<EmergencyUnitUser>()
  const formErrorMessage = ref<string>()

  const unitStore = usePositionUnitStore()
  const tacticalSignStore = useTacticalSignStore()

  const schema = object({
    callSign: string().required('Bitte geben sie ein Rufzeichen an'),
    unitType: string().required('Bitte geben sie einen Einheitstyp an'),
    tacticalSignPath: string().required(
      'Bitte wählen Sie ein taktisches Zeichen'
    ),
  })
  const initialValues = {
    unitId: '',
    callSign: '',
    unitType: '',
    tacticalSignPath: '',
    user: '',
  }

  const publishUnit = async (formValues: CreateUnitForm) => {
    const existingCustomTacticalSign =
      tacticalSignStore.getCustomTacticalSignByPath(formValues.tacticalSignPath)
    if (existingCustomTacticalSign === undefined) {
      tacticalSign.value = TacticalSign.withPrefix(formValues.tacticalSignPath)
    } else {
      tacticalSign.value = existingCustomTacticalSign
    }
    if (!tacticalSign.value) {
      formErrorMessage.value =
        'Das taktische Zeichen konnte nicht gefunden werden'
      return
    }
    const apiUnit: CreateApiUnit = {
      unitType: formValues.unitType,
      callSign: formValues.callSign,
      tacticalSignPath: tacticalSign.value.relativePath,
      gpsLocation: new GpsLocation(0, 0),
      lastLocationUpdate: new Date().toISOString(),
    }

    const unit = await unitStore.createUnit(apiUnit)
    user.value = formValues.user
    if (unit && user.value !== undefined) {
      await unitStore.addUserToUnit(unit.id, user.value)
    }
  }

  const editUnit = async (formValues: CreateUnitForm) => {
    const existingCustomTacticalSign =
      tacticalSignStore.getCustomTacticalSignByPath(formValues.tacticalSignPath)
    if (existingCustomTacticalSign === undefined) {
      tacticalSign.value = TacticalSign.withPrefix(formValues.tacticalSignPath)
    } else {
      tacticalSign.value = existingCustomTacticalSign
    }
    if (!tacticalSign.value) {
      formErrorMessage.value =
        'Das taktische Zeichen konnte nicht gefunden werden'
      return
    }

    const unitUpdate: UpdateApiUnit = {
      unitType: formValues.unitType,
      callSign: formValues.callSign,
      tacticalSignPath: tacticalSign.value.relativePath,
    }

    const unit = await unitStore.patchUnit(formValues.unitId, unitUpdate)
    user.value = formValues.user
    if (unit && user.value !== undefined) {
      await unitStore.addUserToUnit(unit.id, user.value)
    }
  }

  const resetFormErrors = () => {
    formErrorMessage.value = ''
  }

  return {
    formSchema: schema,
    initialValues,
    callSign,
    unitType,
    tacticalSign,
    user,
    formErrorMessage,
    resetFormErrors,
    publishUnit,
    editUnit,
  }
})
