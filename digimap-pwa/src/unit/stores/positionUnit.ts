import { defineStore } from 'pinia'
import { ref, toRaw } from 'vue'
import { GpsLocation } from '@lion5/component-library-leaflet'
import { object, string } from 'yup'
import type {
  AddUserToEmergencyUnitRequest,
  CreateApiUnit,
  DeleteUserFromEmergencyUnitRequest,
  EmergencyUnitUser,
  UpdateApiUnit,
} from '@/unit/models/unit'
import { Unit } from '@/unit/models/unit'
import { useUnit } from '@/unit/services/useUnit'
import { useMissionsStore } from '@/mission/stores/missions'
import { useLocationService } from '@/base/composables/useLocationService'
import { UnitAvailability } from '@/mqtt/payloads/UnitAvailability'
import { useUnitAvailability } from '@/unit/composables/useUnitAvailability'

export interface PositionUnitForm {
  unitId: string
}

export const usePositionUnitStore = defineStore('positionUnit', () => {
  const unitLocation = ref<GpsLocation>()
  const allUnits = ref<Unit[]>([])

  const formSchema = object({
    unitId: string().required('Bitte wählen Sie eine Einheit'),
  })

  const fetchUnits = async () => {
    const { getAllUnits } = useUnit()

    try {
      allUnits.value = await getAllUnits()
    } catch (e) {
      throw new Error('Could not retrieve units, ' + e)
    }
  }

  const setUnitLocation = (location: GpsLocation) => {
    unitLocation.value = location
  }

  const setUnitUser = (unitId: string, user: EmergencyUnitUser | undefined) => {
    const unit = getUnitById(unitId)
    if (unit) {
      const newUnit = new Unit(
        unit.id,
        unit.unitType,
        unit.gpsLocation,
        unit.callSign,
        unit.tacticalSign,
        unit.uri,
        user,
        unit.creationMethod
      )
      const index = allUnits.value.findIndex((unit) => unit.id === unitId)
      allUnits.value[index] = newUnit
      allUnits.value = [...allUnits.value]
    }
  }

  const positionUnit = async (formValues: PositionUnitForm) => {
    const { unitId } = formValues
    const {
      currentMission,
      addUnit: addUnitToCurrentMission,
      updateUnitLocationById,
      unitBelongsToCurrentMission,
    } = useMissionsStore()
    const missionId = currentMission?.missionId
    const updatedLocation = toRaw(unitLocation.value)

    if (!unitId) {
      throw new Error(
        'Keine Einheit zugewiesen. Bitte kontaktieren Sie einen System-Administrator.'
      )
    }
    if (!currentMission || !missionId) {
      throw new Error(
        'Kein Einsatz ausgewählt. Bitte wählen Sie einen Einsatz in den Einstellungen oder auf der Lagekarte.'
      )
    }
    if (!updatedLocation) {
      throw new Error(
        'Ort der Einheit konnte nicht ermittelt werden. Bitte setzen Sie den Ort neu.'
      )
    }

    const unit = getUnitById(unitId)
    if (!unit) {
      return
    }
    if (unitBelongsToCurrentMission(unit)) {
      // unit already belongs to mission => update location, and publish location update
      const { publishUnitLocationUpdate } = useLocationService()
      updateUnitLocationById(unitId, updatedLocation)
      publishUnitLocationUpdate(unitId, updatedLocation)
    } else {
      // unit joined mission => update location, add to current mission and publish availability change
      const { publishUnitAvailabilityChanged } = useUnitAvailability()
      unit.gpsLocation = updatedLocation
      addUnitToCurrentMission(unit)
      publishUnitAvailabilityChanged(
        UnitAvailability.JOINED,
        unitId,
        missionId,
        updatedLocation
      )
    }
  }

  const getUnitById = (unitId: string): Unit | undefined =>
    allUnits.value.find((unit) => unit.id === unitId)

  const isUnitAlreadyPositioned = (unitId: string) =>
    getUnitById(unitId)?.isPositioned()

  const createUnit = async (apiUnit: CreateApiUnit) => {
    const { createUnit } = useUnit()

    const unit = await createUnit(apiUnit)
    if (unit !== null) {
      allUnits.value.push(unit)
      return unit
    } else {
      console.error('Error creating unit:', unit)
    }
  }

  const deleteUnit = async (unitId: string) => {
    const { deleteUnit } = useUnit()

    if (await deleteUnit(unitId)) {
      allUnits.value = allUnits.value.filter((unit) => unit.id !== unitId)
      return true
    } else {
      return false
    }
  }

  const patchUnit = async (unitId: string, unitUpdate: UpdateApiUnit) => {
    const { patchUnit } = useUnit()

    const unit = await patchUnit(unitId, unitUpdate)
    if (unit !== null) {
      const index = allUnits.value.findIndex((unit) => unit.id === unitId)
      allUnits.value[index] = unit
      return unit
    } else {
      console.error('Error creating unit:', unit)
    }
  }

  const addUserToUnit = async (unitId: string, user: EmergencyUnitUser) => {
    const { addUserToUnit } = useUnit()
    const email = user.email
    const addUserToEmergencyUnitRequest: AddUserToEmergencyUnitRequest = {
      unitId,
      email,
    }

    const success = await addUserToUnit(addUserToEmergencyUnitRequest)
    if (success) {
      setUnitUser(unitId, user)
    } else {
      throw new Error('Error adding user to unit')
    }
  }

  const removeUserFromUnit = async (unitId: string, email: string) => {
    const { removeUserFromUnit } = useUnit()
    const deleteUserToEmergencyUnitRequest: DeleteUserFromEmergencyUnitRequest =
      {
        unitId,
        email,
      }
    const success = await removeUserFromUnit(deleteUserToEmergencyUnitRequest)
    if (success) {
      setUnitUser(unitId, undefined)
    } else {
      throw new Error('Error removing user from unit')
    }
  }

  return {
    allUnits,
    unitLocation,
    formSchema,
    positionUnit,
    setUnitLocation,
    fetchUnits,
    getUnitById,
    createUnit,
    deleteUnit,
    patchUnit,
    isUnitAlreadyPositioned,
    addUserToUnit,
    removeUserFromUnit,
  }
})
