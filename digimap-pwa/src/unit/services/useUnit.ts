import { RestClient } from '@/base/services/restClient'
import type {
  AddUserToEmergencyUnitRequest,
  ApiUnit,
  CreateApiUnit,
  DeleteUserFromEmergencyUnitRequest,
  EmergencyUnitUser,
  UpdateApiUnit,
} from '@/unit/models/unit'
import { Unit } from '@/unit/models/unit'
import type { State } from 'ketting'
import { useAccountStore } from '@/account/stores/account'
import { storeToRefs } from 'pinia'

export function useUnit() {
  const _restClient: RestClient = RestClient.getInstance()
  const baseUrl = import.meta.env.VITE_APP_DIGIMAP_API_BASE_URL

  const getAllUnits = async (): Promise<Unit[]> => {
    const unitResources = await _restClient
      //FIXME: This is a workaround to get all units. The API should be paginated.
      //FIXME: The table in the frontend should use the pagination from the backend
      //FIXME: Issue #583
      .go<ApiUnit[]>('/units?page=0&size=300')
      .followAll<ApiUnit>('emergencyUnits')
      .preFetch()
    try {
      const units: Promise<Unit[]> = Promise.all(
        unitResources.map(async (resource) => {
          const state = await resource.get()
          return Unit.fromKettingState(state)
        })
      )

      console.log('Retrieved all units', units)
      return units
    } catch (e) {
      throw new Error('Could not retrieve units, ' + e)
    }
  }
  const createUnit = async (apiUnit: CreateApiUnit) => {
    return await _restClient
      .go('/units')
      .post({ data: apiUnit })
      .then((response) => {
        console.log('Success:', response)
        return Unit.fromKettingState(response)
      })
      .catch((error) => {
        console.error('Error:', error)
        return null
      })
  }

  const deleteUnit = async (unitId: string): Promise<boolean> => {
    try {
      await _restClient.go(`/units/${unitId}`).delete()
      console.log('Successfully deleted unit:', unitId)
      return true
    } catch (error) {
      console.error('Error deleting unit:', error)
      return false
    }
  }

  const patchUnit = async (unitId: string, unitUpdate: UpdateApiUnit) => {
    return await _restClient
      .go(`/units/${unitId}`)
      .patch({ data: unitUpdate })
      .then((response) => {
        console.log('Success:', response)
        return Unit.fromKettingState(response as State<ApiUnit>)
      })
      .catch((error) => {
        console.error('Error:', error)
        return null
      })
  }

  const getFirebaseUsers = async (): Promise<EmergencyUnitUser[]> => {
    const users = await _restClient.go('/users/firebase').get()
    return users.data
  }

  const addUserToUnit = async (
    addUserToEmergencyUnitRequest: AddUserToEmergencyUnitRequest
  ): Promise<boolean> => {
    return await _restClient
      .go('/users/unit')
      .put({ data: addUserToEmergencyUnitRequest })
      .then(() => {
        return true
      })
      .catch((error) => {
        console.error('Error:', error)
        return false
      })
  }

  const removeUserFromUnit = async (
    deleteUserFromEmergencyUnitRequest: DeleteUserFromEmergencyUnitRequest
  ): Promise<boolean> => {
    const accountStore = useAccountStore()
    const { bearerToken } = storeToRefs(accountStore)

    const response = await fetch(baseUrl + '/users/unit', {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${bearerToken.value}`,
      },
      body: JSON.stringify(deleteUserFromEmergencyUnitRequest),
    })

    return response.ok
  }
  
  const getUnitByUserId = async (userId: string): Promise<Unit | undefined> => {
    if (!userId) {
      return undefined
    }
    try {
      const unitState = await _restClient
        .go(`/units/search/findByUserId?userId=${userId}`)
        .get()
      return unitState ? Unit.fromKettingState(unitState) : undefined
    } catch (error) {
      if (error instanceof Error && error.message.includes('404')) {
        return undefined
      }
    }
  }

  return {
    getAllUnits,
    createUnit,
    deleteUnit,
    patchUnit,
    getFirebaseUsers,
    addUserToUnit,
    removeUserFromUnit,
    getUnitByUserId
  }
}