import type { ApiGpsLocation } from '@lion5/component-library-leaflet'
import { GpsLocation } from '@lion5/component-library-leaflet'
import type { State } from 'ketting'
import { TacticalSign } from '@/base/models/tacticalSign'
import type { ISubUnitAvailabilityChanged } from '@/mqtt/payloads/sub/SubUnitAvailabilityChanged'

/**
 * https://github.com/lion5/smart-city-digimap/wiki/Glossary#einheit-unit
 */
export class Unit {
  /**
   * Unique identifier of the unit.
   */
  readonly id: string
  /**
   * Type of the unit, e.g. "TLF 24/50" (Tanklöschfahrzeug)
   */
  readonly unitType: string
  /**
   * "Funkrufname" of the unit, see https://github.com/lion5/smart-city-digimap/wiki/Glossary#funkrufname-call-sign.
   */
  readonly callSign: string
  /**
   * Path to the SVG file of the tactical sign, relative to https://github.com/jonas-koeritz/Taktische-Zeichen/tree/v1.9.9.4/symbols (as .svg)
   */
  readonly tacticalSign: TacticalSign
  /**
   * URI to the unit in the REST API.
   */
  readonly uri?: string
  /**
   * Current location of the unit.
   */
  gpsLocation: GpsLocation
  /*
   * User assigned to the unit.
   */
  user?: EmergencyUnitUser
  /*
   * Flag if the unit is temporary or permanent
   */
  creationMethod?: creationMethod

  constructor(
    id: string,
    unitType: string,
    gpsLocation: GpsLocation,
    callSign: string,
    tacticalSign: TacticalSign,
    uri?: string,
    user?: EmergencyUnitUser,
    creationMethod?: creationMethod
  ) {
    this.id = id
    this.unitType = unitType
    this.gpsLocation = gpsLocation
    this.callSign = callSign
    this.tacticalSign = tacticalSign
    this.uri = uri
    this.user = user
    this.creationMethod = creationMethod
  }

  public static fromKettingState(unitState: State<ApiUnit>) {
    return new Unit(
      unitState.data.unitId,
      unitState.data.unitType,
      GpsLocation.fromApi(unitState.data.gpsLocation),
      unitState.data.callSign,
      new TacticalSign(unitState.data.tacticalSignPath),
      unitState.uri,
      unitState.data.user || undefined,
      unitState.data.creationMethod
    )
  }

  public static fromSubUnitAvailabilityChanged(
    payload: ISubUnitAvailabilityChanged
  ): Unit | undefined {
    if (!payload.location) {
      return undefined
    }
    return new Unit(
      payload.unitId,
      '',
      GpsLocation.fromApi(payload.location),
      payload.callSign,
      new TacticalSign(payload.tacticalSign)
    )
  }

  getAidOrg() {
    const re = /^([\w\-]+)/
    return this.callSign.match(re)?.at(0)
  }

  public isPositioned() {
    return (
      this.gpsLocation &&
      this.gpsLocation.latitude &&
      this.gpsLocation.longitude
    )
  }
}

export interface EmergencyUnitUser {
  userId: string
  displayName: string
  email: string
}

//enum
export enum creationMethod {
  PERMANENT = 'PERMANENT',
  TEMPORARY = 'TEMPORARY',
}

export interface AddUserToEmergencyUnitRequest {
  unitId: string
  email: string
}

export interface DeleteUserFromEmergencyUnitRequest {
  unitId: string
  email: string
}

export interface CreateApiUnit {
  unitType: string
  callSign: string
  tacticalSignPath: string
  gpsLocation: ApiGpsLocation
  lastLocationUpdate: string
}

export interface UpdateApiUnit {
  unitType: string
  callSign: string
  tacticalSignPath: string
}

export interface ApiUnit extends CreateApiUnit {
  unitId: string
  user: EmergencyUnitUser
  creationMethod: creationMethod
}
