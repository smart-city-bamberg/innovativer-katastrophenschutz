import { UnitAvailability } from '@/mqtt/payloads/UnitAvailability'
import { GpsLocation } from '@lion5/component-library-leaflet'
import { PubUnitAvailabilityChanged } from '@/mqtt/payloads/pub/PubUnitAvailabilityChanged'
import { MqttJsClient } from '@/mqtt/mqttClient'
import type { ISubUnitAvailabilityChanged } from '@/mqtt/payloads/sub/SubUnitAvailabilityChanged'
import type { GenericMqttPayload } from '@/mqtt/iMqttClient'
import { useMissionsStore } from '@/mission/stores/missions'
import { Unit } from '@/unit/models/unit'
import { storeToRefs } from 'pinia'
import { ref, watch } from 'vue'
import type { Mission } from '@/mission/models/mission'
import type { ISubMissionStateChanged } from '@/mqtt/payloads/sub/SubMissionStateChanged'

const mqttClient = MqttJsClient.getInstance()
const subscribedTopic = ref<string>('')
export const useUnitAvailability = () => {
  const { updateMissionState, getMissionById } = useMissionsStore()
  const mission = ref<Mission>()

  /**
   * Listens for units that join / leave the mission, updating the units of the
   * current mission in the missions store.
   * Finally, such updates will lead to an update of the unit markers on the
   * situation map.
   * @param missionId The current mission's ID.
   */
  const _subscribe = (missionId: string) => {
    const relativeTopic = _getSubscriptionTopic(missionId)
    if (subscribedTopic.value === relativeTopic) {
      // topic of this mission is already subscribed
      return
    }
    mission.value = getMissionById(missionId)
    mqttClient.subscribe(relativeTopic, _handleUnitAvailabilityChange)
    subscribedTopic.value = relativeTopic
  }

  const _handleUnitAvailabilityChange = (
    _topic: string,
    payload: GenericMqttPayload
  ) => {
    const typedPayload = payload as ISubUnitAvailabilityChanged
    if (typedPayload.messageType === 'UnitAvailabilityChanged') {
      const { addUnit, removeUnitById, unitBelongsToCurrentMission } =
        useMissionsStore()
      const unit = Unit.fromSubUnitAvailabilityChanged(typedPayload)

      switch (typedPayload.unitAvailability) {
        case UnitAvailability.JOINED:
          // add unit to current mission
          if (!unit) {
            return
          }
          if (unitBelongsToCurrentMission(unit)) {
            // the unit has already been added (e.g. when we published the unit availability change ourselves)
            return
          }
          addUnit(unit)
          return

        case UnitAvailability.REMOVED:
          // remove unit from current mission
          removeUnitById(typedPayload.unitId)
          return
      }
    }
    const typedPayloadMissionState = payload as ISubMissionStateChanged
    if (typedPayloadMissionState.messageType === 'MissionStateChanged') {
      updateMissionState(typedPayloadMissionState.missionState, mission.value!)
    }
  }

  const publishUnitAvailabilityChanged = (
    availability: UnitAvailability,
    unitId: string,
    missionId: string,
    unitLocation?: GpsLocation
  ) => {
    const relativeTopic = _getPublishTopic(missionId)
    const payload = new PubUnitAvailabilityChanged(
      availability,
      unitId,
      unitLocation
    )
    mqttClient.publish(relativeTopic, payload)
  }

  const _getSubscriptionTopic = (missionId: string) => {
    return `missions/v1/outbound/${missionId}`
  }

  const _getPublishTopic = (missionId: string) => {
    return `missions/v1/inbound/${missionId}`
  }

  const _unsubscribe = (missionId: string) => {
    const topic = _getSubscriptionTopic(missionId)
    if (subscribedTopic.value !== topic) return
    mqttClient.unsubscribe(topic)
    subscribedTopic.value = ''
  }

  const { currentMission } = storeToRefs(useMissionsStore())
  watch(
    currentMission,
    (newMission, oldMission) => {
      if (oldMission?.missionId) {
        _unsubscribe(oldMission?.missionId)
      }
      if (newMission?.missionId) {
        _subscribe(newMission?.missionId)
      }
    },
    { immediate: true }
  )

  return {
    publishUnitAvailabilityChanged,
  }
}
