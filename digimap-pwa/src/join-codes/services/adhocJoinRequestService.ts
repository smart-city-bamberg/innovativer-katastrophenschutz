import { RestClient } from '@/base/services/restClient'
import type {
  ApiAdhocJoinRequest,
  ApiCreateAdhocJoinRequest,
  ApiWaitingRoomStatus,
} from '@/join-codes/models/adhocJoinRequest'
import { AdhocJoinRequest } from '@/join-codes/models/adhocJoinRequest'

export class AdhocJoinRequestService {
  private static readonly _client = RestClient.getInstance()

  public static async validateJoinCode(joinCode: string): Promise<string> {
    try {
      const response = await this._client
        .go(`/adhoc-join?joinCode=${joinCode}`)
        .get()
      return await response.data.missionNr
    } catch (error) {
      console.error('Error validating join code:', error)
      return ''
    }
  }

  public static async validateJoinPin(
    joinCode: string,
    joinPin: string
  ): Promise<string> {
    try {
      const response = await this._client
        .go(`/adhoc-join/validate?joinCode=${joinCode}&joinPin=${joinPin}`)
        .get()

      return response.data.missionNr
    } catch (error) {
      console.error('Error validating join pin:', error)
      return ''
    }
  }

  public static async getAllAdhocJoinRequests(): Promise<AdhocJoinRequest[]> {
    const adhocJoinRequests: AdhocJoinRequest[] = []
    const adhocJoinRequestsResources = await this._client
      .go<ApiAdhocJoinRequest[]>('/adhoc-join/waiting-room')
      .get()

    adhocJoinRequestsResources.data.map((resource) => {
      const adhocJoinRequest = AdhocJoinRequest.fromKettingState(resource)
      adhocJoinRequests.push(adhocJoinRequest)
    })

    console.log('Retrieved all adhocJoinRequests', adhocJoinRequests)
    return adhocJoinRequests
  }

  public static async getAdhocJoinRequestByMissionId(
    missionId: string
  ): Promise<AdhocJoinRequest[]> {
    const adhocJoinRequests: AdhocJoinRequest[] = []
    const adhocJoinRequestsResources = await this._client
      .go<ApiAdhocJoinRequest[]>(`/adhoc-join/waiting-room/${missionId}`)
      .get()

    adhocJoinRequestsResources.data.map((resource) => {
      const adhocJoinRequest = AdhocJoinRequest.fromKettingState(resource)
      adhocJoinRequests.push(adhocJoinRequest)
    })

    return adhocJoinRequests
  }

  public static async getWaitingRoomStatus(): Promise<ApiWaitingRoomStatus> {
    const response = await this._client
      .go('/adhoc-join/waiting-room/status')
      .get()

    return response.data
  }

  public static async createAdhocJoinRequest(
    joinRequest: ApiCreateAdhocJoinRequest
  ): Promise<AdhocJoinRequest | Error> {
    return this._client
      .go('/adhoc-join')
      .post({ data: joinRequest })
      .then((response) => {
        return AdhocJoinRequest.fromKettingState(response.data)
      })
      .catch(() => {
        const error = new Error(
          'Angemeldete Nutzer können keine Adhoc-Anfragen stellen'
        )
        error.name = 'PermissionError'
        throw error
      })
  }
  public static async approveAdhocJoinRequest(
    adhocJoinRequestId: string
  ): Promise<AdhocJoinRequest> {
    const response = await this._client
      .go<ApiAdhocJoinRequest>(`adhoc-join/${adhocJoinRequestId}/approve`)
      .patch({})

    if (response === undefined) {
      throw new Error('No response from server')
    }
    return AdhocJoinRequest.fromKettingState(response.data)
  }

  public static async rejectAdhocJoinRequest(
    adhocJoinRequestId: string
  ): Promise<AdhocJoinRequest> {
    const response = await this._client
      .go<ApiAdhocJoinRequest>(`adhoc-join/${adhocJoinRequestId}/reject`)
      .patch({})

    if (response === undefined) {
      throw new Error('No response from server')
    }
    return AdhocJoinRequest.fromKettingState(response.data)
  }
}
