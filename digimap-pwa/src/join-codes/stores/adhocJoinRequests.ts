import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import type {
  AdhocJoinRequest,
  ApiCreateAdhocJoinRequest,
} from '@/join-codes/models/adhocJoinRequest'
import { ApprovalStatus } from '@/join-codes/models/adhocJoinRequest'
import { AdhocJoinRequestService } from '@/join-codes/services/adhocJoinRequestService'

export const useAdhocJoinRequestStore = defineStore('adhocJoinRequest', () => {
  const allAdhocJoinRequests = ref<AdhocJoinRequest[]>([])
  const currentAdhocJoinRequests = ref<AdhocJoinRequest[]>([])
  const createdAdhocJoinRequest = ref<AdhocJoinRequest | undefined>()
  const pendingAdhocJoinRequests = computed(() =>
    currentAdhocJoinRequests.value.filter(
      (r) => r.status === ApprovalStatus.PENDING
    )
  )
  const busy = ref<boolean>(false)
  const errorMessage = ref<string>('')
  const notifications = computed(() => pendingAdhocJoinRequests.value.length)

  async function fetchAllAdhocJoinRequests() {
    try {
      busy.value = true
      errorMessage.value = ''
      allAdhocJoinRequests.value =
        await AdhocJoinRequestService.getAllAdhocJoinRequests()
    } catch (error: unknown) {
      errorMessage.value =
        'Einsätze konnten nicht abgefragt werden. Bitte versuchen Sie es erneut.'
      console.error('Could not fetch adhocJoinRequests:', error)
    } finally {
      busy.value = false
    }
  }

  async function fetchAdhocJoinRequestByMissionId(missionId: string) {
    try {
      busy.value = true
      errorMessage.value = ''
      currentAdhocJoinRequests.value =
        await AdhocJoinRequestService.getAdhocJoinRequestByMissionId(missionId)
    } catch (error: unknown) {
      errorMessage.value =
        'Einsätze konnten nicht abgefragt werden. Bitte versuchen Sie es erneut.'
      console.error('Could not fetch adhocJoinRequests:', error)
    } finally {
      busy.value = false
    }
  }

  async function createAdhocJoinRequests(
    apiAdhocJoinRequests: ApiCreateAdhocJoinRequest
  ) {
    const success =
      await AdhocJoinRequestService.createAdhocJoinRequest(apiAdhocJoinRequests)
    if (!(success instanceof Error)) {
      currentAdhocJoinRequests.value.push(success)
      return success
    } else {
      return undefined
    }
  }

  async function approveAdhocJoinRequest(adhocJoinRequestId: string) {
    const success =
      await AdhocJoinRequestService.approveAdhocJoinRequest(adhocJoinRequestId)
    if (success !== undefined) {
      updateAdhocJoinRequest(success)
      return success
    } else {
      return undefined
    }
  }

  async function rejectAdhocJoinRequest(adhocJoinRequestId: string) {
    const success =
      await AdhocJoinRequestService.rejectAdhocJoinRequest(adhocJoinRequestId)
    if (success !== undefined) {
      updateAdhocJoinRequest(success)
      return success
    } else {
      return undefined
    }
  }

  function addAdhocJoinRequest(adhocJoinRequest: AdhocJoinRequest) {
    currentAdhocJoinRequests.value.push(adhocJoinRequest)
  }

  function updateAdhocJoinRequest(adhocJoinRequest: AdhocJoinRequest) {
    const index = currentAdhocJoinRequests.value.findIndex(
      (r) => r.adhocJoinRequestId === adhocJoinRequest.adhocJoinRequestId
    )
    currentAdhocJoinRequests.value[index] = adhocJoinRequest
  }

  function hasAdhocJoinRequest(adhocJoinRequestId: string) {
    return currentAdhocJoinRequests.value.find(
      (r) => r.adhocJoinRequestId === adhocJoinRequestId
    )
  }

  function setCreatedAdhocJoinRequest(adhocJoinRequests: AdhocJoinRequest) {
    createdAdhocJoinRequest.value = adhocJoinRequests
  }

  return {
    busy,
    allAdhocJoinRequests,
    currentAdhocJoinRequests,
    createdAdhocJoinRequest,
    pendingAdhocJoinRequests,
    errorMessage,
    notifications,
    fetchAllAdhocJoinRequests,
    fetchAdhocJoinRequestByMissionId,
    createAdhocJoinRequests,
    approveAdhocJoinRequest,
    rejectAdhocJoinRequest,
    addAdhocJoinRequest,
    updateAdhocJoinRequest,
    hasAdhocJoinRequest,
    setCreatedAdhocJoinRequest,
  }
})
