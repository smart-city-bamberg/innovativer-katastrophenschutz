import { beforeEach, describe, expect, it, vi } from 'vitest'
import { MockData } from '@/assets/mocks/data'
import { createPinia, setActivePinia } from 'pinia'
import { useAdhocJoinRequestStore } from '@/join-codes/stores/adhocJoinRequests'
import { AdhocJoinRequestService } from '@/join-codes/services/adhocJoinRequestService'
import type { ApiCreateAdhocJoinRequest } from '@/join-codes/models/adhocJoinRequest'
import { ApprovalStatus } from '@/join-codes/models/adhocJoinRequest'

describe('useAdhocJoinRequestsStore', () => {
  const mockAdhocJoinRequests = MockData.getAdhocJoinRequests()

  beforeEach(() => {
    setActivePinia(createPinia())
  })

  describe('methods', () => {
    it('fetchAdhocJoinRequestByMissionId - calls service and sets allAdhocJoinRequests correctly', async () => {
      const store = useAdhocJoinRequestStore()
      const missionId = 'someMissionId' // replace with actual missionId
      AdhocJoinRequestService.getAdhocJoinRequestByMissionId = vi
        .fn()
        .mockImplementation(() => mockAdhocJoinRequests)

      await store.fetchAdhocJoinRequestByMissionId(missionId)

      expect(
        AdhocJoinRequestService.getAdhocJoinRequestByMissionId
      ).toHaveBeenCalledWith(missionId)
      expect(store.currentAdhocJoinRequests).toStrictEqual(
        mockAdhocJoinRequests
      )
    })

    it('createAdhocJoinRequests - calls service and adds adhocJoinRequest to allAdhocJoinRequests', async () => {
      const store = useAdhocJoinRequestStore()
      const apiCreateAdhocJoinRequest: ApiCreateAdhocJoinRequest = {
        userName: 'User1',
        unitCallSign: 'unit1',
        unitTacticalSignPath: 'Feuerwehr_Einheiten/Führungsgruppe_TEL.svg',
        departmentName: 'Feuerwehr 1',
        joinCode: 'joinCode',
        joinPin: 'joinPin',
      }
      const expectedAdhocJoinRequest = mockAdhocJoinRequests[0]
      AdhocJoinRequestService.createAdhocJoinRequest = vi
        .fn()
        .mockImplementation(() => Promise.resolve(expectedAdhocJoinRequest))

      const result = await store.createAdhocJoinRequests(
        apiCreateAdhocJoinRequest
      )

      expect(
        AdhocJoinRequestService.createAdhocJoinRequest
      ).toHaveBeenCalledWith(apiCreateAdhocJoinRequest)
      expect(result).toEqual(expectedAdhocJoinRequest)
      expect(store.currentAdhocJoinRequests).toContainEqual(
        expectedAdhocJoinRequest
      ) // Use toContainEqual instead of toContain
    })

    it('approveAdhocJoinRequest - calls service and updates adhocJoinRequest status', async () => {
      const store = useAdhocJoinRequestStore()
      store.currentAdhocJoinRequests = mockAdhocJoinRequests
      const expectedAdhocJoinRequest = {
        ...mockAdhocJoinRequests[0],
        status: ApprovalStatus.APPROVED,
      }
      AdhocJoinRequestService.approveAdhocJoinRequest = vi
        .fn()
        .mockImplementation(() => Promise.resolve(expectedAdhocJoinRequest))

      const result = await store.approveAdhocJoinRequest(
        expectedAdhocJoinRequest.adhocJoinRequestId
      )

      expect(
        AdhocJoinRequestService.approveAdhocJoinRequest
      ).toHaveBeenCalledOnce()
      expect(result).toEqual(expectedAdhocJoinRequest)
      expect(
        store.currentAdhocJoinRequests.find(
          (request) =>
            request.adhocJoinRequestId ===
            expectedAdhocJoinRequest.adhocJoinRequestId
        )
      ).toEqual(expectedAdhocJoinRequest)
    })

    it('rejectAdhocJoinRequest - calls service and updates adhocJoinRequest status', async () => {
      const store = useAdhocJoinRequestStore()
      store.currentAdhocJoinRequests = mockAdhocJoinRequests
      const expectedAdhocJoinRequest = {
        ...mockAdhocJoinRequests[0],
        status: ApprovalStatus.REJECTED,
      }
      AdhocJoinRequestService.rejectAdhocJoinRequest = vi
        .fn()
        .mockImplementation(() => Promise.resolve(expectedAdhocJoinRequest))

      const result = await store.rejectAdhocJoinRequest(
        expectedAdhocJoinRequest.adhocJoinRequestId
      )

      expect(
        AdhocJoinRequestService.rejectAdhocJoinRequest
      ).toHaveBeenCalledOnce()
      expect(result).toEqual(expectedAdhocJoinRequest)
      expect(
        store.currentAdhocJoinRequests.find(
          (request) =>
            request.adhocJoinRequestId ===
            expectedAdhocJoinRequest.adhocJoinRequestId
        )
      ).toEqual(expectedAdhocJoinRequest)
    })
  })
})
