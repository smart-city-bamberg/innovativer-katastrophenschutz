import { defineStore } from 'pinia'
import { ref } from 'vue'
import { object, string } from 'yup'
import { TacticalSign } from '@/base/models/tacticalSign'
import type { ApiCreateAdhocJoinRequest } from '@/join-codes/models/adhocJoinRequest'
import { AdhocJoinRequest } from '@/join-codes/models/adhocJoinRequest'
import { AdhocJoinRequestService } from '@/join-codes/services/adhocJoinRequestService'

export interface CreateAdhocJoinRequestForm {
  name: string
  organization: string
  callSign: string
  tacticalSignPath: string
}

export const useCreateAdhocJoinRequestStore = defineStore(
  'create-adhoc-join-request',
  () => {
    const name = ref<string>()
    const organization = ref<string>()
    const callSign = ref<string>()
    const tacticalSign = ref<TacticalSign>()
    const formErrorMessage = ref<string>()

    const schema = object({
      name: string().required('Bitte geben Sie Ihren Vor- & Nachnamen an'),
      organization: string().required('Bitte geben Sie Ihre Dienststelle an'),
      callSign: string().required('Bitte geben Sie Ihren Funkrufnamen an'),
      tacticalSignPath: string().required(
        'Bitte wählen Sie ein Taktisches Zeichen'
      ),
    })
    const initialValues = {
      name: '',
      organization: '',
      callSign: '',
      tacticalSignPath: '',
    }

    const publishAdhocJoinRequest = async (
      formValues: CreateAdhocJoinRequestForm,
      joinCode: string,
      joinPin: string
    ): Promise<AdhocJoinRequest | Error> => {
      tacticalSign.value = TacticalSign.withPrefix(formValues.tacticalSignPath)
      if (!tacticalSign.value) {
        formErrorMessage.value =
          'Das taktische Zeichen konnte nicht gefunden werden'
        return Promise.reject(
          new Error('Das taktische Zeichen konnte nicht gefunden werden')
        )
      }

      const apiJoinRequest: ApiCreateAdhocJoinRequest = {
        unitCallSign: formValues.callSign,
        unitTacticalSignPath: tacticalSign.value.relativePath,
        departmentName: formValues.organization,
        userName: formValues.name,
        joinCode: joinCode.match(/.{1,4}/g)!.join('-'),
        joinPin: joinPin,
      }

      return await AdhocJoinRequestService.createAdhocJoinRequest(
        apiJoinRequest
      )
    }

    const resetFormErrors = () => {
      formErrorMessage.value = ''
    }

    return {
      formSchema: schema,
      initialValues,
      name,
      organization,
      callSign,
      tacticalSign,
      formErrorMessage,
      resetFormErrors,
      publishAdhocJoinRequest,
    }
  }
)
