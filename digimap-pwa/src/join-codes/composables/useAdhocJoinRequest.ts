import { MqttJsClient } from '@/mqtt/mqttClient'
import type { GenericMqttPayload } from '@/mqtt/iMqttClient'
import { useMissionsStore } from '@/mission/stores/missions'
import { storeToRefs } from 'pinia'
import { ref, watch } from 'vue'
import type { ApiMqttAdhocJoinRequest } from '@/join-codes/models/adhocJoinRequest'
import { AdhocJoinRequest } from '@/join-codes/models/adhocJoinRequest'
import { useAdhocJoinRequestStore } from '@/join-codes/stores/adhocJoinRequests'

const mqttClient = MqttJsClient.getInstance()
const subscribedTopic = ref<string>('')
export const useAdhocJoinRequest = () => {
  /**
   * Listens for changes at adhocJoinRequests
   * @param missionId The current mission's ID.
   */
  const _subscribe = (missionId: string) => {
    const relativeTopic = _getSubscriptionTopic(missionId)
    if (subscribedTopic.value === relativeTopic) {
      // topic of this mission is already subscribed
      return
    }
    mqttClient.subscribe(relativeTopic, _handleAdhocJoinRequests)
    subscribedTopic.value = relativeTopic
  }

  const _handleAdhocJoinRequests = (
    _topic: string,
    payload: GenericMqttPayload
  ) => {
    console.log('Received adhoc join request', payload)
    const typedPayload = payload as ApiMqttAdhocJoinRequest
    const joinRequest = AdhocJoinRequest.fromMqttPayload(typedPayload)
    const {
      addAdhocJoinRequest,
      updateAdhocJoinRequest,
      hasAdhocJoinRequest,
      createdAdhocJoinRequest,
      setCreatedAdhocJoinRequest,
    } = useAdhocJoinRequestStore()

    if (
      createdAdhocJoinRequest?.adhocJoinRequestId ===
      joinRequest.adhocJoinRequestId
    ) {
      setCreatedAdhocJoinRequest(joinRequest)
    }
    if (hasAdhocJoinRequest(joinRequest.adhocJoinRequestId)) {
      updateAdhocJoinRequest(joinRequest)
    } else {
      addAdhocJoinRequest(joinRequest)
    }
  }

  const _getSubscriptionTopic = (missionId: string) => {
    return `adhoc-join-requests/v1/outbound/${missionId}`
  }

  const _unsubscribe = (missionId: string) => {
    const topic = _getSubscriptionTopic(missionId)
    if (subscribedTopic.value !== topic) return
    mqttClient.unsubscribe(topic)
    subscribedTopic.value = ''
  }

  const { currentMission } = storeToRefs(useMissionsStore())
  watch(
    currentMission,
    (newMission, oldMission) => {
      if (oldMission?.missionId) {
        _unsubscribe(oldMission?.missionId)
      }
      if (newMission?.missionId) {
        _subscribe(newMission?.missionId)
      }
    },
    { immediate: true }
  )
}
