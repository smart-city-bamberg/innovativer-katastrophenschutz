import type { Unit } from '@/unit/models/unit'
import { Mission } from '@/mission/models/mission'
import { GpsLocation } from '@lion5/component-library-leaflet'
import { Address } from '@/base/models/address'
import { MissionState } from '@/mission/models/missionState'

export class AdhocJoinRequest {
  /*
   * The id of the adhoc join request.
   */
  readonly adhocJoinRequestId: string
  /*
   * The firebase id of the user who requested to join the mission.
   */
  readonly userFirebaseId: string
  /*
   * The username of the requesting user.
   */
  readonly userName: string
  /*
   * The name of the unit the requesting user.
   */
  readonly unitCallSign: string
  /*
   * The path to the tactical sign of the unit.
   */
  readonly unitTacticalSignPath: string
  /*
   *The name of the department of the requesting user.
   */
  readonly departmentName: string
  /*
   * The mission the user wants to join.
   */
  readonly mission: Mission | null
  /*
   * The status of the request.
   */
  readonly status: ApprovalStatus
  /*
   * The time the request was approved.
   */
  readonly approvalTime: Date | null
  /*
   * The unit that was created for the user.
   */
  readonly createdUnit: Unit | null

  constructor(
    adhocJoinRequestId: string,
    userFirebaseId: string,
    userName: string,
    unitCallSign: string,
    unitTacticalSignPath: string,
    departmentName: string,
    mission: Mission | null,
    status: ApprovalStatus,
    approvalTime: Date | null,
    createdUnit: Unit | null
  ) {
    this.adhocJoinRequestId = adhocJoinRequestId
    this.userFirebaseId = userFirebaseId
    this.userName = userName
    this.unitCallSign = unitCallSign
    this.unitTacticalSignPath = unitTacticalSignPath
    this.departmentName = departmentName
    this.mission = mission
    this.status = status
    this.approvalTime = approvalTime
    this.createdUnit = createdUnit
  }

  static fromKettingState(state: ApiAdhocJoinRequest) {
    return new AdhocJoinRequest(
      state.adhocJoinRequestId,
      state.userFirebaseId,
      state.userName,
      state.unitCallSign,
      state.unitTacticalSignPath,
      state.departmentName,
      state.mission as Mission,
      ApprovalStatus[state.status as ApprovalStatus],
      state.approvalTime,
      state.createdUnit
    )
  }

  static fromMqttPayload(payload: ApiMqttAdhocJoinRequest) {
    return new AdhocJoinRequest(
      payload.adhocJoinRequestId,
      payload.userFirebaseId,
      payload.userName,
      payload.unitCallSign,
      payload.unitTacticalSignPath,
      payload.departmentName,
      new Mission(
        payload.missionId,
        payload.missionNr,
        new GpsLocation(0, 0),
        new Address('', '', 0, ''),
        new Date(),
        '',
        [],
        MissionState.RUNNING,
        [],
        [],
        [],
        [],
        [],
        ''
      ),
      payload.status,
      null,
      null
    )
  }
}

export enum ApprovalStatus {
  PENDING = 'PENDING',
  APPROVED = 'APPROVED',
  REJECTED = 'REJECTED',
}

export interface ApiCreateAdhocJoinRequest {
  unitCallSign: string
  unitTacticalSignPath: string
  departmentName: string
  userName: string
  joinCode: string
  joinPin: string
}

export interface ApiAdhocJoinRequest {
  adhocJoinRequestId: string
  userFirebaseId: string
  userName: string
  unitCallSign: string
  unitTacticalSignPath: string
  departmentName: string
  mission: Mission
  status: string
  approvalTime: Date
  createdUnit: Unit | null
}

export interface ApiMqttAdhocJoinRequest {
  adhocJoinRequestId: string
  departmentName: string
  missionId: string
  missionNr: string
  status: ApprovalStatus
  unitCallSign: string
  unitTacticalSignPath: string
  userFirebaseId: string
  userName: string
}

export interface ApiWaitingRoomStatus {
  missionId: string
  adhocJoinRequestId: string
  status: ApprovalStatus
}
