import { mount } from '@vue/test-utils'
import { afterEach, beforeEach, describe, expect, it } from 'vitest'
import type { defineComponent } from 'vue'
import { MockData } from '@/assets/mocks/data'
import AdhocJoinRequestCard from '@/join-codes/components/AdhocJoinRequestCard.vue'

const adhocJoinRequests = MockData.getAdhocJoinRequests()

describe('AdhocJoinRequestCard', () => {
  let wrapper: ReturnType<typeof defineComponent>

  beforeEach(() => {
    wrapper = mount(AdhocJoinRequestCard, {
      props: {
        request: adhocJoinRequests[0],
      },
    })
  })

  afterEach(() => {
    wrapper.unmount()
  })

  it('should get request from WaitingRoom', () => {
    expect(wrapper.find('.call-sign').text()).toBe(
      adhocJoinRequests[0].unitCallSign
    )

    expect(wrapper.find('.content-text').text()).toBe(
      'User1 möchte dem Einsatz B 4.1 2108XX XXX beitreten.'
    )
  })

  it('should emit the rejection', async () => {
    const button = wrapper.find('.reject-button')
    await button.trigger('click')

    expect(wrapper.emitted()).toHaveProperty('reject')
    expect(wrapper.emitted().reject[0]).toEqual([
      adhocJoinRequests[0].adhocJoinRequestId,
    ])
  })

  it('should emit the acceptation', async () => {
    const button = wrapper.find('.accept-button')
    await button.trigger('click')

    expect(wrapper.emitted()).toHaveProperty('accept')
    expect(wrapper.emitted().accept[0]).toEqual([
      adhocJoinRequests[0].adhocJoinRequestId,
    ])
  })
})
