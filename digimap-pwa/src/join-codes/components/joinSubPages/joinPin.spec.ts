import { afterEach, beforeEach, describe, expect, it } from 'vitest'
import type { defineComponent } from 'vue'
import { mount } from '@vue/test-utils'
import JoinPin from '@/join-codes/components/joinSubPages/JoinPin.vue'

describe('JoinPin', () => {
  let wrapper: ReturnType<typeof defineComponent>

  beforeEach(() => {
    wrapper = mount(JoinPin, {
      props: {
        missionNr: 'Mission1',
        pin: '123456',
        'onUpdate:code': (e: string) => wrapper.setProps({ pin: e }),
        error: '$',
        'onUpdate:error': (e: string) => wrapper.setProps({ error: e }),
      },
    })
  })

  afterEach(() => {
    wrapper.unmount()
  })

  it('should get the missionNr from props', async () => {
    const missionNr = wrapper.find('p.mission')
    expect(missionNr.text()).toBe('Mission1')
  })

  it('should emit nextPage if button is clicked', async () => {
    const button = wrapper.find('.button')
    await button.trigger('click')
    expect(wrapper.emitted()).toHaveProperty('nextPage')
  })

  it('should emit previousPage if button is clicked', async () => {
    const button = wrapper.find('.icon-button')
    await button.trigger('click')
    expect(wrapper.emitted()).toHaveProperty('previousPage')
  })
})
