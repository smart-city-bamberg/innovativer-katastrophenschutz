import { afterEach, beforeEach, describe, expect, it } from 'vitest'
import type { defineComponent } from 'vue'
import { mount } from '@vue/test-utils'
import JoinCode from '@/join-codes/components/joinSubPages/JoinCode.vue'

describe('joinCode', () => {
  let wrapper: ReturnType<typeof defineComponent>

  beforeEach(() => {
    wrapper = mount(JoinCode, {
      props: {
        code: 'ABCDEFGH',
        'onUpdate:code': (e: string) => wrapper.setProps({ code: e }),
        error: '$',
        'onUpdate:error': (e: string) => wrapper.setProps({ error: e }),
      },
    })
  })

  afterEach(() => {
    wrapper.unmount()
  })

  it('should emit nextPage if button is clicked', async () => {
    const button = wrapper.find('.button')
    await button.trigger('click')
    expect(wrapper.emitted()).toHaveProperty('nextPage')
  })
})
