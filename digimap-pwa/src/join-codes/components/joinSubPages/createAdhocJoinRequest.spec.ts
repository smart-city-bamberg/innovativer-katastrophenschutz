import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest'
import { flushPromises, mount } from '@vue/test-utils'
import type { defineComponent } from 'vue'
import { createTestingPinia } from '@pinia/testing'
import waitForExpect from 'wait-for-expect'
import { MockData } from '@/assets/mocks/data'
import { useCreateAdhocJoinRequestStore } from '@/join-codes/stores/createAdhocJoinRequest'
import CreateAdhocJoinRequest from '@/join-codes/components/joinSubPages/CreateAdhocJoinRequest.vue'
import { TacticalSign } from '@/base/models/tacticalSign'
import TacticalSignSelect from '@/base/components/input/TacticalSignSelect/TacticalSignSelect.vue'

vi.mock('@vue/router', () => ({
  useRouter: vi.fn().mockReturnValue({ push: vi.fn() }),
}))

describe('CreateAdhocJoinRequest', () => {
  let wrapper: ReturnType<typeof defineComponent>
  const adhocJoinRequests = MockData.getAdhocJoinRequests()
  const pinia = createTestingPinia()
  const createAdhocJoinRequestStore = useCreateAdhocJoinRequestStore()
  const publishAdhocJoinRequestSpy = vi
    .spyOn(createAdhocJoinRequestStore, 'publishAdhocJoinRequest')
    .mockReturnValue(Promise.resolve(adhocJoinRequests[0]))

  beforeEach(() => {
    wrapper = mount(CreateAdhocJoinRequest, {
      global: {
        plugins: [pinia],
        stubs: {
          UnitsTable: true,
          LeafletMap: true,
        },
      },
      props: {
        code: 'joinCode',
        pin: 'joinPin',
        tacticalSignOptions: [
          new TacticalSign('Feuerwehr_Einheiten/Führungsgruppe_TEL.svg'),
        ],
      },
    })
  })

  afterEach(() => {
    wrapper.unmount()
  })

  it('should render the form', () => {
    const form = wrapper.find('form')
    expect(form.exists()).toBe(true)
  })

  it('should render the input fields', () => {
    const inputs = wrapper.findAll('input')
    expect(inputs.length).toBe(4)
  })

  it('should get the pin, code and tacticalSignOptions as props', () => {
    expect(wrapper.vm.$props.code).toBe('joinCode')
    expect(wrapper.vm.$props.pin).toBe('joinPin')
    expect(wrapper.vm.$props.tacticalSignOptions).toStrictEqual([
      new TacticalSign('Feuerwehr_Einheiten/Führungsgruppe_TEL.svg'),
    ])
  })

  //TODO fix this test (tacticalSign)
  it.skip('should call publishAdhocJoinRequest when the form is submitted', async () => {
    await wrapper.find('input[name="name"]').setValue('User1')
    await wrapper.find('input[name="organization"]').setValue('Feuerwehr 1')
    await wrapper.find('input[name="callSign"]').setValue('unit1')

    await wrapper
      .findComponent(TacticalSignSelect)
      .vm.$emit(
        'update:modelValue',
        'Feuerwehr_Einheiten/Führungsgruppe_TEL.svg'
      )

    await wrapper.find('button[type="submit"]').trigger('submit')
    await flushPromises()

    await waitForExpect(() => {
      expect(publishAdhocJoinRequestSpy).toHaveBeenCalled()
      expect(wrapper.emitted()).toHaveProperty('updateJoinRequest')
    })
  })
})
