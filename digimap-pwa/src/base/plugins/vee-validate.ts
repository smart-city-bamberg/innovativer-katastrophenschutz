import { configure, defineRule } from 'vee-validate'
import { email, required } from '@vee-validate/rules'
import { localize, setLocale } from '@vee-validate/i18n'

export default {
  install: () => {
    defineRule('required', required)
    defineRule('email', email)

    configure({
      generateMessage: localize('de', {
        messages: {
          required: 'Dieses Feld ist ein Pflichtfeld.',
          email: 'Bitte geben Sie eine korrekte E-Mail-Adresse an.',
        },
      }),
    })

    setLocale('de')
  },
}
