export enum TacticalSignType {
  DANGER = 'Gefahren',
  DAMAGE = 'Schäden',
  FACILITY = 'Einrichtungen',
  MANAGEMENT_POSITION = 'Führungsstellen',
  PATIENTS = 'Patienten',
  UNITS = 'Einheiten',
  VEHICLES = 'Fahrzeuge',
  PERSONS = 'Personen',
}
