export class Address {
  readonly street: string
  readonly houseNumber: string
  readonly zipCode: number
  /**
   * Locality with own addresses, such as a city or town
   */
  readonly locality: string

  constructor(
    street: string,
    houseNumber: string,
    zipCode: number,
    locality: string
  ) {
    this.street = street
    this.houseNumber = houseNumber
    this.zipCode = zipCode
    this.locality = locality
  }

  toLocalString() {
    return `${this.street} ${this.houseNumber}, ${this.zipCode} ${this.locality}`
  }

  public static fromApi(data: ApiAddress) {
    return new Address(
      data.street,
      data.houseNumber,
      data.zipCode,
      data.locality
    )
  }
}

export interface ApiAddress {
  street: string
  houseNumber: string
  zipCode: number
  locality: string
}
