import { mount, VueWrapper } from '@vue/test-utils'
import { afterEach, beforeEach, describe, expect, it } from 'vitest'
import TacticalSignSelect from '@/base/components/input/TacticalSignSelect/TacticalSignSelect.vue'
import { TacticalSign } from '@/base/models/tacticalSign'
import { TacticalSignType } from '@/base/models/tacticalSignType'
import { SelectOption } from '@lion5/component-library'
import Multiselect from 'vue-multiselect'

describe('TacticalSignSelect', () => {
  let wrapper: ReturnType<typeof mountComponent>

  const options = [
    new TacticalSign(TacticalSignType.DANGER + '/Gefahr.svg'),
    new TacticalSign(TacticalSignType.DANGER + '/Vollbrand.svg'),
  ]

  const selectOptions = [
    new SelectOption(options[0].path, options[0].name),
    new SelectOption(options[1].path, options[1].name),
  ]

  const mountComponent = () => {
    return mount(TacticalSignSelect, {
      attachTo: document.body,
      props: {
        name: 'tactical-sign',
        options,
      },
    })
  }

  beforeEach(() => {
    wrapper = mountComponent()
  })

  afterEach(() => {
    wrapper.unmount()
  })

  describe(':props', () => {
    it(':name - is applied to BaseSelect', async () => {
      const expected = 'expected'
      await wrapper.setProps({ name: expected })

      const selectInput = wrapper.findComponent(
        Multiselect as unknown as VueWrapper<typeof Multiselect>
      )
      expect(selectInput.vm.name).toBe(expected)
    })

    it(':options - are passed as SelectOptions to BaseSelect', async () => {
      await wrapper.setProps({ options })

      const selectInput = wrapper.findComponent(
        Multiselect as unknown as VueWrapper<typeof Multiselect>
      )
      expect(selectInput.vm.options).toStrictEqual(selectOptions)
    })
  })
})
