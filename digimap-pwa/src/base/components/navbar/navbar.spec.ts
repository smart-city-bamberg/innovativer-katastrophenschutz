import { afterEach, beforeEach, describe, expect, it } from 'vitest'
import { mount } from '@vue/test-utils'
import type { defineComponent } from 'vue'
import { createTestingPinia } from '@pinia/testing'
import NavBar from '@/base/components/navbar/NavBar.vue'
import { MenuBarItem } from '@/base/components/navbar/menuBarItem'
import { IconDashboard, IconGear, IconMap } from '@lion5/component-library'
import TaskIcon from '@/base/components/navbar/TaskIcon.vue'
import MissionIcon from '@/base/components/navbar/MissionIcon.vue'
import { createRouter, createWebHistory } from 'vue-router'

describe('NavBar', () => {
  let wrapper: ReturnType<typeof defineComponent>
  const pinia = createTestingPinia({})
  const router = createRouter({
    history: createWebHistory(),
    routes: [],
  })

  const menuBarItems = [
    new MenuBarItem('Lagekarte', '/', IconMap, []),
    new MenuBarItem('Aufgaben', '/commands', TaskIcon, []),
    new MenuBarItem('Dashboard', '/dashboard', IconDashboard, []),
    new MenuBarItem('Einsätze', '/missions', MissionIcon, []),
    new MenuBarItem('Einstellungen', '/settings', IconGear, []),
  ]

  beforeEach(() => {
    wrapper = mount(NavBar, {
      props: {
        menuBarItems: menuBarItems,
      },
      global: {
        plugins: [pinia, router],
      },
    })
  })

  afterEach(() => {
    wrapper.unmount()
  })

  it('displays renders the NavBar correctly', () => {
    console.log(wrapper.html())
    const menuItems = wrapper.findAll('.NavigationMenuTrigger')
    expect(menuItems.length).toBe(5)

    expect(menuItems[0].text()).toBe('Lagekarte')
    expect(menuItems[1].text()).toBe('Aufgaben')
    expect(menuItems[2].text()).toBe('Dashboard')
    expect(menuItems[3].text()).toBe('Einsätze')
    expect(menuItems[4].text()).toBe('Einstellungen')
  })
})
