import type { RouteLocationRaw } from 'vue-router'
import type { Ref } from 'vue'
import { defineComponent, markRaw } from 'vue'

export class ChildMenuBarItem {
  readonly label: string
  readonly to: RouteLocationRaw
  readonly icon?: ReturnType<typeof defineComponent>
  readonly notifications: Ref<number> | undefined

  constructor(
    label: string,
    to: RouteLocationRaw,
    icon: ReturnType<typeof defineComponent>,
    notifications?: Ref<number>
  ) {
    this.label = label
    this.to = to
    this.icon = markRaw(icon)
    this.notifications = notifications
  }
}

export class MenuBarItem {
  readonly label: string
  readonly to: RouteLocationRaw
  readonly icon?: ReturnType<typeof defineComponent>
  readonly children: ChildMenuBarItem[]

  constructor(
    label: string,
    to: RouteLocationRaw,
    icon: ReturnType<typeof defineComponent>,
    children: ChildMenuBarItem[]
  ) {
    this.label = label
    this.icon = markRaw(icon)
    this.to = to
    this.children = children
  }
}
