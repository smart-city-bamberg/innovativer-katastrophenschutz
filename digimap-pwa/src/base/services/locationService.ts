import { GpsLocation } from '@lion5/component-library-leaflet'

export class LocationService {
  public static buildUrl(
    street: string | undefined,
    houseNumber: string | undefined,
    city: string | undefined,
    postalCode: number | undefined
  ) {
    return `https://nominatim.openstreetmap.org/search.php?format=jsonv2${
      street && street !== '' ? `&street=${street}` : ''
    }${houseNumber && houseNumber !== '' ? `+${houseNumber}` : ''}${
      city && city !== '' ? `&city=${city}` : ''
    }${postalCode ? `&postalcode=${postalCode}` : ''}`
  }
  public static async getLocation(
    street: string | undefined,
    houseNumber: string | undefined,
    postalCode: number | undefined,
    city: string | undefined
  ) {
    const url = this.buildUrl(street, houseNumber, city, postalCode)
    const response = await fetch(url)
    const data = await response.json()
    if (data.length > 0) {
      return new GpsLocation(data[0].lat, data[0].lon)
    }
  }
}
