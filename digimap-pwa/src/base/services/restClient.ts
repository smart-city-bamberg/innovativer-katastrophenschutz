import Client, { bearerAuth } from 'ketting'

export class RestClient extends Client {
  private static readonly BASE_URL = import.meta.env
    .VITE_APP_DIGIMAP_API_BASE_URL
  private static instance: RestClient

  private constructor(bookmarkUri: string) {
    super(bookmarkUri)
  }

  public static getInstance(): RestClient {
    if (!RestClient.instance) {
      RestClient.instance = new RestClient(this.BASE_URL)
    }

    return RestClient.instance
  }

  public static useBearerToken(bearerToken: string) {
    this.getInstance().use(bearerAuth(bearerToken))
  }
}
