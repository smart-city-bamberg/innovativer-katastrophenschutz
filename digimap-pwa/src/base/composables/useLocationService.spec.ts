import { beforeEach, describe, expect, it, vi } from 'vitest'
import { createPinia, setActivePinia } from 'pinia'
import { useMissionsStore } from '@/mission/stores/missions'
import { MockData } from '@/assets/mocks/data'
import { useLocationService } from '@/base/composables/useLocationService'
import { SubLocationUpdate } from '@/mqtt/payloads/sub/SubLocationUpdate'
import { GpsLocation } from '@lion5/component-library-leaflet'

describe('useLocationService', () => {
  describe('handleInboundLocationUpdates', () => {
    let locationService: ReturnType<typeof useLocationService>

    beforeEach(() => {
      setActivePinia(createPinia())
      locationService = useLocationService()
    })

    it('updates gpsLocations of units correctly', () => {
      const missionStore = useMissionsStore()
      const mission = MockData.getMissions()[0]
      const units = mission.units
      missionStore.currentMission = mission
      locationService.unprocessedUpdates.push(
        new SubLocationUpdate(
          1,
          units[0].id,
          new GpsLocation(0.0, 0.0),
          new Date()
        ),
        new SubLocationUpdate(
          2,
          units[1].id,
          new GpsLocation(0.0, 0.0),
          new Date()
        )
      )

      locationService.handleInboundLocationUpdates()

      const resultUnits = missionStore.currentMission.units
      expect(resultUnits[0].gpsLocation.latitude).toBe(0.0)
      expect(resultUnits[0].gpsLocation.longitude).toBe(0.0)
      expect(resultUnits[1].gpsLocation.latitude).toBe(0.0)
      expect(resultUnits[1].gpsLocation.longitude).toBe(0.0)

      locationService.unprocessedUpdates.push(
        new SubLocationUpdate(
          1,
          units[0].id,
          new GpsLocation(10.0, 20.0),
          new Date()
        ),
        new SubLocationUpdate(
          2,
          units[1].id,
          new GpsLocation(20.0, 30.0),
          new Date()
        )
      )

      locationService.handleInboundLocationUpdates()

      const resultUnits2 = missionStore.currentMission.units
      expect(resultUnits2[0].gpsLocation.latitude).toBe(10.0)
      expect(resultUnits2[0].gpsLocation.longitude).toBe(20.0)
      expect(resultUnits2[1].gpsLocation.latitude).toBe(20.0)
      expect(resultUnits2[1].gpsLocation.longitude).toBe(30.0)
    })

    it('skips updates that do not belong to any unit', () => {
      const missionStore = useMissionsStore()
      const mission = MockData.getMissions()[0]
      const units = mission.units
      missionStore.currentMission = mission
      locationService.unprocessedUpdates.push(
        new SubLocationUpdate(
          1,
          units[0].id,
          new GpsLocation(1.0, 2.0),
          new Date()
        ),
        new SubLocationUpdate(
          2,
          units[1].id,
          new GpsLocation(3.0, 4.0),
          new Date()
        ),
        new SubLocationUpdate(
          3,
          'unknownId',
          new GpsLocation(90.0, 90.0),
          new Date()
        )
      )

      locationService.handleInboundLocationUpdates()

      const resultUnits = missionStore.currentMission.units
      expect(resultUnits[0].gpsLocation.latitude).toBe(1.0)
      expect(resultUnits[0].gpsLocation.longitude).toBe(2.0)
      expect(resultUnits[1].gpsLocation.latitude).toBe(3.0)
      expect(resultUnits[1].gpsLocation.longitude).toBe(4.0)
    })

    it('skips execution if no units are present', () => {
      const missionStore = useMissionsStore()
      const mission = MockData.getMissions()[0]
      mission.setUnits([])
      missionStore.currentMission = mission
      const spy = vi.spyOn(missionStore, 'setUnitsOfCurrentMission')
      mission.units.shift = vi.fn()

      locationService.handleInboundLocationUpdates()

      expect(spy).not.toHaveBeenCalled()
      expect(mission.units.shift).not.toHaveBeenCalled()
    })
  })
})
