import { useFirebaseStorage as useComponentLibFirebaseStorage } from '@lion5/component-library-firebase'

export function useFirebaseStorage() {
  const { uploadFile, getImageUrl } = useComponentLibFirebaseStorage()

  const uploadReportImage = async (
    file: File,
    missionId: string,
    reportId: string
  ): Promise<string> => {
    const path = `/${missionId}/reports/${reportId}`
    console.log('Uploading file to firebase storage', file, path)
    return await uploadFile(file, path)
  }

  return { getImageUrl, uploadReportImage }
}
