import { MqttJsClient } from '@/mqtt/mqttClient'
import { PubLocationUpdate } from '@/mqtt/payloads/pub/PubLocationUpdate'
import { SubLocationUpdate } from '@/mqtt/payloads/sub/SubLocationUpdate'
import type { GenericMqttPayload } from '@/mqtt/iMqttClient'
import { useMissionsStore } from '@/mission/stores/missions'
import {
  PermissionState,
  useBrowserPermissions,
} from '@lion5/component-library'
import { GpsLocation } from '@lion5/component-library-leaflet'
import { ref, watch } from 'vue'
import { storeToRefs } from 'pinia'

// global variables to prevent re-initialization
const subscribedTopic = ref<string>('')
const mostRecentEtag = 0
let sendLocationUpdateThrottled = false
const unprocessedUpdates: SubLocationUpdate[] = []
const HANDLE_LOCATION_UPDATES_DELAY_MS = 10_000
const SEND_LOCATION_UPDATE_DEBOUNCE_MS = 5_000
const currentLocation = ref<GpsLocation>()
const mqttClient: MqttJsClient = MqttJsClient.getInstance()

export function useLocationService() {
  const getCurrentUserPosition = async (): Promise<GpsLocation> => {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        (position: GeolocationPosition) => {
          const location = GpsLocation.fromGeolocationPosition(position)
          currentLocation.value = location
          resolve(location)
        },
        (positionError) => {
          handleLocationError(positionError)
          reject(positionError)
        }
      )
    })
  }

  const startContinuousUnitLocationUpdates = (unitId: string) => {
    if (!unitId || !mqttClient) {
      return
    }
    if (!navigator.geolocation) {
      console.warn('Could not access geolocation: missing browser support')
      return
    }

    navigator.geolocation.watchPosition((position: GeolocationPosition) => {
      handleOutboundLocationUpdate(position, unitId)
    }, handleLocationError)
  }

  const subscribeToMissionUnitLocationUpdates = (missionId: string) => {
    const relativeTopic = _getSubscriptionTopic(missionId)
    if (subscribedTopic.value === relativeTopic) {
      // topic of this mission is already subscribed
      return
    }

    mqttClient.subscribe(
      relativeTopic,
      (_topic, payload: GenericMqttPayload) => {
        const locationUpdate = SubLocationUpdate.fromMqtt(payload)
        if (locationUpdate.eTag <= mostRecentEtag) {
          console.log('Received old location update, will be ignored', {
            locationUpdate,
            mostRecentEtag,
          })
          return
        }
        // we buffer inbound location updates to reduce the number of writes to the
        // units in the store, so that the unit-markers do not need to be re-drawn
        // for every incoming message, but can be processed batch-wise
        unprocessedUpdates.push(locationUpdate)
      }
    )
    subscribedTopic.value = relativeTopic

    setInterval(() => {
      handleInboundLocationUpdates()
    }, HANDLE_LOCATION_UPDATES_DELAY_MS)
  }

  const handleInboundLocationUpdates = () => {
    const { setUnitsOfCurrentMission } = useMissionsStore()
    const units = currentMission.value?.units
    if (!units) {
      return
    }
    // process all buffered location updates (in FIFO order)
    while (unprocessedUpdates.length >= 0) {
      const locationUpdate = unprocessedUpdates.shift()
      if (!locationUpdate) {
        return
      }
      const unit = units.find((unit) => unit.id === locationUpdate.unitId)
      if (!unit) {
        return
      }
      if (unit.gpsLocation.equals(locationUpdate.location)) {
        // ignore redundant location updates
        continue
      }
      unit.gpsLocation = locationUpdate.location
      console.log('Updated unit location', {
        unit,
        locationUpdate,
      })
    }
    setUnitsOfCurrentMission(units)
  }

  const handleOutboundLocationUpdate = (
    position: GeolocationPosition,
    unitId: string
  ) => {
    const { locationPermissionState } = useBrowserPermissions()
    locationPermissionState.value = PermissionState.GRANTED

    if (import.meta.env.DEV) {
      // only send location updates in prod
      return
    }

    const location = GpsLocation.fromGeolocationPosition(position)
    const { updateUnitLocationById } = useMissionsStore()
    currentLocation.value = location
    // update local unit
    updateUnitLocationById(unitId, location)

    // publish position update
    // we do not want to send too many position updates, so we prevent any new
    // outbound updates for a specified amount of time after each update
    if (!sendLocationUpdateThrottled) {
      publishUnitLocationUpdate(unitId, location)

      sendLocationUpdateThrottled = true
      setTimeout(() => {
        sendLocationUpdateThrottled = false
      }, SEND_LOCATION_UPDATE_DEBOUNCE_MS)
    }
  }

  const publishUnitLocationUpdate = (unitId: string, location: GpsLocation) => {
    const relativeTopic = _getPublishTopic(unitId)
    const payload = new PubLocationUpdate(location)

    mqttClient.publish(relativeTopic, payload)
  }

  const handleLocationError = (positionError: GeolocationPositionError) => {
    const { locationPermissionState } = useBrowserPermissions()

    if (positionError.code === positionError.PERMISSION_DENIED) {
      locationPermissionState.value = PermissionState.DENIED
      console.warn(
        'Could not publish location: geolocation permission denied.',
        positionError
      )
    }
    if (positionError.code === positionError.POSITION_UNAVAILABLE) {
      locationPermissionState.value = PermissionState.DENIED
      console.warn(
        `Could not publish location: geolocation position unavailable.`,
        positionError
      )
    }
  }

  const _getSubscriptionTopic = (missionId: string) => {
    return `units/v1/outbound/${missionId}/location`
  }

  const _getPublishTopic = (unitId: string) => {
    return `units/v1/inbound/${unitId}/location`
  }

  const _unsubscribe = (missionId: string) => {
    const topic = _getSubscriptionTopic(missionId)
    if (subscribedTopic.value !== topic) return
    mqttClient.unsubscribe(topic)
    subscribedTopic.value = ''
  }

  const { currentMission } = storeToRefs(useMissionsStore())
  watch(
    currentMission,
    (newMission, oldMission) => {
      if (oldMission) {
        _unsubscribe(oldMission.missionId)
      }
      if (newMission) {
        subscribeToMissionUnitLocationUpdates(newMission.missionId)
      }
    },
    { immediate: true }
  )

  return {
    currentLocation,
    unprocessedUpdates,
    getCurrentUserPosition,
    startContinuousUnitLocationUpdates,
    handleInboundLocationUpdates, // public for testing
    publishUnitLocationUpdate,
  }
}
