import { FirebaseError } from 'firebase/app'
import { EmailNotVerifiedError } from '@/account/models/exceptions'
import { ref } from 'vue'

export function useErrorHandling() {
  const defaultErrorMessage =
    'Ein unbekannter Fehler ist aufgetreten. Bitte versuchen Sie es später erneut.'

  const errorMessage = ref<string>()

  const handleFirebaseError = (error: FirebaseError) => {
    console.warn(error)
    const errorCode = (error as FirebaseError).code
    switch (errorCode) {
      case 'auth/user-not-found':
        errorMessage.value =
          'Nutzer mit angegebener E-Mail-Adresse konnte nicht gefunden werden.'
        break

      case 'auth/wrong-password':
        errorMessage.value = 'Falsches Passwort.'
        break

      case 'auth/too-many-requests':
        errorMessage.value =
          'Zu viele Anmeldeversuche in kurzer Zeit. Bitte versuchen Sie es später erneut.'
        break

      case 'auth/expired-action-code':
        errorMessage.value =
          'Verifizierungscode abgelaufen. Bitte fordern Sie einen neuen Code an, indem Sie sich erneut einloggen.'
        break

      case 'auth/invalid-action-code':
        errorMessage.value =
          'Verifizierungscode falsch. Bitte versuchen Sie erneut, sich einzuloggen.'
        break

      case 'auth/user-disabled':
        errorMessage.value =
          'Nutzer gesperrt. Bitte kontaktieren Sie die Administratoren.'
        break

      default:
        errorMessage.value = defaultErrorMessage
    }
  }

  const handleError = (error: Error | unknown): void => {
    if (error instanceof FirebaseError) {
      handleFirebaseError(error)
      return
    }

    if (error instanceof EmailNotVerifiedError) {
      errorMessage.value = error.message
      return
    }

    errorMessage.value = defaultErrorMessage
  }

  return { errorMessage, handleError, handleFirebaseError }
}
