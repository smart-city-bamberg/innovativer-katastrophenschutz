import { TacticalSign } from '@/base/models/tacticalSign'
import { TacticalSignType } from '@/base/models/tacticalSignType'

export const LOCAL_DIR_PREFIX = '/public' + TacticalSign.PATH_PREFIX

export function useTacticalSigns() {
  /**
   * Paths to the tactical sign without the local directory prefix, so that
   * it can directly be used as a constructor parameter to create a {@link TacticalSign}.
   */
  const tacticalSignPaths = Object.keys(
    import.meta.glob('@/../public/svg/tactical-signs/**/*.svg')
  ).map((path) => path.replace(LOCAL_DIR_PREFIX, ''))

  const _getTacticalSignsByTypes = (
    types: TacticalSignType[]
  ): TacticalSign[] =>
    tacticalSignPaths
      .filter((path) => {
        for (const type of types) {
          // only keep tactical signs that belong to any of the types
          if (_isOfType(path, type)) return true
        }
        return false
      })
      .map((path) => new TacticalSign(path))

  const _isOfType = (tacticalSignPath: string, type: TacticalSignType) =>
    tacticalSignPath.includes(type)

  /**
   * Retrieves all tactical signs that are either of type
   * {@link TacticalSignType.DANGER} or {@link TacticalSignType.DAMAGE}.
   */
  const getReportTacticalSigns = (): TacticalSign[] =>
    _getTacticalSignsByTypes([TacticalSignType.DANGER, TacticalSignType.DAMAGE])

  const getPoiTacticalSigns = (): TacticalSign[] =>
    _getTacticalSignsByTypes([
      TacticalSignType.MANAGEMENT_POSITION,
      TacticalSignType.FACILITY,
    ])

  const getUnitTacticalSigns = (): TacticalSign[] =>
    _getTacticalSignsByTypes([
      TacticalSignType.UNITS,
      TacticalSignType.VEHICLES,
      TacticalSignType.PERSONS,
    ])

  return {
    getReportTacticalSigns,
    getPoiTacticalSigns,
    getUnitTacticalSigns,
    tacticalSignPaths,
  }
}
