import { describe, expect, it } from 'vitest'
import {
  LOCAL_DIR_PREFIX,
  useTacticalSigns,
} from '@/base/composables/useTacticalSigns'
import { TacticalSignType } from '@/base/models/tacticalSignType'

describe('useTacticalSigns', () => {
  const { tacticalSignPaths } = useTacticalSigns()

  describe.concurrent('tacticalSignPaths', () => {
    tacticalSignPaths.forEach((path) => {
      it(`tacticalSignPath ${path} contains no prefix`, () => {
        expect(path.includes(LOCAL_DIR_PREFIX)).toBe(false)
        expect(path.includes('//')).toBe(false)
      })
    })
  })

  describe.concurrent('getReportTacticalSigns', () => {
    const { getReportTacticalSigns } = useTacticalSigns()
    const reportTacticalSigns = getReportTacticalSigns()

    reportTacticalSigns.forEach((tacticalSign) => {
      it(`contains expected report tactical sign ${tacticalSign.name}`, () => {
        expect(
          tacticalSign.relativePath.includes(TacticalSignType.DANGER) ||
            tacticalSign.relativePath.includes(TacticalSignType.DAMAGE)
        ).toBe(true)
      })
    })
  })

  describe.concurrent('getPoiTacticalSigns', () => {
    const { getPoiTacticalSigns } = useTacticalSigns()
    const poiTacticalSigns = getPoiTacticalSigns()

    poiTacticalSigns.forEach((tacticalSign) => {
      it(`contains expected POI tactical sign ${tacticalSign.name}`, () => {
        expect(
          tacticalSign.relativePath.includes(
            TacticalSignType.MANAGEMENT_POSITION
          ) ||
            tacticalSign.relativePath.includes(TacticalSignType.PATIENTS) ||
            tacticalSign.relativePath.includes(TacticalSignType.FACILITY)
        ).toBe(true)
      })
    })
  })
})
