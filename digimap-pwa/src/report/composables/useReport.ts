import { MqttJsClient } from '@/mqtt/mqttClient'
import type { GenericMqttPayload } from '@/mqtt/iMqttClient'
import { SubReport } from '@/report/models/subReport'
import { ref } from 'vue'

const subscribedTopic = ref<string>('')

export function useReport() {
  const mqttClient: MqttJsClient = MqttJsClient.getInstance()

  const _getSubscriptionTopic = (missionId: string) => {
    return `units/v1/outbound/${missionId}/report`
  }

  const subscribe = (
    missionId: string,
    callback: (report: SubReport) => void
  ) => {
    const relativeTopic = _getSubscriptionTopic(missionId)
    if (subscribedTopic.value === relativeTopic) {
      // topic of this mission is already subscribed
      return
    }
    mqttClient.subscribe(
      relativeTopic,
      (_topic, payload: GenericMqttPayload) => {
        try {
          const report = SubReport.fromMqtt(payload)
          if (!report) return
          callback(report)
        } catch (e) {
          console.error('Cannot execute callback. Unknown error', e)
        }
      }
    )
    subscribedTopic.value = relativeTopic
  }

  const unsubscribe = (missionId: string) => {
    const topic = _getSubscriptionTopic(missionId)
    if (subscribedTopic.value !== topic) return
    mqttClient.unsubscribe(topic)
    subscribedTopic.value = ''
  }

  return { subscribe, unsubscribe }
}
