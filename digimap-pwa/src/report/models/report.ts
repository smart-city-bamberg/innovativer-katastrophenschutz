import { ReportImage } from '@/report/models/reportImage'
import { GpsLocation } from '@lion5/component-library'
import { TacticalSign } from '@/base/models/tacticalSign'

export class Report {
  location: GpsLocation
  datetime: Date
  description: string
  tacticalSign: TacticalSign
  image?: ReportImage

  constructor(
    location: GpsLocation,
    datetime: Date,
    description: string,
    tacticalSign: TacticalSign,
    image: ReportImage
  ) {
    this.location = location
    this.datetime = datetime
    this.description = description
    this.tacticalSign = tacticalSign
    this.image = image
  }
}
