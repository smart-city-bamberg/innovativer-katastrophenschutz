export class ReportImage {
  file?: File
  storagePath?: string

  constructor(file?: File, storagePath?: string) {
    this.file = file
    this.storagePath = storagePath
  }
}
