import type { ApiGpsLocation } from '@lion5/component-library-leaflet'
import { GpsLocation } from '@lion5/component-library-leaflet'
import { TacticalSign } from '@/base/models/tacticalSign'
import type { GenericMqttPayload } from '@/mqtt/iMqttClient'

export interface ISubReport {
  etag: number
  reportId: string // uuid
  senderUnitId: string
  senderCallSign: string
  report: string
  tacticalSignPath?: string
  createdAt: number | string
  referencingCommandId?: string // uuid
  images?: string[]
  location?: ApiGpsLocation
}

// TODO: differentiate between "original" Report and CommandResponse (responses to commands)
export class SubReport {
  readonly etag: number
  readonly reportId: string
  readonly senderUnitId: string
  readonly senderCallSign: string
  readonly report: string
  readonly createdAt: Date
  readonly storageUrls: string[]
  readonly location?: GpsLocation
  readonly tacticalSign?: TacticalSign
  readonly referencingCommandId?: string

  constructor(
    etag: number,
    reportId: string,
    senderUnitId: string,
    senderCallSign: string,
    report: string,
    createdAt: Date,
    storageUrls: string[],
    location?: GpsLocation,
    tacticalSign?: TacticalSign,
    referencingCommandId?: string
  ) {
    this.etag = etag
    this.reportId = reportId
    this.senderUnitId = senderUnitId
    this.senderCallSign = senderCallSign
    this.report = report
    this.createdAt = createdAt
    this.storageUrls = storageUrls
    this.location = location
    this.tacticalSign = tacticalSign
    this.referencingCommandId = referencingCommandId
  }

  public static fromMqtt(payload: GenericMqttPayload) {
    const typedPayload = payload as ISubReport
    try {
      return new SubReport(
        typedPayload.etag,
        typedPayload.reportId,
        typedPayload.senderUnitId,
        typedPayload.senderCallSign,
        typedPayload.report,
        typeof typedPayload.createdAt === 'string'
          ? new Date(typedPayload.createdAt)
          : new Date(typedPayload.createdAt * 1000), // TODO: fix unix time stamp conversion
        typedPayload.images || [],
        typedPayload.location && GpsLocation.fromApi(typedPayload.location),
        typedPayload.tacticalSignPath
          ? new TacticalSign(typedPayload.tacticalSignPath)
          : undefined,
        typedPayload.referencingCommandId
      )
    } catch (e) {
      // Ignore invalid reports
      console.error(e)
      return undefined
    }
  }

  public isCommandResponse() {
    return !!this.referencingCommandId
  }
}
