import { GpsLocation } from '@lion5/component-library-leaflet'
import { TacticalSign } from '@/base/models/tacticalSign'

export interface MapReport {
  readonly reportId: string
  readonly etag: number
  readonly senderCallSign: string
  readonly report: string
  readonly createdAt: Date
  readonly location: GpsLocation
  readonly tacticalSign: TacticalSign
}
