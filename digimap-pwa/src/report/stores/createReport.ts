import { defineStore, storeToRefs } from 'pinia'
import { computed, ref, toRaw } from 'vue'
import { ReportImage } from '@/report/models/reportImage'
import { useMissionsStore } from '@/mission/stores/missions'
import { date, mixed, object, string } from 'yup'
import type CreateReportForm from '@/report/pages/CreateReport.vue'
import { MqttJsClient } from '@/mqtt/mqttClient'
import { PubReport } from '@/mqtt/payloads/pub/PubReport'
import { useAccountStore } from '@/account/stores/account'
import { TacticalSign } from '@/base/models/tacticalSign'
import { Report } from '@/report/models/report'
import { v4 as uuidv4 } from 'uuid'
import { useDate } from '@lion5/component-library'
import { type GpsLocation } from '@lion5/component-library-leaflet'
import { useFirebaseStorage } from '@/base/composables/useFirebaseStorage'

export interface CreateReportForm {
  image: File
  tacticalSignPath: string
  description: string
  datetime: string
}

export const useCreateReportStore = defineStore('create-report', () => {
  const reportImage = ref<File>()
  const reportLocation = ref<GpsLocation>()
  const reportTacticalSign = ref<TacticalSign>()
  const formErrorMessage = ref<string>()

  const maxDate = new Date()
  maxDate.setMinutes(maxDate.getMinutes() + 1)

  const { getDateTimeLocalInputValue } = useDate()
  const schema = object({
    tacticalSignPath: string().required(
      'Bitte wählen Sie ein taktisches Zeichen'
    ),
    image: mixed(),
    datetime: date()
      .required('Bitte wählen Sie einen korrekten Zeitpunkt')
      // invalid dates would produce weird error message: https://github.com/jquense/yup/issues/764
      .transform((v) =>
        v instanceof Date && !isNaN(v as unknown as number) ? v : null
      ),
    description: string().required('Bitte geben Sie eine Beschreibung an'),
  })
  const initialValues = {
    tacticalSignPath: '',
    datetime: getDateTimeLocalInputValue(new Date()),
    image: reportImage.value,
    description: '',
  }

  const firebaseUploadService = useFirebaseStorage()
  const { currentMission } = storeToRefs(useMissionsStore())
  const { currentUser } = storeToRefs(useAccountStore())

  const setReportImage = (image: File) => {
    reportImage.value = image
  }

  const setReportLocation = (location: GpsLocation) => {
    reportLocation.value = location
  }

  const tacticalSignMarkerUrl = computed(() => reportTacticalSign.value?.path)

  const publishReport = async (formValues: CreateReportForm) => {
    const unitId = currentUser.value?.unitId
    const missionId = currentMission.value?.missionId
    if (!unitId) {
      formErrorMessage.value =
        'Keine Einheit zugewiesen. Bitte kontaktieren Sie einen System-Administrator.'
      return
    }
    if (!missionId) {
      formErrorMessage.value =
        'Kein Einsatz ausgewählt. Bitte wählen Sie einen Einsatz in den Einstellungen oder auf der Lagekarte.'
      return
    }
    if (!reportLocation.value) {
      formErrorMessage.value =
        'Ort der Lagemeldung konnte nicht ermittelt werden. Bitte setzen Sie den Ort neu.'
      return
    }

    // store form information in report object, as it might be reset
    const report = new Report(
      toRaw(reportLocation.value),
      new Date(formValues.datetime),
      formValues.description,
      TacticalSign.withPrefix(formValues.tacticalSignPath),
      new ReportImage(formValues.image)
    )
    const imagePaths = []

    // upload image to bucket, if it has been set
    if (reportImage.value) {
      const image = await uploadReportImage(formValues.image, missionId)
      if (!image || !image.storagePath) {
        formErrorMessage.value =
          'Interner Fehler beim Hochladen des Bildes. Bitte versuchen Sie es erneut.'
        return
      }
      report.image!.storagePath = image.storagePath
      imagePaths.push(image.storagePath)
    }
    // publish mqtt message to mission topic
    const topic = `units/v1/inbound/${missionId}/report`
    const pubReportPayload = new PubReport(
      uuidv4(),
      unitId,
      report.description,
      report.tacticalSign.relativePath,
      report.datetime.toISOString(),
      imagePaths,
      report.location
    )
    MqttJsClient.getInstance().publish(topic, pubReportPayload)
    resetFormErrors()
  }

  const uploadReportImage = async (file: File, missionId: string) => {
    try {
      const path = await firebaseUploadService.uploadReportImage(
        file,
        missionId,
        file.lastModified + ''
      )
      return new ReportImage(file, path)
    } catch (error: unknown) {
      console.error(error)
    }
  }

  const resetFormErrors = () => {
    formErrorMessage.value = ''
  }

  return {
    formSchema: schema,
    initialValues,
    reportImage,
    reportLocation,
    reportTacticalSign,
    tacticalSignMarkerUrl,
    maxDate,
    formErrorMessage,
    setReportImage,
    setReportLocation,
    publishReport,
    uploadReportImage,
    resetFormErrors,
  }
})
