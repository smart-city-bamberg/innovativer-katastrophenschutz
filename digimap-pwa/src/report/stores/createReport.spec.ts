import { beforeEach, describe, expect, it, vi } from 'vitest'
import { createPinia, setActivePinia } from 'pinia'
import { useMissionsStore } from '@/mission/stores/missions'
import { useAccountStore } from '@/account/stores/account'
import { MqttJsClient } from '@/mqtt/mqttClient'
import type { CreateReportForm } from '@/report/stores/createReport'
import { useCreateReportStore } from '@/report/stores/createReport'
import { User } from '@/account/models/user'
import { Role } from '@/account/models/role'
import { MockData } from '@/assets/mocks/data'
import { PubReport } from '@/mqtt/payloads/pub/PubReport'
import { ReportImage } from '@/report/models/reportImage'
import * as useFirebaseServiceExports from '@/base/composables/useFirebaseStorage'
import { GpsLocation } from '@lion5/component-library-leaflet'
import { TacticalSign } from '@/base/models/tacticalSign'
import * as exportUUID from 'uuid'
import { useFirebaseStorage } from '@/base/composables/useFirebaseStorage'

vi.mock('firebase/storage', () => ({
  getStorage: vi.fn(),
  ref: vi.fn(),
  uploadBytes: vi.fn().mockResolvedValue({ fullPath: 'storage/path' }),
}))
vi.mock('mqtt', () => ({
  default: {
    connect: vi.fn(),
  },
}))
vi.spyOn(useFirebaseServiceExports, 'useFirebaseStorage').mockReturnValue({
  uploadReportImage: vi.fn().mockResolvedValue('storage/path'),
  getImageUrl: vi.fn(),
})
vi.mock('@/mqtt/mqttClient.ts', () => ({
  MqttJsClient: {
    getInstance: vi.fn().mockReturnValue({
      onConnect: vi.fn(),
      publish: vi.fn(),
    }),
  },
}))
const testFile = new File([], 'testFile')
const mockMission = MockData.getMissions()[0]
const mockUser = new User(null, null, Role.USER, 'unit1')
const formValues = {
  image: testFile,
  tacticalSignPath: '/svg/tactical-signs/test/path.svg',
  description: 'test description',
  datetime: '2020-01-01T12:12',
} as CreateReportForm

describe('useCreateReportStore', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
    vi.clearAllMocks()
  })

  describe('publishReport', () => {
    it('publishes correct payload', async () => {
      const expectedLocation = new GpsLocation(0.0, 0.0)
      const reportImage = new ReportImage(testFile, 'storage/path')
      Date.prototype.toISOString = vi
        .fn()
        .mockReturnValue('2020-01-01T12:12:00.000Z')
      const createReportStore = useCreateReportStore()
      createReportStore.reportLocation = expectedLocation
      createReportStore.reportImage = testFile
      const mqttClient = MqttJsClient.getInstance()
      const missionsStore = useMissionsStore()
      const accountStore = useAccountStore()
      accountStore.currentUser = mockUser
      missionsStore.currentMission = mockMission

      const expectedPayload = new PubReport(
        'b347cf8b-0f78-4ec1-9f48-5364659f0350',
        'unit1',
        formValues.description,
        TacticalSign.withPrefix(formValues.tacticalSignPath).relativePath,
        '2020-01-01T12:12:00.000Z',
        [reportImage.storagePath || ''],
        expectedLocation
      )
      vi.spyOn(exportUUID, 'v4').mockReturnValue(expectedPayload.reportId)

      await createReportStore.publishReport(formValues)

      expect(mqttClient.publish).toHaveBeenCalledWith(
        `units/v1/inbound/${mockMission.missionId}/report`,
        expectedPayload
      )
    })

    it('publishes correct payload without image', async () => {
      const expectedLocation = new GpsLocation(0.0, 0.0)
      Date.prototype.toISOString = vi
        .fn()
        .mockReturnValue('2020-01-01T12:12:00.000Z')
      const createReportStore = useCreateReportStore()
      createReportStore.reportLocation = expectedLocation
      const mqttClient = MqttJsClient.getInstance()
      const missionsStore = useMissionsStore()
      const accountStore = useAccountStore()
      accountStore.currentUser = mockUser
      missionsStore.currentMission = mockMission
      createReportStore.uploadReportImage = vi.fn()

      const expectedPayload = new PubReport(
        'b347cf8b-0f78-4ec1-9f48-5364659f0350',
        'unit1',
        formValues.description,
        TacticalSign.withPrefix(formValues.tacticalSignPath).relativePath,
        '2020-01-01T12:12:00.000Z',
        [],
        expectedLocation
      )
      vi.spyOn(exportUUID, 'v4').mockReturnValue(expectedPayload.reportId)

      await createReportStore.publishReport(formValues)

      expect(createReportStore.uploadReportImage).not.toHaveBeenCalled()
      expect(mqttClient.publish).toHaveBeenCalledWith(
        `units/v1/inbound/${mockMission.missionId}/report`,
        expectedPayload
      )
    })

    it('returns early if currentUser is not set', () => {
      const mqttClient = MqttJsClient.getInstance()
      const createReportStore = useCreateReportStore()
      createReportStore.uploadReportImage = vi.fn()
      const accountStore = useAccountStore()
      accountStore.currentUser = undefined

      createReportStore.publishReport(formValues)

      expect(createReportStore.uploadReportImage).not.toHaveBeenCalled()
      expect(mqttClient.publish).not.toHaveBeenCalled()
      expect(createReportStore.formErrorMessage).toBe(
        'Keine Einheit zugewiesen. Bitte kontaktieren Sie einen System-Administrator.'
      )
    })

    it('returns early if currentUser has no unitId', () => {
      const mqttClient = MqttJsClient.getInstance()
      const createReportStore = useCreateReportStore()
      createReportStore.uploadReportImage = vi.fn()
      const accountStore = useAccountStore()
      accountStore.currentUser = new User(null, null, Role.USER, null)

      createReportStore.publishReport(formValues)

      expect(createReportStore.uploadReportImage).not.toHaveBeenCalled()
      expect(mqttClient.publish).not.toHaveBeenCalled()
      expect(createReportStore.formErrorMessage).toBe(
        'Keine Einheit zugewiesen. Bitte kontaktieren Sie einen System-Administrator.'
      )
    })

    it('returns early if currentMission is not set', () => {
      const mqttClient = MqttJsClient.getInstance()
      const createReportStore = useCreateReportStore()
      createReportStore.uploadReportImage = vi.fn()
      const missionsStore = useMissionsStore()
      const accountStore = useAccountStore()
      accountStore.currentUser = mockUser
      missionsStore.currentMission = undefined

      createReportStore.publishReport(formValues)

      expect(createReportStore.uploadReportImage).not.toHaveBeenCalled()
      expect(mqttClient.publish).not.toHaveBeenCalled()
      expect(createReportStore.formErrorMessage).toBe(
        'Kein Einsatz ausgewählt. Bitte wählen Sie einen Einsatz in den Einstellungen oder auf der Lagekarte.'
      )
    })

    it('returns early if reportLocation is not set', () => {
      const mqttClient = MqttJsClient.getInstance()
      const createReportStore = useCreateReportStore()
      createReportStore.uploadReportImage = vi.fn()
      const missionsStore = useMissionsStore()
      const accountStore = useAccountStore()
      accountStore.currentUser = mockUser
      missionsStore.currentMission = mockMission

      createReportStore.reportLocation = undefined

      createReportStore.publishReport(formValues)

      expect(createReportStore.uploadReportImage).not.toHaveBeenCalled()
      expect(mqttClient.publish).not.toHaveBeenCalled()
      expect(createReportStore.formErrorMessage).toBe(
        'Ort der Lagemeldung konnte nicht ermittelt werden. Bitte setzen Sie den Ort neu.'
      )
    })
  })

  describe('uploadReportImage', () => {
    it('calls firebaseUploadService and returns expected object', async () => {
      const firebaseUploadService = useFirebaseStorage()
      const createReportStore = useCreateReportStore()

      const result = await createReportStore.uploadReportImage(
        testFile,
        mockMission.missionId
      )

      expect(firebaseUploadService.uploadReportImage).toHaveBeenCalledWith(
        testFile,
        mockMission.missionId,
        testFile.lastModified + ''
      )
      expect(result).toStrictEqual(new ReportImage(testFile, 'storage/path'))
    })

    it('returns undefined when upload fails', async () => {
      const error = new Error('error message')
      vi.spyOn(useFirebaseServiceExports, 'useFirebaseStorage').mockReturnValue(
        {
          uploadReportImage: vi.fn().mockRejectedValue(error),
          getImageUrl: vi.fn(),
        }
      )

      console.error = vi.fn()

      const firebaseUploadService = useFirebaseStorage()
      const createReportStore = useCreateReportStore()

      const result = await createReportStore.uploadReportImage(
        testFile,
        mockMission.missionId
      )

      expect(firebaseUploadService.uploadReportImage).toHaveBeenCalledWith(
        testFile,
        mockMission.missionId,
        testFile.lastModified + ''
      )
      expect(console.error).toHaveBeenCalledWith(error)
      expect(result).toBeUndefined()
    })
  })
})
