import Join from '@/join-codes/pages/Join.vue'
import WaitingRoom from '@/join-codes/pages/WaitingRoom.vue'

export const joinRoutes = [
  {
    path: '/join',
    name: 'JoinWithoutCode',
    component: Join,
  },
  {
    path: '/join/:code',
    name: 'JoinWithCode',
    component: Join,
  },
  {
    path: '/waiting-room',
    name: 'WaitingRoom',
    component: WaitingRoom,
  },
]
