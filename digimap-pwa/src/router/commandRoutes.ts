import CreateCommand from '@/command/pages/CreateCommand.vue'
import CommandList from '@/command/pages/CommandList.vue'
import CreateCommandResponse from '@/command/pages/CreateCommandResponse.vue'

export const commandRoutes = [
  {
    path: '/command/create/:reportId',
    name: 'CreateCommand',
    component: CreateCommand,
  },
  {
    path: '/commands',
    name: 'CommandList',
    component: CommandList,
  },
  {
    path: '/commands/:commandId/response',
    name: 'CreateCommandResponse',
    component: CreateCommandResponse,
  },
]
