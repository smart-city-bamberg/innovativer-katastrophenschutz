import Login from '@/account/pages/Login.vue'
import ResetPassword from '@/account/pages/ResetPassword.vue'

export const authRoutes = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/reset-password',
    name: 'ResetPassword',
    component: ResetPassword,
  },
]
