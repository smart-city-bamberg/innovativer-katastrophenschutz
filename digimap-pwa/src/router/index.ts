import { createRouter, createWebHistory } from 'vue-router'
import { useAccountStore } from '@/account/stores/account'
import Home from '@/base/pages/Home.vue'
import Settings from '@/base/pages/Settings.vue'
import DashboardView from '@/dashboard/pages/DashboardView.vue'
import CreateReport from '@/report/pages/CreateReport.vue'
import { commandRoutes } from '@/router/commandRoutes'
import { authRoutes } from '@/router/authRoutes'
import CreatePoi from '@/poi/pages/CreatePoi.vue'
import PositionUnit from '@/unit/pages/PositionUnit.vue'
import Imprint from '@/base/pages/Imprint.vue'
import { missionRoutes } from '@/router/missionRoutes'
import UnitsView from '@/unit/pages/UnitsView.vue'
import CreateUnit from '@/unit/pages/CreateUnit.vue'
import EditUnit from '@/unit/pages/EditUnit.vue'
import { joinRoutes } from '@/router/joinRoutes'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      alias: '/home',
      name: 'Home',
      component: Home,
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
    },
    {
      path: '/imprint',
      name: 'Imprint',
      component: Imprint,
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: DashboardView,
    },
    {
      path: '/position-unit',
      name: 'PositionUnit',
      component: PositionUnit,
    },
    {
      path: '/unit/create',
      name: 'CreateUnit',
      component: CreateUnit,
    },
    {
      path: '/unit/edit/:unitId',
      name: 'EditUnit',
      component: EditUnit,
    },
    {
      path: '/admin',
      name: 'UnitsView',
      component: UnitsView,
    },
    {
      path: '/report/create',
      name: 'CreateReport',
      component: CreateReport,
    },
    {
      path: '/poi/create',
      name: 'CreatePoi',
      component: CreatePoi,
    },

    ...joinRoutes,
    ...missionRoutes,
    ...commandRoutes,
    ...authRoutes,
  ],
})

export const registerAuthNavigationGuard = () => {
  router.beforeEach(async (to, _from) => {
    const accountStore = useAccountStore()
    // redirect the user to the login page if not logged in
    if (
      !accountStore.isLoggedIn &&
      to.name !== 'Login' &&
      to.name !== 'ResetPassword' &&
      to.name !== 'Imprint' &&
      to.name !== 'JoinWithoutCode' &&
      to.name !== 'JoinWithCode'
    ) {
      return { name: 'Login' }
    }

    if (
      accountStore.isAnonymous &&
      (to.name === 'UnitsView' ||
        to.name === 'CreateUnit' ||
        to.name === 'EditUnit' ||
        to.name === 'EditMission' ||
        to.name === 'MissionForm' ||
        to.name === 'WaitingRoom')
    ) {
      return { name: 'Home' }
    }
  })
}

export default router
