import missionList from '@/mission/pages/MissionList.vue'
import missionView from '@/mission/pages/MissionView.vue'
import MissionForm from '@/mission/pages/CreateMission.vue'
import EditMission from '@/mission/pages/EditMission.vue'

export const missionRoutes = [
  {
    path: '/missions',
    name: 'missionList',
    component: missionList,
  },
  {
    path: '/mission/:missionId',
    name: 'missionView',
    component: missionView,
  },
  {
    path: '/mission/edit/:missionId',
    name: 'EditMission',
    component: EditMission,
  },
  {
    path: '/mission/create',
    name: 'MissionForm',
    component: MissionForm,
  },
]
