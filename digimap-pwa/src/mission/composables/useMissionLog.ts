import { SubReport } from '@/report/models/subReport'
import { SubCommand } from '@/command/models/subCommand'
import { computed, ref } from 'vue'

export function useMissionLog() {
  const log = ref<(SubReport | SubCommand)[]>([])
  const reverseLog = computed(() => [...log.value].reverse())
  const reportReferences = ref<Map<string, number>>(new Map())
  const commandReferences = ref<Map<string, number>>(new Map())

  /**
   * Reports that are not a response to a command
   */
  const reports = computed(
    (): SubReport[] =>
      log.value.filter(
        (entry) => isReport(entry) && !(entry as SubReport).isCommandResponse()
      ) as SubReport[]
  )

  const addMessage = (message: SubReport | SubCommand) => {
    const newLength = log.value.push(message)
    const index = newLength - 1
    if (message instanceof SubReport) {
      reportReferences.value.set(message.reportId, index)
      if (message.referencingCommandId) {
        _addCommandResponse(message.referencingCommandId, message.reportId)
      }
    } else {
      commandReferences.value.set(message.commandId, index)
    }
  }

  const getReport = (reportId: string): SubReport | undefined => {
    const index = reportReferences.value.get(reportId)
    if (index == null) return
    return log.value.at(index) as SubReport | undefined
  }

  const _getCommandIndex = (commandId: string): number | undefined => {
    return commandReferences.value.get(commandId)
  }

  const getCommand = (commandId: string): SubCommand | undefined => {
    const index = _getCommandIndex(commandId)
    if (index == null) return
    return log.value.at(index) as SubCommand | undefined
  }

  const _addCommandResponse = (commandId: string, responseReportId: string) => {
    const command = getCommand(commandId)
    if (!command) {
      console.error('Command cannot be found!')
      return
    }
    const newCommand = command.clone()
    newCommand.commandResponses.push(responseReportId)
    const index = _getCommandIndex(commandId)
    if (index == null) {
      console.error('Command cannot be found!')
      return
    }
    log.value[index] = newCommand
  }

  const isReport = (item: SubReport | SubCommand) => item instanceof SubReport

  return {
    getReport,
    getCommand,
    addMessage,
    isReport,
    missionLog: reverseLog,
    reports,
  }
}
