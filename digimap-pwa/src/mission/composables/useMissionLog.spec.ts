import { describe, expect, it } from 'vitest'
import { useMissionLog } from '@/mission/composables/useMissionLog'
import { SubReport } from '@/report/models/subReport'
import { SubCommand } from '@/command/models/subCommand'

describe('useMissionLog.ts', () => {
  function getSubReport(etag: number) {
    const reportId = `report-${etag}`
    return new SubReport(
      etag,
      reportId,
      `sender-id-${reportId}`,
      `sender-call-sign-${reportId}`,
      `report-${reportId}`,
      new Date(),
      []
    )
  }

  function getSubCommand(etag: number) {
    const commandId = `command-${etag}`
    return new SubCommand(
      etag,
      `sender-id-${commandId}`,
      `sender-call-sign-${commandId}`,
      commandId,
      '',
      new Date(),
      []
    )
  }

  it('addMessage - adds message to missionLog', () => {
    const expectedMessage = getSubReport(1)
    const { missionLog, addMessage } = useMissionLog()

    addMessage(expectedMessage)

    expect(missionLog.value.at(0)).toStrictEqual(expectedMessage)
  })
  it('addMessage - adds message in correct order to missionLog', () => {
    const expectedMessages = [getSubReport(1), getSubReport(2), getSubReport(3)]
    const { missionLog, addMessage } = useMissionLog()

    expectedMessages.forEach((message) => addMessage(message))

    expect(missionLog.value).toStrictEqual(expectedMessages.reverse())
  })

  it('getReport - returns undefined if report with id cannot be found', () => {
    const messages = [getSubReport(1), getSubReport(2), getSubReport(3)]
    const { addMessage, getReport } = useMissionLog()

    messages.forEach((message) => addMessage(message))

    expect(getReport('unknown-id')).toBe(undefined)
  })

  it('getReport - returns report if exists', () => {
    const expectedReport = getSubReport(2)
    const messages = [
      getSubReport(1),
      expectedReport,
      getSubReport(3),
      getSubCommand(4),
    ]
    const { addMessage, getReport } = useMissionLog()

    messages.forEach((message) => addMessage(message))

    expect(getReport(expectedReport.reportId)).toStrictEqual(expectedReport)
  })

  it('getCommand - returns undefined if report with id cannot be found', () => {
    const messages = [getSubCommand(1), getSubCommand(2), getSubCommand(3)]
    const { addMessage, getCommand } = useMissionLog()

    messages.forEach((message) => addMessage(message))

    expect(getCommand('unknown-id')).toBe(undefined)
  })

  it('getCommand - returns command if exists', () => {
    const expectedCommand = getSubCommand(2)
    const messages = [getSubCommand(1), expectedCommand, getSubReport(3)]
    const { addMessage, getCommand } = useMissionLog()

    messages.forEach((message) => addMessage(message))

    expect(getCommand(expectedCommand.commandId)).toStrictEqual(expectedCommand)
  })
})
