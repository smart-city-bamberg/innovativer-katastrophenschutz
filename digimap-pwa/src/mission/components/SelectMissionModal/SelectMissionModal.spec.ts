import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest'
import { mount, VueWrapper } from '@vue/test-utils'
import SelectMissionModal from '@/mission/components/SelectMissionModal/SelectMissionModal.vue'
import { MockData } from '@/assets/mocks/data'
import {
  DismissibleModal,
  SelectInput,
  SelectOption,
} from '@lion5/component-library'
import { createTestingPinia } from '@pinia/testing'
import { MissionState } from '@/mqtt/payloads/MissionState'

describe('SelectMissionModal', () => {
  let wrapper: ReturnType<typeof mountComponent>

  const missions = MockData.getMissions()
  const runningMissions = missions.filter(
    (mission) => mission.state === MissionState.RUNNING
  )

  const pinia = createTestingPinia({
    initialState: {
      missions: {
        allMissions: missions,
      },
    },
  })

  const mountComponent = (visible?: boolean) => {
    return mount(SelectMissionModal, {
      attachTo: document.body,
      props: {
        missions,
        visible,
      },
      global: {
        stubs: {
          SelectInput: true,
        },
        plugins: [pinia],
      },
    })
  }

  beforeEach(() => {
    wrapper = mountComponent()
  })

  afterEach(() => {
    wrapper && wrapper.unmount()
    vi.clearAllMocks()
  })

  describe(':props', () => {
    it(':missions - missions are passed to select input', async () => {
      wrapper = mountComponent(true)
      const selectInput = wrapper.findComponent(
        SelectInput as unknown as VueWrapper<typeof SelectInput>
      )

      expect(selectInput.props().options).toStrictEqual(
        runningMissions.map(
          (m) => new SelectOption(m.missionNumber, `Einsatz ${m.missionNumber}`)
        )
      )
    })

    describe(':visible', () => {
      it('initially sets displayModal of modal to false by default', async () => {
        const dismissibleModal = wrapper.findComponent(DismissibleModal)

        expect(dismissibleModal.props('modalDisplayed')).toBe(false)
      })

      it('initially sets displayModal of modal to true if specified', async () => {
        wrapper = mountComponent(true)
        const dismissibleModal = wrapper.findComponent(DismissibleModal)

        expect(dismissibleModal.props('modalDisplayed')).toBe(true)
      })

      it('updates displayModal of modal to true if set after mount', async () => {
        const dismissibleModal = wrapper.findComponent(DismissibleModal)

        await wrapper.setProps({ visible: true })

        expect(dismissibleModal.props('modalDisplayed')).toBe(true)
      })

      it('updates displayModal of modal to false if set after mount', async () => {
        wrapper = mountComponent(true)
        const dismissibleModal = wrapper.findComponent(DismissibleModal)

        await wrapper.setProps({ visible: false })

        expect(dismissibleModal.props('modalDisplayed')).toBe(false)
      })
    })
  })

  describe('@events', () => {
    it('@mission-select - fires with correct mission when mission is selected', async () => {
      const selectInput = wrapper.findComponent(
        SelectInput as unknown as VueWrapper<typeof SelectInput>
      )
      await selectInput.vm.$emit('update:modelValue', missions[0].missionNumber)
      await wrapper.find('.select-mission-button').trigger('click')

      expect(wrapper.emitted('mission-select')?.length).toBe(1)
      expect(wrapper.emitted('mission-select')?.at(0)).toStrictEqual([
        missions[0],
      ])
    })

    it('@update:visible - fires with correct value if modalDisplayed of modal is updated', async () => {
      const modal = await wrapper.findComponent(DismissibleModal)

      await modal.vm.$emit('update:modalDisplayed', true)

      expect(wrapper.emitted('update:visible')).toBeDefined()
      expect(wrapper.emitted('update:visible')![0]).toStrictEqual([true])
    })
  })
})
