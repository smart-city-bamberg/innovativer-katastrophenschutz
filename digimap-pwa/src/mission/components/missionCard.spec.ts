import { afterEach, beforeEach, describe, expect, it } from 'vitest'
import type { defineComponent } from 'vue'
import MissionCard from '@/mission/components/MissionCard.vue'
import { mount } from '@vue/test-utils'
import { MockData } from '@/assets/mocks/data'
import { createTestingPinia } from '@pinia/testing'

const missions = MockData.getMissions()
const user = MockData.getUser()

describe('MissionsCard', () => {
  let wrapper: ReturnType<typeof defineComponent>
  const pinia = createTestingPinia({
    initialState: {
      account: {
        currentUser: user,
      },
    },
  })

  beforeEach(() => {
    wrapper = mount(MissionCard, {
      global: { plugins: [pinia] },
      props: {
        mission: missions[0],
        currentMissionId: missions[2].missionId,
      },
    })
  })

  afterEach(() => {
    wrapper.unmount()
  })

  it('should display mission details', () => {
    const missionTitle = wrapper.find('[data-test="mission-title"]')
    const missionStreet = wrapper.find('[data-test="mission-address-street"]')
    const missionCity = wrapper.find('[data-test="mission-address-city"]')
    const missionType = wrapper.find('[data-test="mission-type"]')
    const missionDate = wrapper.find('[data-test="mission-date"]')

    expect(missionTitle.text()).toBe('B 4.1 2108XX XXX')
    expect(missionStreet.text()).toContain('Brennerstraße 12A,')
    expect(missionCity.text()).toContain('96052 Bamberg')
    expect(missionType.text()).toBe('cues mission1')
    expect(missionDate.text()).toBe('Mo., 01. Januar um 12:00')
  })

  it('should display the correct mission state', async () => {
    const missionState = wrapper.find('.mission-state-badge')
    expect(missionState.text()).toBe('Laufend')

    await wrapper.setProps({ mission: missions[1] })

    expect(missionState.text()).toBe('Abgeschlossen')
  })

  it('should not display the pencil icon if the mission is finished', async () => {
    let editMission = wrapper.find('.edit-button')
    expect(editMission.exists()).toBe(true)

    await wrapper.setProps({ mission: missions[1] })

    editMission = wrapper.find('.edit-button')
    expect(editMission.exists()).toBe(false)
  })

  it('should disable the share button if the mission is not the current mission', async () => {
    let shareMission = wrapper.find('.share-button')
    expect(shareMission.exists()).toBe(true)
    expect(shareMission.element.hasAttribute('disabled')).toBe(true) // The button is disabled

    await wrapper.setProps({ mission: missions[2] })

    shareMission = wrapper.find('.share-button')
    expect(shareMission.exists()).toBe(true)
    expect(shareMission.element.hasAttribute('disabled')).toBe(false) // The button is enabled
  })

  it('should not show the select button if the mission is the current mission', async () => {
    let selectMission = wrapper.find('.select-mission-button')
    expect(selectMission.exists()).toBe(true)
    expect(selectMission.element.hasAttribute('disabled')).toBe(false) // The button is enabled

    await wrapper.setProps({ mission: missions[2] })

    selectMission = wrapper.find('.select-mission-button')
    expect(selectMission.exists()).toBe(false)
  })
})
