import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest'
import type { defineComponent } from 'vue'
import { mount } from '@vue/test-utils'
import UnitsTable from '@/mission/components/UnitsTable.vue'
import { createTestingPinia } from '@pinia/testing'
import { MissionService } from '@/mission/services/missionService'
import { MockData } from '@/assets/mocks/data'

describe('UnitsTable', () => {
  let wrapper: ReturnType<typeof defineComponent>

  const missions = MockData.getMissions()
  const units = MockData.getAllUnits()
  const pinia = createTestingPinia({})

  MissionService.addUnitsToMission = vi.fn()
  MissionService.removeUnitsFromMission = vi.fn()

  beforeEach(() => {
    wrapper = mount(UnitsTable, {
      props: {
        allUnits: units,
        selectedMission: missions[0],
      },
      global: { plugins: [pinia] },
    })
  })

  afterEach(() => {
    wrapper.unmount()
  })

  it('should render the units', () => {
    const rows = wrapper.findAll('tr')
    expect(rows).toHaveLength(units.length + 1) // +1 for the header row
  })

  it('should render the correct unit data', () => {
    const rows = wrapper.findAll('tr')
    const firstRow = rows[1]
    const secondRow = rows[2]
    expect(firstRow.text()).toContain(units[0].callSign)
    expect(firstRow.html()).toContain(units[0].tacticalSign.path)
    expect(secondRow.text()).toContain(units[1].callSign)
    expect(secondRow.html()).toContain(units[1].tacticalSign.path)
  })

  it('should render the correct tactical sign', () => {
    const rows = wrapper.findAll('tr')
    const firstRow = rows[1]
    const secondRow = rows[2]
    const firstRowTacticalSign = firstRow.find('img')
    const secondRowTacticalSign = secondRow.find('img')
    expect(firstRowTacticalSign.attributes('src')).toBe(
      units[0].tacticalSign.path
    )
    expect(secondRowTacticalSign.attributes('src')).toBe(
      units[1].tacticalSign.path
    )
  })

  it('should render the checkboxes correctly', () => {
    const rows = wrapper.findAll('tr')
    const firstRow = rows[1]
    const fourthRow = rows[4]
    const firstRowCheckbox = firstRow.find('input[type="checkbox"]')
    const secondRowCheckbox = fourthRow.find('input[type="checkbox"]')

    expect(firstRowCheckbox.element.checked).toBeTruthy()
    expect(secondRowCheckbox.element.checked).toBeFalsy()
  })

  it('should be sorted alphabetically by call sign', () => {
    const rows = wrapper.findAll('tr')
    const firstRow = rows[1]
    const fifthRow = rows[5]
    expect(firstRow.text()).toContain(units[0].callSign)
    expect(fifthRow.text()).toContain(units[4].callSign)
  })

  it('should filter the units by call sign', async () => {
    const input = wrapper.find('input')
    await input.setValue('Feuerwehr')
    const rows = wrapper.findAll('tr')
    expect(rows).toHaveLength(5)
    expect(rows[1].text()).toContain(units[0].callSign)
    expect(rows[2].text()).toContain(units[1].callSign)
    expect(rows[3].text()).toContain(units[2].callSign)
    expect(rows[4].text()).toContain(units[4].callSign)

    await input.setValue('2')
    const filteredRows = wrapper.findAll('tr')
    expect(filteredRows).toHaveLength(2)
    expect(filteredRows[1].text()).toContain(units[1].callSign)
  })

  it('should change selectedUnits when a checkbox gets clicked', async () => {
    const initialSelectedUnits = wrapper.vm.selectedUnits

    const rows = wrapper.findAll('tr')
    const firstRow = rows[1]
    const firstRowCheckbox = firstRow.find('input[type="checkbox"]')
    await firstRowCheckbox.setChecked(false)
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.selectedUnits).not.toEqual(initialSelectedUnits)
  })
})
