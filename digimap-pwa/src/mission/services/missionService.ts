import type { ApiCreateMission, ApiMission } from '@/mission/models/mission'
import { Mission } from '@/mission/models/mission'
import { MissionState } from '@/mission/models/missionState'
import type { ApiUnit } from '@/unit/models/unit'
import { Unit } from '@/unit/models/unit'
import { RestClient } from '@/base/services/restClient'
import { storeToRefs } from 'pinia'
import { useAccountStore } from '@/account/stores/account'

export class MissionService {
  private static readonly _client = RestClient.getInstance()
  private static readonly baseUrl = import.meta.env
    .VITE_APP_DIGIMAP_API_BASE_URL

  public static async getAllMissions(): Promise<Mission[]> {
    const missions: Mission[] = []
    const missionResources = await this._client
      .go<ApiMission[]>('/missions')
      .followAll<ApiMission>('missions')
      .preFetch()

    for (const resource of missionResources) {
      const state = await resource.get()
      const mission = Mission.fromKettingState(state)
      missions.push(mission)
    }

    console.log('Retrieved all missions', missions)
    return missions
  }

  public static async fetchMissionById(
    missionId: string
  ): Promise<Mission | undefined> {
    const state = await this._client.go(`/missions/${missionId}`).get()
    return state ? Mission.fromKettingState(state) : undefined
  }

  public static async getMissionUnits(mission: Mission): Promise<Unit[]> {
    const units: Unit[] = []
    if (!mission.unitsUri) {
      console.log('No uri to the units of mission found', mission.uri)
      return units
    }

    this._client.cache.clear()
    const unitResources = await this._client
      .go<ApiUnit[]>(mission.unitsUri)
      .followAll<ApiUnit>('emergencyUnits')
      .preFetch()

    for (const resource of unitResources) {
      const state = await resource.get()
      units.push(Unit.fromKettingState(state))
    }

    console.log('Retrieved mission units', units)
    return units
  }

  public static async getUnitById(unitId: string): Promise<Unit | undefined> {
    if (!unitId) {
      return undefined
    }
    const unitState = await this._client.go(`/units/${unitId}`).get()
    return unitState ? Unit.fromKettingState(unitState) : undefined
  }

  public static async getMissionByUnitId(
    unitId: string
  ): Promise<Mission | undefined> {
    if (!unitId) {
      return undefined
    }
    const missionState = await this._client
      .go(`/missions/search/findMissionByUnitsEquals?unitId=${unitId}`)
      .get()
    return missionState ? Mission.fromKettingState(missionState) : undefined
  }

  public static async addUnitsToMission(
    mission: Mission,
    units: Unit[]
  ): Promise<boolean> {
    if (!mission.uri) {
      throw new Error('Fehler beim Lesen der URI der Mission')
    }

    const data = units.map((unit) => unit.id)

    try {
      await this._client
        .go(`/missions/${mission.missionId}/units`)
        .patch({ data: { unitIds: data } })
      return true
    } catch (error) {
      console.error('Error:', error)
      return false
    }
  }

  public static async removeUnitsFromMission(
    mission: Mission,
    units: Unit[]
  ): Promise<boolean> {
    if (!mission.uri) {
      throw new Error('Error while reading uri of mission')
    }

    for (const unit of units) {
      await this._client
        .go(
          this.baseUrl + '/missions/' + mission.missionId + '/units/' + unit.id
        )
        .delete()
    }

    return true
  }

  public static async createMission(apiMission: ApiCreateMission) {
    const missionState = await this._client
      .go('/missions')
      .post({ data: apiMission })
    return missionState ? Mission.fromKettingState(missionState) : undefined
  }

  static async updateMissionState(state: MissionState, mission: Mission) {
    const missionState = await this._client
      .go(mission.uri)
      .patch({ data: { state: state } })
    return missionState ? Mission.fromKettingState(missionState) : undefined
  }

  static async downloadReport(missionId: string) {
    const accountStore = useAccountStore()
    const { bearerToken } = storeToRefs(accountStore)
    const response = await fetch(
      this._client.bookmarkUri + '/protocols/' + missionId,
      {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${bearerToken.value}`,
        },
      }
    )
    // download the document via a temporary doc element
    const blob = await response.blob()
    const file = window.URL.createObjectURL(blob)
    const a = document.createElement('a')
    a.href = file
    a.download = MissionService.getDocumentTitle(missionId)
    document.body.appendChild(a) // we need to append the element to the dom -> otherwise it will not work in firefox
    a.click()
    a.remove() // afterward we remove the element again
  }

  private static getDocumentTitle(missionId: string) {
    const now = new Date()
    const day = String(now.getDate()).padStart(2, '0')
    const month = String(now.getMonth() + 1).padStart(2, '0') // Months are zero-based
    const year = now.getFullYear()
    const hours = String(now.getHours()).padStart(2, '0')
    const minutes = String(now.getMinutes()).padStart(2, '0')
    const seconds = String(now.getSeconds()).padStart(2, '0')
    return `${day}-${month}-${year}_${hours}-${minutes}-${seconds}_Einsatzprotokoll_${missionId}.pdf`
  }
}
