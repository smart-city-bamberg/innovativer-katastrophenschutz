import type { Unit } from '@/unit/models/unit'
import type { ApiAddress } from '@/base/models/address'
import { Address } from '@/base/models/address'
import type { ApiGpsLocation } from '@lion5/component-library-leaflet'
import { GpsLocation } from '@lion5/component-library-leaflet'
import type { ApiGkLocation } from '@/mission/models/gkLocation'
import type { State } from 'ketting'
import { MissionState } from '@/mission/models/missionState'
import type { ISubReport } from '@/report/models/subReport'
import { SubReport } from '@/report/models/subReport'
import type { ISubCommand } from '@/command/models/subCommand'
import { SubCommand } from '@/command/models/subCommand'
import type { IApiDangerZone } from '@/map/shapes/dangerZone/models/dangerZone'
import { DangerZone } from '@/map/shapes/dangerZone/models/dangerZone'
import type { IApiMissionSection } from '@/map/shapes/missionSection/models/missionSection'
import { MissionSection } from '@/map/shapes/missionSection/models/missionSection'
import type { IApiPointOfInterest } from '@/poi/models/pointOfInterest'
import { PointOfInterest } from '@/poi/models/pointOfInterest'

/**
 * https://github.com/lion5/smart-city-digimap/wiki/Glossary#einsatz-mission
 */
export class Mission {
  /**
   * Unique internal identifier of the mission.
   */
  readonly missionId: string
  /**
   * Human-readable identifier of the mission.
   */
  readonly missionNumber: string
  /**
   * Geolocation of the mission.
   */
  readonly gpsLocation: GpsLocation
  /**
   * Address of the mission.
   */
  readonly address: Address
  /**
   * Timestamp of the alarm of this mission.
   */
  readonly alarmDate: Date
  /**
   * The units assigned to this mission.
   */
  units: Unit[]
  /**
   * Type of the mission, e.g., "Brand".
   */
  readonly type: string
  /**
   * State of the mission, i.e., whether it is running or finished.
   */
  readonly state: MissionState
  /**
   * mission reports
   */
  readonly reports: SubReport[]
  /**
   * mission commands
   */
  readonly commands: SubCommand[]
  /**
   * danger zones
   * TODO: remove any type fix for DangerZone. If we add the correct type we got type collisions in other parts of the app. The root cause seem to be the GeoJson type inside the mission section. See https://github.com/lion5/smart-city-digimap/issues/322
   */
  readonly dangerZones: any[]
  /**
   * mission sections
   * TODO: remove any type fix for MissionSection. If we add the correct type we got type collisions in other parts of the app. The root cause seem to be the GeoJson type inside the mission section. See https://github.com/lion5/smart-city-digimap/issues/322
   */
  readonly missionSections: any[]
  /**
   * Points of interest of the mission.
   */
  readonly pointsOfInterest: PointOfInterest[]
  /**
   * URI to the mission in the REST API.
   */
  readonly uri: string
  /**
   * URI to the mission's units in the REST API.
   */
  readonly unitsUri?: string
  /**
   * keyword
   */
  readonly keyword?: string
  /**
   * cues
   */
  readonly cues?: string
  /*
   * The join code of the mission.
   */
  readonly joinCode?: string
  /*
   * The join pin of the mission.
   */
  readonly joinPin?: string

  constructor(
    missionId: string,
    missionNumber: string,
    gpsLocation: GpsLocation,
    address: Address,
    alarmDate: Date,
    type: string,
    units: Unit[] = [],
    state: MissionState,
    reports: SubReport[],
    commands: SubCommand[],
    dangerZones: DangerZone[],
    missionSections: MissionSection[],
    pointsOfInterest: PointOfInterest[],
    uri: string,
    unitsUri?: string,
    keyword?: string,
    cues?: string,
    joinCode?: string,
    joinPin?: string
  ) {
    this.missionId = missionId
    this.missionNumber = missionNumber
    this.gpsLocation = gpsLocation
    this.address = address
    this.alarmDate = alarmDate
    this.units = units
    this.state = state
    this.type = type
    this.reports = reports
    this.commands = commands
    this.dangerZones = dangerZones
    this.missionSections = missionSections
    this.pointsOfInterest = pointsOfInterest
    this.uri = uri
    this.unitsUri = unitsUri
    this.keyword = keyword
    this.cues = cues
    this.joinCode = joinCode
    this.joinPin = joinPin
  }

  static fromKettingState(state: State<ApiMission>) {
    const reports = state.data.reports || []
    const commands = state.data.commands || []
    const dangerZones = state.data.dangerZones || []
    const missionSections = state.data.sections || []
    const pois = state.data.pois || []

    return new Mission(
      state.data.missionId,
      state.data.missionNr,
      GpsLocation.fromApi(state.data.gpsLocation),
      Address.fromApi(state.data.missionAddress),
      new Date(state.data.alarmDate),
      state.data.missionTypeName,
      [],
      MissionState[state.data.state as MissionState],
      reports
        .map((report) => SubReport.fromMqtt(report))
        .filter((report) => !!report) as SubReport[],
      commands.map((command) => SubCommand.fromMqtt(command)),
      dangerZones.map((dangerZone) => DangerZone.fromApi(dangerZone)),
      missionSections.map((missionSection) =>
        MissionSection.fromApi(missionSection)
      ),
      pois.map((poi) => PointOfInterest.fromApi(poi)),
      state.uri,
      state.links.get('units')?.href,
      state.data.keyword,
      state.data.cues,
      state.data.joinCode,
      state.data.joinPin
    )
  }

  public setUnits(units: Unit[]) {
    this.units = units
  }

  public hasUnit(unit: Unit) {
    return this.units.find((u) => u.id === unit.id) !== undefined
  }

  public addUnit(unit: Unit) {
    if (this.hasUnit(unit)) {
      throw new Error('Unit already exists in this mission')
    }
    this.units.push(unit)
  }

  public removeUnit(unitId: string) {
    const index = this.units.findIndex((unit) => unit.id === unitId)
    if (index === -1) {
      throw new Error('Unit does not exist in this mission')
    }
    this.units.splice(index, 1)
  }

  public setUnitLocationById(unitId: string, newLocation: GpsLocation) {
    const unit = this.units.find((unit) => unit.id === unitId)
    if (!unit) {
      throw new Error('Could not update location of non-existing unit')
    }
    if (unit.gpsLocation.equals(newLocation)) {
      // ignore redundant location updates
      console.log('Ignored redundant location update of unit', {
        unit,
        newLocation,
      })
      return
    }
    unit.gpsLocation = newLocation
    console.log('Updated unit location', {
      unit,
      newLocation,
    })
  }

  public hasPoi(poi: PointOfInterest) {
    return this.pointsOfInterest.find((p) => p.id === poi.id) !== undefined
  }

  public addPoi(poi: PointOfInterest) {
    if (this.hasPoi(poi)) {
      throw new Error('POI already exists in this mission')
    }
    this.pointsOfInterest.push(poi)
  }
}

export interface ApiCreateMission {
  alarmDate: string
  missionNr: string
  missionType: number
  missionTypeName: string
  gpsLocation: ApiGpsLocation
  gkLocation: ApiGkLocation
  missionAddress: ApiAddress
  units: string[]
  radioGroup: string
  state: string
  keyword: string
  cues: string
}

export interface ApiMission extends ApiCreateMission {
  missionId: string
  reports: ISubReport[]
  commands: ISubCommand[]
  dangerZones: IApiDangerZone[]
  sections: IApiMissionSection[]
  pois: IApiPointOfInterest[]
  joinCode: string
  joinPin: string
}
