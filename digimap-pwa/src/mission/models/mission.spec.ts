import { beforeEach, describe, expect, it, vi } from 'vitest'
import { MockData } from '@/assets/mocks/data'
import { Mission } from '@/mission/models/mission'
import { GpsLocation } from '@lion5/component-library-leaflet'

describe('Mission', () => {
  const referenceMission = MockData.getMissions()[0]
  const otherMission = MockData.getMissions()[1]

  let mission: Mission
  beforeEach(() => {
    // create copy of mission and units to prevent shared references
    mission = new Mission(
      referenceMission.missionId,
      referenceMission.missionNumber,
      referenceMission.gpsLocation,
      referenceMission.address,
      referenceMission.alarmDate,
      referenceMission.type,
      [...referenceMission.units],
      referenceMission.state,
      [...referenceMission.reports],
      [...referenceMission.commands],
      [...referenceMission.dangerZones],
      [...referenceMission.missionSections],
      [...referenceMission.pointsOfInterest],
      referenceMission.uri
    )
  })

  describe('hasUnit', () => {
    it('should return true if unit exists', () => {
      const unitFromDifferentMission = otherMission.units[0]

      expect(mission.hasUnit(unitFromDifferentMission)).toBe(false)
    })
    it('should return false if unit does not exist', () => {
      const unitFromSameMission = mission.units[0]

      expect(mission.hasUnit(unitFromSameMission)).toBe(true)
    })
  })

  describe('addUnit', () => {
    it('should add unit to mission', () => {
      const unitFromDifferentMission = otherMission.units[0]
      const expectedUnitsLength = mission.units.length + 1

      mission.addUnit(unitFromDifferentMission)

      expect(mission.units.indexOf(unitFromDifferentMission)).not.toBe(-1)
      expect(mission.units.length).toBe(expectedUnitsLength)
    })
    it('should not add already existing unit', () => {
      const unitFromSameMission = mission.units[0]
      const expectedUnits = mission.units

      expect(() => mission.addUnit(unitFromSameMission)).toThrowError()

      expect(mission.units.indexOf(unitFromSameMission)).toBe(0)
      expect(mission.units).toStrictEqual(expectedUnits)
    })
  })

  describe('removeUnit', () => {
    it('should remove existing unit from mission', () => {
      const unitIdFromSameMission = mission.units[0].id

      mission.removeUnit(unitIdFromSameMission)

      expect(
        mission.units.find((unit) => unit.id === unitIdFromSameMission)
      ).toBeUndefined()
    })
    it('should not remove non-existing unit', () => {
      const unitIdFromDifferentMission = otherMission.units[0].id
      const expectedUnits = mission.units

      expect(() =>
        mission.removeUnit(unitIdFromDifferentMission)
      ).toThrowError()

      expect(mission.units).toStrictEqual(expectedUnits)
    })
  })

  describe('setUnitLocationById', () => {
    it('should update unit location of existing unit', () => {
      const updatedLocation = new GpsLocation(
        mission.units[0].gpsLocation.latitude + 1,
        mission.units[0].gpsLocation.longitude + 1
      )
      const unitIdFromSameMission = mission.units[0].id

      mission.setUnitLocationById(unitIdFromSameMission, updatedLocation)

      expect(mission.units[0].gpsLocation.equals(updatedLocation)).toBe(true)
    })
    it('should throw error when non-existing unitId is passed', () => {
      const updatedLocation = new GpsLocation(
        mission.units[0].gpsLocation.latitude + 1,
        mission.units[0].gpsLocation.longitude + 1
      )
      const unitIdFromDifferentMission = otherMission.units[0].id
      const expectedUnits = mission.units

      expect(() =>
        mission.setUnitLocationById(unitIdFromDifferentMission, updatedLocation)
      ).toThrowError()

      expect(mission.units).toStrictEqual(expectedUnits)
    })
    it('should log a message if update is redundant', () => {
      const sameLocation = new GpsLocation(
        mission.units[0].gpsLocation.latitude,
        mission.units[0].gpsLocation.longitude
      )
      const unitIdFromSameMission = mission.units[0].id
      const originalLocation = mission.units[0].gpsLocation
      const expectedUnits = mission.units
      console.log = vi.fn()

      mission.setUnitLocationById(unitIdFromSameMission, sameLocation)

      expect(console.log).toHaveBeenCalledOnce()
      expect(mission.units).toStrictEqual(expectedUnits)
      expect(mission.units[0].gpsLocation).toBe(originalLocation) // reference has not changed
    })
  })
})
