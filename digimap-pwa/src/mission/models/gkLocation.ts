export class GkLocation {
  readonly x: number
  readonly y: number
  readonly zone: number

  constructor(x: number, y: number, zone: number) {
    this.x = x
    this.y = y
    this.zone = zone
  }

  public static fromApi(data: ApiGkLocation) {
    return new GkLocation(data.x, data.y, data.zone)
  }
}

export interface ApiGkLocation {
  readonly x: number
  readonly y: number
  readonly zone: number
}
