import { mount } from '@vue/test-utils'
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest'
import MissionView from '@/mission/pages/MissionView.vue'
import { createTestingPinia } from '@pinia/testing'
import type { defineComponent } from 'vue'
import MissionInfoCard from '@/mission/components/MissionInfoCard.vue'
import { useMissionsStore } from '@/mission/stores/missions'
import CommandCard from '@/mission/components/CommandCard.vue'
import MiniMap from '@/dashboard/widgets/MissionLogWidget/MiniMap.vue'
import { MockData } from '@/assets/mocks/data'

const missions = MockData.getMissions()
vi.mock('vue-router', async () => {
  const actual = (await vi.importActual('vue-router')) as Record<
    string,
    unknown
  >
  return {
    ...actual,
    useRouter: vi.fn().mockReturnValue({
      push: vi.fn(),
    }),
    useRoute: vi.fn().mockReturnValue({
      params: {
        missionId: '1',
      },
    }),
  }
})

describe('MissionView', () => {
  let wrapper: ReturnType<typeof defineComponent>

  const pinia = createTestingPinia({
    initialState: {
      missions: {
        selectedMission: missions[0],
      },
    },
  })

  beforeEach(() => {
    const missionsStore = useMissionsStore()
    vi.spyOn(missionsStore, 'getMissionById').mockReturnValue(missions[0])
    wrapper = mount(MissionView, {
      global: { plugins: [pinia], stubs: ['MiniMap'] },
    })
  })

  afterEach(() => {
    wrapper.unmount()
  })

  it('should pass missions to MissionInfoCard', async () => {
    const missionInfoCard = wrapper.findComponent(MissionInfoCard)
    expect(missionInfoCard.props('mission')).toStrictEqual(missions[0])
  })

  it('should pass commands to CommandCard', async () => {
    const commandCard = wrapper.findComponent(CommandCard)
    expect(commandCard.props('commands')).toStrictEqual(missions[0].commands)
    expect(commandCard.props('reports')).toStrictEqual(missions[0].reports)
  })

  it('should pass the id, gps-location and tactical-sign of the mission to the map', () => {
    const map = wrapper.findComponent(MiniMap)
    expect(map.props('id')).toBe(`map-${missions[0].missionId}`)
    expect(map.props('gpsLocation')).toStrictEqual(missions[0].gpsLocation)
  })
})
