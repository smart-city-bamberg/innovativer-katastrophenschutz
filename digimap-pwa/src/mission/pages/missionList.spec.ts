import { mount } from '@vue/test-utils'
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest'
import MissionList from '@/mission/pages/MissionList.vue'
import { createTestingPinia } from '@pinia/testing'
import MissionFilter from '@/mission/components/MissionFilter.vue'
import type { defineComponent } from 'vue'
import { useMissionsStore } from '@/mission/stores/missions'
import { MockData } from '@/assets/mocks/data'

const missions = MockData.getMissions()

describe('MissionsList', () => {
  let wrapper: ReturnType<typeof defineComponent>

  const pinia = createTestingPinia({
    initialState: {
      missions: {
        allMissions: missions,
        currentMission: missions[1],
      },
    },
  })

  beforeEach(() => {
    wrapper = mount(MissionList, { global: { plugins: [pinia] } })
  })

  afterEach(() => {
    wrapper.unmount()
  })

  const missionStore = useMissionsStore()
  const setCurrentMissionSpy = vi
    .spyOn(missionStore, 'setCurrentMission')
    .mockReturnValue()

  it('should pass missions to mission card', async () => {
    const missionCards = wrapper.findAllComponents({ name: 'MissionCard' })
    expect(missionCards).toHaveLength(3)
    expect(missionCards[0].props('mission')).toStrictEqual(missions[0])
    expect(missionCards[1].props('mission')).toStrictEqual(missions[2])

    expect(missionCards[2].props('mission')).toStrictEqual(missions[1])
  })

  it('should filter missions correctly when @button-clicked is emitted', async () => {
    // Emit the @button-clicked event with 'buttonRunning' as the argument
    await wrapper
      .findComponent(MissionFilter)
      .vm.$emit('button-clicked', 'buttonRunning')

    // Wait for the DOM to update
    await wrapper.vm.$nextTick()

    // Check if only the running missions are displayed
    let missionCards = wrapper.findAllComponents({ name: 'MissionCard' })
    expect(missionCards).toHaveLength(2)
    expect(missionCards[0].props('mission')).toStrictEqual(missions[0])

    // Emit the @button-clicked event with 'buttonFinished' as the argument
    await wrapper
      .findComponent(MissionFilter)
      .vm.$emit('button-clicked', 'buttonFinished')

    // Wait for the DOM to update
    await wrapper.vm.$nextTick()

    // Check if only the finished missions are displayed
    missionCards = wrapper.findAllComponents({ name: 'MissionCard' })
    expect(missionCards).toHaveLength(1)
    expect(missionCards[0].props('mission')).toStrictEqual(missions[1])

    // Emit the @button-clicked event with '' as the argument
    await wrapper.findComponent(MissionFilter).vm.$emit('button-clicked', '')

    // Wait for the DOM to update
    await wrapper.vm.$nextTick()

    // Check if all missions are displayed
    missionCards = wrapper.findAllComponents({ name: 'MissionCard' })
    expect(missionCards).toHaveLength(3)
  })

  it('should change the current mission when @missionSelected is emitted', async () => {
    await wrapper
      .findComponent({ name: 'MissionCard' })
      .vm.$emit('missionSelected', missions[1].missionId)

    expect(setCurrentMissionSpy).toHaveBeenCalledTimes(1)
    expect(setCurrentMissionSpy).toHaveBeenCalledWith(missions[1])
  })

  it('should pass currentMissionId to MissionCard', () => {
    const missionCards = wrapper.findAllComponents({ name: 'MissionCard' })
    expect(missionCards[0].props('currentMissionId')).toBe(
      missions[1].missionId
    )
    expect(missionCards[1].props('currentMissionId')).toBe(
      missions[1].missionId
    )
  })
})
