import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest'
import { createTestingPinia } from '@pinia/testing'
import { defineComponent } from 'vue'
import { useMissionsStore } from '@/mission/stores/missions'
import { mount } from '@vue/test-utils'
import EditMission from '@/mission/pages/EditMission.vue'
import { Unit } from '@/unit/models/unit'
import UnitsTable from '@/mission/components/UnitsTable.vue'
import { MockData } from '@/assets/mocks/data'

const missions = MockData.getMissions()
const units = MockData.getAllUnits()
vi.mock('vue-router', async () => {
  const actual = (await vi.importActual('vue-router')) as Record<
    string,
    unknown
  >
  return {
    ...actual,
    useRouter: vi.fn().mockReturnValue({
      push: vi.fn(),
    }),
    useRoute: vi.fn().mockReturnValue({
      params: {
        missionId: '1',
      },
    }),
  }
})

describe('EditMission', () => {
  let wrapper: ReturnType<typeof defineComponent>
  const pinia = createTestingPinia({
    initialState: {
      missions: {
        selectedMission: missions[0],
      },
      positionUnit: {
        allUnits: units,
      },
    },
  })

  const missionsStore = useMissionsStore()
  vi.spyOn(missionsStore, 'getMissionById').mockReturnValue(missions[0])
  const addUnitsToMissionSpy = vi
    .spyOn(missionsStore, 'addUnitsToMission')
    .mockReturnValue(Promise.resolve())
  const removeUnitsFromMissionSpy = vi
    .spyOn(missionsStore, 'removeUnitsFromMission')
    .mockReturnValue(Promise.resolve())

  beforeEach(() => {
    wrapper = mount(EditMission, {
      global: {
        plugins: [pinia],
      },
    })
  })

  afterEach(() => {
    wrapper.unmount()
  })

  it('should pass the mission to the UnitsTable', () => {
    const unitsTable = wrapper.findComponent({ name: 'UnitsTable' })
    expect(unitsTable.props('selectedMission')).toStrictEqual(missions[0])
  })

  it('should pass units to the UnitsTable', async () => {
    const unitsTable = wrapper.findComponent({ name: 'UnitsTable' })
    expect(unitsTable.props('allUnits')).toStrictEqual(units)
  })

  it('should call add units when the save button is clicked', async () => {
    //add units[1] to the selectedUnits
    const selectedUnits = units.map((unit) => unit.id)
    wrapper
      .findComponent(UnitsTable)
      .vm.$emit('update:selectedUnits', selectedUnits)

    await wrapper.find('.save').trigger('click')

    expect(addUnitsToMissionSpy).toHaveBeenCalledWith(
      [units[2], units[3], units[4]],
      missions[0]
    )
  })

  it('should call remove units when the save button is clicked', async () => {
    //add units[1] to the selectedUnits
    const selectedUnits = [] as Unit[]
    wrapper
      .findComponent(UnitsTable)
      .vm.$emit('update:selectedUnits', selectedUnits)

    await wrapper.find('.save').trigger('click')

    expect(removeUnitsFromMissionSpy).toHaveBeenCalledWith(
      [units[0], units[1]],
      missions[0]
    )
  })
})
