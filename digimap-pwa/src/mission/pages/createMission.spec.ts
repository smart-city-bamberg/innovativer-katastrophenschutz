import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest'
import { flushPromises, mount } from '@vue/test-utils'
import type { defineComponent } from 'vue'
import { createTestingPinia } from '@pinia/testing'
import CreateMission from '@/mission/pages/CreateMission.vue'
import { useCreateMissionStore } from '@/mission/stores/createMission'
import { GeolocationInput, GpsLocation } from '@lion5/component-library-leaflet'
import waitForExpect from 'wait-for-expect'
import { MockData } from '@/assets/mocks/data'

vi.mock('@vue/router', () => ({
  useRouter: vi.fn().mockReturnValue({ push: vi.fn() }),
}))

describe('CreateMission', () => {
  let wrapper: ReturnType<typeof defineComponent>
  const missions = MockData.getMissions()
  const pinia = createTestingPinia()
  const createMissionStore = useCreateMissionStore()
  const publishMissionSpy = vi
    .spyOn(createMissionStore, 'publishMission')
    .mockReturnValue(Promise.resolve(missions[0]))

  beforeEach(() => {
    wrapper = mount(CreateMission, {
      global: {
        plugins: [pinia],
        stubs: {
          UnitsTable: true,
          LeafletMap: true,
        },
      },
    })
  })

  afterEach(() => {
    wrapper.unmount()
  })

  it('should render the form', () => {
    const form = wrapper.find('form')
    expect(form.exists()).toBe(true)
  })

  it('should render the input fields', () => {
    const inputs = wrapper.findAll('input')
    expect(inputs.length).toBe(10)
  })

  it('should call publishMission when the form is submitted', async () => {
    await wrapper.find('input[name="missionName"]').setValue('Test Mission')
    await wrapper.find('input[name="alarmDate"]').setValue('2022-12-31T23:59')
    await wrapper.find('input[name="street"]').setValue('Test Street')
    await wrapper.find('input[name="houseNumber"]').setValue('123')
    await wrapper.find('input[name="postalCode"]').setValue(12345)
    await wrapper.find('input[name="city"]').setValue('Test City')
    await wrapper.find('input[name="keyword"]').setValue('Test Keyword')
    await wrapper.find('input[name="cues"]').setValue('Test Cues')
    await wrapper.find('input[name="missionNumber"]').setValue('123456')

    await wrapper
      .findComponent(GeolocationInput)
      .vm.$emit('update:modelValue', new GpsLocation(0, 0))

    await wrapper.find('input[name="missionName"]').trigger('input')
    await wrapper.find('input[name="alarmDate"]').trigger('input')
    await wrapper.find('input[name="street"]').trigger('input')
    await wrapper.find('button[type="submit"]').trigger('submit')

    await flushPromises()
    await waitForExpect(() => {
      expect(publishMissionSpy).toHaveBeenCalled()
    })
  })
})
