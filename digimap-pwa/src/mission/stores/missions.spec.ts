import { beforeEach, describe, expect, it, vi } from 'vitest'
import { useMissionsStore } from '@/mission/stores/missions'
import { MissionService } from '@/mission/services/missionService'
import { MockData } from '@/assets/mocks/data'
import { createPinia, setActivePinia } from 'pinia'
import { User } from '@/account/models/user'
import { Role } from '@/account/models/role'

describe('useMissionsStore', () => {
  const mockMissions = MockData.getMissions()

  beforeEach(() => {
    setActivePinia(createPinia())
  })

  describe('methods', () => {
    it('fetchAllMissions - calls service and sets missions correctly', async () => {
      const store = useMissionsStore()
      MissionService.getAllMissions = vi
        .fn()
        .mockImplementation(() => mockMissions)

      await store.fetchAllMissions()

      expect(MissionService.getAllMissions).toHaveBeenCalledOnce()
      expect(store.allMissions).toStrictEqual(mockMissions)
    })

    it('setCurrentMission - updates currentMission', async () => {
      const store = useMissionsStore()

      store.setCurrentMission(mockMissions[0])

      expect(store.currentMission).toStrictEqual(mockMissions[0])
    })

    it('fetchUnitsOfCurrentMission - calls service and adds units to currentMission', async () => {
      const store = useMissionsStore()
      const expectedUnits = mockMissions[0].units
      MissionService.getMissionUnits = vi
        .fn()
        .mockImplementation(() => expectedUnits)

      store.setCurrentMission(mockMissions[0])

      await store.fetchUnitsOfCurrentMission()

      expect(MissionService.getMissionUnits).toHaveBeenCalledOnce()
      expect(MissionService.getMissionUnits).toHaveBeenCalledWith(
        mockMissions[0]
      )
      expect(store.currentMission).toBeDefined()
      expect(store.currentMission?.units).toStrictEqual(expectedUnits)
    })

    it('fetchUnitsOfCurrentMission - returns early if no currentMission is set', async () => {
      const store = useMissionsStore()
      MissionService.getMissionUnits = vi.fn()

      await store.fetchUnitsOfCurrentMission()

      expect(store.currentMission).toBeFalsy()
      expect(MissionService.getMissionUnits).toHaveBeenCalledTimes(0)
    })

    it('setCurrentMissionByUser - sets currentMission to expected mission and fetches units', async () => {
      const store = useMissionsStore()
      const user = new User('name', 'name@example.com', Role.USER, '1234')
      const expectedMission = mockMissions[0]
      MissionService.getMissionByUnitId = vi
        .fn()
        .mockImplementation(() => expectedMission)

      await store.setCurrentMissionByUser(user)

      expect(MissionService.getMissionByUnitId).toHaveBeenCalledOnce()
      expect(MissionService.getMissionByUnitId).toHaveBeenCalledWith(
        user.unitId
      )
      expect(store.currentMission).toStrictEqual(mockMissions[0])
      expect(store.currentMission?.units).toStrictEqual(mockMissions[0].units)
    })

    it('setCurrentMissionByUser - sets currentMission to null if user has no assigned unit', async () => {
      const store = useMissionsStore()
      const user = new User('name', 'name@example.com', Role.USER, null)
      MissionService.getMissionByUnitId = vi.fn()

      await store.setCurrentMissionByUser(user)

      expect(MissionService.getMissionByUnitId).not.toHaveBeenCalled()
      expect(store.currentMission).toBeUndefined()
    })
  })
})
