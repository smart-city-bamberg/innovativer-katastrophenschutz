import type { CreateMissionForm } from '@/mission/stores/createMission'
import { useCreateMissionStore } from '@/mission/stores/createMission'
import { createPinia, setActivePinia } from 'pinia'
import { beforeEach, describe, expect, it, vi } from 'vitest'
import { MissionService } from '@/mission/services/missionService'
import { GpsLocation } from '@lion5/component-library'
import { GkLocation } from '@/mission/models/gkLocation'
import { Address } from '@/base/models/address'

//create mock of MissionService createMission
vi.mock('@/mission/services/missionService', () => ({
  MissionService: {
    createMission: vi.fn(),
  },
}))

const formValues = {
  missionName: 'TestMission',
  alarmDate: '2021-12-12T12:00:00',
  street: 'TestStreet',
  houseNumber: '1',
  postalCode: 12345,
  city: 'TestCity',
  keyword: 'TestKeyword',
  cues: 'TestCues',
} as CreateMissionForm

describe('useCreateMissionStore', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
    vi.clearAllMocks()
  })

  describe('publishMission', () => {
    it('publishes correct payload', async () => {
      const createMissionStore = useCreateMissionStore()

      Date.prototype.toISOString = vi
        .fn()
        .mockReturnValue('2020-01-01T12:12:00.000Z')
      const expectedGpsLocation = new GpsLocation(0.0, 0.0)
      const expectedGkLocation = new GkLocation(0, 0, 0)
      const expectedAddress = new Address('TestStreet', '1', 12345, 'TestCity')
      const expectedAlarmDate = new Date(
        '2021-12-12T12:00:00.000Z'
      ).toISOString()

      const expectedApiMission = {
        alarmDate: expectedAlarmDate,
        missionNr: 'TestMission',
        missionType: 0,
        missionTypeName: '',
        gpsLocation: expectedGpsLocation,
        gkLocation: expectedGkLocation,
        missionAddress: expectedAddress,
        units: [],
        radioGroup: '',
        state: 'RUNNING',
        keyword: 'TestKeyword',
        cues: 'TestCues',
      }

      await createMissionStore.publishMission(formValues)

      expect(MissionService.createMission).toHaveBeenCalledOnce()
      expect(MissionService.createMission).toHaveBeenCalledWith(
        expectedApiMission
      )
    })
  })
})
