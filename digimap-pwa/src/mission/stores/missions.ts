import { defineStore } from 'pinia'
import type { Ref } from 'vue'
import { computed, ref } from 'vue'
import type { ApiCreateMission, Mission } from '@/mission/models/mission'
import { GpsLocation } from '@lion5/component-library-leaflet'
import { MissionService } from '@/mission/services/missionService'
import type { User } from '@/account/models/user'
import { Unit } from '@/unit/models/unit'
import { MissionState } from '@/mission/models/missionState'

export const DEFAULT_MISSION_LOCATION = new GpsLocation(49.8857, 10.8936)

export const useMissionsStore = defineStore('missions', () => {
  const allMissions = ref<Mission[]>([])
  const missions = computed<Mission[]>(() =>
    allMissions.value.filter((it) => it.state === MissionState.RUNNING)
  )
  const currentMission = ref<Mission | undefined>()
  const selectMissionModalVisible = ref<boolean>(false)
  const busy = ref<boolean>(false)
  const errorMessage = ref<string>('')

  const hasCurrentMission = computed((): boolean => !!currentMission.value)

  const infoMessage = computed((): string =>
    missions.value ? '' : 'Keine Einsätze vorhanden.'
  )

  async function fetchAllMissions() {
    try {
      busy.value = true
      errorMessage.value = ''
      allMissions.value = await MissionService.getAllMissions()
    } catch (error: unknown) {
      errorMessage.value =
        'Einsätze konnten nicht abgefragt werden. Bitte versuchen Sie es erneut.'
      throw new Error('Could not fetch missions: ' + error)
    } finally {
      busy.value = false
    }
  }

  function setCurrentMission(mission?: Mission) {
    currentMission.value = mission
  }

  function setUnitsOfCurrentMission(units: Unit[]) {
    if (!currentMission.value) {
      return
    }
    currentMission.value.setUnits(units)
  }

  const unitBelongsToCurrentMission = (unit: Unit): boolean => {
    if (!currentMission.value) {
      return false
    }
    return currentMission.value.hasUnit(unit)
  }

  const addUnit = (unit: Unit) => {
    if (!currentMission.value) {
      return
    }
    try {
      currentMission.value.addUnit(unit)
    } catch (e: unknown) {
      console.warn((e as Error).message, {
        mission: currentMission.value,
        unit,
        e,
      })
    }
  }

  const removeUnitById = (unitId: string) => {
    if (!currentMission.value) {
      return
    }
    try {
      currentMission.value.removeUnit(unitId)
    } catch (e: unknown) {
      console.warn((e as Error).message, {
        mission: currentMission.value,
        unitId,
        e,
      })
    }
  }

  const updateUnitLocationById = (unitId: string, newLocation: GpsLocation) => {
    if (!currentMission.value) {
      return
    }
    try {
      currentMission.value.setUnitLocationById(unitId, newLocation)
    } catch (e: unknown) {
      console.warn((e as Error).message, {
        mission: currentMission.value,
        unitId,
        newLocation,
      })
    }
  }

  async function updateMissionState(
    missionState: MissionState,
    mission: Mission
  ) {
    const updatedMission = await MissionService.updateMissionState(
      missionState,
      mission
    )
    if (updatedMission !== undefined) {
      const missionIndex = allMissions.value.findIndex(
        (it) => it.missionId === updatedMission.missionId
      )
      if (missionIndex !== -1) {
        allMissions.value[missionIndex] = updatedMission
      }
    }
  }

  async function fetchUnitsOfMission(mission: Ref<Mission | undefined>) {
    if (!mission.value) {
      return
    }
    busy.value = true
    errorMessage.value = ''
    try {
      const units = await MissionService.getMissionUnits(mission.value)
      mission.value.setUnits(units)
    } catch (error: unknown) {
      errorMessage.value =
        'Einheiten konnten nicht abgefragt werden. Bitte versuchen Sie es erneut.'
      console.error(
        'Could not fetch units of current mission',
        currentMission.value,
        error
      )
    } finally {
      busy.value = false
    }
  }

  async function fetchUnitsOfCurrentMission() {
    if (!currentMission.value) {
      return
    }
    busy.value = true
    errorMessage.value = ''
    try {
      const units = await MissionService.getMissionUnits(currentMission.value)
      setUnitsOfCurrentMission(units)
    } catch (error: unknown) {
      errorMessage.value =
        'Einheiten konnten nicht abgefragt werden. Bitte versuchen Sie es erneut.'
      console.error(
        'Could not fetch units of current mission',
        currentMission.value,
        error
      )
    } finally {
      busy.value = false
    }
  }

  async function setCurrentMissionByUser(user?: User): Promise<void> {
    if (!user || !user.unitId) {
      console.log('Mission not automatically selected:', {
        user: user,
      })
      currentMission.value = undefined
      return
    }
    // automatically set mission according to user's assigned unit
    try {
      currentMission.value = await MissionService.getMissionByUnitId(
        user.unitId
      )
      console.log('Automatically set current mission by unit id of user:', {
        currentMission: currentMission.value,
        user,
      })
    } catch (error: unknown) {
      console.log('Mission could not be automatically selected:', {
        user: user,
      })
    }
  }

  const getMissionById = (id?: string): Mission | undefined => {
    return allMissions.value.filter((it) => it.missionId == id).pop()
  }

  async function createMission(apiMission: ApiCreateMission) {
    const success = await MissionService.createMission(apiMission)
    if (success !== undefined) {
      const mission = await MissionService.fetchMissionById(success.missionId)
      if (mission !== undefined) {
        allMissions.value.push(mission)
        missions.value.push(mission)
        return mission
      } else {
        return undefined
      }
    } else {
      return undefined
    }
  }

  async function addUnitsToMission(units: Unit[], mission: Mission) {
    try {
      const success = await MissionService.addUnitsToMission(mission, units)

      if (success) {
        const selectedMission = allMissions.value.find(
          (it) => it.missionId === mission.missionId
        )
        for (const unit of units) {
          selectedMission?.addUnit(unit)
        }
      }
    } catch (error: unknown) {
      console.error('Could not add unit to mission:', error)
    }
  }

  async function removeUnitsFromMission(units: Unit[], mission: Mission) {
    try {
      const success = await MissionService.removeUnitsFromMission(
        mission,
        units
      )

      if (success) {
        const selectedMission = allMissions.value.find(
          (it) => it.missionId === mission.missionId
        )
        for (const unit of units) {
          selectedMission?.removeUnit(unit.id)
        }
      }
    } catch (error: unknown) {
      console.error('Could not remove unit from mission:', error)
    }
  }

  return {
    busy,
    allMissions,
    missions,
    currentMission,
    hasCurrentMission,
    selectMissionModalVisible,
    errorMessage,
    infoMessage,
    fetchAllMissions,
    getMissionById,
    setCurrentMission,
    fetchUnitsOfCurrentMission,
    fetchUnitsOfMission,
    setCurrentMissionByUser,
    setUnitsOfCurrentMission,
    addUnit,
    addUnitsToMission,
    removeUnitsFromMission,
    removeUnitById,
    updateUnitLocationById,
    updateMissionState,
    unitBelongsToCurrentMission,
    createMission,
  }
})
