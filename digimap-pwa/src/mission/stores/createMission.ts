import { defineStore } from 'pinia'
import { ref, toRaw } from 'vue'
import { date, object, string } from 'yup'
import { GpsLocation, useDate } from '@lion5/component-library'
import { Address } from '@/base/models/address'
import { MissionState } from '@/mission/models/missionState'
import { useMissionsStore } from '@/mission/stores/missions'
import { GkLocation } from '@/mission/models/gkLocation'
import type { ApiCreateMission } from '@/mission/models/mission'

export interface CreateMissionForm {
  missionName: string
  alarmDate: string
  street: string
  houseNumber: string
  postalCode: number
  city: string
  keyword: string
  cues: string
  missionNumber: string
  missionLocation: GpsLocation
}

export const useCreateMissionStore = defineStore('create-mission', () => {
  const missionName = ref<string>()
  const alarmDate = ref<string>()
  const street = ref<string>()
  const houseNumber = ref<string>()
  const postalCode = ref<number>()
  const city = ref<string>()
  const keyword = ref<string>()
  const cues = ref<string>()
  const missionNumber = ref<string>()
  const formErrorMessage = ref<string>()

  const { getDateTimeLocalInputValue } = useDate()
  const missionStore = useMissionsStore()
  const { createMission } = missionStore

  const schema = object({
    missionName: string().required(
      'Bitte geben sie einen Namen für den Einsatz an'
    ),
    alarmDate: date()
      .required('Bitte wählen Sie einen korrekten Zeitpunkt')
      // invalid dates would produce weird error message: https://github.com/jquense/yup/issues/764
      .transform((v) =>
        v instanceof Date && !isNaN(v as unknown as number) ? v : null
      ),
    street: string().required('Bitte geben Sie eine Straße an'),
    houseNumber: string().required('Bitte geben Sie eine Hausnummer an'),
    postalCode: string()
      .required('Bitte geben Sie eine Postleitzahl an')
      .length(5, 'Die Postleitzahl muss genau 5 Ziffern lang sein'),

    city: string().required('Bitte geben Sie eine Stadt an'),
    keyword: string().required('Bitte geben Sie ein Stichwort an'),
    cues: string().required('Bitte geben Sie ein Schlagwort an'),
    missionNumber: string().required('Bitte geben Sie eine Einsatznummer an'),
  })
  const initialValues = {
    missionName: '',
    alarmDate: getDateTimeLocalInputValue(new Date()),
    street: '',
    houseNumber: '',
    postalCode: undefined,
    city: '',
    keyword: '',
    cues: '',
    missionNumber: '',
    missionLocation: undefined,
  }

  const publishMission = async (formValues: CreateMissionForm) => {
    const address = new Address(
      formValues.street,
      formValues.houseNumber,
      formValues.postalCode,
      formValues.city
    )

    const apiMission: ApiCreateMission = {
      alarmDate: new Date(formValues.alarmDate).toISOString(),
      missionNr: formValues.missionName,
      missionType: 0,
      missionTypeName: '',
      gpsLocation:
        formValues.missionLocation === undefined
          ? new GpsLocation(0, 0)
          : toRaw(formValues.missionLocation),
      gkLocation: new GkLocation(0, 0, 0),
      missionAddress: address,
      units: [],
      radioGroup: '',
      state: MissionState.RUNNING,
      keyword: formValues.keyword,
      cues: formValues.cues,
    }

    return await createMission(apiMission)
  }

  const resetFormErrors = () => {
    formErrorMessage.value = ''
  }

  return {
    formSchema: schema,
    initialValues,
    missionName,
    alarmDate,
    street,
    houseNumber,
    postalCode,
    city,
    keyword,
    cues,
    missionNumber,
    formErrorMessage,
    resetFormErrors,
    publishMission,
  }
})
