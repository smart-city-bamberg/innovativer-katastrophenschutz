import { defineStore, storeToRefs } from 'pinia'
import { watch } from 'vue'
import { useMissionsStore } from '@/mission/stores/missions'
import { useReport } from '@/report/composables/useReport'
import { useCommand } from '@/command/composables/useCommand'
import { Mission } from '@/mission/models/mission'
import { useMissionLog } from '@/mission/composables/useMissionLog'

export const useMissionLogStore = defineStore('missionLog', () => {
  const { missionLog, reports, addMessage, getReport, getCommand } =
    useMissionLog()

  const { subscribe: subscribeReport, unsubscribe: unsubscribeReport } =
    useReport()
  const { subscribe: subscribeCommand, unsubscribe: unsubscribeCommand } =
    useCommand()
  const subscribe = (mission?: Mission | null) => {
    if (!mission?.missionId) return
    subscribeReport(mission?.missionId, addMessage)
    subscribeCommand(mission?.missionId, addMessage)
  }

  const unsubscribe = (missionId?: string) => {
    if (!missionId) return
    unsubscribeReport(missionId)
    unsubscribeCommand(missionId)
  }

  const { currentMission } = storeToRefs(useMissionsStore())
  const initMissionLog = () => {
    const reports = currentMission.value?.reports || []
    const commands = currentMission.value?.commands || []
    const messages = [...reports, ...commands].sort((messageA, messageB) => {
      return messageA.createdAt > messageB.createdAt ? 1 : -1
    })
    messages.forEach((message) => addMessage(message))
  }

  watch(
    currentMission,
    (newMission, oldMission) => {
      unsubscribe(oldMission?.missionId)
      initMissionLog()
      subscribe(newMission)
    },
    {
      immediate: true,
    }
  )

  return {
    missionLog,
    reports,
    getReport,
    getCommand,
  }
})
