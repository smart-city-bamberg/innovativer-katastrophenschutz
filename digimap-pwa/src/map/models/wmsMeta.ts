export interface WMSMeta {
  name: string
  url: string
  layer: string
}
