import { Control } from 'leaflet'
import 'leaflet.locatecontrol'
import 'leaflet.locatecontrol/dist/L.Control.Locate.min.css'
import 'leaflet-contextmenu'
import 'leaflet-contextmenu/dist/leaflet.contextmenu.css'

export const zoomControl = new Control.Zoom({
  position: 'topright',
  zoomInTitle: 'Vergrößern',
  zoomOutTitle: 'Verkleinern',
})
export const scaleControl = new Control.Scale({
  position: 'topright',
  imperial: false,
})
export const attributionControl = new Control.Attribution({
  position: 'bottomleft',
})

// location control (to center the map to the user location and show a marker)
export const locateControl = new Control.Locate({
  position: 'topright',
  showPopup: true,
  strings: {
    title: 'Auf aktuelle Position zentrieren',
    metersUnit: 'Meter',
    popup: 'Aktuelle Position (auf {distance} {unit} genau)',
  },
  onLocationError: (event) => {
    // TODO: maybe use custom component here
    window.alert(
      'Aktuelle Position konnte nicht ermittelt werden. Bitte stellen Sie sicher, dass die Ortung erlaubt ist. Falls Sie diese Option nicht sehen, laden Sie die Anwendung neu.'
    )
    console.error(event)
  },
})

export const SITUATION_MAP_CONTROLS = [
  zoomControl,
  scaleControl,
  attributionControl,
  locateControl,
]
