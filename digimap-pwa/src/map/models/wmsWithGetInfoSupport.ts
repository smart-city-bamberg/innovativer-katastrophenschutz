import type { Content, LatLngExpression } from 'leaflet'
import { Map, Popup, TileLayer } from 'leaflet'

// Source: https://gist.github.com/rclark/6908938
export class WMSTileLayer extends TileLayer.WMS {
  popup: Popup | undefined = undefined

  onAdd(map: Map) {
    // Triggered when the layer is added to a map.
    //   Register a click listener, then do all the upstream WMS things
    map.on('click', this.getFeatureInfo, this)
    return super.onAdd(map)
  }

  onRemove(map: Map) {
    // Triggered when the layer is removed from a map.
    //   Unregister a click listener, then do all the upstream WMS things
    map.off('click', this.getFeatureInfo, this)
    if (this.popup) {
      this.popup.remove()
    }
    return super.onRemove(map)
  }

  async getFeatureInfo(evt: { latlng: LatLngExpression }) {
    // Make an AJAX request to the server and hope for the best
    const url = this.getFeatureInfoUrl(evt.latlng)
    try {
      const response = await fetch(url)
      const text = await response.text()
      const doc = new DOMParser().parseFromString(text, 'text/html')
      if (doc.body.innerHTML.trim().length > 0) {
        this.showGetFeatureInfo(evt.latlng, text)
      }
    } catch (e) {
      //   DO Nothing
    }
  }

  getFeatureInfoUrl(latlng: LatLngExpression) {
    // Construct a GetFeatureInfo request URL given a point
    const point = this._map.latLngToContainerPoint(latlng)
    const size = this._map.getSize()
    const queryParams = new URLSearchParams({
      request: 'GetFeatureInfo',
      service: 'WMS',
      srs: 'EPSG:4326',
      styles: this.wmsParams.styles || '',
      transparent: this.wmsParams.transparent?.toString() || 'true',
      version: this.wmsParams.version?.toString() || '1.3.0',
      format: this.wmsParams.format as string,
      bbox: this._map.getBounds().toBBoxString(),
      height: size.y.toString(),
      width: size.x.toString(),
      layers: this.wmsParams.layers,
      query_layers: this.wmsParams.layers,
      info_format: 'text/html',
    })
    if (queryParams.get('version') === '1.3.0') {
      queryParams.set('i', point.x.toString())
      queryParams.set('j', point.y.toString())
    } else {
      queryParams.set('x', point.x.toString())
      queryParams.set('y', point.y.toString())
    }
    // @ts-ignore
    return `${this._url}?${queryParams.toString()}`
  }

  showGetFeatureInfo(latlng: LatLngExpression, content: Content) {
    if (this.popup) {
      this.popup.remove()
    }
    // Otherwise show the content in a popup, or something.
    this.popup = new Popup({ maxWidth: 800 })
      .setLatLng(latlng)
      .setContent(content)
      .openOn(this._map)
  }
}
