export interface WKEMeta {
  name: string
  url: string
  color: string
}
