import { useMissionsStore } from '@/mission/stores/missions'
import { defineStore, storeToRefs } from 'pinia'
import { computed, ref, watch } from 'vue'

export enum FilterOption {
  REPORTS = 'REPORTS',
  POI = 'POI',
  DANGER_ZONES = 'DANGER_ZONES',
  MISSION_SECTIONS = 'MISSION_SECTIONS',
}

export const useFilterStore = defineStore('filter', () => {
  const filter = ref<FilterOption[]>([
    FilterOption.REPORTS,
    FilterOption.POI,
    FilterOption.DANGER_ZONES,
    FilterOption.MISSION_SECTIONS,
  ])

  const { currentMission } = storeToRefs(useMissionsStore())
  const availableCallSigns = computed(
    () => currentMission.value?.units.map((unit) => unit.callSign) || []
  )
  const aidOrgFilter = ref<string[]>(availableCallSigns.value)
  watch(
    () => availableCallSigns.value,
    (newCallSigns, oldCallSigns) => {
      aidOrgFilter.value = aidOrgFilter.value.filter((callSign) =>
        newCallSigns.includes(callSign)
      )

      for (const callSign of newCallSigns) {
        if (!oldCallSigns.includes(callSign)) {
          aidOrgFilter.value.push(callSign)
        }
      }
    }
  )

  const setFilter = (newFilter: FilterOption[]) => {
    filter.value = newFilter
  }

  return {
    filter,
    aidOrgFilter,
    availableCallSigns,
    setFilter,
  }
})

export function useFilter(filterOption: FilterOption) {
  const { filter } = storeToRefs(useFilterStore())
  const displayFilterOption = computed(() =>
    filter.value.includes(filterOption)
  )

  return { displayFilterOption }
}

export function useAidOrgFilter() {
  const { aidOrgFilter } = storeToRefs(useFilterStore())

  const displayAidOrg = (aidOrg?: string) =>
    aidOrg && aidOrgFilter.value.includes(aidOrg)
  return { displayAidOrg }
}
