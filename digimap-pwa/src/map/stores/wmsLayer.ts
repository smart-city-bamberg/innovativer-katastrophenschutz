import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { WMSMeta } from '@/map/models/wmsMeta'

export const AVAILABLE_WMS_LAYER: WMSMeta[] = [
  {
    name: 'Überschwemmungsgebiete',
    url: 'https://www.lfu.bayern.de/gdi/wms/wasser/ueberschwemmungsgebiete',
    layer: 'festsetzung',
  },
  {
    name: 'Gefahrenhinweisbereich Rutschanfälligkeit',
    url: 'https://www.lfu.bayern.de/gdi/wms/geologie/georisiken',
    layer: 'ghk_rutschanf',
  },
  {
    name: 'GEORISK - Punktobjekte',
    url: 'https://www.lfu.bayern.de/gdi/wms/geologie/georisiken',
    layer: 'georisk_objekt',
  },
  {
    name: 'DWD Warnungen',
    url: 'https://maps.dwd.de/geoproxy_warnungen/service/',
    layer: 'Warnungen_Gemeinden_vereinigt',
    // options: {
    //   attribution: 'Warndaten: &copy; <a href="https://www.dwd.de">DWD</a>',
    //   sld: 'https://eigenerserver/alternativer.sld
    // }
  },
]

export const useWMSLayerStore = defineStore('wmsLayer', () => {
  const wmsLayerMetas = ref<WMSMeta[]>([])

  return {
    wmsLayerMetas,
  }
})
