import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { WKEMeta } from '@/map/models/wkeMeta'

export const AVAILABLE_WKE_LAYER: WKEMeta[] = [
  // {
  //   name: 'Sparkassen Lauf',
  //   url: '/wke/sparkassen-lauf.geojson',
  //   color: 'hsl(153,100%,40%)',
  // },
  // {
  //   name: 'Wieland Lauf',
  //   url: '/wke/wieland-lauf.geojson',
  //   color: 'hsl(190,100%,40%)',
  // },
  // {
  //   name: 'Brose Lauf',
  //   url: '/wke/brose-lauf.geojson',
  //   color: 'hsl(290,100%,40%)',
  // },
  // {
  //   name: 'Hydranten Bamberg',
  //   url: '/external-geojson/fire-hydrant.geojson',
  //   color: 'hsl(23,100%,40%)',
  // },
  {
    name: 'BR-Radl Tour Etappe 6',
    url: '/external-geojson/2024_BR-Radltour_Etappe_06.geojson',
    color: 'hsl(23,100%,40%)',
  },
]

export const useWKELayerStore = defineStore('wkeLayer', () => {
  const wkeLayerMetas = ref<WKEMeta[]>([])

  return {
    wkeLayerMetas,
  }
})
