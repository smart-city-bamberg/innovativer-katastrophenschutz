import type { GeoJSONOptions } from 'leaflet'
import { GeoJSON, Marker } from 'leaflet'
import { useGeoManUtils } from '@/map/shapes/composables/useGeoManUtils'
import { useGeoJSON } from '@lion5/component-library-leaflet'

export class MissionSectionGeoJSON extends GeoJSON {
  constructor(
    geojson: GeoJSON.FeatureCollection<any>,
    options?: GeoJSONOptions
  ) {
    const localOptions: GeoJSONOptions = {
      pmIgnore: false,
      markersInheritOptions: true,
      style: (feature) => {
        return {
          color: feature?.properties.color,
        }
      },
      pointToLayer: (feature, latlng) => {
        if (feature.properties.name) {
          const marker = new Marker(latlng).bindTooltip(
            feature.properties.name,
            { permanent: true }
          )
          marker.setOpacity(0)
          return marker
        }
        const { fixCircleMarkerCollision } = useGeoJSON()
        return fixCircleMarkerCollision(feature, latlng)
      },
    }
    super(geojson, {
      ...localOptions,
      ...options,
    })
    const { enableGeoManOnLayer } = useGeoManUtils()
    enableGeoManOnLayer(this)
    this.pm.setOptions({
      syncLayersOnDrag: true,
    })
  }

  addUpdateListener(
    callback: (missionSectionGeoJSON: MissionSectionGeoJSON) => void
  ) {
    this.on('pm:update', (e) => {
      callback(this)
    })
  }

  addRemoveListener(
    callback: (missionSectionGeoJSON: MissionSectionGeoJSON) => void
  ) {
    this.on('pm:remove', (e) => {
      callback(this)
    })
  }
}
