import { ShapeCommand } from '@/map/shapes/models/shapeCommand'
import type { GenericMqttPayload } from '@/mqtt/iMqttClient'
import { GeoJSON, LatLng } from 'leaflet'
import { v4 } from 'uuid'
import { ShapeType } from '@/map/shapes/models/shapeType'
import { markRaw } from 'vue'
import type { GeoJsonConvertible } from '@lion5/component-library-leaflet'
import { useGeoJSON, useShape } from '@lion5/component-library-leaflet'

export interface IMqttMissionSection {
  sectionId: string // uuid
  operation: string
  createdAt: string | number
  geoJson: string // geojson feature
}

export interface IMqttPubMissionSection extends IMqttMissionSection {
  senderUnitId: string
}

export interface IApiMissionSection {
  sectionId: string // uuid
  geoJson: string // geojson feature
}

const getNameTooltipGeoJson = (
  latLng: LatLng,
  name: string
): GeoJSON.Feature<any> => {
  return {
    type: 'Feature',
    properties: {
      name: name,
    },
    geometry: {
      coordinates: [latLng.lng, latLng.lat],
      type: 'Point',
    },
  }
}

const getMissionSectionGeoJsonFeatureCollection = (
  layer: GeoJsonConvertible,
  name: string,
  color: string
): GeoJSON.FeatureCollection<any> => {
  const { getGeoJson } = useGeoJSON()
  const rawDangerZoneGeoJSON = getGeoJson(layer) as GeoJSON.Feature
  if (!rawDangerZoneGeoJSON.properties) {
    throw new Error('GeoJson feature does not contain the properties attribute')
  }
  rawDangerZoneGeoJSON.properties.type = ShapeType.MISSION_SECTION
  rawDangerZoneGeoJSON.properties.color = color

  const features = [rawDangerZoneGeoJSON]
  const { getShapeCenter } = useShape()
  const center = getShapeCenter(layer)
  if (center) {
    features.push(getNameTooltipGeoJson(center, name))
  }

  return {
    type: 'FeatureCollection',
    features,
  }
}

export class MissionSection {
  readonly missionSectionId: string // uuid
  readonly operation: ShapeCommand
  readonly createdAt: Date
  readonly geoJson: GeoJSON.FeatureCollection<any>

  constructor(
    missionSectionId: string,
    operation: ShapeCommand,
    createdAt: Date,
    geoJson: GeoJSON.FeatureCollection<any>
  ) {
    this.missionSectionId = missionSectionId
    this.operation = operation
    this.createdAt = createdAt
    this.geoJson = markRaw(geoJson)
  }

  static fromLayer(layer: GeoJsonConvertible, name: string, color: string) {
    const rawGeoJson = getMissionSectionGeoJsonFeatureCollection(
      layer,
      name,
      color
    )
    return new MissionSection(
      v4(),
      ShapeCommand.CREATED,
      new Date(),
      rawGeoJson
    )
  }

  toMqtt(senderUnitId: string): IMqttPubMissionSection {
    return {
      sectionId: this.missionSectionId,
      senderUnitId,
      operation: this.operation.toString(),
      createdAt: this.createdAt.toISOString(),
      geoJson: JSON.stringify(this.geoJson),
    }
  }

  static fromApi(payload: IApiMissionSection) {
    return new MissionSection(
      payload.sectionId,
      ShapeCommand.CREATED,
      new Date(),
      JSON.parse(payload.geoJson)
    )
  }

  static fromMqtt(payload: GenericMqttPayload) {
    const typedPayload = payload as IMqttMissionSection
    const typedShapeCommandString =
      typedPayload.operation as keyof typeof ShapeCommand
    return new MissionSection(
      typedPayload.sectionId,
      ShapeCommand[typedShapeCommandString],
      new Date((typedPayload.createdAt as number) * 1000),
      JSON.parse(typedPayload.geoJson)
    )
  }
}
