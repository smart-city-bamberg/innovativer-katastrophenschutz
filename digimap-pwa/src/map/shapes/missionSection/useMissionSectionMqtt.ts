import { ref } from 'vue'
import { MqttJsClient } from '@/mqtt/mqttClient'
import type { GenericMqttPayload } from '@/mqtt/iMqttClient'
import { storeToRefs } from 'pinia'
import { useMissionsStore } from '@/mission/stores/missions'
import { useAccountStore } from '@/account/stores/account'
import { MissionSection } from '@/map/shapes/missionSection/models/missionSection'

const subscribedTopic = ref<string>('')

export function useMissionSectionMqtt() {
  const mqttClient: MqttJsClient = MqttJsClient.getInstance()
  const _getInboundTopic = (missionId: string) => {
    return `units/v1/inbound/${missionId}/section`
  }
  const _getOutboundTopic = (missionId: string) => {
    return `units/v1/outbound/${missionId}/section`
  }

  const subscribe = (
    missionId: string,
    callback: (missionSection: MissionSection) => void
  ) => {
    const relativeTopic = _getOutboundTopic(missionId)
    if (subscribedTopic.value === relativeTopic) {
      // topic of this mission is already subscribed
      return
    }
    mqttClient.subscribe(
      relativeTopic,
      (_topic, payload: GenericMqttPayload) => {
        try {
          const missionSection = MissionSection.fromMqtt(payload)
          console.info('New Mission Section Message', missionSection)
          callback(missionSection)
        } catch (e) {
          console.error('Cannot execute callback. Unknown error', e)
        }
      }
    )
    subscribedTopic.value = relativeTopic
  }

  const unsubscribe = (missionId: string) => {
    const topic = _getOutboundTopic(missionId)
    if (subscribedTopic.value !== topic) return
    mqttClient.unsubscribe(topic)
    subscribedTopic.value = ''
  }

  const publish = async (missionSection: MissionSection) => {
    const { currentMission } = storeToRefs(useMissionsStore())
    const { currentUser } = storeToRefs(useAccountStore())
    const unitId = currentUser.value?.unitId
    const missionId = currentMission.value?.missionId
    if (!unitId) {
      throw new Error(
        'Keine Einheit zugewiesen. Bitte kontaktieren Sie einen System-Administrator.'
      )
    }
    if (!missionId) {
      throw new Error(
        'Kein Einsatz ausgewählt. Bitte wählen Sie einen Einsatz in den Einstellungen oder auf der Lagekarte.'
      )
    }

    mqttClient.publish(
      _getInboundTopic(missionId),
      missionSection.toMqtt(unitId)
    )
  }

  return { subscribe, unsubscribe, publish }
}
