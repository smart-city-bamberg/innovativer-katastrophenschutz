import '@geoman-io/leaflet-geoman-free'
import '@geoman-io/leaflet-geoman-free/dist/leaflet-geoman.css'
import { computed, ref, watch } from 'vue'
import { ShapeCommand } from '@/map/shapes/models/shapeCommand'
import { defineStore, storeToRefs } from 'pinia'
import { useMissionsStore } from '@/mission/stores/missions'
import { MissionSection } from '@/map/shapes/missionSection/models/missionSection'
import { useMissionSectionMqtt } from '@/map/shapes/missionSection/useMissionSectionMqtt'
import { useFilterStore } from '@/map/stores/filter'

export const useMissionSectionStore = defineStore('missionSectionStore', () => {
  const missionSections = ref(new Map<string, MissionSection>())
  const { unsubscribe, subscribe, publish } = useMissionSectionMqtt()
  const publishMissionSection = async (missionSection: MissionSection) => {
    processMissionSectionMessage(missionSection)
    await publish(missionSection)
  }
  const processMissionSectionMessage = (missionSection: MissionSection) => {
    switch (missionSection.operation) {
      case ShapeCommand.CREATED:
        return addMissionSection(missionSection)
      case ShapeCommand.UPDATED:
        return updateMissionSection(missionSection)
      case ShapeCommand.DELETED:
        return deleteMissionSection(missionSection.missionSectionId)
      default:
        console.error(
          `Unknown danger zone operation ${missionSection.operation}.`
        )
    }
  }

  const addMissionSection = (missionSection: MissionSection) => {
    if (missionSections.value.has(missionSection.missionSectionId)) return
    missionSections.value.set(missionSection.missionSectionId, missionSection)
  }
  const updateMissionSection = (missionSection: MissionSection) => {
    const oldMissionSection = missionSections.value.get(
      missionSection.missionSectionId
    )
    if (!oldMissionSection) {
      return addMissionSection(missionSection)
    }
    // The updated mission section was updated by this client
    if (oldMissionSection.createdAt === missionSection.createdAt) return
    missionSections.value.set(missionSection.missionSectionId, missionSection)
  }
  const deleteMissionSection = (missionSectionId: string) => {
    const missionSection = missionSections.value.get(missionSectionId)
    if (!missionSection) return
    missionSections.value.delete(missionSectionId)
  }

  const { currentMission } = storeToRefs(useMissionsStore())
  const initMissionLog = () => {
    missionSections.value = new Map()
    const missionSection =
      (currentMission.value?.missionSections as MissionSection[]) || []
    missionSection.forEach((missionSection) =>
      processMissionSectionMessage(missionSection)
    )
  }
  watch(
    currentMission,
    (newMission, oldMission) => {
      if (oldMission?.missionId) {
        unsubscribe(oldMission.missionId)
      }
      initMissionLog()
      if (newMission) {
        subscribe(newMission.missionId, (missionSection) =>
          processMissionSectionMessage(missionSection)
        )
      }
    },
    {
      immediate: true,
    }
  )

  return {
    processMissionSectionMessage,
    publishMissionSection,
    missionSections,
  }
})
