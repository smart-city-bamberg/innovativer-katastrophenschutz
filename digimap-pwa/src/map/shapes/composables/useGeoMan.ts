import { Map, PM } from 'leaflet'
import { ref, shallowRef, watch } from 'vue'
import '@geoman-io/leaflet-geoman-free'
import '@geoman-io/leaflet-geoman-free/dist/leaflet-geoman.css'
import { ButtonControl } from '@lion5/component-library-leaflet'

export function useGeoMan() {
  const isActive = ref<boolean>(false)
  const geoManControl = new ButtonControl(
    isActive,
    'bi-bounding-box-circles',
    'Gefahrenbereiche & Einsatzbereiche einzeichnen'
  )
  const leafletMap = shallowRef<Map | undefined>(undefined)
  const enableGeoMan = async (map: Map) => {
    leafletMap.value = map
    // geoman ignores all map objects (nothing can be dragged / edited) by default
    PM.setOptIn(true)
  }
  watch(isActive, (newIsActive) => {
    if (!leafletMap.value) {
      console.error(
        'leafletMap is not initialized. When using this composable, do not forget to call enableGeoMan.'
      )
      return
    }
    if (newIsActive) {
      showControls(leafletMap.value)
    } else {
      hideControls(leafletMap.value)
    }
  })

  const showControls = (map: Map) => {
    map.pm.addControls({
      position: 'topleft',
      drawMarker: false,
      drawCircleMarker: false,
      drawText: false,
      drawPolyline: false,
      cutPolygon: false,
    })
  }

  const hideControls = (map: Map) => {
    map.pm.removeControls()
  }

  return { enableGeoMan, geoManControl }
}
