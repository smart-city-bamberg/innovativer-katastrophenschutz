import { ref, shallowRef } from 'vue'
import type { GeoJsonConvertible } from '@lion5/component-library-leaflet'
import { useShape } from '@lion5/component-library-leaflet'

export function useShapeCreatorControl() {
  const shapeModalDisplayed = ref(false)
  const currentShape = shallowRef<GeoJsonConvertible | undefined>(undefined)
  const onAddShape = (layer: GeoJsonConvertible) => {
    shapeModalDisplayed.value = true
    currentShape.value = layer
  }

  const { removeShape } = useShape()
  const onFinishAddShape = () => {
    if (!currentShape.value) return
    removeShape(currentShape.value)
    currentShape.value = undefined
    shapeModalDisplayed.value = false
  }
  return {
    shapeModalDisplayed,
    currentShape,
    onAddShape,
    onFinishAddShape,
  }
}
