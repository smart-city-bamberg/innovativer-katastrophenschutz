import { Circle, GeoJSON, Layer, Map, PM, Polygon, Rectangle } from 'leaflet'
import type { GeoJsonConvertible } from '@lion5/component-library-leaflet'
import '@geoman-io/leaflet-geoman-free'

export function useGeoManUtils() {
  const enableGeoManOnLayer = (layer: Layer | GeoJSON) => {
    const layers = layer instanceof GeoJSON ? layer.getLayers() : [layer]
    for (const layer of layers) {
      layer.options.pmIgnore = false
      PM.reInitLayer(layer)
    }
  }

  const registerShapeCreateListener = (
    map: Map,
    callback: (layer: GeoJsonConvertible) => void
  ) => {
    map.on('pm:create', (e) => {
      if (
        !(
          e.layer instanceof Polygon ||
          e.layer instanceof Rectangle ||
          e.layer instanceof Circle
        )
      ) {
        console.error(`Unknown layer type ${e.shape}`)
        return
      }
      callback(e.layer)
    })
  }

  return { enableGeoManOnLayer, registerShapeCreateListener }
}
