import '@geoman-io/leaflet-geoman-free'
import '@geoman-io/leaflet-geoman-free/dist/leaflet-geoman.css'
import { ref, watch } from 'vue'
import { DangerZone } from '@/map/shapes/dangerZone/models/dangerZone'
import { ShapeCommand } from '@/map/shapes/models/shapeCommand'
import { defineStore, storeToRefs } from 'pinia'
import { useMissionsStore } from '@/mission/stores/missions'
import { useDangerZoneMqtt } from '@/map/shapes/dangerZone/useDangerZoneMqtt'

export const useDangerZoneStore = defineStore('dangerZoneStore', () => {
  const dangerZones = ref(new Map<string, DangerZone>())
  const { unsubscribe, subscribe, publish } = useDangerZoneMqtt()
  const publishDangerZone = async (dangerZone: DangerZone) => {
    processDangerZoneMessage(dangerZone)
    await publish(dangerZone)
  }
  const processDangerZoneMessage = (dangerZone: DangerZone) => {
    switch (dangerZone.operation) {
      case ShapeCommand.CREATED:
        return addDangerZone(dangerZone)
      case ShapeCommand.UPDATED:
        return updateDangerZone(dangerZone)
      case ShapeCommand.DELETED:
        return deleteDangerZone(dangerZone.dangerZoneId)
      default:
        console.error(`Unknown danger zone operation ${dangerZone.operation}.`)
    }
  }

  const addDangerZone = (dangerZone: DangerZone) => {
    if (dangerZones.value.has(dangerZone.dangerZoneId)) return
    dangerZones.value.set(dangerZone.dangerZoneId, dangerZone)
  }
  const updateDangerZone = (dangerZone: DangerZone) => {
    const oldDangerZone = dangerZones.value.get(dangerZone.dangerZoneId)
    if (!oldDangerZone) {
      return addDangerZone(dangerZone)
    }
    // The updated danger zone was updated by this client
    if (oldDangerZone.createdAt === dangerZone.createdAt) return
    dangerZones.value.set(dangerZone.dangerZoneId, dangerZone)
  }
  const deleteDangerZone = (dangerZoneId: string) => {
    const dangerZone = dangerZones.value.get(dangerZoneId)
    if (!dangerZone) return
    dangerZones.value.delete(dangerZoneId)
  }

  const { currentMission } = storeToRefs(useMissionsStore())
  const initDangerZones = () => {
    dangerZones.value = new Map()
    const localDangerZones =
      (currentMission.value?.dangerZones as DangerZone[]) || []
    localDangerZones.forEach((dangerZone) =>
      processDangerZoneMessage(dangerZone)
    )
  }
  watch(
    currentMission,
    (newMission, oldMission) => {
      if (oldMission?.missionId) {
        unsubscribe(oldMission.missionId)
      }
      initDangerZones()
      if (newMission) {
        subscribe(newMission.missionId, (dangerZone) =>
          processDangerZoneMessage(dangerZone)
        )
      }
    },
    {
      immediate: true,
    }
  )

  return { processDangerZoneMessage, publishDangerZone, dangerZones }
})
