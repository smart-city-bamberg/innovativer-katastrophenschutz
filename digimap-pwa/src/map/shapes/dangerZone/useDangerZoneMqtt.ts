import { ref } from 'vue'
import { MqttJsClient } from '@/mqtt/mqttClient'
import { DangerZone } from '@/map/shapes/dangerZone/models/dangerZone'
import type { GenericMqttPayload } from '@/mqtt/iMqttClient'
import { storeToRefs } from 'pinia'
import { useMissionsStore } from '@/mission/stores/missions'
import { useAccountStore } from '@/account/stores/account'

const subscribedTopic = ref<string>('')

export function useDangerZoneMqtt() {
  const mqttClient: MqttJsClient = MqttJsClient.getInstance()
  const _getInboundTopic = (missionId: string) => {
    return `units/v1/inbound/${missionId}/dangerZone`
  }
  const _getOutboundTopic = (missionId: string) => {
    return `units/v1/outbound/${missionId}/dangerZone`
  }

  const subscribe = (
    missionId: string,
    callback: (dangerZone: DangerZone) => void
  ) => {
    const relativeTopic = _getOutboundTopic(missionId)
    if (subscribedTopic.value === relativeTopic) {
      // topic of this mission is already subscribed
      return
    }
    mqttClient.subscribe(
      relativeTopic,
      (_topic, payload: GenericMqttPayload) => {
        try {
          const dangerZone = DangerZone.fromMqtt(payload)
          console.info('New Danger Zone Message', dangerZone)
          callback(dangerZone)
        } catch (e) {
          console.error('Cannot execute callback. Unknown error', e)
        }
      }
    )
    subscribedTopic.value = relativeTopic
  }

  const unsubscribe = (missionId: string) => {
    const topic = _getOutboundTopic(missionId)
    if (subscribedTopic.value !== topic) return
    mqttClient.unsubscribe(topic)
    subscribedTopic.value = ''
  }

  const publish = async (dangerZone: DangerZone) => {
    const { currentMission } = storeToRefs(useMissionsStore())
    const { currentUser } = storeToRefs(useAccountStore())
    const unitId = currentUser.value?.unitId
    const missionId = currentMission.value?.missionId
    if (!unitId) {
      throw new Error(
        'Keine Einheit zugewiesen. Bitte kontaktieren Sie einen System-Administrator.'
      )
    }
    if (!missionId) {
      throw new Error(
        'Kein Einsatz ausgewählt. Bitte wählen Sie einen Einsatz in den Einstellungen oder auf der Lagekarte.'
      )
    }

    mqttClient.publish(_getInboundTopic(missionId), dangerZone.toMqtt(unitId))
  }

  return { subscribe, unsubscribe, publish }
}
