import { TacticalSign } from '@/base/models/tacticalSign'
import { ShapeCommand } from '@/map/shapes/models/shapeCommand'
import type { GenericMqttPayload } from '@/mqtt/iMqttClient'
import { GeoJSON, LatLng } from 'leaflet'
import { v4 } from 'uuid'
import { ShapeType } from '@/map/shapes/models/shapeType'
import { markRaw } from 'vue'
import type { GeoJsonConvertible } from '@lion5/component-library-leaflet'
import { useGeoJSON, useShape } from '@lion5/component-library-leaflet'

export interface IMqttDangerZone {
  dangerZoneId: string // uuid
  operation: string
  createdAt: string | number
  geoJson: string // stringified geojson feature
}

export interface IMqttPubDangerZone extends IMqttDangerZone {
  senderUnitId: string
}

export interface IApiDangerZone {
  dangerZoneId: string // uuid
  geoJson: string // stringified geojson feature
}

const getTacticalSignMarkerGeoJson = (
  latLng: LatLng,
  tacticalSign: TacticalSign,
  label: string
): GeoJSON.Feature<any> => {
  return {
    type: 'Feature',
    properties: {
      tacticalSignPath: tacticalSign.relativePath,
      label,
    },
    geometry: {
      coordinates: [latLng.lng, latLng.lat],
      type: 'Point',
    },
  }
}

const getDangerZoneGeoJsonFeatureCollection = (
  layer: GeoJsonConvertible,
  label: string,
  tacticalSign: TacticalSign
): GeoJSON.FeatureCollection<any> => {
  const { getGeoJson } = useGeoJSON()
  const rawDangerZoneGeoJSON = getGeoJson(layer) as GeoJSON.Feature
  if (!rawDangerZoneGeoJSON.properties) {
    throw new Error('GeoJson feature does not contain the properties attribute')
  }
  rawDangerZoneGeoJSON.properties.type = ShapeType.DANGER_ZONE

  const features = [rawDangerZoneGeoJSON]
  const { getShapeCenter } = useShape()
  const center = getShapeCenter(layer)
  if (center) {
    features.push(getTacticalSignMarkerGeoJson(center, tacticalSign, label))
  }

  return {
    type: 'FeatureCollection',
    features,
  }
}

export class DangerZone {
  readonly dangerZoneId: string // uuid
  readonly operation: ShapeCommand
  readonly createdAt: Date
  readonly geoJson: GeoJSON.FeatureCollection<any>

  constructor(
    dangerZoneId: string,
    operation: ShapeCommand,
    createdAt: Date,
    geoJson: any
  ) {
    this.dangerZoneId = dangerZoneId
    this.operation = operation
    this.createdAt = createdAt
    this.geoJson = markRaw(geoJson)
  }

  static fromLayer(
    layer: GeoJsonConvertible,
    label: string,
    tacticalSign: TacticalSign
  ) {
    const rawGeoJson = getDangerZoneGeoJsonFeatureCollection(
      layer,
      label,
      tacticalSign
    )

    return new DangerZone(v4(), ShapeCommand.CREATED, new Date(), rawGeoJson)
  }

  toMqtt(senderUnitId: string): IMqttPubDangerZone {
    return {
      dangerZoneId: this.dangerZoneId,
      senderUnitId,
      operation: this.operation.toString(),
      createdAt: this.createdAt.toISOString(),
      geoJson: JSON.stringify(this.geoJson),
    }
  }

  static fromApi(payload: IApiDangerZone) {
    return new DangerZone(
      payload.dangerZoneId,
      ShapeCommand.CREATED,
      new Date(),
      JSON.parse(payload.geoJson)
    )
  }

  static fromMqtt(payload: GenericMqttPayload) {
    const typedPayload = payload as IMqttDangerZone
    const typedShapeCommandString =
      typedPayload.operation as keyof typeof ShapeCommand
    return new DangerZone(
      typedPayload.dangerZoneId,
      ShapeCommand[typedShapeCommandString],
      new Date((typedPayload.createdAt as number) * 1000),
      JSON.parse(typedPayload.geoJson)
    )
  }
}
