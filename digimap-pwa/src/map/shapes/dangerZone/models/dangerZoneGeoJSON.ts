import type { GeoJSONOptions } from 'leaflet'
import { GeoJSON } from 'leaflet'
import { TacticalSign } from '@/base/models/tacticalSign'
import { useGeoManUtils } from '@/map/shapes/composables/useGeoManUtils'
import { useGeoJSON } from '@lion5/component-library-leaflet'
import { useTacticalSignMarker } from '@/map/composables/useTacticalSignMarker'

export class DangerZoneGeoJSON extends GeoJSON {
  constructor(
    geojson: GeoJSON.FeatureCollection<any>,
    options?: GeoJSONOptions
  ) {
    const localOptions: GeoJSONOptions = {
      pmIgnore: false,
      markersInheritOptions: true,
      style: (feature) => ({
        color: 'var(--color-danger)',
      }),
      pointToLayer: (feature, latlng) => {
        const { getTacticalSignMarker } = useTacticalSignMarker()
        if (feature.properties.tacticalSignPath) {
          return getTacticalSignMarker(
            latlng,
            new TacticalSign(feature.properties.tacticalSignPath),
            feature.properties.label
          )
        }
        const { fixCircleMarkerCollision } = useGeoJSON()
        return fixCircleMarkerCollision(feature, latlng)
      },
    }
    super(geojson, {
      ...localOptions,
      ...options,
    })
    const { enableGeoManOnLayer } = useGeoManUtils()
    enableGeoManOnLayer(this)
    this.pm.setOptions({
      syncLayersOnDrag: true,
    })
  }

  addUpdateListener(callback: (dangerZoneGeoJSON: DangerZoneGeoJSON) => void) {
    this.on('pm:update', (e) => {
      callback(this)
    })
  }

  addRemoveListener(callback: (dangerZoneGeoJSON: DangerZoneGeoJSON) => void) {
    this.on('pm:remove', (e) => {
      callback(this)
    })
  }
}
