import type { ContextMenuItem } from 'leaflet'
import { Map } from 'leaflet'

export const useSituationMapContextMenu = () => {
  const addContextMenuItems = (
    map: Map,
    contextMenuItems: ContextMenuItem[]
  ) => {
    for (const itemConfig of contextMenuItems) {
      ;(map as MapWithCustomProps).contextmenu.addItem(itemConfig)
    }
  }

  return { addContextMenuItems }
}
