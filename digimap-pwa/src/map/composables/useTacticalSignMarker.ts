import { LatLng } from 'leaflet'
import { useLeafletMarker } from '@lion5/component-library-leaflet'
import { useLeafletPopup } from '@/map/composables/useLeafletPopup'
import { TacticalSign } from '@/base/models/tacticalSign'

export function useTacticalSignMarker() {
  const getTacticalSignMarker = (
    position: LatLng,
    tacticalSign: TacticalSign,
    label: string
  ) => {
    const { getMarkerWithPopup } = useLeafletMarker()
    const { createPopupWithButtons } = useLeafletPopup()
    return getMarkerWithPopup(
      position,
      tacticalSign.path,
      undefined,
      undefined,
      {},
      createPopupWithButtons(label, tacticalSign.name || '', [])
    )
  }

  return {
    getTacticalSignMarker,
  }
}
