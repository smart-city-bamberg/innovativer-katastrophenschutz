import { storeToRefs } from 'pinia'
import { useMissionLogStore } from '@/mission/stores/missionLog'
import { useMissionsStore } from '@/mission/stores/missions'
import { GpsLocation } from '@lion5/component-library-leaflet'
import { useCreateReportStore } from '@/report/stores/createReport'
import type { Mission } from '@/mission/models/mission'
import type { Ref } from 'vue'
import { usePositionUnitStore } from '@/unit/stores/positionUnit'
import { useCreatePoiStore } from '@/poi/stores/createPoi'
import { useRouter } from 'vue-router'

export function useSituationMap() {
  const router = useRouter()
  const { reports } = storeToRefs(useMissionLogStore())
  const { currentMission }: { currentMission: Ref<Mission | undefined> } =
    storeToRefs(useMissionsStore())

  const onCreateReportContextMenuClick = async (location: GpsLocation) => {
    if (!location) {
      return
    }
    const { setReportLocation } = useCreateReportStore()
    setReportLocation(location)
    await router.push({ name: 'CreateReport' })
  }

  const onCreatePoiClick = async (location: GpsLocation) => {
    if (!location) {
      return
    }
    const { setLocation } = useCreatePoiStore()
    setLocation(location)
    await router.push({ name: 'CreatePoi' })
  }

  const onAddUnitClick = async (location: GpsLocation) => {
    if (!location) {
      return
    }
    const { setUnitLocation } = usePositionUnitStore()
    setUnitLocation(location)
    await router.push({ name: 'PositionUnit' })
  }

  return {
    reports,
    currentMission,
    onCreateReportContextMenuClick,
    onAddUnitClick,
    onCreatePoiClick,
  }
}
