import { Control, divIcon, Map } from 'leaflet'
import 'leaflet-measure/dist/leaflet-measure.de.js'
import 'leaflet-measure/dist/leaflet-measure.css'

export function useLeafletMeasure() {
  // Fix for Measure lib to work with new leaflet version. See https://github.com/ljagis/leaflet-measure/issues/171#issuecomment-1137483548
  // @ts-ignore
  Control.Measure.include({
    // set icon on the capture marker
    _setCaptureMarkerIcon: function () {
      // disable autopan
      this._captureMarker.options.autoPanOnFocus = false

      // default function
      this._captureMarker.setIcon(
        divIcon({
          iconSize: this._map.getSize().multiplyBy(2),
        })
      )
    },
  })
  // @ts-ignore
  const measureControl = new Control.Measure({
    position: 'topright',
    primaryLengthUnit: 'meters',
    primaryAreaUnit: 'sqmeters',
    activeColor: 'var(--color-primary)',
    completedColor: 'var(--color-neutral-800)',
    decPoint: ',',
    thousandsSep: '.',
    labels: {
      measure: 'Messen',
      measureDistancesAndAreas: 'Entfernungen und Flächen messen',
      createNewMeasurement: 'Neue Messung erstellen',
      startCreating: 'Füge Punkte der Karte hinzu, um eine Messung zu starten',
      finishMeasurement: 'Messung beenden',
      lastPoint: 'Letzter Punkt',
      area: 'Fläche',
      perimeter: 'Perimeter',
      pointLocation: 'Point location',
      areaMeasurement: 'Flächenmessung',
      linearMeasurement: 'Steckenmessung',
      pathDistance: 'Pfadentfernung',
      centerOnArea: 'Fläche zentrieren',
      centerOnLine: 'Stecke zentrieren',
      centerOnLocation: 'Standort zentrieren',
      cancel: 'Abrechen',
      delete: 'Löschen',
      acres: 'Acres',
      feet: 'Feet',
      kilometers: 'Kilometer',
      hectares: 'Hektar',
      meters: 'Meter',
      miles: 'Miles',
      sqfeet: 'Sq Feet',
      sqmeters: 'Quadratmeter',
      sqmiles: 'Sq Miles',
      decPoint: ',',
      thousandsSep: '.',
    },
  })
  return { measureControl }
}
