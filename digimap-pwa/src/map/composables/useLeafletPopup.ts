import type { LatLng, Map } from 'leaflet'
import { DomEvent, DomUtil, Popup } from 'leaflet'

export interface PopupButton {
  label: string
  cls: string
  handler: () => void
}

export function useLeafletPopup() {
  const showLocationInfoPopup = (map: Map, latLng: LatLng) => {
    new Popup(latLng)
      .setContent(
        `<span><b>Breitengrad:</b> ${latLng.lat}</span><br />` +
          `<span><b>Längengrad:</b> ${latLng.lng}</span>`
      )
      .openOn(map)
  }

  const createPopupWithButtons = (
    title: string,
    body: string,
    buttons: PopupButton[]
  ) => {
    const popupContainer = DomUtil.create('div', 'popup-container')
    const titleEl = DomUtil.create('b', '', popupContainer)
    titleEl.innerHTML = title

    const popupContentContainer = DomUtil.create(
      'div',
      'popup-content',
      popupContainer
    )
    const bodyEl = DomUtil.create('span', '', popupContentContainer)
    bodyEl.innerHTML = body

    const buttonContainer = DomUtil.create(
      'div',
      'popup-btn-container',
      popupContentContainer
    )
    for (const button of buttons) {
      buttonContainer.appendChild(
        createPopupButton(
          button.label,
          button.cls,
          button.handler,
          buttonContainer
        )
      )
    }

    const popup = new Popup()
    popup.setContent(popupContainer)
    return popup
  }

  const createPopupButton = (
    innerHtml: string,
    cls: string,
    handlerFn: () => void,
    container?: HTMLElement | undefined
  ) => {
    const buttonEl = DomUtil.create('button', 'portal-button', container)
    buttonEl.classList.add(cls)
    buttonEl.setAttribute('type', 'button')
    buttonEl.innerHTML = innerHtml
    DomEvent.on(buttonEl, 'click', handlerFn)
    return buttonEl
  }

  return { showLocationInfoPopup, createPopupWithButtons }
}
