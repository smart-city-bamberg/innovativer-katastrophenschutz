import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest'
import { MockData } from '@/assets/mocks/data'
import { mount } from '@vue/test-utils'
import SituationMap from '@/map/components/SituationMap/SituationMap.vue'
import { Mission } from '@/mission/models/mission'
import { SubReport } from '@/report/models/subReport'

describe.skip('SituationMap', () => {
  let wrapper: ReturnType<typeof mountComponent>

  const missions = MockData.getMissions()
  const reports = MockData.getReports(2)
  const updatedReports = MockData.getReports(3)

  const mountComponent = () => {
    return mount(SituationMap, {
      attachTo: document.body,
      props: {
        mission: missions[0],
        reports,
      },
      global: {
        stubs: ['ShapeCreatorControl'],
      },
    })
  }

  beforeEach(() => {
    wrapper = mountComponent()
  })

  afterEach(() => {
    wrapper && wrapper.unmount()
    vi.clearAllMocks()
  })

  describe(':props', () => {
    it(':mission - unit markers are displayed on map', () => {
      const markerPane = wrapper.find('.leaflet-marker-pane')
      expect(markerPane.isVisible()).toBe(true)

      const mission = wrapper.props('mission') as Mission
      expect(mission.units.length).toBeTruthy()

      for (const unit of mission.units) {
        const marker = wrapper.find(`img[src="${unit.tacticalSign.path}"]`)
        expect(marker.isVisible()).toBe(true)
      }
    })

    it(':mission - unit markers are changed when the mission changes', async () => {
      const markerPane = wrapper.find('.leaflet-marker-pane')
      expect(markerPane.isVisible()).toBe(true)

      const mission1 = wrapper.props('mission') as Mission
      expect(mission1.units.length).toBeTruthy()

      for (const unit of mission1.units) {
        const marker = wrapper.find(`img[src="${unit.tacticalSign.path}"]`)
        expect(marker.isVisible()).toBe(true)
      }

      await wrapper.setProps({ mission: missions[1] })
      const mission2 = wrapper.props('mission') as Mission
      expect(mission2.units.length).toBeTruthy()

      for (const unit of mission2.units) {
        const marker = wrapper.find(`img[src="${unit.tacticalSign.path}"]`)
        expect(marker.isVisible()).toBe(true)
      }
    })

    it(':reports - report markers are displayed on map', () => {
      const markerPane = wrapper.find('.leaflet-marker-pane')
      expect(markerPane.isVisible()).toBe(true)

      const reports = wrapper.props('reports') as SubReport[]
      expect(reports.length).toBeTruthy()

      for (const report of reports) {
        const marker = wrapper.find(`img[src="${report.tacticalSign?.path}"]`)

        if (!report.location) {
          expect(marker.exists()).toBe(false)
        } else {
          // expect only reports with a location to be shown
          expect(marker.isVisible()).toBe(true)
        }
      }
    })

    it(':reports - report markers are updated when new reports are added', async () => {
      const markerPane = wrapper.find('.leaflet-marker-pane')
      expect(markerPane.isVisible()).toBe(true)

      const reports1 = wrapper.props('reports') as SubReport[]
      expect(reports1.length).toBeTruthy()

      for (const report of reports1) {
        const marker = wrapper.find(`img[src="${report.tacticalSign?.path}"]`)

        if (!report.location) {
          expect(marker.exists()).toBe(false)
        } else {
          // expect only reports with a location to be shown
          expect(marker.isVisible()).toBe(true)
        }
      }

      await wrapper.setProps({ reports: updatedReports })
      const reports2 = wrapper.props('reports') as SubReport[]
      expect(reports2.length).toBeTruthy()

      for (const report of reports2) {
        const marker = wrapper.find(`img[src="${report.tacticalSign?.path}"]`)

        if (!report.location) {
          expect(marker.exists()).toBe(false)
        } else {
          // expect only reports with a location to be shown
          expect(marker.isVisible()).toBe(true)
        }
      }
    })
  })
})
