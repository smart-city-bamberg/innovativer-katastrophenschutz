import SituationMapWidget from '@/dashboard/widgets/SituationMapWidget/SituationMapWidget.vue'
import {
  FormSchema,
  WidgetComponentWrapper,
} from '@lion5/component-library-gridstack'

export const SITUATION_MAP_WIDGET_WRAPPER = new WidgetComponentWrapper(
  'Lagekarte',
  SituationMapWidget,
  new Map(),
  new FormSchema([])
)
