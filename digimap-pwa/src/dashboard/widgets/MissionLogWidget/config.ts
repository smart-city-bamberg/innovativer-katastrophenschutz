import MissionLogWidget from '@/dashboard/widgets/MissionLogWidget/MissionLogWidget.vue'
import {
  FormSchema,
  WidgetComponentWrapper,
} from '@lion5/component-library-gridstack'

export const MISSION_LOG_WIDGET_WRAPPER = new WidgetComponentWrapper(
  'Einsatzprotokoll',
  MissionLogWidget,
  new Map(),
  new FormSchema([])
)
