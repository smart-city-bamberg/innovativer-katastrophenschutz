import ContributionWidget from '@/dashboard/widgets/SmartCityContributionWidget/ContributionWidget.vue'
import {
  FormSchema,
  WidgetComponentWrapper,
} from '@lion5/component-library-gridstack'

export const CONTRIBUTION_WIDGET_WRAPPER = new WidgetComponentWrapper(
  'Contribution Widget',
  ContributionWidget,
  new Map(),
  new FormSchema([])
)
