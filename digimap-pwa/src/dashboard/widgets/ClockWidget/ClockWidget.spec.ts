import {
  afterEach,
  beforeAll,
  beforeEach,
  describe,
  expect,
  it,
  vi,
} from 'vitest'
import { mount } from '@vue/test-utils'
import ClockWidget from '@/dashboard/widgets/ClockWidget/ClockWidget.vue'
import type { defineComponent } from 'vue'
import { CLOCK_WIDGET_WRAPPER } from '@/dashboard/widgets/ClockWidget/config'
import type { WidgetSetting } from '@lion5/component-library-gridstack'
import { ClockWidgetSetting } from '@/dashboard/widgets/ClockWidget/settings'

describe('ClockWidget.vue', () => {
  let wrapper: ReturnType<typeof defineComponent>

  beforeAll(() => {
    vi.mock('@/dashboard/widgets/ClockWidget/getAvailableTimeZones', () => ({
      getAvailableTimeZones: vi.fn(),
    }))
  })

  beforeEach(() => {
    wrapper = mount(ClockWidget, {
      props: {
        settings: new Map(),
        defaultSettings: CLOCK_WIDGET_WRAPPER.defaultSettings,
      },
    })
  })

  afterEach(() => {
    wrapper.unmount()
  })
  describe(':props', () => {
    it(':settings - uses timeZone', async () => {
      const expectedTimeZone = 'Europe/Zurich'
      const expectedDateString = '01.01.2023'
      const expectedTimeString = '12:12'

      const toLocaleDateStringSpy = vi
        .spyOn(Date.prototype, 'toLocaleDateString')
        .mockReturnValue(expectedDateString)
      const toLocaleTimeStringSpy = vi
        .spyOn(Date.prototype, 'toLocaleTimeString')
        .mockReturnValue(expectedTimeString)
      const settings = new Map([
        [ClockWidgetSetting.TIME_ZONE, expectedTimeZone],
      ])

      await wrapper.setProps({ settings })

      expect(toLocaleDateStringSpy).toHaveBeenCalledWith('de-DE', {
        timeZone: expectedTimeZone,
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
      })
      expect(toLocaleTimeStringSpy).toHaveBeenCalledWith('de-DE', {
        timeZone: expectedTimeZone,
        hour: '2-digit',
        minute: '2-digit',
      })
      expect(wrapper.find('.clock-date').text()).toBe(expectedDateString)
      expect(wrapper.find('.clock-time').text()).toBe(expectedTimeString)
      expect(wrapper.find('.clock-tactical-time').exists()).toBeFalsy()
    })
    it(':settings - uses timeZone and tactical time', async () => {
      const expectedTimeZone = 'Europe/Zurich'

      const toLocaleDateStringSpy = vi
        .spyOn(Date.prototype, 'toLocaleDateString')
        .mockReturnValueOnce('01')
        .mockReturnValueOnce('jan')
        .mockReturnValueOnce('23')
      const toLocaleTimeStringSpy = vi
        .spyOn(Date.prototype, 'toLocaleTimeString')
        .mockReturnValue('06:13')
      const settings = new Map<string, WidgetSetting>([
        [ClockWidgetSetting.TIME_ZONE, expectedTimeZone],
        [ClockWidgetSetting.TACTICAL_TIME, true],
      ])

      await wrapper.setProps({ settings })

      expect(toLocaleDateStringSpy).toHaveBeenNthCalledWith(1, 'de-DE', {
        timeZone: expectedTimeZone,
        day: '2-digit',
      })
      expect(toLocaleDateStringSpy).toHaveBeenNthCalledWith(2, 'en-US', {
        timeZone: expectedTimeZone,
        month: 'short',
      })
      expect(toLocaleDateStringSpy).toHaveBeenNthCalledWith(3, 'de-DE', {
        timeZone: expectedTimeZone,
        year: '2-digit',
      })
      expect(toLocaleTimeStringSpy).toHaveBeenCalledWith('de-DE', {
        timeZone: expectedTimeZone,
        hour: '2-digit',
        minute: '2-digit',
      })
      expect(wrapper.find('.clock-date').exists()).toBeFalsy()
      expect(wrapper.find('.clock-time').exists()).toBeFalsy()
      expect(wrapper.find('.clock-tactical-time').text()).toBe('010613jan23')
    })
  })
})
