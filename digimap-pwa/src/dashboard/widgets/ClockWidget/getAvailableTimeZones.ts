/**
 * Returns browser available time zones. Calls Intl.supportedValuesOf('timeZone') and is used to better mock this function.
 */
export function getAvailableTimeZones() {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  return Intl.supportedValuesOf('timeZone')
}
