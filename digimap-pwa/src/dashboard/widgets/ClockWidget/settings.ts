import { boolean, string } from 'yup'
import { getAvailableTimeZones } from '@/dashboard/widgets/ClockWidget/getAvailableTimeZones'
import { CheckboxInput, BaseSelect } from '@lion5/component-library'
import { FormField } from '@lion5/component-library-gridstack'

export enum ClockWidgetSetting {
  TIME_ZONE = 'TIME_ZONE',
  TACTICAL_TIME = 'TACTICAL_TIME',
}

const availableTimeZones = getAvailableTimeZones()

export const ClockWidgetSettingsFormSchema = {
  fields: [
    new FormField(
      BaseSelect,
      'Zeitzone',
      ClockWidgetSetting.TIME_ZONE,
      string().required(),
      {
        options: availableTimeZones,
        optionsLabel: 'Bitte wählen Sie eine Zeitzone aus',
      }
    ),
    new FormField(
      CheckboxInput,
      'Taktische Zeit?',
      ClockWidgetSetting.TACTICAL_TIME,
      boolean()
    ),
  ],
}
