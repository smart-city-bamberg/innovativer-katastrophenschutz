import { WidgetComponentWrapper } from '@lion5/component-library-gridstack'
import ClockWidget from '@/dashboard/widgets/ClockWidget/ClockWidget.vue'
import type { WidgetSetting } from '@lion5/component-library-gridstack'
import {
  ClockWidgetSetting,
  ClockWidgetSettingsFormSchema,
} from '@/dashboard/widgets/ClockWidget/settings'

export const CLOCK_WIDGET_WRAPPER = new WidgetComponentWrapper(
  'Uhr',
  ClockWidget,
  new Map<string, WidgetSetting>([
    [ClockWidgetSetting.TACTICAL_TIME, false],
    [ClockWidgetSetting.TIME_ZONE, 'Europe/Berlin'],
  ]),
  ClockWidgetSettingsFormSchema
)
