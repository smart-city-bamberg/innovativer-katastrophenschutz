import {
  WidgetComponentWrapper,
  IFRAME_WIDGET_WRAPPER,
  REFRESHABLE_IMAGE_WIDGET_WRAPPER,
  WEATHER_WARN_FEED_WIDGET_WRAPPER,
  WEATHER_FORECAST_WIDGET_WRAPPER,
  WEATHER_WIDGET_WRAPPER,
  DWD_WARN_WIDGET_WRAPPER,
  TWITTER_WIDGET_WRAPPER,
  DWD_RAIN_RADAR_WIDGET_WRAPPER,
} from '@lion5/component-library-gridstack'
import { CLOCK_WIDGET_WRAPPER } from '@/dashboard/widgets/ClockWidget/config'
import { SITUATION_MAP_WIDGET_WRAPPER } from '@/dashboard/widgets/SituationMapWidget/config'
import { MISSION_LOG_WIDGET_WRAPPER } from '@/dashboard/widgets/MissionLogWidget/config'
import { CONTRIBUTION_WIDGET_WRAPPER } from '@/dashboard/widgets/SmartCityContributionWidget/config'

/**
 * This map is used to resolve the componentId reference to its corresponding widget component and name ({@link WidgetComponentWrapper}).
 * It contains all available dashboard widgets.
 *
 * <strong>Do never change the keys of the map! It can break dashboard configurations that are loaded from the backend. If you need to change the keys you also need to migrate the change into the backend / DB. </strong>
 */
export const availableWidgets: Map<string, WidgetComponentWrapper> = new Map([
  ['map', SITUATION_MAP_WIDGET_WRAPPER],
  ['weatherWarn', WEATHER_WARN_FEED_WIDGET_WRAPPER],
  ['weatherForecast', WEATHER_FORECAST_WIDGET_WRAPPER],
  ['weather', WEATHER_WIDGET_WRAPPER],
  ['iframe', IFRAME_WIDGET_WRAPPER],
  ['rainRadar', DWD_RAIN_RADAR_WIDGET_WRAPPER],
  ['refreshImage', REFRESHABLE_IMAGE_WIDGET_WRAPPER],
  ['dwdWarn', DWD_WARN_WIDGET_WRAPPER],
  ['clock', CLOCK_WIDGET_WRAPPER],
  ['twitterFeed', TWITTER_WIDGET_WRAPPER],
  ['missionLog', MISSION_LOG_WIDGET_WRAPPER],
  ['contribution', CONTRIBUTION_WIDGET_WRAPPER],
])
