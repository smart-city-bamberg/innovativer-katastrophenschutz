import { ref } from 'vue'
import { WidgetConfiguration } from '@lion5/component-library-gridstack'
import { RestClient } from '@/base/services/restClient'
import type { ApiDashboardConfiguration } from '@/dashboard/model/DashboardConfiguration'
import { DashboardConfiguration } from '@/dashboard/model/DashboardConfiguration'
import type { ApiDashboardConfigurationOption } from '@/dashboard/model/DashboardConfigurationOption'
import { DashboardConfigurationOption } from '@/dashboard/model/DashboardConfigurationOption'
import type { HttpError } from 'ketting/dist/http/error'

export function useDashboardConfiguration() {
  const isLoading = ref(false)
  const error = ref<Error | HttpError | null>(null)

  const saveDashboardConfiguration = async (
    widgetConfigurations: WidgetConfiguration[],
    name: string
  ) => {
    isLoading.value = true
    error.value = null
    try {
      const client = RestClient.getInstance()
      const body = {
        name: name,
        widgetConfigurations: widgetConfigurations.map((wc) => ({
          id: wc.id,
          position: {
            x: wc.position.x,
            y: wc.position.y,
            width: wc.position.width,
            height: wc.position.height,
          },
          componentId: wc.componentId,
          settings: Object.fromEntries(wc.settings),
        })),
      }

      const response = await client
        .go('dashboard-configurations')
        .post({ data: body })
      return DashboardConfiguration.fromApi(response.data)
    } catch (e) {
      console.error('Error saving dashboard configuration:', e)
      error.value = e as Error | HttpError
    } finally {
      isLoading.value = false
    }
  }

  const deleteDashboardConfiguration = async (id: string) => {
    isLoading.value = true
    error.value = null
    try {
      const client = RestClient.getInstance()
      await client.go(`dashboard-configurations/${id}`).delete()
    } catch (e) {
      console.error(`Error deleting dashboard configuration with ID ${id}:`, e)
      error.value = e as Error | HttpError
    } finally {
      isLoading.value = false
    }
  }

  const fetchAllDashboardConfigurations = async () => {
    isLoading.value = true
    error.value = null
    try {
      const client = RestClient.getInstance()
      const response = await client.go('dashboard-configurations').get()
      return (response.data as ApiDashboardConfigurationOption[]).map(
        (dcOption) => DashboardConfigurationOption.fromApi(dcOption)
      )
    } catch (e) {
      console.error('Error fetching dashboard configurations:', e)
      error.value = e as Error | HttpError
    } finally {
      isLoading.value = false
    }
  }

  // Function to fetch a single dashboard configuration by ID
  const fetchDashboardConfigurationById = async (id: string) => {
    isLoading.value = true
    error.value = null
    try {
      const client = RestClient.getInstance()
      const response = await client.go(`dashboard-configurations/${id}`).get()
      return DashboardConfiguration.fromApi(
        response.data as ApiDashboardConfiguration
      )
    } catch (e) {
      console.error(`Error fetching dashboard configuration with ID ${id}:`, e)
      error.value = e as Error | HttpError
    } finally {
      isLoading.value = false
    }
  }

  return {
    saveDashboardConfiguration,
    deleteDashboardConfiguration,
    fetchAllDashboardConfigurations,
    fetchDashboardConfigurationById,
    isLoading,
    error,
  }
}
