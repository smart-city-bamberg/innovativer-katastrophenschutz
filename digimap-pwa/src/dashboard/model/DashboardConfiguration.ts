import { WidgetConfiguration } from '@lion5/component-library-gridstack'

export interface ApiDashboardConfiguration {
  id: string
  name: string
  widgetConfigurations: WidgetConfiguration[]
}

export class DashboardConfiguration {
  readonly id: string
  name: string
  widgetConfigurations: WidgetConfiguration[]

  constructor(
    id: string,
    name: string,
    widgetConfigurations: WidgetConfiguration[]
  ) {
    this.id = id
    this.name = name
    this.widgetConfigurations = widgetConfigurations
  }

  static fromApi(
    apiDashboardConfiguration: ApiDashboardConfiguration
  ): DashboardConfiguration {
    return new DashboardConfiguration(
      apiDashboardConfiguration.id,
      apiDashboardConfiguration.name,
      apiDashboardConfiguration.widgetConfigurations.map((wc) =>
        WidgetConfiguration.fromApi(wc)
      )
    )
  }
}
