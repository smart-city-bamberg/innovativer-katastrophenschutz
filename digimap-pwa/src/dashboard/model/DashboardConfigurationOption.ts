export interface ApiDashboardConfigurationOption {
  id: string
  name: string
}

export class DashboardConfigurationOption {
  readonly id: string
  name: string

  constructor(id: string, name: string) {
    this.id = id
    this.name = name
  }

  static fromApi(
    apiOption: ApiDashboardConfigurationOption
  ): DashboardConfigurationOption {
    return new DashboardConfigurationOption(apiOption.id, apiOption.name)
  }
}
