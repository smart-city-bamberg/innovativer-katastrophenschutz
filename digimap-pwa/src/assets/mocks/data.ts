import { Mission } from '@/mission/models/mission'
import { creationMethod, Unit } from '@/unit/models/unit'
import { Address } from '@/base/models/address'
import { TacticalSign } from '@/base/models/tacticalSign'
import { MissionState } from '@/mission/models/missionState'
import { SubReport } from '@/report/models/subReport'
import { GpsLocation } from '@lion5/component-library-leaflet'
import {
  AdhocJoinRequest,
  ApprovalStatus,
} from '@/join-codes/models/adhocJoinRequest'
import { SubCommand } from '@/command/models/subCommand'
import { User } from '@/account/models/user'
import { Role } from '@/account/models/role'

export class MockData {
  private static readonly _units = [
    new Unit(
      'unit1',
      'UnitType1',
      new GpsLocation(49.89381, 10.89331),
      'Feuerwehr 1',
      new TacticalSign('Feuerwehr_Einheiten/ABC_Erkundungsgruppe.svg'),
      'uri/to/unit1',
      {
        userId: 'firebaseUser1',
        displayName: 'User1',
        email: 'User1@test.com',
      },
      creationMethod.PERMANENT
    ),
    new Unit(
      'unit2',
      'UnitType2',
      new GpsLocation(49.89467, 10.89366),
      'Feuerwehr 2',
      new TacticalSign('Feuerwehr_Einheiten/Führungsgruppe_TEL.svg'),
      'uri/to/unit2',
      {
        userId: 'firebaseUser2',
        displayName: 'User2',
        email: 'User2@test.com',
      },
      creationMethod.PERMANENT
    ),
    new Unit(
      'unit3',
      'UnitType1',
      new GpsLocation(49.89381, 10.89331),
      'Feuerwehr 3',
      new TacticalSign('THW_Einheiten/1._Bergungsgruppe.svg'),
      'uri/to/unit3',
      {
        userId: 'firebaseUser3',
        displayName: 'User3',
        email: 'User3@test.com',
      },
      creationMethod.PERMANENT
    ),
    new Unit(
      'unit4',
      'UnitType2',
      new GpsLocation(49.89467, 10.89366),
      'Polizei 1',
      new TacticalSign('THW_Einheiten/ENT.svg'),
      'uri/to/unit4',
      undefined,
      creationMethod.PERMANENT
    ),
    new Unit(
      'unit5',
      'UnitType2',
      new GpsLocation(49.89467, 10.89366),
      'Feuerwehr 4',
      new TacticalSign('THW_Einheiten/ENT.svg'),
      'uri/to/unit5',
      undefined,
      creationMethod.PERMANENT
    ),
  ]
  private static _missions = [
    new Mission(
      'mission1',
      'B 4.1 2108XX XXX',
      new GpsLocation(49.89307, 10.89367),
      new Address('Brennerstraße', '12A', 96052, 'Bamberg'),
      new Date(2024, 0, 1, 12, 0),
      'Brand',
      this._units.slice(0, 2),
      MissionState.RUNNING,
      [
        new SubReport(
          1,
          '2',
          '3',
          '4',
          '5',
          new Date(2024, 0, 1, 12, 0),
          [],
          new GpsLocation(13.1, 13.1),
          new TacticalSign('Rettungswesen_Personen/Leitender_Notarzt.svg'),
          '1'
        ),
      ],
      [
        new SubCommand(
          1,
          '123',
          '123',
          '1',
          'Das ist ein Befehl!',
          new Date(2024, 0, 1, 12, 0),
          [],
          '123'
        ),
      ],
      [],
      [],
      [],
      'uri/to/mission1',
      'uri/to/mission1/units',
      'keyword mission1',
      'cues mission1'
    ),
    new Mission(
      'mission2',
      'B 4.2 2109XX XXX',
      new GpsLocation(49.89307, 10.89367),
      new Address('Dummy Street', '12A', 96052, 'Bamberg'),
      new Date(2024, 1, 1, 17, 0),
      'Überschwemmung',
      this._units.slice(3),
      MissionState.FINISHED,
      [],
      [],
      [],
      [],
      [],
      'uri/to/mission2',
      'uri/to/mission2/units',
      'keyword mission2',
      'cues mission2'
    ),
    new Mission(
      'mission3',
      'B 4.3 2110XX XXX',
      new GpsLocation(49.89307, 10.89367),
      new Address('Straße', '2', 99999, 'Musterstadt'),
      new Date(2024, 1, 1, 17, 0),
      'Überschwemmung',
      this._units.slice(3),
      MissionState.RUNNING,
      [],
      [],
      [],
      [],
      [],
      'uri/to/mission3',
      'uri/to/mission3/units',
      'keyword mission3',
      'cues mission3'
    ),
  ]

  private static _reports = [
    new SubReport(
      1,
      'report1',
      'unit1',
      'Feuerwehr 1',
      'test',
      new Date('2023-01-01T01:01'),
      ['dummy/url'],
      new GpsLocation(49.0, 10.0),
      new TacticalSign('Gefahren/Entstehungsbrand.svg')
    ),
    new SubReport(
      2,
      'report2',
      'unit2',
      'Feuerwehr 2',
      'example',
      new Date('2023-01-01T01:02'),
      ['dummy/example/url'],
      new GpsLocation(50.0, 11.0),
      new TacticalSign('Schäden/Zerstört.svg')
    ),
    // report without gps location
    new SubReport(
      3,
      'report3',
      'unit1',
      'Feuerwehr 1',
      'test',
      new Date('2023-01-01T01:03'),
      ['dummy/example/url'],
      undefined,
      new TacticalSign('Schäden/Blockiert.svg')
    ),
  ]

  private static _adhocJoinRequests = [
    new AdhocJoinRequest(
      'adhocJoinRequest1',
      'firebaseUser1',
      'User1',
      'unit1',
      'Feuerwehr_Einheiten/Führungsgruppe_TEL.svg',
      'Feuerwehr 1',
      this._missions[0],
      ApprovalStatus.PENDING,
      new Date(2024, 2, 11, 12, 0, 0, 0),
      null
    ),
    new AdhocJoinRequest(
      'adhocJoinRequest2',
      'firebaseUser2',
      'User2',
      'unit2',
      'Feuerwehr_Einheiten/Führungsgruppe_TEL.svg',
      'Feuerwehr 2',
      this._missions[1],
      ApprovalStatus.APPROVED,
      new Date(),
      new Unit(
        'unit2',
        'UnitType2',
        new GpsLocation(49.89467, 10.89366),
        'Feuerwehr 2',
        new TacticalSign('Feuerwehr_Einheiten/Führungsgruppe_TEL.svg'),
        'uri/to/unit2'
      )
    ),
    new AdhocJoinRequest(
      'adhocJoinRequest3',
      'firebaseUser3',
      'User3',
      'unit3',
      'Feuerwehr_Einheiten/Führungsgruppe_TEL.svg',
      'Feuerwehr 3',
      this._missions[1],
      ApprovalStatus.REJECTED,
      new Date(2024, 2, 12, 12, 0, 0, 0),
      null
    ),
  ]

  private static _tacticalSigns = [
    new TacticalSign(
      'https://storage.googleapis.com/staging-tactical-signs/tactical-sign/123',
      'tacticalSign1'
    ),
    new TacticalSign(
      'https://storage.googleapis.com/staging-tactical-signs/tactical-sign/123456',
      'tacticalSign2'
    ),
  ]

  private static _User = new User(
    'TestUser',
    'TestUser@Test.com',
    Role.fromKey('USER'),
    '1',
    '1',
    false
  )

  public static getMissions() {
    return this._missions
  }

  public static getAllUnits() {
    return this._units
  }

  public static getReports(count: number) {
    return this._reports.slice(0, count)
  }

  public static getAdhocJoinRequests() {
    return this._adhocJoinRequests
  }

  public static getTacticalSigns() {
    return this._tacticalSigns
  }

  public static getUser() {
    return this._User
  }
}
