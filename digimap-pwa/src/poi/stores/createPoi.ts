import { defineStore } from 'pinia'
import { ref } from 'vue'
import { mixed, object, string } from 'yup'
import { useDate } from '@lion5/component-library'
import { GpsLocation } from '@lion5/component-library-leaflet'
import { PointOfInterest } from '@/poi/models/pointOfInterest'
import { TacticalSign } from '@/base/models/tacticalSign'
import { usePointsOfInterest } from '@/poi/services/usePointsOfInterest'

export interface CreatePoiForm {
  senderUnitId: string
  createdAt: string
  tacticalSignPath: string
  description: string
  location: GpsLocation
}

export const useCreatePoiStore = defineStore('createPoi', () => {
  const location = ref<GpsLocation>()

  const setLocation = (newLocation: GpsLocation) => {
    location.value = newLocation
  }

  const _reset = () => {
    location.value = undefined
  }

  const { getDateTimeLocalInputValue } = useDate()
  const validationSchema = object({
    senderUnitId: string(),
    createdAt: string(),
    tacticalSignPath: string().required(
      'Bitte wählen Sie ein taktisches Zeichen'
    ),
    description: string().required('Bitte geben Sie eine Beschreibung an'),
    location: mixed().required('Bitte wählen Sie einen Ort'),
  })
  const initialValues = {
    createdAt: getDateTimeLocalInputValue(new Date()),
    tacticalSignPath: '',
    location: location.value,
  }

  const publishPoi = async (formValues: CreatePoiForm) => {
    const poi = new PointOfInterest(
      formValues.senderUnitId,
      formValues.description,
      TacticalSign.withPrefix(formValues.tacticalSignPath),
      formValues.location
    )
    const { publish, addPoi } = usePointsOfInterest()
    publish(poi)
    addPoi(poi)
    _reset()
    return poi // for testing
  }

  return { location, validationSchema, initialValues, setLocation, publishPoi }
})
