import {
  beforeAll,
  beforeEach,
  afterEach,
  describe,
  expect,
  it,
  vi,
} from 'vitest'
import type { CreatePoiForm } from '@/poi/stores/createPoi'
import { useCreatePoiStore } from '@/poi/stores/createPoi'
import { MockData } from '@/assets/mocks/data'
import { GpsLocation } from '@lion5/component-library-leaflet'
import { createPinia, setActivePinia, storeToRefs } from 'pinia'
import { useMissionsStore } from '@/mission/stores/missions'
import { MqttJsClient } from '@/mqtt/mqttClient'
import { PointOfInterest } from '@/poi/models/pointOfInterest'
import { TacticalSign } from '@/base/models/tacticalSign'
import * as usePoiExports from '@/poi/services/usePointsOfInterest'
import { usePointsOfInterest } from '@/poi/services/usePointsOfInterest'

vi.spyOn(usePoiExports, 'usePointsOfInterest').mockReturnValue({
  publish: vi.fn(),
  addPoi: vi.fn(),
})

const mission = MockData.getMissions()[0]
const senderUnit = mission.units[0]

beforeAll(() => {
  setActivePinia(createPinia())
  vi.clearAllMocks()
})

beforeEach(() => {
  vi.useFakeTimers()
})
afterEach(() => {
  vi.useRealTimers()
})

describe('createPoi', () => {
  describe('publishPoi', () => {
    it('publishes and adds expected POI', async () => {
      const formValues = {
        senderUnitId: senderUnit.id,
        location: new GpsLocation(1.0, 2.0),
        tacticalSignPath: '/svg/tactical-signs/test/path.svg',
        description: 'this is a description',
      } as CreatePoiForm
      MqttJsClient.getInstance().publish = vi.fn()
      const { currentMission } = storeToRefs(useMissionsStore())
      currentMission.value = mission
      const { addPoi, publish } = usePointsOfInterest()

      const { publishPoi, location } = useCreatePoiStore()
      const actual = await publishPoi(formValues)

      const expectedPoi = new PointOfInterest(
        senderUnit.id,
        formValues.description,
        TacticalSign.withPrefix(formValues.tacticalSignPath),
        formValues.location,
        actual.id // we need to use the actual id
      )
      expect(addPoi).toHaveBeenCalledWith(expectedPoi)
      expect(publish).toHaveBeenCalledWith(expectedPoi)
      expect(location)
    })
  })
})
