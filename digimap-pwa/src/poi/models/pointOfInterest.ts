import type { GenericMqttPayload } from '@/mqtt/iMqttClient'
import type { SubPointOfInterest } from '@/mqtt/payloads/sub/SubPointOfInterest'
import type { ApiGpsLocation } from '@lion5/component-library-leaflet'
import { GpsLocation } from '@lion5/component-library-leaflet'
import { AsyncCrudOperation } from '@/mqtt/payloads/AsyncCrudOperation'
import { TacticalSign } from '@/base/models/tacticalSign'
import { v4 as uuidv4 } from 'uuid'

export interface IApiPointOfInterest {
  poiId: string
  etag: number
  senderUnitId: string
  details: string
  tacticalSignPath: string
  createdAt: string
  operation: AsyncCrudOperation
  location: ApiGpsLocation
}

export interface PubPointOfInterest {
  createdAt: string
  details: string
  location: ApiGpsLocation
  operation: AsyncCrudOperation
  poiId: string
  senderUnitId: string
  tacticalSignPath: string
}

export class PointOfInterest {
  id: string
  senderUnitId: string
  details: string
  tacticalSign: TacticalSign
  createdAt: Date
  operation: AsyncCrudOperation
  location: GpsLocation
  eTag?: number

  constructor(
    senderUnitId: string,
    details: string,
    tacticalSign: TacticalSign,
    location: GpsLocation,
    id = uuidv4(),
    operation = AsyncCrudOperation.CREATED,
    createdAt = new Date(),
    eTag?: number
  ) {
    this.id = id
    this.eTag = eTag
    this.operation = operation
    this.createdAt = createdAt
    this.senderUnitId = senderUnitId
    this.details = details
    this.tacticalSign = tacticalSign
    this.location = location
  }

  public toMqtt(): PubPointOfInterest {
    return {
      poiId: this.id,
      operation: this.operation,
      createdAt: this.createdAt.toISOString(),
      senderUnitId: this.senderUnitId,
      details: this.details,
      tacticalSignPath: this.tacticalSign.relativePath,
      location: this.location.toApi(),
    }
  }

  public static fromApi(poi: IApiPointOfInterest) {
    return new PointOfInterest(
      poi.senderUnitId,
      poi.details,
      new TacticalSign(poi.tacticalSignPath),
      GpsLocation.fromApi(poi.location),
      poi.poiId,
      poi.operation,
      new Date(poi.createdAt),
      poi.etag
    )
  }

  public static fromMqtt(payload: GenericMqttPayload) {
    const typedPayload = payload as SubPointOfInterest
    return new PointOfInterest(
      typedPayload.senderUnitId,
      typedPayload.details,
      new TacticalSign(typedPayload.tacticalSignPath),
      GpsLocation.fromApi(typedPayload.location),
      typedPayload.poiId,
      typedPayload.operation,
      typeof typedPayload.createdAt === 'string'
        ? new Date(typedPayload.createdAt)
        : new Date(typedPayload.createdAt * 1000), // TODO: fix unix time stamp conversion
      typedPayload.etag
    )
  }
}
