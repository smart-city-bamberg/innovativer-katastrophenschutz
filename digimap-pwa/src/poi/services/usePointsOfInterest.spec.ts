import { beforeEach, describe, expect, it, vi } from 'vitest'
import { MqttJsClient } from '@/mqtt/mqttClient'
import { createPinia, setActivePinia, storeToRefs } from 'pinia'
import { PointOfInterest } from '@/poi/models/pointOfInterest'
import { useMissionsStore } from '@/mission/stores/missions'
import { usePointsOfInterest } from '@/poi/services/usePointsOfInterest'
import { TacticalSign } from '@/base/models/tacticalSign'
import { GpsLocation } from '@lion5/component-library-leaflet'
import { MockData } from '@/assets/mocks/data'

const poi = new PointOfInterest(
  'unit1',
  'details',
  new TacticalSign('An_und_Abfahrtswege/Anfahrtsweg.svg'),
  new GpsLocation(0.0, 0.0),
  'poi1'
)
const mission = MockData.getMissions()[0]

beforeEach(() => {
  setActivePinia(createPinia())
  vi.clearAllMocks()
})

describe('usePointsOfInterest', () => {
  describe('publish', () => {
    it('should throw error if no currentMission is set', function () {
      MqttJsClient.getInstance().publish = vi.fn()

      const { publish } = usePointsOfInterest()

      expect(() => publish(poi)).toThrowError(
        'Kein Einsatz ausgewählt. Bitte wählen Sie einen Einsatz in den Einstellungen oder auf der Lagekarte.'
      )
      expect(MqttJsClient.getInstance().publish).not.toHaveBeenCalled()
    })
    it('should publish correct mqtt message', function () {
      MqttJsClient.getInstance().publish = vi.fn()
      const { currentMission } = storeToRefs(useMissionsStore())
      currentMission.value = mission

      const { publish } = usePointsOfInterest()
      publish(poi)

      expect(MqttJsClient.getInstance().publish).toHaveBeenCalledWith(
        `missions/v1/inbound/${mission.missionId}/poi`,
        poi.toMqtt()
      )
    })
  })
})
