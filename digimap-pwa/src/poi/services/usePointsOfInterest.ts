import { MqttJsClient } from '@/mqtt/mqttClient'
import type { GenericMqttPayload } from '@/mqtt/iMqttClient'
import { ref, watch } from 'vue'
import { PointOfInterest } from '@/poi/models/pointOfInterest'
import { storeToRefs } from 'pinia'
import { useMissionsStore } from '@/mission/stores/missions'

const subscribedTopic = ref<string>('')
const mqttClient: MqttJsClient = MqttJsClient.getInstance()

export function usePointsOfInterest() {
  const { currentMission } = storeToRefs(useMissionsStore())

  const addPoi = (poi: PointOfInterest) => {
    if (!currentMission.value || currentMission.value?.hasPoi(poi)) {
      return
    }
    try {
      currentMission.value.addPoi(poi)
    } catch (e: unknown) {
      console.warn((e as Error).message, {
        mission: currentMission.value,
        poi,
        e,
      })
    }
  }

  const publish = (poi: PointOfInterest) => {
    const missionId = currentMission.value?.missionId
    if (!missionId) {
      throw new Error(
        'Kein Einsatz ausgewählt. Bitte wählen Sie einen Einsatz in den Einstellungen oder auf der Lagekarte.'
      )
    }
    const topic = _getPublishTopic(missionId)
    MqttJsClient.getInstance().publish(topic, poi.toMqtt())
  }

  const _getSubscriptionTopic = (missionId: string) => {
    return `missions/v1/outbound/${missionId}/poi`
  }

  const _getPublishTopic = (missionId: string) => {
    return `missions/v1/inbound/${missionId}/poi`
  }

  const _subscribe = (missionId: string) => {
    const topic = _getSubscriptionTopic(missionId)
    if (subscribedTopic.value === topic) {
      // already subscribed
      return
    }
    mqttClient.subscribe(topic, (_topic, payload: GenericMqttPayload) => {
      try {
        const poi = PointOfInterest.fromMqtt(payload)
        console.log('Retrieved poi from mqtt', poi)
        addPoi(poi)
      } catch (e) {
        console.error('Cannot execute callback. Unknown error', e)
      }
    })
    subscribedTopic.value = topic
  }

  const _unsubscribe = (missionId: string) => {
    const topic = _getSubscriptionTopic(missionId)
    if (subscribedTopic.value !== topic) return
    mqttClient.unsubscribe(topic)
    subscribedTopic.value = ''
  }

  watch(
    currentMission,
    (newMission, oldMission) => {
      if (oldMission?.missionId) {
        _unsubscribe(oldMission?.missionId)
      }
      if (newMission?.missionId) {
        _subscribe(newMission?.missionId)
      }
    },
    { immediate: true }
  )

  return { publish, addPoi }
}
