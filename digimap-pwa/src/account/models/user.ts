import type { Role } from '@/account/models/role'

export interface IUser {
  displayName: string | null
  email: string | null
  role: Role
  /**
   * ID of the unit the user is assigned to.
   */
  unitId: string | null
  firebaseUserId?: string | null
  isAnonymous?: boolean
}
export class User implements IUser {
  readonly displayName: string | null
  readonly email: string | null
  readonly role: Role
  readonly unitId: string | null
  readonly firebaseUserId?: string | null
  readonly isAnonymous?: boolean

  constructor(
    displayName: string | null,
    email: string | null,
    role: Role,
    unitId: string | null,
    firebaseUserId?: string | null,
    isAnonymous: boolean = true
  ) {
    this.displayName = displayName
    this.email = email
    this.role = role
    this.unitId = unitId
    this.firebaseUserId = firebaseUserId
    this.isAnonymous = isAnonymous
  }
}
