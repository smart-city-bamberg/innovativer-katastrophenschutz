/**
 * Available roles, in decreasing level of rights:
 * - {@link ADMIN}: can edit system data (e.g. add users or units) and all other rights
 * - {@link USER}: can report damages & assign units (to be discussed, this might be two roles), and interact with units
 * - {@link VEHICLE}: can only publish its location
 */
export class Role {
  static readonly SUPER_ADMIN = new Role('SUPER_ADMIN', 'System-Administrator')
  static readonly ADMIN = new Role('ADMIN', 'Administrator')
  static readonly USER = new Role('USER', 'Nutzer')
  static readonly VEHICLE = new Role('VEHICLE', 'Fahrzeug')

  private constructor(
    /**
     * Unique identifier of the role
     */
    readonly key: string,
    /**
     * Human-readable name of the role
     */
    readonly name: string
  ) {}

  public static fromKey(key: string): Role {
    switch (key) {
      case this.SUPER_ADMIN.key:
        return this.SUPER_ADMIN

      case this.ADMIN.key:
        return this.ADMIN

      case this.VEHICLE.key:
        return this.VEHICLE

      default:
        return this.USER
    }
  }

  toString() {
    return this.key
  }

  toLocalString() {
    return this.name
  }
}
