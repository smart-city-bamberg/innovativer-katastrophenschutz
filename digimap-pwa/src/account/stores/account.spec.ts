import type { Mock } from 'vitest'
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest'
import { useAccountStore } from '@/account/stores/account'
import { createPinia, setActivePinia } from 'pinia'
import { Role } from '@/account/models/role'
import type {
  Auth,
  IdTokenResult,
  User as FBUser,
  UserCredential,
} from 'firebase/auth'
import {
  getAuth,
  onAuthStateChanged,
  sendEmailVerification,
  signInAnonymously,
  signInWithEmailAndPassword,
} from 'firebase/auth'
import { EmailNotVerifiedError } from '@/account/models/exceptions'
import { User } from '@/account/models/user'
import { RestClient } from '@/base/services/restClient'

vi.mock('axios')

vi.mock('firebase/auth', () => ({
  getAuth: vi.fn(),
  signInWithEmailAndPassword: vi.fn(),
  sendEmailVerification: vi.fn(),
  signInAnonymously: vi.fn().mockResolvedValue({}),
  onAuthStateChanged: vi.fn(),
}))

const getAuthMock = (user?: FBUser): Auth => {
  return {
    onIdTokenChanged: vi.fn(),
    currentUser:
      user ||
      ({
        displayName: 'testName',
        email: 'test@test.com',
        emailVerified: true,
        uid: 'firebaseUserId',
        getIdTokenResult: vi.fn().mockResolvedValue({
          claims: { role: Role.USER.key, unitId: 'unitId' },
          token: 'dummyToken',
        }),
      } as unknown as FBUser),
    signOut: vi.fn(),
  } as unknown as Auth
}

describe('useAccountStore', () => {
  let accountStore: ReturnType<typeof useAccountStore>
  let authMock: Auth

  beforeEach(() => {
    authMock = getAuthMock()
    ;(getAuth as unknown as Mock).mockReturnValue(authMock)
    setActivePinia(createPinia())
  })

  afterEach(() => {
    vi.clearAllMocks()
  })

  describe('initAuth', () => {
    it('returns the Firebase instance if not set', () => {
      accountStore = useAccountStore()
      const auth = accountStore.initAuth()

      expect(getAuth).toHaveBeenCalled()
      expect(auth).toBeDefined()
      expect(auth?.onIdTokenChanged).toHaveBeenCalled()
    })

    it('returns early if instance was already set', () => {
      accountStore = useAccountStore()
      accountStore.auth = authMock

      const auth = accountStore.initAuth()

      expect(getAuth).not.toHaveBeenCalled()
      expect(auth?.onIdTokenChanged).not.toHaveBeenCalled()
      expect(auth).toStrictEqual(authMock)
    })
  })

  describe('useBearerToken', () => {
    RestClient.useBearerToken = vi.fn()

    it('sets expected bearerToken', async () => {
      const mockIdTokenResult = { token: 'dummyToken' } as IdTokenResult
      accountStore = useAccountStore()

      expect(accountStore.bearerToken).toBe(undefined)

      await accountStore.useBearerToken(mockIdTokenResult)

      expect(accountStore.bearerToken).toBe('dummyToken')
      expect(RestClient.useBearerToken).toHaveBeenCalledWith('dummyToken')
    })

    it('updates expected bearerToken', async () => {
      const mockIdTokenResult1 = { token: 'dummyToken1' } as IdTokenResult
      const mockIdTokenResult2 = { token: 'dummyToken2' } as IdTokenResult
      accountStore = useAccountStore()

      await accountStore.useBearerToken(mockIdTokenResult1)

      expect(accountStore.bearerToken).toBe('dummyToken1')

      await accountStore.useBearerToken(mockIdTokenResult2)

      expect(accountStore.bearerToken).toBe('dummyToken2')
      expect(RestClient.useBearerToken).toHaveBeenNthCalledWith(
        1,
        'dummyToken1'
      )
      expect(RestClient.useBearerToken).toHaveBeenNthCalledWith(
        2,
        'dummyToken2'
      )
    })
  })

  describe('updateCurrentUser', () => {
    RestClient.useBearerToken = vi.fn()

    const user = new User(
      'testName',
      'test@test.com',
      Role.USER,
      'unitId',
      'firebaseUserId'
    )

    it('sets currentUser to expected user', async () => {
      accountStore = useAccountStore()

      await accountStore.updateCurrentUser(authMock.currentUser)

      await expect(
        authMock.currentUser?.getIdTokenResult
      ).toHaveBeenCalledOnce()
      expect(accountStore.currentUser).toStrictEqual(user)
      expect(RestClient.useBearerToken).toHaveBeenCalledWith('dummyToken')
    })

    it('unsets currentUser and returns early if passed user is null', async () => {
      accountStore = useAccountStore()
      accountStore.currentUser = user

      await accountStore.updateCurrentUser()

      expect(authMock.currentUser?.getIdTokenResult).not.toHaveBeenCalled()
      expect(accountStore.currentUser).toBeUndefined()
    })

    it('unsets currentUser and returns early if email is not verified', async () => {
      const firebaseUser = {
        displayName: 'testName',
        email: 'test@test.com',
        emailVerified: false,
        getIdToken: vi.fn().mockResolvedValue('dummyToken'),
        getIdTokenResult: vi
          .fn()
          .mockResolvedValue({ claims: { role: Role.USER.key } }),
      } as unknown as FBUser
      const authMock = getAuthMock(firebaseUser)

      accountStore = useAccountStore()
      accountStore.currentUser = user

      await accountStore.updateCurrentUser(firebaseUser)

      expect(authMock.currentUser?.getIdTokenResult).not.toHaveBeenCalled()
      expect(accountStore.currentUser).toBeUndefined()
    })
  })

  describe('logInUser', () => {
    it('signs out user and throws error if email not verified', async () => {
      const userCredential = {
        user: {
          emailVerified: false,
        },
      } as unknown as UserCredential
      ;(signInWithEmailAndPassword as unknown as Mock).mockResolvedValue(
        userCredential
      )

      accountStore = useAccountStore()

      await expect(() =>
        accountStore.logInUser('dummy@example.com', 'password')
      ).rejects.toThrowError(EmailNotVerifiedError)

      expect(signInWithEmailAndPassword).toHaveBeenCalledWith(
        authMock,
        'dummy@example.com',
        'password'
      )
      expect(authMock.signOut).toHaveBeenCalledOnce()
      expect(sendEmailVerification).toHaveBeenCalledWith(userCredential.user)
      expect(accountStore.currentUser).toBeUndefined()
    })

    it('signs in user if email is verified', async () => {
      const userCredential = {
        user: {
          emailVerified: true,
        },
      } as unknown as UserCredential
      ;(signInWithEmailAndPassword as unknown as Mock).mockResolvedValue(
        userCredential
      )

      accountStore = useAccountStore()

      await accountStore.logInUser('dummy@example.com', 'password')

      expect(signInWithEmailAndPassword).toHaveBeenCalledWith(
        authMock,
        'dummy@example.com',
        'password'
      )
      expect(authMock.signOut).not.toHaveBeenCalled()
      expect(sendEmailVerification).not.toHaveBeenCalled()
    })
  })
  describe('signInAnonymousUser', () => {
    it('signs in an anonymous user', async () => {
      accountStore = useAccountStore()

      await accountStore.signInAnonym()

      expect(signInAnonymously).toHaveBeenCalledWith(authMock)
      expect(onAuthStateChanged).toHaveBeenCalledWith(
        authMock,
        expect.any(Function)
      )
    })
  })
})
