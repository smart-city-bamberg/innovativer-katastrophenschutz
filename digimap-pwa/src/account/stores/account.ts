import { defineStore } from 'pinia'
import type { IUser } from '@/account/models/user'
import { User } from '@/account/models/user'
import { computed, ref } from 'vue'
import { Role } from '@/account/models/role'
import type { Auth, IdTokenResult, User as FBUser } from 'firebase/auth'
import {
  getAuth,
  onAuthStateChanged,
  sendEmailVerification,
  sendPasswordResetEmail,
  signInAnonymously,
  signInWithEmailAndPassword,
} from 'firebase/auth'
import { RestClient } from '@/base/services/restClient'
import { EmailNotVerifiedError } from '@/account/models/exceptions'
import { MissionService } from '@/mission/services/missionService'
import type { Unit } from '@/unit/models/unit'
import { useUnit } from '@/unit/services/useUnit'

export const useAccountStore = defineStore('account', () => {
  const currentUser = ref<User>()
  const auth = ref<Auth>()
  const busy = ref<boolean>(false)
  const bearerToken = ref<string>()
  const resetPasswordEmail = ref<string>()

  const isLoggedIn = computed(() => !!currentUser.value)
  const isAnonymous = computed(() => currentUser.value?.isAnonymous)

  const initAuth = () => {
    if (auth.value) {
      // already initialized
      return auth.value
    }
    busy.value = true
    auth.value = getAuth()
    // listen for auth changes: user log in / log out / new token
    auth.value.onIdTokenChanged(async (user: FBUser | null) => {
      await updateCurrentUser(user)
    })
    busy.value = false
    return auth.value
  }

  const updateCurrentUser = async (user?: FBUser | null) => {
    const firebaseAuth = getAuth()
    if (
      !user ||
      !firebaseAuth?.currentUser ||
      (!user.emailVerified && !firebaseAuth.currentUser?.isAnonymous)
    ) {
      // log out, or email not verified
      currentUser.value = undefined
      return
    }

    busy.value = true
    // login/revisit: get user role and add interceptor for sending token to backend
    const accessToken = await firebaseAuth.currentUser.getIdTokenResult()
    if (accessToken) {
      const roleKey = accessToken.claims.role
      const unitId = accessToken.claims.unitId
      currentUser.value = new User(
        user.displayName,
        user.email,
        Role.fromKey(roleKey),
        unitId,
        user.uid,
        firebaseAuth.currentUser.isAnonymous
      )
      await useBearerToken(accessToken)
    }
    busy.value = false
    console.log(currentUser.value)
  }

  const logInUser = async (email: string, password: string): Promise<void> => {
    busy.value = true
    try {
      const firebaseAuth = getAuth()
      const userCredential = await signInWithEmailAndPassword(
        firebaseAuth,
        email,
        password
      )
      const user = userCredential.user
      if (user && !user.emailVerified) {
        // sign out user to prevent further authentication and send email
        logOutUser()
        await sendEmailVerification(user)
        throw new EmailNotVerifiedError(
          'E-Mail noch nicht verifiziert. Bitte prüfen Sie ihr E-Mail-Postfach, inklusive Junk-Mails.'
        )
      }
    } finally {
      busy.value = false
    }
  }

  const signInAnonymousUser = async () => {
    busy.value = true
    try {
      const firebaseAuth = getAuth()

      signInAnonymously(firebaseAuth)
        .then(() => {
          console.log('Signed in anonymously')
        })
        .catch((error) => {
          const errorCode = error.code
          const errorMessage = error.message
          throw new Error(
            `Error signing in anonymously: ${errorCode} ${errorMessage}`
          )
        })

      onAuthStateChanged(firebaseAuth, async (user) => {
        if (user) {
          await updateCurrentUser(user)
        } else {
          logOutUser()
        }
      })
    } finally {
      busy.value = false
    }
  }

  const loadUser = async () => {
    await new Promise<FBUser | null>((resolve) => {
      const firebaseAuth = getAuth()
      firebaseAuth.onIdTokenChanged(async (user) => {
        resolve(user)
      })
    })
  }

  /**
   * Adds or updates the bearer token of the ketting request interceptor.
   */
  const useBearerToken = async (idTokenResult?: IdTokenResult) => {
    const token = idTokenResult?.token
    if (token) {
      RestClient.useBearerToken(token)
      bearerToken.value = token
    }
  }

  const logOutUser = () => {
    const firebaseAuth = getAuth()
    firebaseAuth?.signOut()
  }

  const setUser = (user?: IUser) => {
    currentUser.value = user
  }

  const sendPasswordResetMail = async (email: string) => {
    await sendPasswordResetEmail(getAuth(), email)
    resetPasswordEmail.value = email
  }

  const getUserUnit = async (): Promise<Unit | undefined> => {
    if (currentUser.value?.unitId) {
      return await MissionService.getUnitById(currentUser.value?.unitId)
    }
  }

  const fetchUserUnit = async () => {
    if (currentUser.value?.firebaseUserId) {
      const unit = await useUnit().getUnitByUserId(
        currentUser.value.firebaseUserId
      )
      if (currentUser.value && unit) {
        currentUser.value = new User(
          currentUser.value?.displayName,
          currentUser.value.email,
          currentUser.value.role,
          unit.id,
          currentUser.value.firebaseUserId,
          currentUser.value.isAnonymous
        )
      }
    }
  }

  return {
    isLoggedIn,
    isAnonymous,
    currentUser,
    auth,
    busy,
    bearerToken,
    resetPasswordEmail,
    initAuth,
    setUser,
    signInAnonym: signInAnonymousUser,
    loadUser,
    logOutUser,
    updateCurrentUser,
    useBearerToken,
    logInUser,
    sendPasswordResetMail,
    getUserUnit,
    fetchUserUnit,
  }
})
