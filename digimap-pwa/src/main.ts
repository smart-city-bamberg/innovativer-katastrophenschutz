import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import './assets/styles/main.scss'
import VeeValidate from '@/base/plugins/vee-validate'
import { captureConsoleIntegration } from '@sentry/integrations'
import { browserTracingIntegration, init, replayIntegration } from '@sentry/vue'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(VeeValidate)

init({
  app,
  dsn: import.meta.env.VITE_SENTRY_DSN,
  enabled: import.meta.env.PROD,
  integrations: [
    browserTracingIntegration({
      router,
    }),
    replayIntegration(),
    captureConsoleIntegration({
      levels: ['error', 'warn'],
    }),
  ],
  // Performance Monitoring
  tracesSampleRate: import.meta.env.PROD ? 0.1 : 1.0, // Capture 100% of the transactions, reduce in production!
  // Session Replay
  replaysSessionSampleRate: import.meta.env.PROD ? 0.1 : 1.0, // This sets the sample rate at 10%. You may want to change it to 100% while in development and then sample at a lower rate in production.
  replaysOnErrorSampleRate: 1.0, // If you're not already sampling the entire session, change the sample rate to 100% when sampling sessions where errors occur.
})

app.mount('#app')
