import { defineStore } from 'pinia'
import { ref } from 'vue'
import { mixed, object, string } from 'yup'
import type { ApiCreateTacticalSign } from '@/base/models/tacticalSign'
import { useTacticalSignStore } from '@/tactical-sign/stores/customTacticalSign'

export interface CreateCustomTacticalSignForm {
  name: string
  image: File
}

export const useCreateCustomTacticalSignStore = defineStore(
  'create-tactical-sign',
  () => {
    const tacticalSignImage = ref<File>()
    const name = ref<string>()
    const formErrorMessage = ref<string>()

    const schema = object({
      image: mixed().required('Bitte wählen Sie ein Bild aus'),
      name: string().required('Bitte geben Sie einen Namen an'),
    })
    const initialValues = {
      image: tacticalSignImage.value,
      name: '',
    }

    const store = useTacticalSignStore()
    const { createCustomTacticalSign } = store
    const setImage = (newImage: File) => {
      tacticalSignImage.value = newImage
    }

    const publishTacticalSign = async (
      formValues: CreateCustomTacticalSignForm
    ) => {
      const tacticalSign: ApiCreateTacticalSign = {
        name: formValues.name,
        image: formValues.image,
      }

      await createCustomTacticalSign(tacticalSign)
      resetFormErrors()
    }

    const resetFormErrors = () => {
      formErrorMessage.value = ''
    }

    return {
      formSchema: schema,
      initialValues,
      tacticalSignImage,
      name,
      formErrorMessage,
      publishTacticalSign,
      setImage,
      resetFormErrors,
    }
  }
)
