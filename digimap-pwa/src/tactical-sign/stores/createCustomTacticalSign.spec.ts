import { createPinia, setActivePinia } from 'pinia'
import { beforeEach, describe, expect, it, vi } from 'vitest'
import type { CreateCustomTacticalSignForm } from '@/tactical-sign/stores/createCustomTacticalSign'
import { useCreateCustomTacticalSignStore } from '@/tactical-sign/stores/createCustomTacticalSign'
import type { ApiCreateTacticalSign } from '@/base/models/tacticalSign'
import { TacticalSignService } from '@/tactical-sign/services/tacticalSignService'

//create mock of MissionService createMission
vi.mock('@/tactical-sign/services/tacticalSignService', () => ({
  TacticalSignService: {
    postCustomTacticalSign: vi.fn(),
  },
}))

const formValues = {
  name: 'TestTacticalSign',
  image: new File([''], 'mockImage', { type: 'image/png' }),
} as CreateCustomTacticalSignForm

describe('useCreateCustomTacticalSignStore', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
    vi.clearAllMocks()
  })

  describe('publishTacticalSign', () => {
    it('publishes correct payload', async () => {
      const createCustomTacticalSignStore = useCreateCustomTacticalSignStore()

      const expectedImage = new File([''], 'mockImage', { type: 'image/png' })

      const expectedApiCreateTacticalSign: ApiCreateTacticalSign = {
        name: 'TestTacticalSign',
        image: expectedImage,
      }

      await createCustomTacticalSignStore.publishTacticalSign(formValues)

      expect(TacticalSignService.postCustomTacticalSign).toHaveBeenCalledOnce()
      expect(TacticalSignService.postCustomTacticalSign).toHaveBeenCalledWith(
        expectedApiCreateTacticalSign
      )
    })
  })
})
