import { beforeEach, describe, expect, it, vi } from 'vitest'
import { MockData } from '@/assets/mocks/data'
import { createPinia, setActivePinia } from 'pinia'
import { useTacticalSignStore } from '@/tactical-sign/stores/customTacticalSign'
import { TacticalSignService } from '@/tactical-sign/services/tacticalSignService'
import type { ApiCreateTacticalSign } from '@/base/models/tacticalSign'
import { TacticalSign } from '@/base/models/tacticalSign'

describe('useTacticalSignStore', () => {
  const mockTacticalSigns = MockData.getTacticalSigns()

  beforeEach(() => {
    setActivePinia(createPinia())
  })

  describe('methods', () => {
    it('fetchAllTacticalSigns - calls service and sets missions correctly', async () => {
      const store = useTacticalSignStore()
      const getCustomTacticalSignsSpy = vi
        .fn()
        .mockImplementation(() => mockTacticalSigns)
      TacticalSignService.getCustomTacticalSigns = getCustomTacticalSignsSpy

      await store.fetchAllCustomTacticalSigns()

      expect(getCustomTacticalSignsSpy).toHaveBeenCalledOnce()
      expect(store.customTacticalSigns).toStrictEqual(mockTacticalSigns)
    })

    it('createCustomTacticalSign - calls service and adds new tacticalSign to customTacticalSigns', async () => {
      const store = useTacticalSignStore()
      const newTacticalSign = new TacticalSign(
        'https://storage.googleapis.com/staging-tactical-signs/tactical-sign/345345',
        'TestTacticalSign'
      )
      TacticalSignService.postCustomTacticalSign = vi
        .fn()
        .mockImplementation(() => newTacticalSign)

      const createTacticalSign: ApiCreateTacticalSign = {
        name: 'TestTacticalSign',
        image: new File([''], 'TestTacticalSign'),
      }
      await store.createCustomTacticalSign(createTacticalSign)

      expect(TacticalSignService.postCustomTacticalSign).toHaveBeenCalledOnce()
      expect(store.customTacticalSigns).toStrictEqual([newTacticalSign])
    })
  })
})
