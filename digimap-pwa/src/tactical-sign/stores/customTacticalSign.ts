import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { TacticalSignService } from '@/tactical-sign/services/tacticalSignService'
import type {
  ApiCreateTacticalSign,
  TacticalSign,
} from '@/base/models/tacticalSign'

export const useTacticalSignStore = defineStore('tacticalSigns', () => {
  const customTacticalSigns = ref<TacticalSign[]>([])
  const busy = ref<boolean>(false)
  const errorMessage = ref<string>('')
  const infoMessage = computed((): string =>
    customTacticalSigns.value ? '' : 'Keine Einsätze vorhanden.'
  )

  async function fetchAllCustomTacticalSigns() {
    try {
      busy.value = true
      errorMessage.value = ''
      customTacticalSigns.value =
        await TacticalSignService.getCustomTacticalSigns()
    } catch (error: unknown) {
      errorMessage.value =
        'Taktische Zeichen konnten nicht abgefragt werden. Bitte versuchen Sie es erneut.'
      throw new Error('Could not fetch tactical signs: ' + error)
    } finally {
      busy.value = false
    }
  }

  async function createCustomTacticalSign(tacticalSign: ApiCreateTacticalSign) {
    try {
      const createdTacticalSign =
        await TacticalSignService.postCustomTacticalSign(tacticalSign)
      if (createdTacticalSign) {
        customTacticalSigns.value.push(createdTacticalSign)
      }
    } catch (error: unknown) {
      errorMessage.value = 'Taktisches Zeichen konnte nicht angelegt werden'
    }
  }

  function getCustomTacticalSignByPath(path: string): TacticalSign | undefined {
    return customTacticalSigns.value.find(
      (tacticalSign) => tacticalSign.path === path
    )
  }

  return {
    busy,
    customTacticalSigns,
    errorMessage,
    infoMessage,
    fetchAllCustomTacticalSigns,
    createCustomTacticalSign,
    getCustomTacticalSignByPath,
  }
})
