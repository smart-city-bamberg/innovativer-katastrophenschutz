import { RestClient } from '@/base/services/restClient'
import type {
  ApiCreateTacticalSign,
  ApiTacticalSign,
} from '@/base/models/tacticalSign'
import { TacticalSign } from '@/base/models/tacticalSign'
import { useAccountStore } from '@/account/stores/account'
import { storeToRefs } from 'pinia'

export class TacticalSignService {
  private static readonly _client = RestClient.getInstance()
  private static readonly baseUrl = import.meta.env
    .VITE_APP_DIGIMAP_API_BASE_URL

  public static async getCustomTacticalSigns(): Promise<TacticalSign[]> {
    const tacticalSigns: TacticalSign[] = []
    const tacticalSignResources = await this._client
      .go<ApiTacticalSign[]>('/tactical-signs')
      .get()

    for (const resource of tacticalSignResources.data) {
      const url = resource.image.url
      const name = resource.image.blobName.split('/').pop() // Extract the name from the blobName
      tacticalSigns.push(new TacticalSign(url, name))
    }

    console.log('Retrieved all tacticalSigns', tacticalSigns)
    return tacticalSigns
  }

  static async postCustomTacticalSign(tacticalSign: ApiCreateTacticalSign) {
    const accountStore = useAccountStore()
    const { bearerToken } = storeToRefs(accountStore)

    const formData = new FormData()
    formData.append('name', tacticalSign.name)
    formData.append('image', tacticalSign.image)

    const response = await fetch(this.baseUrl + '/tactical-signs', {
      method: 'POST',
      body: formData,
      headers: {
        Authorization: `Bearer ${bearerToken.value}`,
      },
    })

    if (!response.ok) {
      throw new Error('Fehler beim Erstellen des taktischen Zeichens')
    }
    const responseData: ApiTacticalSign = await response.json()
    const url = responseData.image.url
    const name = responseData.image.blobName.split('/').pop()

    return new TacticalSign(url, name)
  }
}
