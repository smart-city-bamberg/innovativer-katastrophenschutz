FROM --platform=amd64 node:lts as develop-stage

ARG GITHUB_PACKAGE_TOKEN
WORKDIR /app
COPY package*.json ./
RUN env
RUN npm ci

FROM develop-stage as build-stage

ARG ENV_TYPE=prod

COPY ./ .
# generate an up-to-date list of our runtime dependencies which serves as input for our UI. jq makes sure we have the correct format (list of objects)
RUN apt-get update && apt-get -y install jq
RUN npm run licenses
RUN npm run build
RUN if [ "$ENV_TYPE" = "prod" ] ; then npm run build; else npm run build-staging; fi

FROM nginx:alpine as production-stage

ENV TZ=Europe/Berlin
RUN mkdir /app
COPY --from=build-stage /app/dist /app
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
