# DigiMap App

Progressive Web App for the Digital Situation Map ("Smarter Katastrophenschutz") project for Smart City Bamberg.

## Authorize to NPM Github Registry (for Component Library)

- You can follow this guide: https://help.knapsack.cloud/article/83-github-personal-access-tokens-local-dev
- Your `.npmrc` should look like this:
  ```
  //npm.pkg.github.com/:_authToken=${GITHUB_PACKAGE_TOKEN}
  @lion5:registry=https://npm.pkg.github.com
  ```


## IDE Setup: WebStorm

- Configure Prettier as the default formatter: https://prettier.io/docs/en/webstorm.html
- Add `{**/*,*}.{js,ts,jsx,tsx,vue,css,sass,scss}` to `Run for files` section (`Languages & Framework/Javascript/Prettier`)

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Push Image to GCR

- Authenticate to the Artifact/Container Registry
  - Recommended: via [gcloud credential helper](https://cloud.google.com/container-registry/docs/advanced-authentication#gcloud-helper) (requires [Google Cloud CLI](https://cloud.google.com/sdk/docs/install-sdk))
- The docker config (output of `gcloud auth configure-docker`) should contain `"europe-west3-docker.pkg.dev": "gcloud"` within the `credHelpers`.
- Now, you should be able to build & push the image via:
```shell
docker build -t europe-west3-docker.pkg.dev/smart-city-digimap/docker/digimap-pwa:<tag> . --push
```

## Project Setup
### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Run End-to-End Tests with [Cypress](https://www.cypress.io/)

```sh
npm run test:e2e
```

This runs the end-to-end tests against the Vite development server.
It is much faster than the production build.

But it's still recommended to test the production build with `test:e2e:ci` before deploying (e.g. in CI environments):

```sh
npm run build
npm run test:e2e:ci
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

## Attribution

All tacitical signs used are taken from https://github.com/jonas-koeritz/Taktische-Zeichen licensed under CC-BY-4.0 license by the author.
