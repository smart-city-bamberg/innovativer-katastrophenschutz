// TODO: fix e2e
describe.skip('Situation Map Test', () => {
  beforeEach(() => {
    cy.intercept('**/missions', { fixture: 'missions-hal.json' })
    cy.intercept('**/missions/*/units', { fixture: 'units-hal.json' })
  })

  it('Selecting mission on initial app load renders map correctly', () => {
    cy.visit('/')

    cy.get('[data-test="situation-map"]').should('be.visible')

    selectMissionInModal('FooNumber')

    verifyMapIsRendered()
  })

  it('Selecting mission through settings renders map correctly', () => {
    cy.visit('/')

    cy.get('.hide-button').should('be.visible').click()
    cy.get('dialog[data-test="select-mission-modal"]').should('not.be.visible')

    navigateTo('/settings')

    cy.get('[data-test="mission-information"]').should(
      'have.text',
      'Kein Einsatz ausgewählt / zugewiesen'
    )

    cy.get('button').contains('Einsatz wählen').click()

    cy.get('[data-test="situation-map"]').should('be.visible')

    selectMissionInModal('FooNumber')

    verifyMapIsRendered()
  })

  it('Changing the selected mission through settings renders map correctly', () => {
    cy.visit('/')

    selectMissionInModal('FooNumber1')

    navigateTo('/settings')

    cy.get('[data-test="mission-information"]').should(
      'have.text',
      'FooNumber1 (Aufsesshöflein 321, 96049 Aufsesshöflein)'
    )

    cy.get('button').contains('Einsatz wählen').click()

    cy.get('[data-test="situation-map"]').should('be.visible')

    selectMissionInModal('FooNumber')

    verifyMapIsRendered()
  })

  const selectMissionInModal = (missionNumber: string) => {
    const dialog = cy
      .get('dialog[data-test="select-mission-modal"]')
      .should('be.visible')
      .and('have.attr', 'open', 'open')

    cy.get('button[data-test="select-mission-button"]').should('be.disabled')

    dialog.get('select').select(`Einsatz "${missionNumber}"`)

    cy.get('button[data-test="select-mission-button"]')
      .should('be.visible')
      .click()

    cy.get('dialog[data-test="select-mission-modal"]').should('not.be.visible')
  }

  const verifyMapIsRendered = () => {
    cy.get('[data-test="situation-map"]')
      .get('div.leaflet-marker-pane')
      .should('have.length', 1)
      .get('img.leaflet-marker-icon')
      .should('have.length', 2)
      .get('div.leaflet-control')
      .should('have.length', 4)
  }

  const navigateTo = (route: string) => {
    cy.get(`a[href="${route}"]`).click()
  }
})
