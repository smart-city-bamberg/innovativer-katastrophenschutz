// TODO: fix e2e
describe.skip('User Authentication', () => {
  beforeEach(() => {
    cy.intercept('GET', '**/missions', [])
  })

  it('User is redirected to login page if initially not logged in', () => {
    const loginPath = '/login'
    const unauthorizedRoutes = ['/', '/settings']

    for (const route of unauthorizedRoutes) {
      cy.routeTo(route)
      cy.location('pathname').should('equal', loginPath)
    }
  })

  it('User can access authorized routes after login', () => {
    const authorizedRoutes = ['/', '/settings']

    cy.login('test')

    for (const route of authorizedRoutes) {
      cy.routeTo(route)
      cy.location('pathname').should('equal', route)
    }
  })

  it('User is sent to login page when logging out', () => {
    const loginPath = '/login'
    const unauthorizedRoutes = ['/', '/home', '/settings']

    cy.login()

    for (const route of unauthorizedRoutes) {
      cy.routeTo(route)
      cy.location('pathname').should('equal', loginPath)
    }
  })
})
