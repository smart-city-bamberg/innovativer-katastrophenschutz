/// <reference types="cypress" />
// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
import { Role } from '../../src/account/models/role'
import AUTWindow = Cypress.AUTWindow

Cypress.Commands.add(
  'login',
  (displayName = 'testUser', email = 'test@example.com', role = Role.USER) => {
    cy.visit('/', { log: false })
    const log = Cypress.log({
      autoEnd: false,
      displayName: 'login',
      consoleProps: () => ({
        displayName,
        email,
        role,
      }),
      message: `User ${displayName} logged in`,
    })
    cy.window({ log: false }).then((win: AUTWindow) => {
      win.__accountStore__.setUser({
        displayName,
        email,
        role,
      })
      log.end()
    })
  }
)

Cypress.Commands.add('routeTo', (path) => {
  console.log('Entering routeTo')
  const log = Cypress.log({
    autoEnd: false,
    displayName: 'route to',
    consoleProps() {
      return {
        path,
      }
    },
    message: `Routed to ${path}`,
  })
  log.snapshot('before')
  cy.window({ log: false }).then((win) => {
    win.__router__.push(path).catch((e: unknown) => {
      console.error('Cypress Command routeTo error.', e)
    })
  })
  cy.wait(500, { log: false }).then(() => {
    log.snapshot('after')
    log.end()
  })
})
