/// <reference types="cypress" />

import { Role } from '../../src/account/models/role'
import { useAccountStore } from '../../src/account/stores/account'
import Router from '../../src/router'

declare global {
  namespace Cypress {
    interface Chainable<Subject = any> {
      login(
        displayName = 'testUser',
        email = 'test@example.com',
        role = Role.USER
      ): Chainable<void>
      routeTo(path: string): Chainable<void>
    }

    /**
     * The interface for user-defined properties in Window object under test.
     * See https://github.com/cypress-io/cypress/issues/9028.
     */
    interface ApplicationWindow {
      __accountStore__: ReturnType<typeof useAccountStore>
      __router__: Router
    }
  }
}

export { ApplicationWindow }
