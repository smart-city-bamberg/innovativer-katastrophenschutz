import firebase_admin
from firebase_admin import auth

# Script to update user custom claims, see: https://firebase.google.com/docs/auth/admin/custom-claims#python_1

app_options ={
    'apiKey': "<Insert Your API Key>",
    'authDomain': "<your-app-domain>",
  };
app = firebase_admin.initialize_app(options=app_options)

email = input("Enter user email:\n")
user = auth.get_user_by_email(email)
current_claims = user.custom_claims
if user:
  print('Successfully fetched user with UID {0}'.format(user.uid))
  print('Display name: {0}'.format(user.display_name))
  print('Current claims of user: {0}'.format(user.custom_claims))

  role = input("Enter desired role claim (this will overwrite the current role claim!):\n")
  if role:
      current_claims['role'] = role
      auth.set_custom_user_claims(user.uid, current_claims)
      print('Updated role claim of user with UID {0}: {1}'.format(user.uid, current_claims))
  else:
     print('No role provided.')

  unitId = input("Enter desired unitId (this will overwrite the current unitId!):\n")
  if unitId:
      current_claims['unitId'] = unitId
      auth.set_custom_user_claims(user.uid, current_claims)
      print('Updated unitId claim of user with UID {0}: {1}'.format(user.uid, current_claims))
  else:
     print('No unitId provided.')
