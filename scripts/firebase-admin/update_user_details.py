import firebase_admin
from firebase_admin import auth

# Script to update user details, see: https://firebase.google.com/docs/auth/admin/manage-users#python

app_options ={
    'apiKey': "<Insert Your API Key>",
    'authDomain': "<your-app-domain>",
  };
app = firebase_admin.initialize_app(options=app_options)

email = input("Enter user email:\n")
user = auth.get_user_by_email(email)
if user:
  print('Successfully fetched user data: {0}'.format(user.uid))
  print('Display name: {0}'.format(user.display_name))

  display_name = input("Enter desired display name\n")
  if display_name:
      auth.update_user(user.uid, display_name=display_name)
      print('Updated display name of user with UID {0}: {1}'.format(user.uid, display_name))
  else:
     print('No display name provided.')