#!/bin/bash
# Script to generate batch reports limited by $MESSAGE_COUNT
# U need to replace the $BACKEND_PASSWORD with the current one

BROKER_DOMAIN=broker.lagekarte.app
MISSION_ID=6447c344396fa7194bbcc1fb
BACKEND_PASSWORD=<password>
MESSAGE_COUNT=5

publish_report_message() {
    ETAG=$1
    REPORT_ID=$(uuidgen)
    SENDER_UNIT_ID=$(uuidgen)
    SENDER_CALL_SIGN="Call Sign $ETAG"
    JSON_FMT='{"reportId":"%s","senderUnitId":"%s","senderCallSign":"%s","tacticalSignPath":"Gefahren/Entstehungsbrand.svg","report":"Hochwasser im Kellergeschoss festgestellt.","createdAt":"2019-08-24T14:15:23Z","images":["gs://smart-city-digimap.appspot.com/641069d283313c4b7726b3a7/reports/zerstoertes-haus-612x612.jpg","gs://smart-city-digimap.appspot.com/641069d283313c4b7726b3a7/reports/report-1234","gs://smart-city-digimap.appspot.com/641069d283313c4b7726b3a7/reports/test-merchant-logo.jpeg"],"location":{"latitude":49.91454,"longitude":10.88511},"etag":%s}'
    JSON_STRING=$(printf "$JSON_FMT" "$REPORT_ID" "$SENDER_UNIT_ID" "$SENDER_CALL_SIGN" "$ETAG")
    mqtt pub -h=$BROKER_DOMAIN -p 8883 -t digimap/units/v1/outbound/${MISSION_ID}/report -m "$JSON_STRING" -d -u backend -pw $BACKEND_PASSWORD -i localMqttClient -s
}



for i in $(seq 1 $MESSAGE_COUNT); do
    etag=$((i + 1000))
    publish_report_message $etag
done
